#include <fct/device/device.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL.h>


static const uint8_t f32x_video_font_cursor_default[] =
{
	#include "font_cursor.h"
};


typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		unsigned red   : 3;
		unsigned green : 3;
		unsigned blue  : 2;
	};
	uint8_t mask;
} f32x_video_color_t;

static const f32x_video_color_t F32X_VIDEO_COLOR_BLACK   = { .mask = 0x00 };
static const f32x_video_color_t F32X_VIDEO_COLOR_WHITE   = { .mask = 0xFF };
static const f32x_video_color_t F32X_VIDEO_COLOR_MAGENTA = { .mask = 0xC7 };

typedef struct
__attribute__((__packed__))
{
	bool     enable      : 1;
	bool     framebuffer : 1;
	bool     text        : 1;
	bool     cursor      : 1;
	bool     hblank_int  : 1;
	bool     vblank_int  : 1;
	unsigned reserved    : 2;
} f32x_video_reg_config_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		f32x_video_reg_config_t config;
		f32x_video_color_t      background;
		f32x_video_color_t      foreground;
		f32x_video_color_t      cursor_key;
		uint16_t                cursor_x;
		uint16_t                cursor_y;
		uint16_t                text_offset;
		uint16_t                reserved;
		uint32_t                framebuffer;
	};
	uint8_t reg[16];
} f32x_video_reg_map_t;


struct fct_device_subclass_s
{
	uint8_t              text[4096];
	uint8_t              font_cursor[4096];
	f32x_video_reg_map_t config;

	bool         dirty;
	SDL_Window*  window;
	SDL_Surface* surface;
	SDL_Palette* palette;
	uint32_t     ticks;
};



static fct_device_type_t f32x_video__type;

static bool f32x_video__initialized = false;

static fct_device_t* f32x_video__create(
	fct_string_t* name, const fct_tok_t* params)
{
	if (params && (params->type != FCT_TOK_END))
		return NULL;

	if (f32x_video__initialized)
		return NULL;

	fct_device_t* device
		= fct_device_create(
			&f32x_video__type, name);
	if (!device) return NULL;

	device->subclass
		= (fct_device_subclass_t*)malloc(
			sizeof(fct_device_subclass_t));
	if (!device->subclass)
	{
		free(device);
		return NULL;
	}

	fct_device_subclass_t* subclass
		= device->subclass;

	memset(subclass->text, 0x00, 4096);
	memcpy(subclass->font_cursor,
		f32x_video_font_cursor_default, 4096);

	subclass->config.config.enable      = false;
	subclass->config.config.framebuffer = false;
	subclass->config.config.text        = false;
	subclass->config.config.cursor      = false;
	subclass->config.config.hblank_int  = false;
	subclass->config.config.vblank_int  = false;
	subclass->config.config.reserved    = 0;

	subclass->config.background  = F32X_VIDEO_COLOR_BLACK;
	subclass->config.foreground  = F32X_VIDEO_COLOR_WHITE;
	subclass->config.cursor_key  = F32X_VIDEO_COLOR_MAGENTA;
	subclass->config.cursor_x    = 0;
	subclass->config.cursor_y    = 0;
	subclass->config.text_offset = 0;
	subclass->config.reserved    = 0;
	subclass->config.framebuffer = 0;

	subclass->dirty = true;

	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		free(subclass);
		free(device);
		return NULL;
	}

	subclass->window = NULL;

	subclass->palette
		= SDL_AllocPalette(256);
	if (subclass->palette)
	{
		unsigned r, g, b, i;
		for (i = 0, r = 0; r < 8; r++)
		{
			for (g = 0; g < 8; g++)
			{
				for (b = 0; b < 4; b++, i++)
				{
					subclass->palette->colors[i].r = (r << 5) | (r << 2) | (r >> 1);
					subclass->palette->colors[i].g = (g << 5) | (g << 2) | (g >> 1);
					subclass->palette->colors[i].b = (b << 6) | (b << 4) | (b >> 2) | b;
					subclass->palette->colors[i].a = 0xFF;
				}
			}
		}
	}

	subclass->surface = SDL_CreateRGBSurface(
		0, 640, 480, 8, 0xE0, 0x1C, 0x03, 0x00);
	if (subclass->surface)
		SDL_SetSurfacePalette(subclass->surface, subclass->palette);

	if (!subclass->palette
		|| !subclass->surface)
	{
		SDL_FreeSurface(subclass->surface);
		SDL_FreePalette(subclass->palette);
		SDL_Quit();
		free(subclass);
		free(device);
		return NULL;
	}

	subclass->ticks = SDL_GetTicks();

	f32x_video__initialized = true;
	return device;
}

static void f32x_video__delete(
	fct_device_t* device)
{
	if (!device || !device->subclass)
		return;

	SDL_FreeSurface(device->subclass->surface);
	SDL_FreePalette(device->subclass->palette);
	if (device->subclass->window)
		SDL_DestroyWindow(device->subclass->window);
	SDL_Quit();
	free(device->subclass);

	f32x_video__initialized = false;
}



static bool f32x_video__params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width)
{
	if (!device
		|| !device->subclass)
		return false;

	if (mau  ) *mau   = 8;
	if (width) *width = 1;
	return true;
}



static void f32x_video__tick(fct_device_t* device)
{
	if (!device || !device->subclass)
		return;

	if (!device->subclass->window)
		return;

	uint32_t ticks = SDL_GetTicks();
	if (ticks < (device->subclass->ticks + (1000 / 60)))
		return;
	device->subclass->ticks += (1000 / 60);

	if (device->subclass->dirty
		|| device->subclass->config.config.framebuffer)
	{
		SDL_Surface* screen
			= SDL_GetWindowSurface(
				device->subclass->window);
		if (!screen) return;

		SDL_Surface* surface
			= device->subclass->surface;
		if (!surface) return;

		uint8_t* pixels = (uint8_t*)surface->pixels;

		if (device->subclass->config.config.framebuffer)
		{
			/* TODO - Implement framebuffer. */
		}
		else
		{
			unsigned y, j;
			for (y = 0, j = 0; y < 480; y++, j += surface->pitch)
			{
				unsigned x, i;
				for (x = 0, i = j; x < 640; x++, i++)
					pixels[i] = device->subclass->config.background.mask;
			}
		}

		if (device->subclass->config.config.text)
		{
			unsigned py, pj, cy = device->subclass->config.text_offset;
			for (py = 0, pj = 0; py < 480; cy += 80)
			{
				unsigned fy;
				for (fy = 0; fy < 12; py++, pj += surface->pitch, fy++)
				{
					unsigned px, pi;
					for (px = 0, pi = pj; px < 640; )
					{
						uint8_t c = device->subclass->text[(cy + (px >> 3)) % 4096];
						unsigned ch = (c >> 5), cl = (c & 0x1F);

						unsigned fi = cl | (fy << 5) | (ch << 9);
						uint8_t f = device->subclass->font_cursor[fi];

						unsigned fx;
						for (fx = 0; fx < 8; px++, pi++, fx++)
						{
							if ((f & (1 << fx)) != 0)
								pixels[pi] = device->subclass->config.foreground.mask;
						}
					}
				}
			}
		}

		if (device->subclass->config.config.cursor)
		{
			unsigned py = device->subclass->config.cursor_y;
			unsigned pi = (py * 640);

			unsigned cy;
			for (cy = 0; (cy < 32) && (py < 480); cy++, py++, pi += 640)
			{
				unsigned px = device->subclass->config.cursor_x;
				unsigned ci = 384 + (((cy & 0x1C) << 7) | ((cy & 0x03) << 5));

				unsigned cx;
				for (cx = 0; (cx < 32) && (px < 640); cx++, px++)
				{
					uint8_t pixel = device->subclass->font_cursor[ci | cx];
					if (pixel != device->subclass->config.cursor_key.mask)
						pixels[pi + px] = pixel;
				}
			}
		}

		SDL_BlitSurface(device->subclass->surface, NULL, screen, NULL);
		SDL_UpdateWindowSurface(device->subclass->window);

		device->subclass->dirty = false;
	}
}

static void f32x_video__write(
	fct_device_t* device, uint64_t addr, uint32_t mask, uint8_t* data)
{
	if (!device || !device->subclass)
		return;

	static uint8_t data_fifo[3];

	mask &= 1;
	if (mask == 0)
		return;

	addr %= (16 << 10);

	if (addr < 0x1000)
	{
		device->subclass->text[addr % 4096] = *data;
	}
	else if (addr < 0x2000)
	{
		device->subclass->font_cursor[addr % 4096] = *data;
	}
	else
	{
		addr %= 16;
		switch (addr)
		{
			case 0:
			case 1:
			case 2:
			case 3:
				device->subclass->config.reg[addr] = *data;
				break;
			case 5:
			case 7:
			case 9:
				device->subclass->config.reg[addr & 0xE] = data_fifo[0];
				device->subclass->config.reg[addr] = *data;
				break;
			case 15:
				device->subclass->config.reg[12] = data_fifo[2];
				device->subclass->config.reg[13] = data_fifo[1];
				device->subclass->config.reg[14] = data_fifo[0];
				device->subclass->config.reg[15] = *data;
				break;
			default:
				break;
		}

		if (addr == 0)
		{
			if (device->subclass->config.config.enable
				&& !device->subclass->window)
			{
				device->subclass->window = SDL_CreateWindow(
					"f32x_video",
					SDL_WINDOWPOS_UNDEFINED,
					SDL_WINDOWPOS_UNDEFINED,
					640, 480, 0);
			}
			else if (!device->subclass->config.config.enable
				&& device->subclass->window)
			{
				SDL_DestroyWindow(
					device->subclass->window);
				device->subclass->window = NULL;
			}

			device->subclass->config.config.reserved = 0;
			device->subclass->config.reserved = 0;
		}
	}

	data_fifo[2] = data_fifo[1];
	data_fifo[1] = data_fifo[0];
	data_fifo[0] = *data;

	device->subclass->dirty = true;
}



static fct_device_type_t f32x_video__type =
{
	.name          =        "f32x_video",
	.create_cb     =        f32x_video__create,
	.delete_cb     =        f32x_video__delete,
	.params_get_cb =        f32x_video__params_get,
	.tick_cb       =        f32x_video__tick,
	.read_cb       =        NULL,
	.write_cb      = (void*)f32x_video__write,
	.bus_cb        =        NULL,
	.clkbus_cb     =        NULL,
};


fct_device_type_t* device_init(void)
{
	return &f32x_video__type;
}

void device_term(fct_device_type_t* type)
{
	(void)type;
}
