module f32x_video(
	vga_clock, vga_color, vga_hs, vga_vs,
	bus_clock, bus_addr, bus_wr, bus_idata, bus_odata);

input  wire       vga_clock;
output reg  [7:0] vga_color;
output reg        vga_hs;
output reg        vga_vs;

input  wire        bus_clock;
input  wire [14:0] bus_addr;
input  wire        bus_wr;
input  wire [ 7:0] bus_idata;
output reg  [ 7:0] bus_odata;


`define COLOR_WHITE 8'hFF
`define COLOR_BLACK 8'h00

`define MODE_OFF   2'h0
`define MODE_COPY  2'h1
`define MODE_XOR   2'h2
`define MODE_BLEND 2'h3


reg [7:0] background_color;

reg        framebuffer_enable;
reg [31:0] framebuffer;

reg [1:0] text_mode;
reg [7:0] text_color;
reg [7:0] text[0:4095];
reg [7:0] font[0:2047];

reg [ 1:0] cursor_mode;
reg [ 7:0] cursor_color;
reg [15:0] cursor_x;
reg [15:0] cursor_y;
reg [15:0] cursor[0:1023];
reg [ 7:0] cursor_low;


initial
begin
	background_color <= COLOR_BLACK;

	text_mode  <= MODE_OFF;
	text_color <= COLOR_WHITE;
	$readmemb("f32x_video_text.bin", text);
	$readmemb("f32x_video_font.bin", font);

	cursor_mode  <= MODE_OFF;
	cursor_color <= COLOR_WHITE;
	cursor_x     <= 0;
	cursor_y     <= 0;
	$readmemb("f32x_video_cursor.bin", cursor);
end

always @ (posedge bus_clock)
begin
	if (bus_wr)
	begin
		if (bus_addr < 14'h1000)
		begin
			text[bus_addr[11:0]] <= bus_idata;
		end
		else if (bus_addr < 14'h2000)
		begin
			font[bus_addr[10:0]] <= bus_idata;
		end
		else if (bus_addr < 14'h3000)
		begin
			if (bus_addr[0] == 0)
				cursor_low <= bus_idata;
			else
				cursor[bus_addr[10:1]] <= { bus_idata, cursor_low };
		end
		else if (bus_addr[3:0] == 0)
		begin
			framebuffer_enable <= bus_idata[1];
			text_mode          <= bus_idata[3:2];
			cursor_mode        <= bus_idata[5:4];
		end
		else
		begin
			case (bus_addr[3:0])
				 1: background_color   <= bus_idata;
				 2: text_color         <= bus_idata;
				 3: cursor_color       <= bus_idata;
				 4: cursor_x[ 7:0]     <= bus_idata;
				 5: cursor_x[15:8]     <= bus_idata;
				 6: cursor_y[ 7:0]     <= bus_idata;
				 7: cursor_z[15:8]     <= bus_idata;
				 8: framebuffer[ 7: 0] <= bus_idata;
				 9: framebuffer[15: 8] <= bus_idata;
				10: framebuffer[23:16] <= bus_idata;
				11: framebuffer[31:24] <= bus_idata;
			endcase
		end
	end
	else
	begin
		if (bus_addr < 14'h1000)
		begin
			bus_odata <= text[bus_addr[11:0]];
		end
		else if (bus_addr < 14'h2000)
		begin
			bus_odata <= font[bus_addr[10:0]];
		end
		else if (bus_addr < 14'h3000)
		begin
			bus_odata <= (cursor[bus_addr[0]]
				? cursor[bus_addr[10:1]][15:8]
				: cursor[bus_addr[10:1]][ 7:0]);
		end
		else
		begin
			case (bus_addr[3:0])
				 0:      bus_odata <= { 1'b0, framebuffer_enable, text_mode, cursor_mode, 2'b00 };
				 1:      bus_odata <= background_color;
				 2:      bus_odata <= text_color;
				 3:      bus_odata <= cursor_color;
				 4:      bus_odata <= cursor_x[ 7:0];
				 5:      bus_odata <= cursor_x[15:8];
				 6:      bus_odata <= cursor_y[ 7:0];
				 7:      bus_odata <= cursor_z[15:8];
				 8:      bus_odata <= framebuffer[ 7: 0];
				 9:      bus_odata <= framebuffer[15: 8];
				10:      bus_odata <= framebuffer[23:16];
				11:      bus_odata <= framebuffer[31:24];
				default: bus_odata <= 0;
			endcase
		end
	end
end




function blend;
input mode, dst, src;
begin
	case mode:
		0: blend = dst;
		1: blend = src;
		2: blend = dst ^ src;
		3: blend = (dst + src) >> 1;
	endcase
end
endfunction


`DEFINE VGA_H_SYNC_PULSE    800
`DEFINE VGA_H_DISPLAY_TIME  640
`DEFINE VGA_H_PULSE_WIDTH    96
`DEFINE VGA_H_FRONT_PORCH    16
`DEFINE VGA_H_BACK_PORCH     48

`DEFINE VGA_V_SYNC_PULSE   521
`DEFINE VGA_V_DISPLAY_TIME 480
`DEFINE VGA_V_PULSE_WIDTH    2
`DEFINE VGA_V_FRONT_PORCH   10
`DEFINE VGA_V_BACK_PORCH    29

/* TODO - Implement framebuffer. */

reg  [9:0] vga_x;
reg  [9:0] vga_y;

reg  [9:0] text_vga_x;
reg  [9:0] text_vga_y;
reg  [7:0] text_vga_color;
reg        text_vga_hs;
reg        text_vga_vs;
reg        text_vga_visible;

/* TODO - Pipeline this? */
wire [ 2:0] text_pix_x = text_vga_x[2:0];
reg  [ 3:0] text_pix_y;
reg  [11:0] text_pos_row;
wire [ 6:0] text_pos_col = text_vga_x[9:3];
wire        text_pos = text_pos_row + text_pos_col;
wire [ 7:0] text_char = text[text_pos];
wire [ 7:0] font_scan = font[{ text_char[6:4], text_pix_y, text_char[3:0] }];
wire font_pixel = font_scan[text_pix_x];

initial
begin
	vga_x            <= 0;
	vga_y            <= 0;

	text_vga_hs      <= 0;
	text_vga_vs      <= 0;
	text_vga_visible <= 0;

	text_pix_y   <= 0;
	text_pos_col <= 0;
end

always @ (posedge vga_clock)
begin
	text_vga_hs <= ((vga_x >= (VGA_H_DISPLAY_TIME + VGA_H_FRONT_PORCH))
		&& (vga_x < (VGA_H_SYNC_PULSE - VGA_H_BACK_PORCH)));
	text_vga_vs <= ((vga_x >= (VGA_V_DISPLAY_TIME + VGA_V_FRONT_PORCH))
		&& (vga_x < (VGA_V_SYNC_PULSE - VGA_V_BACK_PORCH)));

	if ((vga_x < VGA_H_DISPLAY_TIME)
		&& (vga_y < VGA_V_DISPLAY_TIME))
	begin
		text_vga_color <= (font_pixel
			? blend(text_mode, background_color, text_color)
			: background_color);

		if (vga_x >= (VGA_H_DISPLAY_TIME - 1))
		begin
			if (text_pix_y == 11)
			begin
				text_pix_y <= 0;
				if (vga_y >= (VGA_V_DISPLAY_TIME - 1))
					text_pos_row <= 0;
				else
					text_pos_row <= text_pos_row + 80;
			else
				text_pix_y <= text_pix_y + 1;
			end
		end

		text_vga_visible <= 1;
	end
	else
	begin
		text_vga_color   <= background_color;
		text_vga_visible <= 0;
	end

	vga_x <= (vga_x < (VGA_H_SYNC_PULSE - 1) ? vga_x + 1 : 0);
	vga_y <= (vga_y < (VGA_V_SYNC_PULSE - 1) ? text_vga_v + 1 : 0);

	text_vga_x <= vga_x;
	text_vga_y <= vga_y;
end


wire [15:0] cursor_off_x       = (text_vga_x - cursor_x);
wire [15:0] cursor_off_y       = (text_vga_y - cursor_y);
wire        cursor_visible     = (cursor_off_x < 32) && (cursor_off_y < 32);
wire [15:0] cursor_pixel       = cursor[{cursor_off_y[4:0], cursor_off_x[4:0]}];
wire [ 7:0] cursor_pixel_blend = blend(cursor_mode, cursor_pixel, cursor_color);
wire [ 1:0] cursor_pixel_mode  = cursor_pixel[9:8];

always @ (posedge vga_clock)
begin
	if (visible && cursor_visible)
		vga_color <= cursor_blend(
			cursor_pixel_mode, text_vga_color, cursor_pixel_blend);
	else
		vga_color <= text_vga_color;

	vga_hs <= text_vga_hs;
	vga_vs <= text_vga_vs;
end


endmodule
