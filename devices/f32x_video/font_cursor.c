#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>


static uint8_t byte_reverse(uint8_t x)
{
	x = ((x & 0xAA) >> 1) | ((x & 0x55) << 1);
	x = ((x & 0xCC) >> 2) | ((x & 0x33) << 2);
	x = ((x & 0xF0) >> 4) | ((x & 0x0F) << 4);
	return x;
}

static void byte_reverse_copy(
	uint8_t* dst,
	const uint8_t* src,
	unsigned count)
{
	while (count--)
		*dst++ = byte_reverse(*src++);
}


static uint8_t rgb888_to_rgb332(
	uint8_t r, uint8_t g, uint8_t b)
{
	r = ((r + 0x10) >> 5);
	g = ((g + 0x10) >> 5);
	b = ((b + 0x20) >> 6);
	if (r >= 8) r = 7;
	if (g >= 8) g = 7;
	if (b >= 4) b = 3;
	return (r | (g << 3) | (b << 6));
}

static void rgb888_to_rgb332_copy(
	uint8_t* dst,
	const uint8_t* src,
	unsigned count)
{
	while (count--)
	{
		uint8_t r, g, b;
		r = *src++;
		g = *src++;
		b = *src++;
		*dst++ = rgb888_to_rgb332(r, g, b);
	}
}
	


int main(int argc, char* argv[])
{
	if (argc != 3)
		return EXIT_FAILURE;

	const char*    font_header = "P4\n128 192\n";
	const unsigned font_size   = (12 * 256) + strlen(font_header);
	int            font_fd     = open(argv[1], O_RDONLY);
	void*          font_map    = mmap(
		NULL, font_size, PROT_READ, MAP_SHARED, font_fd, 0);

	const char*    cursor_header = "P6\n32 32\n255\n";
	const unsigned cursor_size   = (32 * 32 * 3) + strlen(cursor_header);
	int            cursor_fd     = open(argv[2], O_RDONLY);
	void*          cursor_map    = mmap(
		NULL, cursor_size, PROT_READ, MAP_SHARED, cursor_fd, 0);

	if ((font_fd < 0) || (font_map == MAP_FAILED)
		|| (cursor_fd < 0) || (cursor_map == MAP_FAILED)
		|| (strncmp(  font_map,   font_header, strlen(  font_header)) != 0)
		|| (strncmp(cursor_map, cursor_header, strlen(cursor_header)) != 0))
		return EXIT_FAILURE;

	const uint8_t* font_base   = &((uint8_t*)  font_map)[strlen(font_header  )];
	const uint8_t* cursor_base = &((uint8_t*)cursor_map)[strlen(cursor_header)];

	const uint8_t* font_ptr   = font_base;
	const uint8_t* cursor_ptr = cursor_base;

	uint8_t buffer[4096];
	uint8_t* ptr = buffer;

	unsigned row;
	for (row = 0; row < 8; row++)
	{
		unsigned font_row;
		for (font_row = 0; font_row < 12;
			font_row++, ptr += 32, font_ptr += 16)
		{
			byte_reverse_copy( ptr    ,  font_ptr     , 16);
			byte_reverse_copy(&ptr[16], &font_ptr[192], 16);
		}
		font_ptr += 192;

		unsigned cursor_row;
		for (cursor_row = 0; cursor_row < 4;
			cursor_row++, ptr += 32, cursor_ptr += 96)
			rgb888_to_rgb332_copy(ptr, cursor_ptr, 32);
	}

	write(STDOUT_FILENO, buffer, 4096);
	return EXIT_SUCCESS;
}
