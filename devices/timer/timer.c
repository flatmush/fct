#include <fct/device/device.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>



typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t interrupt;
		uint32_t divide;
		uint32_t latch;
	};
	uint8_t mask[12];
} timer__regmap_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool     reset     : 1;
		bool     latch     : 1;
		unsigned reserved0 : 2;
		bool     recurring : 1;
		bool     one_shot  : 1;
		unsigned reserved1 : 2;
	};
	uint8_t mask;
} timer__flags_t;

struct fct_device_subclass_s
{
	timer__regmap_t regmap;
	uint32_t fract, count;

	bool interrupt_one_shot;
	bool interrupt_recurring;

	fct_device_t* interrupt_target;
	uint64_t      interrupt_number;
};

fct_device_type_t fct_device_timer__type;



static void fct_device_timer__delete(
	fct_device_t* device)
{
	if (!device || !device->subclass)
		return;
	free(device->subclass);
}

static fct_device_t* fct_device_timer__create(
	fct_string_t* name, const fct_tok_t* params)
{
	if (params && (params->type != FCT_TOK_END))
		return NULL;

	fct_device_t* device
		= fct_device_create(
			&fct_device_timer__type, name);
	if (!device) return NULL;

	device->subclass
		= (fct_device_subclass_t*)malloc(
			sizeof(fct_device_subclass_t));
	if (!device->subclass)
	{
		free(device);
		return NULL;
	}

	fct_device_subclass_t* subclass
		= device->subclass;

	subclass->regmap.interrupt = 0xFFFFFFFF;
	subclass->regmap.divide    = 0;
	subclass->regmap.latch     = 0;

	subclass->fract = 0;
	subclass->count = 0;

	subclass->interrupt_one_shot  = false;
	subclass->interrupt_recurring = false;

	subclass->interrupt_target = NULL;
	subclass->interrupt_number = 0;

	return device;
}



static bool fct_device_timer__params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width)
{
	if (!device
		|| !device->subclass)
		return false;

	if (mau  ) *mau   = 8;
	if (width) *width = 1;
	return true;
}



static void fct_device_timer__tick(
	fct_device_t* device)
{
	if (!device || !device->subclass)
		return;

	fct_device_subclass_t* timer
		= device->subclass;

	if (timer->fract >= timer->regmap.divide)
	{
		if (timer->count >= timer->regmap.interrupt)
		{
			timer->count = 0;
			if (timer->interrupt_one_shot
				|| timer->interrupt_recurring)
			{
				timer->interrupt_one_shot = false;

				fct_device_t* target = timer->interrupt_target;
				if (target && target->type
					&& target->type->interrupt_cb)
				{
					target->type->interrupt_cb(
						target, timer->interrupt_number);
				}
			}
		}
		else
		{
			timer->count++;
		}
		timer->fract = 0;
	}
	else
	{
		timer->fract++;
	}
}

static void fct_device_timer__read(
	fct_device_t* device, uint64_t addr, uint8_t* data)
{
	if (!device || !device->subclass || !data)
		return;

	addr %= 16;

	if (addr < 12)
	{
		*data = device->subclass->regmap.mask[addr];
	}
	else if (addr == 12)
	{
		timer__flags_t flags = { .mask = 0x00 };
		flags.one_shot  = device->subclass->interrupt_one_shot;
		flags.recurring = device->subclass->interrupt_recurring;
		*data = flags.mask;
	}
	else
	{
		*data = 0;
	}
}

static void fct_device_timer__write(
	fct_device_t* device, uint64_t addr, uint32_t mask, uint8_t* data)
{
	if (!device || !device->subclass
		|| ((mask & 1) == 0) || !data)
		return;

	addr %= 16;

	if (addr < 8)
	{
		device->subclass->regmap.mask[addr] = *data;
	}
	else if (addr == 12)
	{
		timer__flags_t flags;
		flags.mask = *data;

		if (flags.latch)
			device->subclass->regmap.latch = device->subclass->count;

		if (flags.reset)
		{
			device->subclass->count = 0;
			device->subclass->fract = 0;
		}

		device->subclass->interrupt_one_shot  = flags.one_shot;
		device->subclass->interrupt_recurring = flags.recurring;
	}
}

static bool fct_device_timer__interrupt_attach(
	fct_device_t* device, const char* name,
	fct_device_t* target, uint64_t number)
{
	if (!device || !name || !target)
		return false;

	if (strcmp(name, "interrupt") != 0)
		return false;

	if (device->subclass->interrupt_target)
		return false;

	device->subclass->interrupt_target = target;
	device->subclass->interrupt_number = number;
	return true;
}



fct_device_type_t fct_device_timer__type =
{
	.name                = "timer",
	.create_cb           =        fct_device_timer__create,
	.delete_cb           =        fct_device_timer__delete,
	.params_get_cb       =        fct_device_timer__params_get,
	.tick_cb             =        fct_device_timer__tick,
	.interrupt_cb        =        NULL,
	.read_cb             = (void*)fct_device_timer__read,
	.write_cb            = (void*)fct_device_timer__write,
	.bus_cb              =        NULL,
	.clkbus_cb           =        NULL,
	.port_attach_cb      =        NULL,
	.interrupt_attach_cb = (void*)fct_device_timer__interrupt_attach,
};



fct_device_type_t* device_init(void)
{
	return &fct_device_timer__type;
}

void device_term(fct_device_type_t* type)
{
	(void)type;
}
