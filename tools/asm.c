#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <fct/util/cliarg.h>
#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/job.h>


fct_asm_job_list_t* list;
const char* dest = NULL;

bool args__sources(const char* path)
{
	if (!fct_asm_job_list_add(list, path))
	{
		FCT_DEBUG_ERROR(
			"Failed to add '%s' to job list.", path);
		fct_asm_job_list_delete(list);
		return false;
	}
	return true;
}

bool args__object(const char* path)
{
	dest = path;
	return true;
}



static const cliarg_arg_desc_t args[] =
{
	{ CLIARG_ARG_TYPE_STRING, "sources", 1, 0, (void*)args__sources },
	{ CLIARG_ARG_TYPE_STRING, "object" , 1, 1, (void*)args__object  },
	CLIARG_ARG_DESC_EMPTY
};


int main(int argc, const char* argv[])
{
	list = fct_asm_job_list_create();

	if (!cliarg_parse(argc, argv, NULL, args))
	{
		cliarg_print_usage(argv[0], NULL, args);
		return EXIT_FAILURE;
	}

	fct_object_t* object
		= fct_asm_job_list_process(list);
	fct_asm_job_list_delete(list);
	if (!object)
		return EXIT_FAILURE;

	fct_buffer_t* buffer
		= fct_object_export(object, NULL);
	fct_object_delete(object);

	bool success
		= fct_buffer_file_export(buffer, dest);
	fct_buffer_delete(buffer);

	if (!success)
	{
		FCT_DEBUG_ERROR(
			"Object export failed.");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
