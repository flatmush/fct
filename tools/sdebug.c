#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include <fct/util/cliarg.h>
#include <fct/debug.h>
#include <fct/parse.h>


static bool     data_is_file    = false;
static bool     data_is_word    = false;
static unsigned word_size       = 1;
static bool     data_size_fixed = false;
static uint64_t data_size       = 0;


static bool flags__file(void* ignore)
{
	(void)ignore;

	if (data_is_word)
	{
		FCT_DEBUG_ERROR("Can't specify data as file and word.");
		return false;
	}

	data_is_file = true;
	return true;
}

static bool flags__word(uint64_t* size)
{
	if (data_is_word)
	{
		FCT_DEBUG_ERROR("Can't specify data as file and word.");
		return false;
	}

	if ((*size == 0) || (*size > 8))
	{
		FCT_DEBUG_ERROR("Word size must be 1..8.");
		return false;
	}

	word_size    = *size;
	data_is_word = true;
	return true;
}

static bool flags__size(uint64_t* size)
{
	if ((*size == 0) || (*size > 65536))
	{
		FCT_DEBUG_ERROR("Size must be 1..65536.");
		return false;
	}

	data_size       = *size;
	data_size_fixed = true;
	return true;
}

static const cliarg_flag_desc_t flags[] =
{
	{ 'f', "file", CLIARG_ARG_TYPE_NONE        , NULL   , "Write data from file"       , true, (void*)flags__file },
	{ 'w', "word", CLIARG_ARG_TYPE_NUMBER      , "bytes", "Treat data as numeric words", true, (void*)flags__word },
	{ 's', "size", CLIARG_ARG_TYPE_HUMAN_BINARY, "bytes", "Number of bytes to write"   , true, (void*)flags__size },
	CLIARG_FLAG_DESC_EMPTY
};


int device_fd = -1;

void device__close(void) { close(device_fd); }

static bool args__device(const char* path)
{
	device_fd = open(path, O_RDWR | O_NOCTTY | O_NDELAY);
	if (device_fd < 0)
	{
		FCT_DEBUG_ERROR("Failed to open device '%s'.", path);
		return false;
	}
	atexit(device__close);	

	fcntl(device_fd, F_SETFL, 0);

	struct termios options;
	tcgetattr(device_fd, &options);
	cfsetospeed(&options, B115200);
	options.c_cflag &= ~(PARENB | CSTOPB | CSIZE | CRTSCTS | CREAD);
	options.c_cflag |= CS8;
	options.c_oflag &= ~OPOST;
	tcsetattr(device_fd, TCSAFLUSH, &options);

	return true;
}

uint32_t address;

static bool args__address(uint64_t* addr)
{
	if (*addr >> 32)
	{
		FCT_DEBUG_ERROR(
			"Address size (0x%" PRIX64 ") out of range, must be 32-bit.", *addr);
		return false;
	}

	address = *addr;
	if (data_size_fixed)
	{
		uint16_t wsize = (data_size - 1);
		if ((write(device_fd, &address, sizeof(address)) != sizeof(address))
			|| (write(device_fd, &wsize, sizeof(wsize)) != sizeof(wsize)))
		{
			FCT_DEBUG_ERROR(
				"Failed to write address and size to device.");
			return EXIT_FAILURE;
		}
	}

	return true;
}

static bool args__data(const char* data)
{
	const void* ndata;
	unsigned    nsize;

	fct_buffer_t* buffer = NULL;
	uint64_t      word   = 0;

	if (data_is_file)
	{
		buffer = fct_buffer_file_import(data);
		if (!buffer)
		{
			FCT_DEBUG_ERROR(
				"Failed to write file '%s' to device.", data);
			return false;
		}
		ndata = fct_buffer_data_get(buffer);
		nsize = fct_buffer_size_get(buffer);
	}
	else if (data_is_word)
	{
		if (fct_parse_number(data, &word) != strlen(data))
		{
			FCT_DEBUG_ERROR("Individual word '%s'.", data);
			return false;
		}

		if ((word >> (word_size * 8)) != 0)
		{
			FCT_DEBUG_ERROR("Data word '%s' too long.", data);
			return false;
		}

		ndata = &word;
		nsize = word_size;
	}
	else
	{
		ndata = data;
		nsize = strlen(data);
	}

	if (nsize > 0)
	{
		if (!data_size_fixed)
		{
			if (nsize > 65536)
			{
				FCT_DEBUG_ERROR(
					"Individual data items can't be larger than 65536 bytes.");
				return false;
			}

			uint16_t wsize = (nsize - 1);
			if ((write(device_fd, &address, sizeof(address)) != sizeof(address))
				|| (write(device_fd, &wsize, sizeof(wsize)) != sizeof(wsize)))
			{
				FCT_DEBUG_ERROR(
					"Failed to write address and size to device.");
				return EXIT_FAILURE;
			}
			address += nsize;
		}
		else
		{
			if (nsize > data_size)
			{
				FCT_DEBUG_WARN(
					"Data '%s' is larger than remaining fixed size and will be truncated.", data);
				nsize = data_size;
			}

			if (nsize == 0)
			{
				FCT_DEBUG_WARN(
					"Fixed size already written, data '%s' omitted.", data);
			}

			data_size -= nsize;
		}

		int insize = (int)nsize;
		if (((unsigned)insize != nsize)
			|| (write(device_fd, ndata, insize) != insize))
		{
			FCT_DEBUG_ERROR("Failed to write data to device.");
			return EXIT_FAILURE;
		}
	}

	fct_buffer_delete(buffer);
	return true;
}

static const cliarg_arg_desc_t args[] =
{
	{ CLIARG_ARG_TYPE_STRING, "device" , 1, 1, (void*)args__device  },
	{ CLIARG_ARG_TYPE_NUMBER, "address", 1, 1, (void*)args__address },
	{ CLIARG_ARG_TYPE_STRING, "data"   , 1, 0, (void*)args__data    },
	CLIARG_ARG_DESC_EMPTY
};



int main(int argc, const char* argv[])
{
	if (!cliarg_parse(argc, argv, flags, args))
	{
		cliarg_print_usage(argv[0], flags, args);
		return EXIT_FAILURE;
	}

	if (data_size_fixed && (data_size > 0))
	{
		FCT_DEBUG_WARN(
			"Not enough data to fill fixed size padding with %" PRIu64 " zeros.", data_size);
		for (; data_size > 0; data_size--, address++)
		{
			uint8_t zero = 0;
			if (write(device_fd, &zero, 1) != 1)
			{
				FCT_DEBUG_ERROR(
					"Failed to write zero byte as padding.");
				return EXIT_FAILURE;
			}
		}
	}

	return EXIT_SUCCESS;
}
