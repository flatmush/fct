#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <fct/util/cliarg.h>
#include <fct/debug.h>
#include <fct/asm/context.h>


static const char* format_name = NULL;
static bool        flatten = false;

static bool flags__format(const char* path)
	{ format_name = path; return true; }
static bool flags__flatten(void* value)
	{ (void)value; flatten = true; return true; }

static const cliarg_flag_desc_t flags[] =
{
	{ 'f' , "format", CLIARG_ARG_TYPE_STRING, "format", "Select object format for output", true, (void*)flags__format  },
	{ '\0', "flat"  , CLIARG_ARG_TYPE_NONE  , NULL    , "Flatten object output"          , true, (void*)flags__flatten },
	CLIARG_FLAG_DESC_EMPTY
};


static fct_object_t* object = NULL;

static bool args__object(const char* path)
{
	fct_object_t* nobject
		= fct_object_file_import(path);
	if (!nobject)
	{
		fct_object_delete(object);
		FCT_DEBUG_ERROR("Failed to import object '%s'.", path);
		return false;
	}

	if (!object)
	{
		object = nobject;
	} else {
		fct_object_t* lobject
			= fct_object_merge(object, nobject);
		fct_object_delete(object);
		fct_object_delete(nobject);
		object = lobject;
		if (!object)
		{
			FCT_DEBUG_ERROR("Failed to link object '%s'.", path);
			return false;
		}
	}

	return true;
}

static const char* output = NULL;
static bool args__output(const char* path)
	{ output = path; return true; }

static const cliarg_arg_desc_t args[] =
{
	{ CLIARG_ARG_TYPE_STRING, "objects", 1, 0, (void*)args__object },
	{ CLIARG_ARG_TYPE_STRING, "output" , 1, 1, (void*)args__output },
	CLIARG_ARG_DESC_EMPTY
};


int main(int argc, const char* argv[])
{
	if (!cliarg_parse(argc, argv, flags, args))
	{
		fct_object_delete(object);
		cliarg_print_usage(argv[0], flags, args);
		return EXIT_FAILURE;
	}

	fct_object_format_e format = FCT_OBJECT_FORMAT_FOBJECT;
	if (format_name)
	{
		if (!fct_object_format_from_string(
			format_name, &format))
		{
			FCT_DEBUG_ERROR(
				"Invalid object format name '%s'", format_name);
			fct_object_delete(object);
			return EXIT_FAILURE;
		}
	}

	if (fct_object_format_is_flat(format))
		flatten = true;

	if (flatten)
	{
		fct_object_t* flat
			= fct_object_flatten(object);
		fct_object_delete(object);
		object = flat;

		if (!object)
		{
			FCT_DEBUG_ERROR(
				"Failed to flatten object after linking.");
			return EXIT_FAILURE;
		}
	}

	{
		fct_object_t* opt
			= fct_object_optimize(object);
		if (opt)
		{
			fct_object_delete(object);
			object = opt;
		}
		else
		{
			FCT_DEBUG_WARN(
				"Failed to optimize object during linking.");
		}
	}

	if (!fct_object_file_export_format(object, format, output))
	{
		FCT_DEBUG_ERROR(
			"Failed to export object file '%s'.", output);
		fct_object_delete(object);
		return EXIT_FAILURE;
	}

	fct_object_delete(object);
	return EXIT_SUCCESS;
}
