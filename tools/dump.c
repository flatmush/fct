#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <fct/util/cliarg.h>
#include <fct/debug.h>
#include <fct/object.h>


static bool args__object(const char* path)
{
	fct_object_t* object
		= fct_object_file_import(path);
	if (!object)
	{
		FCT_DEBUG_ERROR(
			"Failed to import object '%s'.", path);
		return EXIT_FAILURE;
	}

	fct_string_t* dump
		= fct_object_dump(object, 0);
	fct_object_delete(object);
	if (!dump)
	{
		FCT_DEBUG_ERROR(
			"Failed to dump object '%s'.", path);
		return EXIT_FAILURE;
	}

	fct_string_fprint(dump, stdout);
	fct_string_delete(dump);

	return true;
}

static const cliarg_arg_desc_t args[] =
{
	{ CLIARG_ARG_TYPE_STRING, "objects", 1, 0, (void*)args__object },
	CLIARG_ARG_DESC_EMPTY
};


int main(int argc, const char* argv[])
{
	if (!cliarg_parse(argc, argv, NULL, args))
	{
		cliarg_print_usage(argv[0], NULL, args);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
