#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <fct/util/cliarg.h>
#include <fct/util/bitaddr.h>
#include <fct/debug.h>


unsigned word        =  8;
unsigned base        = 16;
unsigned cols        =  8;
unsigned offset_bits =  0;
bool     ascii       = false;
bool     auto_prefix = false;
char*    prefix      = NULL;
char*    suffix      = NULL;

static bool flags__word(uint64_t* value)
{
	if ((*value == 0) || (*value > 64))
	{
		FCT_DEBUG_ERROR(
			"Word size must be between 1 and 64 bits.");
		return false;
	}

	word = *value;
	return true;
}

static bool flags__base(uint64_t* value)
{
	if ((*value < 2) || (*value > 36))
	{
		FCT_DEBUG_ERROR(
			"Base must be between 2 and 36.");
		return false;
	}

	base = *value;
	return true;
}

static bool flags__columns(uint64_t* value)
{
	cols = *value;
	return ((uint64_t)cols == *value);
}

static bool flags__offset(uint64_t* value)
{
	if ((*value == 0) || (*value > 64))
	{
		FCT_DEBUG_ERROR(
			"Offset size must be between 1 and 64 bits.");
		return false;
	}

	offset_bits = *value;
	return true;
}

static bool flags__ascii(void* value)
{
	(void)value;
	ascii = true;
	return true;
}

static bool flags__auto_prefix(void* value)
{
	(void)value;
	auto_prefix = true;
	return true;
}

static bool flags__prefix(const char* string)
{
	prefix = strdup(string);
	return (prefix != NULL);
}

static bool flags__suffix(const char* string)
{
	suffix = strdup(string);
	return (suffix != NULL);
}

static const cliarg_flag_desc_t flags[] =
{
	{ 'w', "word"       , CLIARG_ARG_TYPE_NUMBER, "bits"  , "Size of word    (default:  8)", true, (void*)flags__word        },
	{ 'b', "base"       , CLIARG_ARG_TYPE_NUMBER, "number", "Base of value   (default: 16)", true, (void*)flags__base		 },
	{ 'c', "columns"    , CLIARG_ARG_TYPE_NUMBER, "count" , "Values per line (defualt:  8)", true, (void*)flags__columns	 },
	{ 'o', "offset"     , CLIARG_ARG_TYPE_NUMBER, "bits"  , "Print offset"                 , true, (void*)flags__offset      },
	{ 't', "ascii"      , CLIARG_ARG_TYPE_NONE  , NULL    , "Print ASCII data"             , true, (void*)flags__ascii       },
	{ 'a', "auto-prefix", CLIARG_ARG_TYPE_NONE  , NULL    , "Automatically add prefix"     , true, (void*)flags__auto_prefix },
	{ 'p', "prefix"     , CLIARG_ARG_TYPE_STRING, "string", "Prefix for each value" 	   , true, (void*)flags__prefix 	 },
	{ 's', "suffix"     , CLIARG_ARG_TYPE_STRING, "string", "Suffix for each value" 	   , true, (void*)flags__suffix 	 },
	CLIARG_FLAG_DESC_EMPTY
};


FILE* fpi = NULL;
static bool args__input(const char* path)
{
	if (strcmp(path, "-") == 0)
	{
		fpi = stdin;
		return true;
	}

	fpi = fopen(path, "rb");
	if (!fpi)
	{
		FCT_DEBUG_ERROR(
			"Failed to open input file '%s'.", path);
		return false;
	}

	return true;
}

FILE* fpo = NULL;
static bool args__output(const char* path)
{
	fpo = fopen(path, "w");
	if (!fpo)
	{
		FCT_DEBUG_ERROR(
			"Failed to open output file '%s'.", path);
		return false;
	}

	return true;
}

static const cliarg_arg_desc_t args[] =
{
	{ CLIARG_ARG_TYPE_STRING, "input" , 1, 1, (void*)args__input  },
	{ CLIARG_ARG_TYPE_STRING, "output", 0, 1, (void*)args__output },
	CLIARG_ARG_DESC_EMPTY
};

static void cleanup(void)
{
	if (fpi && (fpi != stdin))
		fclose(fpi);
	if (fpo && (fpo != stdout))
		fclose(fpo);
	free(prefix);
	free(suffix);
}


static bool print_digits(uint64_t number, uint64_t offset, unsigned dpw, unsigned col)
{
	if (col != 0)
	{
		if (fprintf(fpo, " ") < 0)
			return false;
	}
	else if (offset_bits != 0)
	{
		offset &= ((1ULL << offset_bits) - 1);

		unsigned offset_nibbles = (offset_bits + 3) / 4;
		char obuff[offset_nibbles];

		unsigned o, j;
		for (j = 0, o = (offset_nibbles - 1);
			j < offset_nibbles; offset >>= 4, j++, o--)
			obuff[o] = "0123456789ABCDEF"[offset & 0xF];

		if (fprintf(fpo, "%.*s: ", offset_nibbles, obuff) <= 0)
			return false;
	}

	char dbuff[dpw + 1];
	dbuff[dpw] = '\0';

	unsigned d, j;
	for (j = 0, d = (dpw - 1); j < dpw; number /= base, j++, d--)
	{
		unsigned digit = (number % base);

		dbuff[d] = (digit < 10
			? ('0' + digit)
			: ('A' + (digit - 10)));
	}

	if (fprintf(fpo, "%s%s%s", (prefix ? prefix : ""),
		dbuff, (suffix ? suffix : "")) <= 0)
		return false;
	fflush(fpo);

	return true;
}


int main(int argc, const char* argv[])
{
	atexit(cleanup);

	if (!cliarg_parse(argc, argv, flags, args))
	{
		cliarg_print_usage(argv[0], flags, args);
		return EXIT_FAILURE;
	}

	if (!fpo) fpo = stdout;

	if (auto_prefix)
	{
		const char* ap;
		switch (base)
		{
			case 2:
				ap = "0b";
				break;
			case 10:
				ap = NULL;
				break;
			case 16:
				ap = "0x";
				break;
			default:
				FCT_DEBUG_ERROR(
					"No default prefix for base %u.", base);
				return EXIT_FAILURE;
		}

		if (ap)
		{
			unsigned prefix_len = (prefix ? strlen(prefix) : 0);
			unsigned ap_len = strlen(ap);
			unsigned np_len = prefix_len + ap_len;
			char* np = (char*)malloc(np_len + 1);
			if (!np) return EXIT_FAILURE;

			if (prefix_len > 0)
				memcpy(np, prefix, prefix_len);
			memcpy(&np[prefix_len], ap, ap_len);
			np[np_len] = '\0';

			free(prefix);
			prefix = np;
		}
	}

	if (ascii && ((word % 8) != 0))
	{
		FCT_DEBUG_ERROR(
			"Words must be a multiple of a byte for ascii representation");
		return EXIT_FAILURE;
	}
	unsigned ascii_cols = (cols * (word / 8));
	char     ascii_buff[ascii_cols];
	unsigned ascii_offset = 0;

	uint64_t word_max = (1ULL << word) - 1;
	unsigned dpw;
	for (dpw = 0; (word_max > 0); word_max /= base, dpw++);

	uint8_t  maubuff[16];
	unsigned maubuff_bits = 0;

	uint64_t offset = 0;
	unsigned col = 0;
	while (true)
	{
		int byte = fgetc(fpi);
		if (byte == EOF) break;

		ascii_buff[ascii_offset++]
			= (isprint(byte) ? byte : '.');
		if (ascii_offset >= ascii_cols)
			ascii_offset = 0;

		bitaddr_write8(
			&maubuff[maubuff_bits >> 3],
			(maubuff_bits & 7), 8, (byte & 0xFF));
		maubuff_bits += 8;

		while (maubuff_bits >= word)
		{
			uint64_t number = 0;
			bitaddr_copy((uint8_t*)&number, 0, maubuff, 0, word);

			uint8_t nmaubuff[16];
			memcpy(nmaubuff, maubuff, 16);
			bitaddr_copy(maubuff, 0, nmaubuff, word, ((16 * 8) - word));
			maubuff_bits -= word;

			if (!print_digits(number, offset++, dpw, col++))
				return EXIT_FAILURE;

			if (col >= cols)
			{
				if (ascii)
				{
					if (fprintf(fpo, "  %.*s",
						ascii_cols, ascii_buff) <= 0)
						return EXIT_SUCCESS;
				}

				if (fprintf(fpo, "\n") < 0)
					return EXIT_FAILURE;
				col = 0;
			}
		}
	}

	if (maubuff_bits > 0)
	{
		uint64_t number = 0;
		bitaddr_copy((uint8_t*)&number, 0, maubuff, 0, maubuff_bits);

		if (!print_digits(number, offset++, dpw, col++))
			return EXIT_FAILURE;
	}

	if (col > 0)
	{
		if (ascii)
		{
			for (; col < cols; col++)
			{
				unsigned pad_len = (prefix ? strlen(prefix) : 0)
					+ dpw + (suffix ? strlen(suffix) : 0) + 1;
				char pad[pad_len];
				memset(pad, ' ', pad_len);
				if (fprintf(fpo, "%.*s", pad_len, pad) <= 0)
					return EXIT_FAILURE;
			}

			for (; ascii_offset < ascii_cols; ascii_offset++)
				ascii_buff[ascii_offset] = ' ';

			if (fprintf(fpo, "  %.*s",
				ascii_cols, ascii_buff) <= 0)
				return EXIT_SUCCESS;
		}

		if (fprintf(fpo, "\n") < 0)
			return EXIT_FAILURE;
	}

	if (ferror(fpi) || ferror(fpo))
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}
