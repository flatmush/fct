#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <fct/util/cliarg.h>
#include <fct/debug.h>
#include <fct/arch.h>
#include <fct/device.h>
#include <fct/device/system.h>
#include <fct/parse.h>


static fct_arch_t* arch = NULL;
static uint64_t cycles = 4096;

static const char* hex = NULL;
static const char* bin = NULL;
static const char* obj = NULL;
static const char* sys = NULL;

static bool flags__arch(const char* name)
{
	arch = fct_arch_by_name(
		FCT_STRING_STATIC(name));
	if (!arch)
	{
		FCT_DEBUG_ERROR(
			"Architecture '%s' not recognized.", name);
		return false;
	}
	return true;
}

static bool flags__object(const char* path)
{
	if (bin || hex)
		return false;
	obj = path;
	return true;
}

static bool flags__binary(const char* path)
{
	if (obj || hex)
		return false;
	bin = path;
	return true;
}

static bool flags__hex(const char* path)
{
	if (obj || bin)
		return false;
	hex = path;
	return true;
}

static bool flags__cycles(uint64_t* value)
{
	cycles = *value;
	return true;
}

static const cliarg_flag_desc_t flags[] =
{
	{ 'a', "arch"  , CLIARG_ARG_TYPE_STRING, "name", "Architecture to emulate", true, (void*)flags__arch   },
	{ 'o', "object", CLIARG_ARG_TYPE_STRING, "path", "Object to execute"      , true, (void*)flags__object },
	{ 'b', "binary", CLIARG_ARG_TYPE_STRING, "path", "Binary to execute"      , true, (void*)flags__binary },
	{ 'h', "hex"   , CLIARG_ARG_TYPE_STRING, "path", "Hex file to execute"    , true, (void*)flags__hex    },
	{ 'c', "cycles", CLIARG_ARG_TYPE_HUMAN_BINARY, "count", "Number of cycles to execute (default: 4k)", true, (void*)flags__cycles },
	CLIARG_FLAG_DESC_EMPTY
};

static bool args__system(const char* path)
{
	sys = path;
	return true;
}

static const cliarg_arg_desc_t args[] =
{
	{ CLIARG_ARG_TYPE_STRING, "system", 1, 1, (void*)args__system },
	CLIARG_ARG_DESC_EMPTY
};


int main(int argc, const char* argv[])
{
	if (!cliarg_parse(argc, argv, flags, args))
	{
		cliarg_print_usage(argv[0], flags, args);
		return EXIT_FAILURE;
	}

	fct_buffer_t* buffer = NULL;
	if (obj)
	{
		fct_object_t* object
			= fct_object_file_import(obj);
		if (!object)
		{
			FCT_DEBUG_ERROR(
				"Failed to import object '%s'.", obj);
			return EXIT_FAILURE;
		}

		/* TODO - Detect if object is already flat. */
		{
			fct_object_t* flat
				= fct_object_flatten(object);
			fct_object_delete(object);
			object = flat;

			if (!object)
			{
				FCT_DEBUG_ERROR(
					"Failed to flatten object '%s'.", obj);
				fct_object_delete(object);
				return EXIT_FAILURE;
			}
		}

		fct_arch_t* oarch
			= fct_object_arch(object);
		if (arch && !fct_arch_is_ancestor(arch, oarch))
		{
			FCT_DEBUG_ERROR(
				"Specified architecture not compatible with object '%s'.", obj);
			fct_object_delete(object);
			return EXIT_FAILURE;
		}
		if (!arch)
			arch = oarch;

		buffer = fct_object_export_raw(object);
		fct_object_delete(object);
		if (!buffer)
			return EXIT_FAILURE;
	}
	else if (hex)
	{
		/* TODO - Implement. */
		return EXIT_FAILURE;
	}
	else if (bin)
	{
		buffer = fct_buffer_file_import(bin);
		if (!buffer)
		{
			FCT_DEBUG_ERROR(
				"Failed to import binary '%s'.", bin);
			return EXIT_FAILURE;
		}
	}
	else
	{
		FCT_DEBUG_ERROR(
			"A single hex, bin or object file must be provided.");
		return EXIT_FAILURE;
	}

	if (!sys)
	{
		FCT_DEBUG_ERROR(
			"System not specified.");
		return EXIT_FAILURE;
	}

	fct_device_t* system
		= fct_device_system_import(arch, sys);
	if (!system)
	{
		FCT_DEBUG_ERROR(
			"Failed to import system '%s'.", sys);
		return EXIT_FAILURE;
	}

	if (buffer)
	{
		unsigned word_size = fct_arch_mau(arch)
			* fct_arch_word_size(arch);
		uint64_t size = fct_buffer_size_get(buffer);
		size *= 8;

		unsigned expand
			= (size % word_size);
		if (expand)
		{
			expand = (word_size - expand);
			if (!fct_buffer_append(buffer,
				NULL, ((expand + 7) >> 3)))
			{
				fct_buffer_delete(buffer);
				fct_device_delete(system);
				return EXIT_FAILURE;
			}
			size += expand;
		}

		size /= word_size;

		fct_device_writes(system,
			0, size, 0xFFFFFFFF,
			(void*)fct_buffer_data_get(buffer));

		fct_buffer_delete(buffer);
	}

	uint64_t c;
	for (c = 0; c < cycles; c++)
		fct_device_tick(system);

	fct_device_delete(system);
	return EXIT_SUCCESS;
}
