#ifndef __FCT_TYPES_BUFFER_H__
#define __FCT_TYPES_BUFFER_H__

typedef struct fct_buffer_s fct_buffer_t;

#include <fct/types.h>

const fct_class_t* fct_class_buffer;


fct_buffer_t* fct_buffer_create();
fct_buffer_t* fct_buffer_create_riff64(const char* title);
fct_buffer_t* fct_buffer_reference(fct_buffer_t* buffer);
fct_buffer_t* fct_buffer_copy(fct_buffer_t* buffer);
void          fct_buffer_delete(fct_buffer_t* buffer);

fct_buffer_t* fct_buffer_file_import(const char* path);
bool          fct_buffer_file_export(fct_buffer_t* buffer, const char* path);

uintptr_t   fct_buffer_size_get(fct_buffer_t* buffer);
const void* fct_buffer_data_get(fct_buffer_t* buffer);

bool fct_buffer_append(fct_buffer_t* buffer, void* data, uintptr_t size);
bool fct_buffer_append_buffer(fct_buffer_t* buffer, fct_buffer_t* append);
bool fct_buffer_modify(fct_buffer_t* buffer, uintptr_t offset, void* data, uintptr_t size);
bool fct_buffer_read(fct_buffer_t* buffer, uintptr_t offset, uintptr_t size, void* data);

fct_buffer_t* fct_buffer_subsection(
	fct_buffer_t* buffer, uintptr_t offset, uintptr_t size);
fct_buffer_t* fct_buffer_subsection_riff64(
	fct_buffer_t* buffer, uintptr_t offset, const char* title);

typedef struct
__attribute__((__packed__))
{
	char     title[8];
	uint64_t size;
} riff64_header_t;

#endif
