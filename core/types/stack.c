#include "stack.h"
#include <stdlib.h>

static const fct_class_t
	fct_class__stack =
	{
		.delete_cb    = (void*)fct_stack_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_stack_copy,
		.import_cb    = NULL,
		.export_cb    = NULL,
	};

const fct_class_t* fct_class_stack
	= &fct_class__stack;



fct_stack_t* fct_stack_create(const fct_class_t* type)
{
	fct_stack_t* stack
		= (fct_stack_t*)malloc(sizeof(fct_stack_t));
	if (!stack)
		return NULL;
	stack->type  = type;
	stack->stack = NULL;
	stack->depth = 0;
	return stack;
}

fct_stack_t* fct_stack_copy(fct_stack_t* stack)
{
	if (!stack)
		return NULL;

	fct_stack_t* copy
		= fct_stack_create(stack->type);
	if (!copy)
		return NULL;

	if (stack->stack
		&& stack->depth)
	{
		if (!stack->type
			|| !stack->type->copy_cb)
		{
			fct_stack_delete(copy);
			return NULL;
		}

		unsigned i;
		for (i = 0; i < stack->depth; i++)
		{
			void* item = stack->type->copy_cb(stack->stack[i]);
			if (!item)
			{
				fct_stack_delete(copy);
				return NULL;
			}
			if (!fct_stack_push(copy, item))
			{
				if (stack->type->delete_cb)
					stack->type->delete_cb(item);
				fct_stack_delete(copy);
				return NULL;
			}
		}
	}

	return copy;
}

void fct_stack_delete(fct_stack_t* stack)
{
	if (!stack)
		return;
	if (stack->stack
		&& stack->type
		&& stack->type->delete_cb)
	{
		unsigned i;
		for (i = 0; i < stack->depth; i++)
			stack->type->delete_cb(stack->stack[i]);
	}
	free(stack->stack);
	free(stack);
}



unsigned fct_stack_pointer(fct_stack_t* stack)
{
	return (stack ? stack->depth : 0);
}

bool fct_stack_revert(fct_stack_t* stack, unsigned pointer)
{
	if (!stack
		|| (pointer > stack->depth))
		return false;
	if (pointer == stack->depth)
		return true;

	if (stack->type
		&& stack->type->delete_cb)
	{
		unsigned i;
		for (i = pointer; i < stack->depth; i++)
			stack->type->delete_cb(stack->stack[i]);
	}
	stack->depth = pointer;
	void** nstack
		= (void**)realloc(stack->stack,
			(stack->depth * sizeof(void*)));
	if (nstack)
		stack->stack = nstack;
	return true;
}



unsigned fct_stack_depth(fct_stack_t* stack)
{
	return (stack ? stack->depth : 0);
}

void* fct_stack_peek_index(fct_stack_t* stack, unsigned index)
{
	if (!stack || (index >= stack->depth))
		return NULL;
	return stack->stack[index];
}

void* fct_stack_peek(fct_stack_t* stack)
{
	if (!stack)
		return NULL;
	return fct_stack_peek_index(stack, (stack->depth - 1));
}



bool fct_stack_push(fct_stack_t* stack, void* item)
{
	if (!stack || !item)
		return false;
	void** nstack
		= (void**)realloc(stack->stack,
			((stack->depth + 1) * sizeof(void*)));
	if (!nstack)
		return false;
	stack->stack = nstack;
	stack->stack[stack->depth++] = item;
	return true;
}

void* fct_stack_pop(fct_stack_t* stack)
{
	if (!stack
		|| (stack->depth == 0))
		return NULL;
	void* item
		= stack->stack[--stack->depth];
	void** nstack = (void**)realloc(stack->stack,
		(stack->depth * sizeof(void*)));
	if (nstack || (stack->depth == 0))
		stack->stack = nstack;
	return item;
}
