#ifndef __FCT_TYPES_STACK_H__
#define __FCT_TYPES_STACK_H__

#include <fct/types.h>

typedef struct
{
	const fct_class_t* type;
	void**             stack;
	unsigned           depth;
} fct_stack_t;

const fct_class_t* fct_class_stack;

fct_stack_t* fct_stack_create(const fct_class_t* type);
fct_stack_t* fct_stack_copy(fct_stack_t* stack);
void         fct_stack_delete(fct_stack_t* stack);

unsigned fct_stack_pointer(fct_stack_t* stack);
bool     fct_stack_revert(fct_stack_t* stack, unsigned pointer);

unsigned fct_stack_depth(fct_stack_t* stack);

bool  fct_stack_push(fct_stack_t* stack, void* item);
void* fct_stack_peek_index(fct_stack_t* stack, unsigned index);
void* fct_stack_peek(fct_stack_t* stack);
void* fct_stack_pop(fct_stack_t* stack);

#endif
