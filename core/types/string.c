#include "string.h"
#include <fct/types/strtab.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>



static const fct_class_t
	fct_class__string =
	{
		.delete_cb    = (void*)fct_string_delete,
		.reference_cb = (void*)fct_string_reference,
		.copy_cb      = (void*)fct_string_copy,
		.import_cb    = (void*)fct_string_import,
		.export_cb    = (void*)fct_string_export,
	};

const fct_class_t* fct_class_string
	= &fct_class__string;



fct_string_t* fct_string_static(const char* string, unsigned size)
{
	if (!string && (size != 0))
		return NULL;

	fct_string_t* s
		= (fct_string_t*)malloc(
			sizeof(fct_string_t));
	if (!s) return NULL;

	s->data      = (char*)string;
	s->size      = size;
	s->dynamic   = false;
	s->null_term = false;
	s->readonly  = false;
	s->refcnt    = 0;

	return s;
}

fct_string_t* fct_string_create(const char* string)
{
	return fct_string_create_format("%s", string);
}

fct_string_t* fct_string_create_format(const char* format, ...)
{
	va_list args;
	va_start(args, format);
	fct_string_t* str
		= fct_string_create_formatv(format, args);
	va_end(args);

	return str;
}

fct_string_t* fct_string_create_formatv(const char* format, va_list arg)
{
	if (!format)
		return NULL;

	va_list argb;
	va_copy(argb, arg);

	char dummy[4];
	int size
		= vsnprintf(dummy, 4, format, argb);

	va_end(argb);

	if (size < 0)
		return NULL;

	fct_string_t* string
		= (fct_string_t*)malloc(sizeof(fct_string_t) + (size + 1));
	if (!string)
		return NULL;
	string->data      = (char*)((uintptr_t)string + sizeof(fct_string_t));
	string->size      = size;
	string->dynamic   = true;
	string->null_term = true;
	string->readonly  = false;
	string->refcnt    = 0;
	vsprintf((char*)string->data, format, arg);
	return string;
}



fct_string_t* fct_string_reference(fct_string_t* string)
{
	if (!string)
		return NULL;

	if (string->readonly
		|| ((string->refcnt + 1) == 0))
		return fct_string_copy(string);

	string->refcnt++;
	return string;
}

fct_string_t* fct_string_copy(fct_string_t* string)
{
	if (!string)
		return NULL;

	fct_string_t* copy
		= fct_string_static(NULL, 0);
	if (!copy) return NULL;

	if (!fct_string_resize(
		copy, string->size))
	{
		fct_string_delete(copy);
		return NULL;
	}

	if (string->data && (string->size > 0))
		memcpy(copy->data, string->data, string->size);

	if (string->null_term
		&& !fct_string_null_terminate(copy))
	{
		fct_string_delete(copy);
		return NULL;
	}

	return copy;
}

void fct_string_delete(fct_string_t* string)
{
	if (!string || string->readonly)
		return;

	if (string->refcnt != 0)
	{
		string->refcnt--;
		return;
	}

	if (string->dynamic)
	{
		if ((uintptr_t)string->data
			!= ((uintptr_t)string + sizeof(fct_string_t)))
			free(string->data);
	}

	free(string);
}



bool fct_string_resize(fct_string_t* string, unsigned size)
{
	if (!string)
		return false;

	if (string->size == size)
		return true;

	unsigned dsize = (size + 1);

	char* data;
	if (string->dynamic
		&& ((uintptr_t)string->data
			!= ((uintptr_t)string + sizeof(fct_string_t))))
	{
		data = (char*)realloc(string->data, dsize);
		if (!data) return false;
	}
	else
	{
		data = (char*)malloc(dsize);
		if (!data) return false;

		if (string->data && (string->size > 0))
			memcpy(data, string->data, string->size);
	}
	data[size] = '\0';

	string->data      = data;
	string->size      = size;
	string->dynamic   = true;
	string->null_term = true;
	return true;
}



fct_string_t* fct_string_join(fct_string_t* a, fct_string_t* b)
{
	if (fct_string_is_empty(a))
		return fct_string_copy(b);
	if (fct_string_is_empty(b))
		return fct_string_copy(a);

	uint32_t size = a->size + b->size;
	if (size < a->size)
		return NULL;

	fct_string_t* string
		= (fct_string_t*)malloc(
			sizeof(fct_string_t) + (size + 1));
	if (!string) return NULL;

	string->data      = (char*)((uintptr_t)string + sizeof(fct_string_t));
	string->size      = size;
	string->dynamic   = true;
	string->null_term = true;
	string->readonly  = false;
	string->refcnt    = 0;

	memcpy(string->data, a->data, a->size);
	memcpy(&string->data[a->size], b->data, b->size);
	string->data[size] = '\0';

	return string;
}



bool fct_string_append(fct_string_t* string, fct_string_t* append)
{
	if (!append)
		return true;

	return fct_string_append_static_format(
		string, "%.*s", append->size, append->data);
}

bool fct_string_append_static(fct_string_t* string, const char* append)
{
	return fct_string_append_static_format(
		string, "%s", append);
}

bool fct_string_append_static_format(fct_string_t* string, const char* format, ...)
{
	va_list args;
	va_start(args, format);
	bool ret
		= fct_string_append_static_formatv(
			string, format, args);
	va_end(args);

	return ret;
}

bool fct_string_append_static_formatv(fct_string_t* string, const char* format, va_list arg)
{
	if (!string || string->readonly)
		return false;

	if (format[0] == '\0')
		return true;

	if (!string->dynamic)
	{
		char* dstr;
		if (string->size != 0)
		{
			dstr = (char*)malloc(string->size);
			if (!dstr) return false;
			memcpy(dstr, string->data, string->size);
		}
		else
		{
			dstr = NULL;
		}

		string->null_term = false;
		string->data      = dstr;
		string->dynamic   = true;
	}
	else if ((uintptr_t)string->data
		== ((uintptr_t)string + sizeof(fct_string_t)))
	{
		char* dstr;
		if (string->size != 0)
		{
			dstr = (char*)malloc(string->size);
			if (!dstr) return false;
			memcpy(dstr, string->data, string->size);
		}
		else
		{
			dstr = NULL;
		}

		string->null_term = false;
		string->data      = dstr;

		fct_string_t* nstr
			= (fct_string_t*)realloc(string,
				sizeof(fct_string_t));
		if (nstr) string = nstr;
	}

	va_list argb;
	va_copy(argb, arg);

	char dummy[4];
	int size
		= vsnprintf(dummy, 4, format, argb);

	va_end(argb);

	if (size < 0)
		return false;

	char* nstr
		= (char*)realloc(string->data,
			string->size + size + 1);
	if(!nstr) return false;
	string->data = nstr;

	vsprintf(&string->data[string->size], format, arg);
	string->size += size;
	string->data[string->size] = '\0';
	string->null_term = true;
	return true;
}



fct_string_t* fct_string_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	(void)strtab;

	unsigned o = (offset ? *offset : 0);

	uint32_t size;
	if (!fct_buffer_read(buffer, o, sizeof(size), &size))
		return NULL;
	o += sizeof(size);

	char data[size];
	if (!fct_buffer_read(buffer, o, size, data))
		return NULL;
	o += size;

	fct_string_t* string
		= (fct_string_t*)malloc(
			sizeof(fct_string_t) + size + 1);
	if (!string) return NULL;

	string->size      = size;
	string->data      = (char*)((uintptr_t)string + sizeof(fct_string_t));
	string->dynamic   = true;
	string->null_term = true;
	string->readonly  = false;
	string->refcnt    = 0;

	memcpy((char*)string->data, data, size);
	((char*)string->data)[size] = '\0';

	if (offset) *offset = o;
	return string;
}

fct_buffer_t* fct_string_export(
	fct_string_t* string, fct_list_t* strtab)
{
	(void)strtab;

	if (!string) return NULL;

	fct_buffer_t* buffer
		= fct_buffer_create();
	if (!buffer) return NULL;

	if (!fct_buffer_append(buffer,
		&string->size, sizeof(string->size))
		|| !fct_buffer_append(buffer,
			(void*)string->data, string->size))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	return buffer;
}



bool fct_string_import_table(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab,
	fct_string_t** string)
{
	unsigned o = (offset ? *offset : 0);
	uint32_t index;

	if (!fct_buffer_read(buffer,
		o, sizeof(index), &index))
		return false;
	o += sizeof(index);

	if (index == 0)
	{
		if (offset) *offset = o;
		if (string) *string = NULL;
		return true;
	}

	fct_string_t* s
		= fct_string_reference(
			fct_array_index_resolve(
				strtab, (index - 1)));
	if (!s) return false;

	if (offset) *offset = o;
	if (string)
		*string = s;
	else
		fct_string_delete(s);
	return true;
}

fct_buffer_t* fct_string_export_table(
	fct_string_t* string, fct_list_t* strtab)
{
	fct_buffer_t* buffer
		= fct_buffer_create();
	if (!buffer) return NULL;

	unsigned i;
	if (!fct_strtab_add(
		strtab, string, &i))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	uint32_t index = i;
	if ((index != i)
		|| !fct_buffer_append(buffer,
			&index, sizeof(index)))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	return buffer;
}



fct_string_t* fct_string_file_import(const char* path)
{
	FILE* fp = fopen(path, "r");
	if (!fp) return NULL;

	long size;
	if ((fseek(fp, 0, SEEK_END) < 0)
		|| ((size = ftell(fp)) < 0)
		|| (fseek(fp, 0, SEEK_SET) < 0))
	{
		fclose(fp);
		return NULL;
	}

	fct_string_t* string
		= fct_string_static(NULL, 0);
	if (!string
		|| !fct_string_resize(string, (unsigned)size))
	{
		fct_string_delete(string);
		fclose(fp);
		return false;
	}

	bool success = (fread(string->data, string->size, 1, fp) == 1);
	fclose(fp);

	if (!success)
	{
		fct_string_delete(string);
		return NULL;
	}

	return string;
}

bool fct_string_file_export(fct_string_t* string, const char* path)
{
	if (!string)
		return false;

	FILE* fp = fopen(path, "w");
	if (!fp) return false;

	bool success = (fwrite(string->data, string->size, 1, fp) == 1);
	fclose(fp);

	return success;
}



bool fct_string_fprint(fct_string_t* string, FILE* stream)
{
	if (!string)
		return false;
	return (fprintf(stream, "%.*s", string->size, string->data) >= 0);
}



bool fct_string_is_empty(fct_string_t* string)
{
	return (!string || (string->size == 0));
}



bool fct_string_null_terminate(fct_string_t* string)
{
	if (!string)
		return false;

	if (string->null_term)
		return true;

	if (!fct_string_resize(
		string, (string->size + 1)))
		return false;

	string->size--;
	string->data[string->size] = '\0';
	string->null_term = true;
	return true;
}



bool fct_string_compare(fct_string_t* a, fct_string_t* b)
{
	if (!a || !b)
		return false;

	if (a == b)
		return true;

	if (a->size != b->size)
		return false;

	if (!a->data) return !b->data;
	if (!b->data) return false;

	return (memcmp(a->data, b->data, a->size) == 0);
}


uint8_t fct_string_hash8(fct_string_t* string)
{
	if (!string)
		return 0;

	uint8_t h;
	unsigned i;
	for (i = 0, h = 0; i < string->size; h += string->data[i++]);
	return h;
}
