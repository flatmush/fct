#ifndef __FCT_STRTAB_H__
#define __FCT_STRTAB_H__

#include <fct/types/string.h>
#include <fct/types/list.h>

fct_list_t* fct_strtab_create(void);

bool fct_strtab_add(
	fct_list_t* array, fct_string_t* string,
	unsigned* index);

fct_string_t* fct_strtab_find(
	fct_list_t* array, fct_string_t* string,
	unsigned* index);

#endif
