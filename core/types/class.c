#include "class.h"
#include <stdlib.h>



void* fct_class_copy(const fct_class_t* class, void* object)
{
	if (!object
		|| !class
		|| !class->copy_cb)
		return NULL;
	return class->copy_cb(object);
}

void* fct_class_reference(const fct_class_t* class, void* object)
{
	if (!object
		|| !class)
		return NULL;
	if (class->reference_cb)
		return class->reference_cb(object);
	if (class->copy_cb)
		return class->copy_cb(object);
	return NULL;
}

void  fct_class_delete(const fct_class_t* class, void* object)
{
	if (object
		&& class
		&& class->delete_cb)
		class->delete_cb(object);
}



void* fct_class_import(const fct_class_t* class,
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	if (!class
		|| !class->import_cb)
		return NULL;
	return class->import_cb(buffer, offset, strtab);
}

fct_buffer_t* fct_class_export(const fct_class_t* class,
	void* object, fct_list_t* strtab)
{
	if (!object
		|| !class
		|| !class->export_cb)
		return NULL;
	return class->export_cb(object, strtab);
}
