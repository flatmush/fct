#ifndef __FCT_TYPES_STRING_H__
#define __FCT_TYPES_STRING_H__

#include <fct/types/class.h>
#include <fct/types/buffer.h>
#include <fct/types/list.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

struct fct_string_s
{
	char*    data;
	uint32_t size;
	bool     dynamic;
	bool     null_term;
	bool     readonly;
	unsigned refcnt;
};

#define FCT_STRING_STATIC_SECTION(s,l) &((fct_string_t){(char*)(s),(l),false,false,true,UINT_MAX})
#define FCT_STRING_STATIC(s) FCT_STRING_STATIC_SECTION(s,strlen(s))

const fct_class_t* fct_class_string;

fct_string_t* fct_string_static(const char* string, unsigned size);
fct_string_t* fct_string_create(const char* string);
fct_string_t* fct_string_create_format(const char* format, ...);
fct_string_t* fct_string_create_formatv(const char* format, va_list arg);

fct_string_t* fct_string_reference(fct_string_t* string);
fct_string_t* fct_string_copy(fct_string_t* string);
void          fct_string_delete(fct_string_t* string);

bool fct_string_resize(fct_string_t* string, unsigned size);

fct_string_t* fct_string_join(fct_string_t* a, fct_string_t* b);

bool fct_string_append(fct_string_t* string, fct_string_t* append);
bool fct_string_append_static(fct_string_t* string, const char* append);
bool fct_string_append_static_format(fct_string_t* string, const char* format, ...);
bool fct_string_append_static_formatv(fct_string_t* string, const char* format, va_list arg);

fct_string_t* fct_string_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_string_export(
	fct_string_t* string, fct_list_t* strtab);

bool fct_string_fprint(fct_string_t* string, FILE* stream);

bool fct_string_import_table(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab,
	fct_string_t** string);
fct_buffer_t* fct_string_export_table(
	fct_string_t* string, fct_list_t* strtab);

fct_string_t* fct_string_file_import(const char* path);
bool          fct_string_file_export(fct_string_t* string, const char* path);

bool fct_string_is_empty(fct_string_t* string);

bool fct_string_null_terminate(fct_string_t* string);

bool fct_string_compare(fct_string_t* a, fct_string_t* b);

uint8_t fct_string_hash8(fct_string_t* string);

#endif
