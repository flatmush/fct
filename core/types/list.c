#include <fct/types.h>
#include "list/interface.h"



static const fct_class_t
	fct_class__list =
	{
		.delete_cb    = (void*)fct_list_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_list_copy,
		.import_cb    = NULL,
		.export_cb    = (void*)fct_list_export,
	};
const fct_class_t* fct_class_list
	= &fct_class__list;



struct __attribute__((__packed__)) fct_list_s
{
	fct_list_interface_t* interface;
};



fct_list_t* fct_list_copy(fct_list_t* list)
{
	if (!list || !list->interface)
		return NULL;
	return fct_class_copy(list->interface->class, list);
}

void fct_list_delete(fct_list_t* list)
{
	if (list && list->interface)
		fct_class_delete(list->interface->class, list);
}



bool fct_list_import_named(
	fct_list_t* list, const char* name,
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	const fct_class_t* type
		= fct_list_type(list);
	if (!type) return false;

	fct_buffer_t* ibuffer
		= fct_buffer_subsection_riff64(buffer,
			(offset ? *offset : 0), name);
	if (!ibuffer) return false;

	unsigned o = 0;

	unsigned count;
	if (!fct_buffer_read(ibuffer, o, sizeof(count), &count))
	{
		fct_buffer_delete(ibuffer);
		return false;
	}
	o += sizeof(count);

	unsigned i;
	for (i = 0; i < count; i++)
	{
		void* object
			= fct_class_import(type, ibuffer, &o, strtab);
		if (!object || !fct_list_add(list, object))
		{
			fct_class_delete(type, object);
			fct_buffer_delete(ibuffer);
			return false;
		}
	}

	if (offset)
		*offset += fct_buffer_size_get(ibuffer)
			+ sizeof(riff64_header_t);
	fct_buffer_delete(ibuffer);
	return true;
}

bool  fct_list_import(
	fct_list_t* list,
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	return fct_list_import_named(list, "LIST", buffer, offset, strtab);
}



fct_buffer_t* fct_list_export_named(
	const char* name,
	fct_list_t* list, fct_list_t* strtab)
{
	if (!list)
		return NULL;

	fct_buffer_t* buffer
		= fct_buffer_create_riff64(name);
	if (!buffer) return NULL;

	unsigned count = fct_list_count(list);
	uint32_t fcount = count;
	if (fcount != count)
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	if (!fct_buffer_append(buffer,
		&fcount, sizeof(fcount)))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	if (fct_list_count(list) == 0)
		return buffer;

	bool export_item(void* item, void* params)
	{
		(void)params;

		fct_buffer_t* itembuff
			= fct_class_export(fct_list_type(list), item, strtab);
		if (!itembuff) return false;

		bool success
			= fct_buffer_append_buffer(
				buffer, itembuff);
		fct_buffer_delete(itembuff);
		return success;
	}

	if (!fct_list_foreach(
		list, NULL, export_item))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	return buffer;
}

fct_buffer_t* fct_list_export(
	fct_list_t* list, fct_list_t* strtab)
{
	return fct_list_export_named("LIST", list, strtab);
}



fct_string_t* fct_list_dump(
	fct_list_t* list, unsigned indent,
	fct_string_t* (*dump_func)(void* item, unsigned indent))
{
	if (!list || !dump_func)
		return NULL;

	char indentstr[indent + 1];
	memset(indentstr, '\t', indent);
	indentstr[indent] = '\0';

	fct_string_t* dump
		= fct_string_create_format("%s{\n", indentstr);
	if (!dump) return NULL;

	bool dump_item(void* item, void* param)
	{
		(void)param;
		fct_string_t* item_dump
			= dump_func(item, (indent + 1));
		if (!item_dump) return false;
		if (!fct_string_append(dump, item_dump))
		{
			fct_string_delete(item_dump);
			return false;
		}
		return true;
	}

	if (!fct_list_foreach(list, NULL, dump_item))
	{
		fct_string_delete(dump);
		return NULL;
	}

	if (!fct_string_append_static_format(
		dump, "%s}\n", indentstr))
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}



unsigned fct_list_count(fct_list_t* list)
{
	if (!list || !list->interface
		|| !list->interface->count)
		return 0;
	return list->interface->count(list);
}

const fct_class_t* fct_list_type(fct_list_t* list)
{
	if (!list || !list->interface
		|| !list->interface->type)
		return NULL;
	return list->interface->type(list);
}



bool fct_list_add(fct_list_t* list, void* item)
{
	if (!list || !list->interface
		|| !list->interface->add)
		return false;
	return list->interface->add(list, item);
}

bool fct_list_add_list(fct_list_t* list, fct_list_t* add)
{
	if (!list || !list->interface
		|| !list->interface->add_list)
		return false;
	return list->interface->add_list(list, add);
}



bool fct_list_foreach(
	fct_list_t* list, void* param,
	bool (*func)(void* item, void* param))
{
	if (!list || !list->interface
		|| !list->interface->foreach)
		return false;
	return list->interface->foreach(list, param, func);
}



void* fct_list_find(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (!list || !list->interface
		|| !list->interface->find)
		return NULL;
	return list->interface->find(list, attrib, item_find);
}

void* fct_list_find_remove(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (!list || !list->interface
		|| !list->interface->find_remove)
		return NULL;
	return list->interface->find_remove(list, attrib, item_find);
}

bool  fct_list_find_delete(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	void* item = fct_list_find_remove(
		list, attrib, item_find);
	fct_class_delete(fct_list_type(list), item);
	return (item != NULL);
}

bool  fct_list_find_delete_all(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (list && list->interface
		&& list->interface->find_delete_all)
		return list->interface->find_delete_all(
			list, attrib, item_find);

	bool success = false;
	while (fct_list_find_delete(
		list, attrib, item_find))
		success = true;
	return success;
}



bool fct_list_contains(fct_list_t* list, void* item)
{
	bool contains(void* litem, void* attrib)
	{
		return (litem == attrib);
	}
	return fct_list_find(list, item, contains);
}
