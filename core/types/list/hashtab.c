#include "hashtab.h"
#include "interface.h"
#include <fct/types/string.h>
#include <stdlib.h>
#include <fct/debug.h>



typedef struct fct_hashtab_node_s fct_hashtab_node_t;

struct fct_hashtab_node_s
{
	void*               item;
	fct_hashtab_node_t* next;
};

struct __attribute__((__packed__)) fct_list_s
{
	const fct_list_interface_t* interface;
	struct
	{
		const fct_class_t*  type;
		fct_hashtab_node_t* base[256];
		unsigned            count;

		void*   (*key  )(void* item);
		bool    (*match)(void* ka, void* kb);
		uint8_t (*hash )(void* item);
	};
};



static void          fct_hashtab_delete(fct_list_t* list);
static fct_list_t*   fct_hashtab_copy(fct_list_t* list);

static const fct_class_t
	fct_class__hashtab =
	{
		.delete_cb    = (void*)fct_hashtab_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_hashtab_copy,
		.import_cb    = NULL,
		.export_cb    = (void*)fct_list_export,
	};
const fct_class_t* fct_class_hashtab
	= &fct_class__hashtab;



static unsigned            fct_hashtab_count(fct_list_t* list);
static const fct_class_t*  fct_hashtab_type(fct_list_t* list);
static bool                fct_hashtab_add(fct_list_t* list, void* item);
static bool                fct_hashtab_foreach(fct_list_t* list, void* param, bool (*func)(void* item, void* param));
static void*               fct_hashtab_find(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));
static void*               fct_hashtab_find_remove(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));
static bool                fct_hashtab_find_delete_all(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));

static const fct_list_interface_t
	fct_list_interface_hashtab =
	{
		.class = &fct_class__hashtab,

		.count           = fct_hashtab_count,
		.type            = fct_hashtab_type,
		.add             = fct_hashtab_add,
		.add_list        = NULL,
		.foreach         = fct_hashtab_foreach,
		.find            = fct_hashtab_find,
		.find_remove     = fct_hashtab_find_remove,
		.find_delete_all = fct_hashtab_find_delete_all,
	};



fct_list_t* fct_hashtab_create(
	const fct_class_t* type,
	void*   (*key  )(void* item),
	bool    (*match)(void* ka, void* kb),
	uint8_t (*hash )(void* item))
{
	if (!key || !hash)
		return NULL;

	fct_list_t* list
		= (fct_list_t*)malloc(sizeof(fct_list_t));
	if (!list)
		return NULL;

	list->interface = &fct_list_interface_hashtab;
	list->type      = type;
	list->count     = 0;
	list->key       = key;
	list->match     = match;
	list->hash      = hash;

	unsigned i;
	for (i = 0; i < 256; i++)
		list->base[i] = NULL;

	return list;
}



static fct_list_t* fct_hashtab_copy(fct_list_t* list)
{
	if (!list)
		return NULL;
	
	fct_list_t* copy
		= fct_hashtab_create(
			list->type, list->key,
			list->match, list->hash);
	if (!copy) return NULL;

	bool copy_items(void* item, void* param)
	{
		(void)param;
		void* nitem = fct_class_copy(list->type, item);
		if (!nitem) return false;
		if (!fct_list_add(copy, nitem))
		{
			fct_class_delete(list->type, nitem);
			return false;
		}
		return true;
	}

	if (!fct_list_foreach(
		list, NULL, copy_items))
	{
		fct_list_delete(copy);
		return NULL;
	}

	return copy;
}

static void fct_hashtab_delete(fct_list_t* list)
{
	if (!list)
		return;

	unsigned i;
	for (i = 0; i < 256; i++)
	{
		fct_hashtab_node_t* node = list->base[i];
		while (node)
		{
			fct_hashtab_node_t* next = node->next;
			fct_class_delete(list->type, node->item);
			free(node);
			node = next;
		}
	}

	free(list);
}



static unsigned fct_hashtab_count(fct_list_t* list)
{
	return (list ? list->count : 0);
}

static const fct_class_t* fct_hashtab_type(fct_list_t* list)
{
	return (list ? list->type : NULL);
}



static bool fct_hashtab_add(fct_list_t* list, void* item)
{
	if (!list || !item)
		return false;

	void* key = list->key(item);
	if (!key) return false;

	void* exists = fct_hashtab_find_by_key(list, key);
	if (exists) return false;

	if ((list->count + 1) == 0)
		return false;

	fct_hashtab_node_t* node
		= (fct_hashtab_node_t*)malloc(
			sizeof(fct_hashtab_node_t));
	if (!node) return false;

	uint8_t hash = list->hash(key);
	node->item = item;
	node->next = list->base[hash];
	list->base[hash] = node;
	list->count++;
	return true;
}



static bool fct_hashtab_foreach(
	fct_list_t* list, void* param,
	bool (*func)(void* item, void* param))
{
	if (!list || !func)
		return false;

	unsigned i;
	for (i = 0; i < 256; i++)
	{
		fct_hashtab_node_t* node;
		for (node = list->base[i];
			node; node = node->next)
		{
			if (!func(node->item, param))
				return false;
		}
	}
	return true;
}



static fct_hashtab_node_t* fct_hashtab_node_find(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (!list || !item_find)
		return false;

	unsigned i;
	for (i = 0; i < 256; i++)
	{
		fct_hashtab_node_t* node;
		for (node = list->base[i];
			node; node = node->next)
		{
			if (item_find(node->item, attrib))
				return node;
		}
	}
	return NULL;
}

static void* fct_hashtab_find(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	fct_hashtab_node_t* node
		= fct_hashtab_node_find(
			list, attrib, item_find);
	if (!node) return NULL;
	return node->item;
}

static void* fct_hashtab_find_remove(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (!list || !item_find
		|| (list->count == 0))
		return false;

	unsigned i;
	for (i = 0; i < 256; i++)
	{
		fct_hashtab_node_t* prev = NULL;
		fct_hashtab_node_t* node;
		for (node = list->base[i];
			node; node = node->next)
		{
			if (item_find(node->item, attrib))
			{
				void* item = node->item;
				if (prev)
					prev->next = node->next;
				else
					list->base[i] = node->next;
				free(node);
				list->count--;
				return item;
			}
		}
	}
	return NULL;
}

static bool fct_hashtab_find_delete_all(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (!list || !item_find
		|| (list->count == 0))
		return false;

	unsigned i, removed = 0;
	for (i = 0; i < 256; i++)
	{
		fct_hashtab_node_t* prev = NULL;
		fct_hashtab_node_t* node = list->base[i];
		while (node)
		{
			fct_hashtab_node_t* next = node->next;

			if (item_find(node->item, attrib))
			{
				void* item = node->item;
				if (prev)
					prev->next = next;
				else
					list->base[i] = next;
				free(node);
				list->count--;
				fct_class_delete(list->type, item);
				removed++;
			}

			node = next;
		}
	}

	return (removed != 0);
}



void* fct_hashtab_find_by_key(
	fct_list_t* list, void* key)
{
	if (!list || !key
		|| !list->key
		|| !list->match
		|| !list->hash)
		return NULL;

	uint8_t hash = list->hash(key);
	fct_hashtab_node_t* node;
	for (node = list->base[hash];
		node; node = node->next)
	{
		void* nkey = list->key(node->item);
		if (list->match(key, nkey))
			return node->item;
	}
	return NULL;
}
