#ifndef __FCT_TYPES_ARRAY_H__
#define __FCT_TYPES_ARRAY_H__

#include <fct/types.h>

const fct_class_t* fct_class_array;

fct_list_t* fct_array_create(const fct_class_t* type);

void* fct_array_index_resolve(fct_list_t* array, unsigned index);
bool  fct_array_index_add(fct_list_t* array, void* item, unsigned* index);
void* fct_array_index_remove(fct_list_t* array, unsigned index);
bool  fct_array_index_delete(fct_list_t* array, unsigned index);
void* fct_array_index_find_remove(fct_list_t* array, void* param, bool (*func)(void* item, void* param), unsigned* index);
void* fct_array_index_find(fct_list_t* array, void* attrib, bool (*item_find)(void* item, void* attrib), unsigned* index);
bool  fct_array_index_find_by_pointer(fct_list_t* array, void* item, unsigned* index);

bool fct_array_sort(fct_list_t* array, int (*item_sort)(void* a, void* b));

#endif
