#ifndef __FCT_TYPES_LIST_INTERFACE_H__
#define __FCT_TYPES_LIST_INTERFACE_H__

typedef struct
{
	const fct_class_t* class;

	unsigned           (*count)(fct_list_t* list);
	const fct_class_t* (*type )(fct_list_t* list);

	bool (*add     )(fct_list_t* list, void* item);
	bool (*add_list)(fct_list_t* list, fct_list_t* add);

	bool (*foreach)(fct_list_t* list, void* param, bool (*func)(void* item, void* param));

	void* (*find           )(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));
	void* (*find_remove    )(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));
	bool  (*find_delete_all)(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));
} fct_list_interface_t;

#endif
