#ifndef __FCT_TYPES_HASHTAB_H__
#define __FCT_TYPES_HASHTAB_H__

#include <fct/types.h>

const fct_class_t* fct_class_hashtab;

fct_list_t* fct_hashtab_create(
	const fct_class_t* type,
	void*   (*key  )(void* item),
	bool    (*match)(void* ka, void* kb),
	uint8_t (*hash )(void* item));

void* fct_hashtab_find_by_key(
	fct_list_t* list, void* key);

#endif
