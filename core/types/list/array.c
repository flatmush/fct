#include "array.h"
#include "interface.h"
#include <fct/types/string.h>
#include <stdlib.h>
#include <fct/debug.h>



struct __attribute__((__packed__)) fct_list_s
{
	const fct_list_interface_t* interface;
	struct
	{
		const fct_class_t* type;
		void**             items;
		unsigned           count, max;
	};
};



static void          fct_array_delete(fct_list_t* array);
static fct_list_t*   fct_array_copy(fct_list_t* array);

static const fct_class_t
	fct_class__array =
	{
		.delete_cb    = (void*)fct_array_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_array_copy,
		.import_cb    = NULL,
		.export_cb    = (void*)fct_list_export,
	};
const fct_class_t* fct_class_array
	= &fct_class__array;



static unsigned            fct_array_count(fct_list_t* array);
static const fct_class_t*  fct_array_type(fct_list_t* array);
static bool                fct_array_add(fct_list_t* array, void* item);
static bool                fct_array_add_list(fct_list_t* array, fct_list_t* add);
static bool                fct_array_foreach(fct_list_t* array, void* param, bool (*func)(void* item, void* param));
static void*               fct_array_find(fct_list_t* array, void* attrib, bool (*item_find)(void* item, void* attrib));
static void*               fct_array_find_remove(fct_list_t* array, void* attrib, bool (*item_find)(void* item, void* attrib));
static bool                fct_array_find_delete_all(fct_list_t* array, void* attrib, bool (*item_find)(void* item, void* attrib));

static const fct_list_interface_t
	fct_list_interface_array =
	{
		.class = &fct_class__array,

		.count           = fct_array_count,
		.type            = fct_array_type,
		.add             = fct_array_add,
		.add_list        = fct_array_add_list,
		.foreach         = fct_array_foreach,
		.find            = fct_array_find,
		.find_remove     = fct_array_find_remove,
		.find_delete_all = fct_array_find_delete_all,
	};



fct_list_t* fct_array_create(const fct_class_t* type)
{
	fct_list_t* array
		= (fct_list_t*)malloc(sizeof(fct_list_t));
	if (!array)
		return NULL;

	array->interface = &fct_list_interface_array;
	array->type      = type;
	array->items     = NULL;
	array->count     = 0;
	array->max       = 0;
	return array;
}



static fct_list_t* fct_array_copy(fct_list_t* array)
{
	if (!array)
		return NULL;
	if (array->count == 0)
		return fct_array_create(array->type);

	fct_list_t* copy
		= fct_array_create(array->type);
	if (!copy)
		return NULL;

	copy->items
		= (void**)malloc(array->count * sizeof(void*));
	if (!copy->items)
	{
		fct_array_delete(copy);
		return NULL;
	}
	copy->max = array->count;

	unsigned i;
	for (i = 0; i < copy->max; i++)
		copy->items[i] = NULL;

	for (i = 0; i < array->max; i++)
	{
		void* item = array->items[i];
		if (!item) continue;

		void* copy_item
			= fct_class_copy(array->type, item);
		if (!copy_item)
		{
			fct_array_delete(copy);
			return NULL;
		}
		copy->items[copy->count++] = copy_item;
	}

	return copy;
}

static void fct_array_delete(fct_list_t* array)
{
	if (!array)
		return;

	unsigned i;
	for (i = 0; i < array->max; i++)
	{
		if (array->items[i])
			fct_class_delete(array->type, array->items[i]);
	}

	free(array);
}



static unsigned fct_array_count(fct_list_t* array)
{
	return (array ? array->count : 0);
}

static const fct_class_t* fct_array_type(fct_list_t* array)
{
	return (array ? array->type : NULL);
}



void* fct_array_index_resolve(
	fct_list_t* array, unsigned index)
{
	if (!array) return NULL;

	if (array->interface != &fct_list_interface_array)
		return NULL;

	if (index >= array->max)
		return NULL;
	return array->items[index];
}

void* fct_array_index_remove(
	fct_list_t* array, unsigned index)
{
	if (array->interface != &fct_list_interface_array)
		return NULL;

	void* item = array->items[index];
	if (item)
	{
		array->items[index] = NULL;
		array->count--;
	}
	return item;
}

bool fct_array_index_delete(
	fct_list_t* array, unsigned index)
{
	if (array->interface != &fct_list_interface_array)
		return false;

	void* item
		= fct_array_index_remove(
			array, index);
	if (!item) return false;

	fct_class_delete(array->type, item);
	return true;
}



bool fct_array_index_add(
	fct_list_t* array, void* item, unsigned* index)
{
	if (!array) return false;

	if (array->interface != &fct_list_interface_array)
		return false;

	if (!item) return false;

	if (array->count < array->max)
	{
		unsigned i;
		for (i = 0; array->items[i]; i++);
		array->items[i] = item;
		array->count++;
		if (index) *index = i;
		return true;
	}

	void** nitems
		= (void**)realloc(array->items,
			(array->max + 1) * sizeof(void*));
	if (!nitems) return false;
	array->items = nitems;
	array->max++;

	array->items[array->count] = item;
	if (index) *index = array->count;
	array->count++;
	return true;
}

static bool fct_array_add(fct_list_t* array, void* item)
{
	return fct_array_index_add(array, item, NULL);
}

static bool fct_array_add_list(fct_list_t* array, fct_list_t* add)
{
	if (!array)
		return false;
	if (fct_list_type(add) != array->type)
		return false;

	unsigned add_count = fct_list_count(add);
	if (add_count == 0) return true;
	unsigned total = (array->count + add_count);
	if (total > array->max)
	{
		void** nitems
			= (void**)realloc(array->items,
				(total * sizeof(void*)));
		if (!nitems) return false;
		unsigned i;
		for (i = array->max; i < total; i++)
			array->items[i] = NULL;
		array->max = total;
	}

	unsigned original_count = array->count;
	unsigned cleanup_index[add_count];
	void*    cleanup_item[add_count];

	unsigned i = 0, j = 0;
	bool add_item(void* item, void* param)
	{
		(void)param;
		if (!item) return true;

		for (; array->items[i]; i++);
		cleanup_index[j] = i;

		if (array->type)
		{
			cleanup_item[j] = fct_class_copy(array->type, item);
			array->items[i] = cleanup_item[j];
			if (!array->items[i]) return false;
		}
		else
		{
			cleanup_item[j] = NULL;
			array->items[i] = item;
		}

		array->count++;
		return true;
	}

	if (!fct_list_foreach(add, NULL, add_item))
	{
		for (i = 0; i < j; i++)
		{
			fct_class_delete(array->type, cleanup_item[i]);
			array->items[cleanup_index[i]] = NULL;
		}
		array->count = original_count;
		return false;
	}

	return true;
}


static bool fct_array_foreach(
	fct_list_t* array, void* param,
	bool (*func)(void* item, void* param))
{
	if (!array || !func)
		return false;

	unsigned i;
	for (i = 0; i < array->max; i++)
	{
		if (!array->items[i])
			continue;
		if (!func(array->items[i], param))
			return false;
	}

	return true;
}



void* fct_array_index_find(
	fct_list_t* array, void* attrib,
	bool (*item_find)(void* item, void* attrib),
	unsigned* index)
{
	if (!array) return NULL;

	if (array->interface != &fct_list_interface_array)
		return NULL;

	if (!item_find) return NULL;

	unsigned i;
	for (i = 0; i < array->max; i++)
	{
		if (!array->items[i])
			continue;
		if (item_find(array->items[i], attrib))
		{
			if (index) *index = i;
			return array->items[i];
		}
	}
	return NULL;
}

static void* fct_array_find(
	fct_list_t* array, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	return fct_array_index_find(
		array, attrib, item_find, NULL);
}

bool fct_array_index_find_by_pointer(
	fct_list_t* array, void* item, unsigned* index)
{
	if (array->interface != &fct_list_interface_array)
		return false;

	bool item_find(void* a, void* b)
	{
		if (!a || !b)
			return false;
		return (a == b);
	}

	return (fct_array_index_find(
		array, item, item_find, index) != NULL);
}

void* fct_array_index_find_remove(
	fct_list_t* array, void* attrib,
	bool (*item_find)(void* item, void* attrib),
	unsigned* index)
{
	if (array->interface != &fct_list_interface_array)
		return NULL;

	unsigned i;
	if (!fct_array_index_find(
		array, attrib, item_find, &i))
		return NULL;
	void* item = fct_array_index_remove(array, i);
	if (!item) return NULL;
	if (index) *index = i;
	return item;
}

static void* fct_array_find_remove(
	fct_list_t* array, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	return fct_array_index_find_remove(
		array, attrib, item_find, NULL);
}

static bool fct_array_find_delete_all(
	fct_list_t* array, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (!array || !item_find
		|| (array->count == 0))
		return false;

	unsigned i, removed = 0;
	for (i = 0; i < array->max; i++)
	{
		if (!array->items[i])
			continue;
		if (item_find(array->items[i], attrib))
		{
			void* item = array->items[i];
			if (item)
			{
				array->items[i] = NULL;
				array->count--;
				fct_class_delete(array->type, item);
				removed++;
			}
		}
	}

	return (removed != 0);
}



bool fct_array_sort(
	fct_list_t* array,
	int (*item_sort)(void* a, void* b))
{
	if (!array)
		return false;
	if (array->interface != &fct_list_interface_array)
		return false;

	if (!item_sort)
		return true;

	unsigned base;
	for (base = 0; base < array->max; base++)
	{
		if (!array->items[base])
			continue;
		unsigned sort;
		for (sort = base + 1; sort < array->max; sort++)
		{
			if (!array->items[sort])
				continue;
			if (item_sort(array->items[base], array->items[sort]) > 0)
			{
				void* swap = array->items[base];
				array->items[base] = array->items[sort];
				array->items[sort] = swap;
			}
		}
	}

	return true;
}
