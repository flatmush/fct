#ifndef __FCT_TYPES_SLIST_H__
#define __FCT_TYPES_SLIST_H__

#include <fct/types.h>

const fct_class_t* fct_class_slist;

fct_list_t* fct_slist_create(
	const fct_class_t* type,
	int (*sort_func)(void* a, void* b));

bool fct_slist_node_merge(
	fct_list_t* list,
	bool  (*find_func )(void* a, void* b),
	void* (*merge_func)(void* a, void* b));

#endif
