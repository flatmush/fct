#include "slist.h"
#include "interface.h"
#include <stdlib.h>


typedef struct fct_slist_node_s fct_slist_node_t;

struct __attribute__((__packed__)) fct_list_s
{
	const fct_list_interface_t* interface;
	struct
	{
		fct_slist_node_t*  base;
		const fct_class_t* type;
		int              (*sort_func)(void* a, void* b);
		unsigned           count;
	};
};

struct fct_slist_node_s
{
	void             *item;
	fct_slist_node_t *prev, *next;
};



static fct_slist_node_t* fct_slist__first(fct_list_t* list)
{
	if (!list || !list->base)
		return NULL;
	fct_slist_node_t* node;
	for(node = list->base; node->prev; node = node->prev);
	return node;
}

/*static fct_slist_node_t* fct_slist__last(fct_list_t* list)
{
	if (!list || !list->base)
		return NULL;
	fct_slist_node_t* node;
	for(node = list->base; node->next; node = node->next);
	return node;
}*/

static fct_slist_node_t* fct_slist__next(fct_slist_node_t* node)
{
	return (node ? node->next : NULL);
}

/*static fct_slist_node_t* fct_slist__prev(fct_slist_node_t* node)
{
	return (node ? node->prev : NULL);
}*/

static void* fct_slist__remove(fct_list_t* list, fct_slist_node_t* node)
{
	if (!list || !node
		|| (list->count == 0))
		return NULL;

	void* item = node->item;

	if (node->prev)
		node->prev->next = node->next;
	if (node->next)
		node->next->prev = node->prev;

	if (list->base == node)
		list->base = (node->prev ? node->prev : node->next);
	list->count--;

	free(node);
	return item;
}



static void          fct_slist_delete(fct_list_t* list);
static fct_list_t*   fct_slist_copy(fct_list_t* list);

static const fct_class_t
	fct_class__slist =
	{
		.delete_cb    = (void*)fct_slist_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_slist_copy,
		.import_cb    = NULL,
		.export_cb    = (void*)fct_list_export,
	};
const fct_class_t* fct_class_slist
	= &fct_class__slist;



static unsigned           fct_slist_count(fct_list_t* slist);
static const fct_class_t* fct_slist_type(fct_list_t* slist);
static bool               fct_slist_add(fct_list_t* slist, void* item);
static bool               fct_slist_add_list(fct_list_t* slist, fct_list_t* add);
static bool               fct_slist_foreach(fct_list_t* slist, void* param, bool (*func)(void* item, void* param));
static void*              fct_slist_find(fct_list_t* slist, void* attrib, bool (*item_find)(void* item, void* attrib));
static void*              fct_slist_find_remove(fct_list_t* slist, void* attrib, bool (*item_find)(void* item, void* attrib));
static bool               fct_slist_find_delete_all(fct_list_t* slist, void* attrib, bool (*item_find)(void* item, void* attrib));

static const fct_list_interface_t
	fct_list_interface_slist =
	{
		.class = &fct_class__slist,

		.count           = fct_slist_count,
		.type            = fct_slist_type,
		.add             = fct_slist_add,
		.add_list        = fct_slist_add_list,
		.foreach         = fct_slist_foreach,
		.find            = fct_slist_find,
		.find_remove     = fct_slist_find_remove,
		.find_delete_all = fct_slist_find_delete_all,
	};



fct_list_t* fct_slist_create(
	const fct_class_t* type,
	int (*sort_func)(void* a, void* b))
{
	fct_list_t* list
		= (fct_list_t*)malloc(
			sizeof(fct_list_t));
	if (!list) return NULL;

	list->interface = &fct_list_interface_slist;
	list->base      = NULL;
	list->type      = type;
	list->sort_func = sort_func;
	list->count     = 0;

	return list;
}



static void fct_slist_delete(fct_list_t* list)
{
	if (!list)
		return;

	fct_slist_node_t* base
		= fct_slist__first(list);
	while (base)
	{
		fct_slist_node_t* next
			= fct_slist__next(base);
		fct_class_delete(list->type, base->item);
		free(base);
		base = next;
	}

	free(list);
}

static fct_list_t* fct_slist_copy(fct_list_t* list)
{
	if (!list)
		return NULL;

	fct_list_t* copy
		= fct_slist_create(list->type, list->sort_func);
	if (!copy) return NULL;

	fct_slist_node_t* base;
	for (base = fct_slist__first(list);
		base; base = fct_slist__next(base))
	{
		void* item
			= fct_class_copy(list->type, base->item);
		if (!item)
		{
			fct_slist_delete(copy);
			return NULL;
		}
		if (!fct_slist_add(copy, item))
		{
			fct_class_delete(list->type, item);
			fct_slist_delete(copy);
			return NULL;
		}
	}

	return copy;
}



static unsigned fct_slist_count(fct_list_t* list)
{
	if (!list)
		return 0;
	return list->count;
}

static const fct_class_t* fct_slist_type(fct_list_t* list)
{
	if (!list)
		return NULL;
	return list->type;
}



static fct_slist_node_t* fct_slist__node_add(
	fct_list_t* list, void* item)
{
	if (!list || ((list->count + 1) == 0))
		return NULL;

	fct_slist_node_t* add
		= (fct_slist_node_t*)malloc(
			sizeof(fct_slist_node_t));
	if (!add) return NULL;

	add->item = item;
	add->prev = NULL;
	add->next = NULL;

	fct_slist_node_t* node
		= fct_slist__first(list);
	while (node)
	{
		int s = 0;
		if (list->sort_func)
			s = list->sort_func(node->item, item);
		if (s <= 0)
		{
			add->prev = node->prev;
			add->next = node;
			if (node->prev)
				node->prev->next = add;
			node->prev = add;
			if (list->base == node)
				list->base = add;
			list->count++;
			return add;
		}

		fct_slist_node_t* next
			= fct_slist__next(node);
		if (!next)
		{
			add->prev = node;
			node->next = add;
			list->count++;
			return add;
		}

		node = next;
	}

	list->base = add;
	list->count++;
	return add;
}

static bool fct_slist_add(fct_list_t* list, void* item)
{
	return (fct_slist__node_add(list, item) != NULL);
}

static bool fct_slist_add_list(fct_list_t* list, fct_list_t* add)
{
	if (!list || !add)
		return false;

	if (list->type != fct_list_type(add))
		return false;

	unsigned count = fct_list_count(add);
	if (count == 0) return true;

	fct_slist_node_t* cleanup[count];
	unsigned i;
	for (i = 0; i < count; i++)
		cleanup[i] = NULL;
	i = 0;

	bool add_list(void* litem, void* params)
	{
		(void)params;
		if (!litem)
			return true;

		void* item = fct_class_copy(list->type, litem);
		cleanup[i] = fct_slist__node_add(list, item);
		if (!item || !cleanup[i])
		{
			fct_class_delete(list->type, item);
			unsigned j;
			for (j = 0; j < i; j++)
				fct_slist__remove(list, cleanup[j]);
			return false;
		}
		i++;

		return true;
	}

	return fct_list_foreach(add, NULL, add_list);

	return true;
}



static bool fct_slist_foreach(
	fct_list_t* list, void* param,
	bool (*func)(void* item, void* param))
{
	if (!list)
		return false;

	fct_slist_node_t* node;
	for (node = fct_slist__first(list);
		node; node = fct_slist__next(node))
	{
		if (!func(node->item, param))
			return false;
	}

	return true;
}



static void* fct_slist_find(
	fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib))
{
	if (!list || !item_find)
		return NULL;

	fct_slist_node_t* node;
	for (node = fct_slist__first(list);
		node; node = fct_slist__next(node))
	{
		if (item_find(node->item, attrib))
			return node->item;
	}

	return NULL;
}

static void* fct_slist_find_remove(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (!list || !item_find)
		return NULL;

	fct_slist_node_t* node;
	for (node = fct_slist__first(list);
		node; fct_slist__next(node))
	{
		if (item_find(node->item, attrib))
			return fct_slist__remove(list, node);
	}

	return NULL;
}

static bool fct_slist_find_delete_all(
	fct_list_t* list, void* attrib,
	bool (*item_find)(void* item, void* attrib))
{
	if (!list || !item_find)
		return NULL;

	unsigned removed = 0;
	fct_slist_node_t* node
		= fct_slist__first(list);
	while (node)
	{
		fct_slist_node_t* next
			= fct_slist__next(node);
		if (item_find(node->item, attrib))
		{
			void* item = fct_slist__remove(list, node);
			fct_class_delete(list->type, item);
			removed++;
		}
		node = next;
	}

	return (removed != 0);
}



bool fct_slist_node_merge(
	fct_list_t* list,
	bool  (*find_func )(void* a, void* b),
	void* (*merge_func)(void* a, void* b))
{
	if (!list
		|| !find_func
		|| !merge_func)
		return false;

	if (list->interface != &fct_list_interface_slist)
		return false;

	fct_slist_node_t* node
		= fct_slist__first(list);
	while (node)
	{
		fct_slist_node_t* next
			= fct_slist__next(node);

		if (next && find_func(
			node->item, next->item))
		{
			void* item = merge_func(
				node->item, next->item);
			if (item && fct_slist_add(list, item))
			{
				void* a = fct_slist__remove(list, node);
				void* b = fct_slist__remove(list, next);
				fct_class_delete(list->type, a);
				fct_class_delete(list->type, b);
				next = fct_slist__first(list);
			}
			else
			{
				fct_class_delete(list->type, item);
			}
		}

		node = next;
	}

	return true;
}
