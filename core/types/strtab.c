#include "strtab.h"



fct_list_t* fct_strtab_create(void)
{
	return fct_array_create(fct_class_string);
}



bool fct_strtab_add(
	fct_list_t* array, fct_string_t* string,
	unsigned* index)
{
	if (!string)
	{
		if (index) *index = 0;
		return true;
	}

	if (fct_strtab_find(
		array, string, index))
		return true;

	fct_string_t* rs
		= fct_string_reference(string);
	if (!rs) return false;

	unsigned i;
	if (!fct_array_index_add(
		array, rs, &i))
	{
		fct_string_delete(rs);
		return false;
	}

	if (index) *index = (i + 1);
	return true;
}



fct_string_t* fct_strtab_find(
	fct_list_t* array, fct_string_t* string,
	unsigned* index)
{
	fct_string_t* s;
	unsigned i;
	s = fct_array_index_find(array, string,
		(void*)fct_string_compare, &i);
	if (index) *index = (i + 1);
	return s;
}
