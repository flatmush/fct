#include "keyval.h"



bool fct_keyval_list_resolve(fct_keyval_t* list, const char* key, uint32_t* val)
{
	fct_keyval_t* entry;
	for (entry = list; entry->key; entry++)
	{
		if (strcmp(key, entry->key) == 0)
		{
			if (val)
				*val = entry->val;
			return true;
		}
	}
	return false;
}

bool fct_keyval_list_resolven(fct_keyval_t* list, const char* key, unsigned keylen, uint32_t* val)
{
	fct_keyval_t* entry;
	for (entry = list; entry->key; entry++)
	{
		if (strlen(entry->key) != keylen)
			continue;
		if (strncmp(key, entry->key, keylen) == 0)
		{
			if (val)
				*val = entry->val;
			return true;
		}
	}
	return false;
}

bool fct_keyval_list_resolves(fct_keyval_t* list, fct_string_t* key, uint32_t* val)
{
	if (!key) return false;
	return fct_keyval_list_resolven(list, key->data, key->size, val);
}
