#ifndef __FCT_TYPES_CLASS_H__
#define __FCT_TYPES_CLASS_H__

#include <stdio.h>
#include <stdbool.h>

typedef struct fct_class_s fct_class_t;

#include <fct/types/buffer.h>
#include <fct/types/list.h>

struct fct_class_s
{
	void  (*delete_cb)(void* object);
	void* (*reference_cb)(void* object);
	void* (*copy_cb)(void* object);

	void*         (*import_cb)(fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
	fct_buffer_t* (*export_cb)(void* object, fct_list_t* strtab);
};

void* fct_class_copy(const fct_class_t* class, void* object);
void* fct_class_reference(const fct_class_t* class, void* object);
void  fct_class_delete(const fct_class_t* class, void* object);

void* fct_class_import(const fct_class_t* class,
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_class_export(const fct_class_t*
	class, void* object, fct_list_t* strtab);

#endif
