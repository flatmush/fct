#ifndef __FCT_TYPES_LIST_H__
#define __FCT_TYPES_LIST_H__

typedef struct fct_list_s fct_list_t;

#include <fct/types.h>

const fct_class_t* fct_class_list;

fct_list_t* fct_list_copy(fct_list_t* list);
void        fct_list_delete(fct_list_t* list);

bool fct_list_import_named(
	fct_list_t* list, const char* name,
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
bool  fct_list_import(
	fct_list_t* list,
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);

fct_buffer_t* fct_list_export_named(
	const char* name,
	fct_list_t* list, fct_list_t* strtab);
fct_buffer_t* fct_list_export(
	fct_list_t* list, fct_list_t* strtab);

fct_string_t* fct_list_dump(
	fct_list_t* list, unsigned indent,
	fct_string_t* (*dump_func)(void* item, unsigned indent));

unsigned           fct_list_count(fct_list_t* list);
const fct_class_t* fct_list_type(fct_list_t* list);

bool fct_list_add(fct_list_t* list, void* item);
bool fct_list_add_list(fct_list_t* list, fct_list_t* add);

bool fct_list_foreach(fct_list_t* list, void* param, bool (*func)(void* item, void* param));

void* fct_list_find(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));
void* fct_list_find_remove(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));
bool  fct_list_find_delete(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));
bool  fct_list_find_delete_all(fct_list_t* list, void* attrib, bool (*item_find)(void* item, void* attrib));

#include <fct/types/list/array.h>
#include <fct/types/list/slist.h>
#include <fct/types/list/hashtab.h>

bool fct_list_contains(fct_list_t* list, void* item);

#endif
