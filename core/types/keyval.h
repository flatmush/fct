#ifndef __FCT_KEYVAL_H__
#define __FCT_KEYVAL_H__

typedef struct fct_keyval_s fct_keyval_t;

#include <stdint.h>
#include <stdbool.h>
#include <fct/types/string.h>

struct fct_keyval_s
{
	const char* key;
	uint32_t    val;
};

bool fct_keyval_list_resolve(fct_keyval_t* list, const char* key, uint32_t* val);
bool fct_keyval_list_resolven(fct_keyval_t* list, const char* key, unsigned keylen, uint32_t* val);
bool fct_keyval_list_resolves(fct_keyval_t* list, fct_string_t* key, uint32_t* val);

#endif
