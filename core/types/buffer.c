#include "buffer.h"
#include <stdlib.h>
#include <stdio.h>



struct fct_buffer_s
{
	uint8_t*         data;
	uintptr_t        size;
	riff64_header_t* riff;
	fct_buffer_t*    parent;
	unsigned         refcnt;
};



static const fct_class_t
	fct_class__buffer =
	{
		.delete_cb    = (void*)fct_buffer_delete,
		.reference_cb = (void*)fct_buffer_reference,
		.copy_cb      = (void*)fct_buffer_copy,
		.import_cb    = NULL,
		.export_cb    = NULL,
	};
const fct_class_t* fct_class_buffer
	= &fct_class__buffer;



fct_buffer_t* fct_buffer_create()
{
	fct_buffer_t* buffer
		= (fct_buffer_t*)malloc(
			sizeof(fct_buffer_t));
	if (!buffer) return NULL;

	buffer->data   = NULL;
	buffer->size   = 0;
	buffer->riff   = NULL;
	buffer->parent = NULL;
	buffer->refcnt = 0;
	return buffer;
}

fct_buffer_t* fct_buffer_create_riff64(const char* title)
{
	if (!title)
		return NULL;

	fct_buffer_t* buffer
		= fct_buffer_create();
	if (!buffer) return NULL;

	if (!fct_buffer_append(
		buffer, NULL, sizeof(riff64_header_t)))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	buffer->riff = (riff64_header_t*)buffer->data;
	strncpy(buffer->riff->title, title, 8);
	return buffer;
}

fct_buffer_t* fct_buffer_reference(fct_buffer_t* buffer)
{
	if (!buffer)
		return NULL;

	if ((buffer->refcnt + 1) == 0)
		return fct_buffer_copy(buffer);

	buffer->refcnt++;
	return buffer;
}

fct_buffer_t* fct_buffer_copy(fct_buffer_t* buffer)
{
	if (!buffer)
		return NULL;
	fct_buffer_t* copy
		= fct_buffer_create();
	if (!copy) return NULL;

	if (!fct_buffer_append_buffer(copy, buffer))
	{
		fct_buffer_delete(copy);
		return NULL;
	}

	if (buffer->riff)
		copy->riff = (riff64_header_t*)copy->data;

	return copy;
}

void fct_buffer_delete(fct_buffer_t* buffer)
{
	if (!buffer)
		return;

	if (buffer->refcnt)
	{
		buffer->refcnt--;
		return;
	}

	if (buffer->parent)
		fct_buffer_delete(buffer->parent);
	else
		free(buffer->data);

	free(buffer);
}



fct_buffer_t* fct_buffer_file_import(const char* path)
{
	FILE* fp = fopen(path, "rb");
	if (!fp) return NULL;

	long size;
	if ((fseek(fp, 0, SEEK_END) != 0)
		|| ((size = ftell(fp)) < 0)
		|| (fseek(fp, 0, SEEK_SET) != 0))
	{
		fclose(fp);
		return NULL;
	}

	fct_buffer_t* buffer
		= fct_buffer_create();
	if (!buffer
		|| !fct_buffer_append(buffer, NULL, (uintptr_t)size)
		|| (fread(buffer->data, buffer->size, 1, fp) != 1))
	{
		fct_buffer_delete(buffer);
		fclose(fp);
		return NULL;
	}

	fclose(fp);
	return buffer;
}

bool fct_buffer_file_export(fct_buffer_t* buffer, const char* path)
{
	if (!buffer)
		return false;

	FILE* fp = fopen(path, "wb");
	if (!fp) return false;
	bool success = (fwrite(buffer->data, buffer->size, 1, fp) == 1);
	fclose(fp);
	return success;
}


uintptr_t fct_buffer_size_get(fct_buffer_t* buffer)
{
	if (!buffer)
		return 0;

	return buffer->size;
}

const void* fct_buffer_data_get(fct_buffer_t* buffer)
{
	if (!buffer)
		return NULL;

	return (const void*)buffer->data;
}



bool fct_buffer_append(fct_buffer_t* buffer, void* data, uintptr_t size)
{
	if (size == 0)
		return true;
	if (!buffer || buffer->parent)
		return false;

	uint8_t* ndata = realloc(
		buffer->data, (buffer->size + size));
	if (!ndata) return false;
	buffer->data = ndata;

	if (data)
		memcpy(&buffer->data[buffer->size], data, size);
	else
		memset(&buffer->data[buffer->size], 0x00, size);

	buffer->size += size;

	if (buffer->riff)
	{
		buffer->riff = (riff64_header_t*)buffer->data;
		uint64_t rsize = buffer->size
			- sizeof(riff64_header_t);
		memcpy(&buffer->riff->size,
			&rsize, sizeof(rsize));
	}

	return true;
}

bool fct_buffer_append_buffer(fct_buffer_t* buffer, fct_buffer_t* append)
{
	if (!buffer || !append)
		return false;
	return fct_buffer_append(buffer, append->data, append->size);
}

bool fct_buffer_modify(fct_buffer_t* buffer,
	uintptr_t offset, void* data, uintptr_t size)
{
	if (!buffer || buffer->parent)
		return false;

	if (buffer->riff && (offset < sizeof(riff64_header_t)))
		return false;

	if ((offset + size) > buffer->size)
		return false;

	if (data)
		memcpy(&buffer->data[offset], data, size);
	else
		memset(&buffer->data[offset], 0x00, size);
	return true;
}

bool fct_buffer_read(fct_buffer_t* buffer,
	uintptr_t offset, uintptr_t size, void* data)
{
	if (!buffer)
		return false;

	if ((offset + size) > buffer->size)
		return false;

	if (data)
		memcpy(data, &buffer->data[offset], size);
	return true;
}


fct_buffer_t* fct_buffer_subsection(
	fct_buffer_t* buffer, uintptr_t offset, uintptr_t size)
{
	if (!buffer)
		return NULL;

	if ((offset + size) > buffer->size)
		return NULL;

	fct_buffer_t* subsection
		= fct_buffer_create();
	if (!subsection) return NULL;

	subsection->parent = fct_buffer_reference(buffer);
	if (!subsection->parent)
	{
		fct_buffer_delete(subsection);
		return NULL;
	}

	subsection->data = &buffer->data[offset];
	subsection->size = size;
	return subsection;
}

fct_buffer_t* fct_buffer_subsection_riff64(
	fct_buffer_t* buffer, uintptr_t offset, const char* title)
{
	size_t tlen = strlen(title);
	if (tlen > 8) return NULL;

	riff64_header_t header;
	if (!fct_buffer_read(buffer,
		offset, sizeof(header), &header))
		return NULL;

	char expected[8];
	memset(&expected, 0x00, 8);
	memcpy(&expected, title, tlen);

	if (memcmp(header.title, expected, 8) != 0)
		return NULL;

	uintptr_t rsize = header.size;
	if ((uint64_t)rsize != header.size)
		return NULL;

	return fct_buffer_subsection(
		buffer, (offset + sizeof(header)), rsize);
}
