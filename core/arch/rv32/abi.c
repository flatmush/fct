#include <fct/debug.h>
#include <fct/asm/context.h>
#include <fct/arch/arch.h>
#include <fct/object/format/elf.h>
#include "rv32.h"


#define RV32_ABI_REG_RA 1
#define RV32_ABI_REG_SP 2


static bool rv32__gen_call(
	fct_asm_context_t* context, fct_asm_field_t* field)
{
	if (!field)
		return false;

	fct_asm_field_t* ra
		= fct_asm_field_create_register(
			RV32_ABI_REG_RA, NULL);
	if (!ra) return false;

	fct_asm_field_t* f[2] = { ra, field };

	fct_tok_t mnem =
	{
		.type = FCT_TOK_IDENT,
		.base = "ljal",
		.size = 4,
		.file = field->file,
		.row  = field->row,
		.col  = field->col,
	};

	bool success = rv32_assemble(
		context, &mnem, NULL, f, 2);
	fct_asm_field_delete(ra);
	return success;
}

static bool rv32__gen_return(fct_asm_context_t* context)
{
	fct_asm_field_t* ra
		= fct_asm_field_create_register(
			RV32_ABI_REG_RA, NULL);
	if (!ra) return false;

	uint64_t uoff = 0;
	fct_asm_field_t* off
		= fct_asm_field_create_immediate(
			12, 1, &uoff, true);
	if (!off)
	{
		fct_asm_field_delete(ra);
		return false;
	}

	fct_tok_t mnem =
	{
		.type = FCT_TOK_IDENT,
		.base = "jr",
		.size = 2,
		.file = NULL,
		.row  = 0,
		.col  = 0,
	};

	fct_asm_field_t* f[2] = { ra, off };

	bool success = rv32_assemble(
		context, &mnem, NULL, f, 2);
	fct_asm_field_delete(off);
	fct_asm_field_delete(ra);
	return success;
}

static bool rv32__gen_push(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (field_count == 0)
		return true;
	if (!field || (field_count >= 512))
		return false;

	fct_asm_field_t* sp
		= fct_asm_field_create_register(
			RV32_ABI_REG_SP, NULL);
	if (!sp) return false;

	{
		uint64_t usize = (field_count * 4);
		usize = ~usize + 1;
		fct_asm_field_t* size
			= fct_asm_field_create_immediate(
				12, 1, &usize, true);
		if (!size)
		{
			fct_asm_field_delete(sp);
			return false;
		}

		fct_tok_t mnem =
		{
			.type = FCT_TOK_IDENT,
			.base = "addi",
			.size = 4,
			.file = field[0]->file,
			.row  = field[0]->row,
			.col  = field[0]->col,
		};	

		fct_asm_field_t* f[3] = { sp, sp, size };

		bool success = rv32_assemble(
			context, &mnem, NULL, f, 3);
		fct_asm_field_delete(size);
		if (!success)
		{
			fct_asm_field_delete(sp);
			return false;
		}
	}

	unsigned i, o;
	for (i = 0, o = (field_count - 1);
		i < field_count; i++, o--)
	{
		if (!field[i])
		{
			fct_asm_field_delete(sp);
			return false;
		}

		if ((field[i]->type == FCT_ASM_FIELD_TYPE_REGISTER)
			&& (field[i]->reg.reg == RV32_ABI_REG_SP))
		{
			/* TODO - Make failures atomic. */
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Can't push stack pointer onto the stack.");
			fct_asm_field_delete(sp);
			return false;
		}

		uint64_t uoffset = (o * 4);
		fct_asm_field_t* offset
			= fct_asm_field_create_immediate(
				12, 1, &uoffset, true);
		if (!offset)
		{
			fct_asm_field_delete(sp);
			return false;
		}		

		fct_tok_t mnem =
		{
			.type = FCT_TOK_IDENT,
			.base = "sw",
			.size = 2,
			.file = field[i]->file,
			.row  = field[i]->row,
			.col  = field[i]->col,
		};	

		fct_asm_field_t* f[3] = { field[i], sp, offset };

		bool success = rv32_assemble(
			context, &mnem, NULL, f, 3);
		fct_asm_field_delete(offset);
		if (!success)
		{
			fct_asm_field_delete(sp);
			return false;
		}
	}

	fct_asm_field_delete(sp);
	return true;
}

static bool rv32__gen_pop(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (field_count == 0)
		return true;
	if (!field || (field_count >= 512))
		return false;

	fct_asm_field_t* sp
		= fct_asm_field_create_register(
			RV32_ABI_REG_SP, NULL);
	if (!sp) return false;

	unsigned i;
	for (i = 0; i < field_count; i++)
	{
		if (!field[i])
		{
			fct_asm_field_delete(sp);
			return false;
		}

		/* TODO - Make failures atomic. */
		if (field[i]->type == FCT_ASM_FIELD_TYPE_REGISTER)
		{
			if (field[i]->reg.reg == RV32_ABI_REG_SP)
			{
				FCT_DEBUG_ERROR_FIELD(field[i],
					"Can't pop stack pointer off the stack.");
				fct_asm_field_delete(sp);
				return false;
			}
		}
		else
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Only registers may be popped off the stack.");
			fct_asm_field_delete(sp);
			return false;
		}

		uint64_t uoffset = (i * 4);
		fct_asm_field_t* offset
			= fct_asm_field_create_immediate(
				12, 1, &uoffset, true);
		if (!offset)
		{
			fct_asm_field_delete(sp);
			return false;
		}		

		fct_tok_t mnem =
		{
			.type = FCT_TOK_IDENT,
			.base = "lw",
			.size = 2,
			.file = field[i]->file,
			.row  = field[i]->row,
			.col  = field[i]->col,
		};	

		fct_asm_field_t* f[3] = { field[i], sp, offset };

		bool success = rv32_assemble(
			context, &mnem, NULL, f, 3);
		fct_asm_field_delete(offset);
		if (!success)
		{
			fct_asm_field_delete(sp);
			return false;
		}
	}

	bool success;
	{
		uint64_t usize = (field_count * 4);
		fct_asm_field_t* size
			= fct_asm_field_create_immediate(
				12, 1, &usize, true);
		if (!size)
		{
			fct_asm_field_delete(sp);
			return false;
		}

		fct_tok_t mnem =
		{
			.type = FCT_TOK_IDENT,
			.base = "addi",
			.size = 4,
			.file = field[0]->file,
			.row  = field[0]->row,
			.col  = field[0]->col,
		};

		fct_asm_field_t* f[3] = { sp, sp, size };

		success = rv32_assemble(
			context, &mnem, NULL, f, 3);
		fct_asm_field_delete(size);
	}

	fct_asm_field_delete(sp);
	return success;
}



static fct_keyval_t fct_arch_rv32__rvg_reg_names[] =
{
	{ "ra" ,  RV32_ABI_REG_RA },
	{ "sp" ,  RV32_ABI_REG_SP },
	{ "gp" ,  3 },
	{ "tp" ,  4 },
	{ "t0" ,  5 }, { "t1" ,  6 }, { "t2" ,  7 },
	{ "s0" ,  8 }, { "fp" ,  8 }, { "s1" ,  9 },
	{ "a0" , 10 }, { "a1" , 11 }, { "a2" , 12 }, { "a3" , 13 },
	{ "a4" , 14 }, { "a5" , 15 }, { "a6" , 16 }, { "a7" , 17 },
	{ "s2" , 18 }, { "s3" , 19 }, { "s4" , 20 }, { "s5" , 21 },
	{ "s6" , 22 }, { "s7" , 23 }, { "s8" , 24 }, { "s9" , 25 },
	{ "s10", 26 }, { "s11", 27 },
	{ "t3" , 28 }, { "t4" , 29 }, { "t5" , 30 }, { "t6" , 31 },
	{ NULL, 0 }
};

fct_arch_t fct_arch_rv32__rvg =
{
	.name      = "rvg",
	.base      = FCT_ARCH_RV32_STATIC,
	.spec      = NULL,
	.reg_names = fct_arch_rv32__rvg_reg_names,

	.elf_osabi      = ELF_OSABI_NONE,
	.elf_abiversion = 0,

	.gen_call    = rv32__gen_call,
	.gen_return  = rv32__gen_return,
	.gen_push    = rv32__gen_push,
	.gen_pop     = rv32__gen_pop,
	.gen_syscall = NULL,
};

fct_arch_t* fct_arch_rv32_rvg
	= &fct_arch_rv32__rvg;

