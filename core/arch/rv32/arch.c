#include <fct/arch/arch.h>
#include <fct/arch/emu.h>
#include <fct/object/format/elf.h>
#include "rv32.h"


static fct_arch_spec_t fct_arch_spec__rv32 =
{
	.mau       =  8,
	.addr_size =  4,
	.word_size =  4,
	.inst_size =  4,

	.reg_count = 32,

	.interrupt_count = 0,

	.elf_machine = ELF_MACHINE_RISCV,
};

static fct_keyval_t fct_arch__rv32_reg_names[] =
{
	{ "zero", RV32_REG_ZERO },
	{ NULL, 0 }
};

fct_arch_emu_t fct_arch_emu__rv32;

fct_arch_t fct_arch__rv32 =
{
	.name = "rv32",
	.base = NULL,
	.emu  = &fct_arch_emu__rv32,
	.spec = &fct_arch_spec__rv32,

	.reg_names = fct_arch__rv32_reg_names,

	.elf_osabi      = ELF_OSABI_STANDALONE,
	.elf_abiversion = 0,

	.gen_call    = NULL,
	.gen_return  = NULL,
	.gen_push    = NULL,
	.gen_pop     = NULL,
	.gen_syscall = NULL,

	.assemble    =        rv32_assemble,
	.disassemble =        NULL,
	.link        = (void*)rv32_link,
};

fct_arch_t* fct_arch_rv32
	= &fct_arch__rv32;
