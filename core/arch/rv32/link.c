#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "rv32.h"


bool rv32_link(
	rv32_instruction_t* code,
	uint64_t size, uint64_t symbol)
{
	if (!code)
		return false;

	if (((symbol >> 32) != 0)
		&& ((symbol >> 32) != 0xFFFFFFFF))
		return false;
	symbol &= 0xFFFFFFFF;

	if (size == 4)
	{
		if (code[0].r.reserved != 0b11)
			return false;

		if (code[0].uj.opcode == RV32_OPCODE_JAL)
		{
			if (symbol & 1)
				return false;

			if (((symbol >> 21) != 0)
				&& ((symbol >> 21) != 0x000007FF))
				return false;

			code[0].uj.imm_19_12 = (symbol >> 12) & 0x0FF;
			code[0].uj.imm_11    = (symbol >> 11) & 0x001;
			code[0].uj.imm_10_1  = (symbol >>  1) & 0x3FF;
			code[0].uj.imm_20    = (symbol >> 20) & 0x001;
			return true;
		}
		else if (code[0].sb.opcode == RV32_OPCODE_BRANCH)
		{
			if (symbol & 1)
				return false;

			if (((symbol >> 13) != 0)
				&& ((symbol >> 13) != 0x0007FFFF))
				return false;

			code[0].sb.imm_11   = (symbol >> 11) & 0x01;
			code[0].sb.imm_4_1  = (symbol >>  1) & 0x0F;
			code[0].sb.imm_10_5 = (symbol >>  5) & 0x3F;
			code[0].sb.imm_12   = (symbol >> 12) & 0x01;
			return true;
		}
		else if ((code[0].u.opcode == RV32_OPCODE_LUI)
			|| (code[0].u.opcode == RV32_OPCODE_AUIPC))
		{
			if (symbol & 0xFFF)
				return false;
			code[0].u.imm = (symbol >> 12);
			return true;
		}
		else if ((code[0].i.opcode == RV32_OPCODE_OP_IMM)
			|| (code[0].i.opcode == RV32_OPCODE_JALR)
			|| (code[0].i.opcode == RV32_OPCODE_LOAD)
			|| (code[0].i.opcode == RV32_OPCODE_STORE))
		{
			if (((symbol >> 12) != 0)
				&& ((symbol >> 12) != 0x000FFFFF))
				return false;
			code[0].i.imm = (symbol & 0x00000FFF);
			return true;
		}
	}
	else if (size == 8)
	{
		if ((code[0].u.reserved != 0b11)
			|| (code[1].u.reserved != 0b11))
			return false;

		if ((code[0].u.opcode != RV32_OPCODE_LUI)
			&& (code[0].u.opcode != RV32_OPCODE_AUIPC))
			return false;

		if (code[0].u.rd == RV32_REG_ZERO)
			return false;

		uint32_t hi, lo;
		if (code[1].i.opcode == RV32_OPCODE_OP_IMM)
		{
			switch (code[1].i.funct3)
			{
				case RV32_F3_ADDI:
					hi = (symbol >> 12);
					lo = (symbol & 0xFFF);
					if (symbol & 0x800)
						hi += 1;
					break;

				case RV32_F3_XORI:
					if (symbol & 0x800)
					{
						hi = (~symbol >> 12);
						lo = symbol & 0xFFF;
					}
					else
					{
						hi = symbol >> 12;
						lo = symbol & 0xFFF;
					}
					break;

				case RV32_F3_ORI:
					if ((symbol & 0x800)
						&& ((symbol >> 12) != 0x000FFFFF))
						return false;
					hi = symbol >> 12;
					lo = symbol & 0xFFF;
					break;

				default:
					return false;
			}
		}
		else if ((code[1].i.opcode == RV32_OPCODE_JALR)
			|| (code[1].i.opcode == RV32_OPCODE_LOAD)
			|| (code[1].i.opcode == RV32_OPCODE_STORE))
		{
			hi = (symbol >> 12);
			lo = (symbol & 0xFFF);
			if (symbol & 0x800)
				hi += 1;
		}
		else
		{
			return false;
		}

		if ((code[1].i.rd != code[0].u.rd)
			|| (code[1].i.rs1 != code[1].i.rd))
			return false;

		code[0].u.imm = hi;
		code[1].i.imm = lo;
		return true;
	}

	return false;
}
