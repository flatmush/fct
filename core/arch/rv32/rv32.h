#ifndef __FCT_ARCH_RV32_H__
#define __FCT_ARCH_RV32_H__

#include <fct/arch/arch.h>


typedef union __attribute__((__packed__))
{
	struct __attribute__((__packed__))
	{
		unsigned reserved : 2;
		unsigned opcode   : 5;
		unsigned rd       : 5;
		unsigned funct3   : 3;
		unsigned rs1      : 5;
		unsigned rs2      : 5;
		unsigned funct7   : 7;
	} r;
	struct __attribute__((__packed__))
	{
		unsigned reserved :  2;
		unsigned opcode   :  5;
		unsigned rd       :  5;
		unsigned funct3   :  3;
		unsigned rs1      :  5;
		unsigned imm      : 12;
	} i;
	struct __attribute__((__packed__))
	{
		unsigned reserved : 2;
		unsigned opcode   : 5;
		unsigned imm_4_0  : 5;
		unsigned funct3   : 3;
		unsigned rs1      : 5;
		unsigned rs2      : 5;
		unsigned imm_11_5 : 7;
	} s;
	struct __attribute__((__packed__))
	{
		unsigned reserved : 2;
		unsigned opcode   : 5;
		unsigned imm_11   : 1;
		unsigned imm_4_1  : 4;
		unsigned funct3   : 3;
		unsigned rs1      : 5;
		unsigned rs2      : 5;
		unsigned imm_10_5 : 6;
		unsigned imm_12   : 1;
	} sb;
	struct __attribute__((__packed__))
	{
		unsigned reserved :  2;
		unsigned opcode   :  5;
		unsigned rd       :  5;
		unsigned imm      : 20;
	} u;
	struct __attribute__((__packed__))
	{
		unsigned reserved  :  2;
		unsigned opcode    :  5;
		unsigned rd        :  5;
		unsigned imm_19_12 :  8;
		unsigned imm_11    :  1;
		unsigned imm_10_1  : 10;
		unsigned imm_20    :  1;
	} uj;
	uint32_t mask;
} rv32_instruction_t;

typedef enum
{
	RV32_REG_ZERO =  0,
} rv32_reg_e;

typedef enum
{
	RV32_OPCODE_LOAD      = 0b00000,
	RV32_OPCODE_LOAD_FP   = 0b00001,
	RV32_OPCODE_CUSTOM_0  = 0b00010,
	RV32_OPCODE_MISC_MEM  = 0b00011,
	RV32_OPCODE_OP_IMM    = 0b00100,
	RV32_OPCODE_AUIPC     = 0b00101,
	RV32_OPCODE_OP_IMM_32 = 0b00110,
	RV32_OPCODE_INST48_0  = 0b00111,
	RV32_OPCODE_STORE     = 0b01000,
	RV32_OPCODE_STORE_FP  = 0b01001,
	RV32_OPCODE_CUSTOM_1  = 0b01010,
	RV32_OPCODE_AMO       = 0b01011,
	RV32_OPCODE_OP        = 0b01100,
	RV32_OPCODE_LUI       = 0b01101,
	RV32_OPCODE_OP_32     = 0b01110,
	RV32_OPCODE_INST64    = 0b01111,
	RV32_OPCODE_MADD      = 0b10000,
	RV32_OPCODE_MSUB      = 0b10001,
	RV32_OPCODE_NMSUB     = 0b10010,
	RV32_OPCODE_NMADD     = 0b10011,
	RV32_OPCODE_OP_FP     = 0b10100,
	RV32_OPCODE_RESERVED0 = 0b10101,
	RV32_OPCODE_CUSTOM_2  = 0b10110,
	RV32_OPCODE_INST48_1  = 0b10111,
	RV32_OPCODE_BRANCH    = 0b11000,
	RV32_OPCODE_JALR      = 0b11001,
	RV32_OPCODE_RESERVED1 = 0b11010,
	RV32_OPCODE_JAL       = 0b11011,
	RV32_OPCODE_SYSTEM    = 0b11100,
	RV32_OPCODE_RESERVED2 = 0b11101,
	RV32_OPCODE_CUSTOM_3  = 0b11110,
	RV32_OPCODE_INST80P   = 0b11111,
} rv32_opcode_e;

typedef enum
{
	RV32_F3_JALR = 0,

	RV32_F3_BEQ  = 0,
	RV32_F3_BNE  = 1,
	RV32_F3_BLT  = 4,
	RV32_F3_BGE  = 5,
	RV32_F3_BLTU = 6,
	RV32_F3_BGEU = 7,

	RV32_F3_LB  = 0,
	RV32_F3_LH  = 1,
	RV32_F3_LW  = 2,
	RV32_F3_LBU = 4,
	RV32_F3_LHU = 5,

	RV32_F3_SB = 0,
	RV32_F3_SH = 1,
	RV32_F3_SW = 2,

	RV32_F3_ADDI  = 0,
	RV32_F3_SLLI  = 1,
	RV32_F3_SLTI  = 2,
	RV32_F3_SLTIU = 3,
	RV32_F3_XORI  = 4,
	RV32_F3_SRLI  = 5,
	RV32_F3_SRAI  = 5,
	RV32_F3_ORI   = 6,
	RV32_F3_ANDI  = 7,

	RV32_F3_ADD  = 0,
	RV32_F3_SUB  = 0,
	RV32_F3_SLL  = 1,
	RV32_F3_SLT  = 2,
	RV32_F3_SLTU = 3,
	RV32_F3_XOR  = 4,
	RV32_F3_SRL  = 5,
	RV32_F3_SRA  = 5,
	RV32_F3_OR   = 6,
	RV32_F3_AND  = 7,

	RV32_F3_FENCE   = 0,
	RV32_F3_FENCE_I = 1,
	RV32_F3_ECALL   = 0,
	RV32_F3_EBREAK  = 0,
	RV32_F3_CSRRW   = 1,
	RV32_F3_CSRRS   = 2,
	RV32_F3_CSRRC   = 3,
	RV32_F3_CSRRWI  = 5,
	RV32_F3_CSRRSI  = 6,
	RV32_F3_CSRRCI  = 7,

	RV32M_F3_MUL    = 0,
	RV32M_F3_MULH   = 1,
	RV32M_F3_MULHSU = 2,
	RV32M_F3_MULHU  = 3,
	RV32M_F3_DIV    = 4,
	RV32M_F3_DIVU   = 5,
	RV32M_F3_REM    = 6,
	RV32M_F3_REMU   = 7,
} rv32_funct3_e;


bool rv32_gen_call(
	fct_asm_context_t* context, fct_asm_field_t* field);
bool rv32_gen_return(fct_asm_context_t* context);
bool rv32_gen_syscall(
	fct_asm_context_t* context, fct_asm_field_t* field);

bool rv32_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count);

bool rv32_link(
	rv32_instruction_t* code,
	uint64_t size, uint64_t symbol);


fct_arch_t fct_arch__rv32;
#define FCT_ARCH_RV32_STATIC &fct_arch__rv32
fct_arch_t* fct_arch_rv32;

fct_arch_t fct_arch_rv32__rvg;
#define FCT_ARCH_RV32_RVG_STATIC &fct_arch_rv32__rvg
fct_arch_t* fct_arch_rv32_rvg;


#define ELF_MACHINE_RISCV (uint16_t)0xF3

#endif
