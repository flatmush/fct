#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "rv32.h"


typedef enum
{
	RV32F__NO,
	RV32F__RD, RV32F__RS1, RV32F__RS2,
	RV32F__II, RV32F__IS, RV32F__ISB, RV32F__IU, RV32F__IUJ,
	RV32F__SHAMT,
} rv32__field_t;

typedef struct
{
	const char*   mnem;
	rv32_opcode_e opcode;
	rv32_funct3_e funct3;
	uint8_t       funct7;
	uint16_t      imm;
	rv32__field_t field[4];
	bool boundary;
	bool symrel;
} rv32__op_t;

static rv32__op_t rv32__op_name[] =
{
	{ "lui"  , RV32_OPCODE_LUI  , 0, 0, 0, { RV32F__RD, RV32F__IU , RV32F__NO, RV32F__NO }, 0, 0 },
	{ "auipc", RV32_OPCODE_AUIPC, 0, 0, 0, { RV32F__RD, RV32F__IU , RV32F__NO, RV32F__NO }, 0, 1 },

	{ "jal" , RV32_OPCODE_JAL , 0, 0, 0, { RV32F__RD , RV32F__IUJ, RV32F__NO, RV32F__NO }, 0, 1 },
	{ "jalr", RV32_OPCODE_JALR, 0, 0, 0, { RV32F__RD , RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "j"   , RV32_OPCODE_JAL , 0, 0, 0, { RV32F__IUJ, RV32F__NO,  RV32F__NO, RV32F__NO }, 1, 1 },
	{ "jr"  , RV32_OPCODE_JALR, 0, 0, 0, { RV32F__RS1, RV32F__II,  RV32F__NO, RV32F__NO }, 1, 0 },

	{ "beq" , RV32_OPCODE_BRANCH, RV32_F3_BEQ , 0, 0, { RV32F__RS1, RV32F__RS2, RV32F__ISB, RV32F__NO }, 0, 1 },
	{ "bne" , RV32_OPCODE_BRANCH, RV32_F3_BNE , 0, 0, { RV32F__RS1, RV32F__RS2, RV32F__ISB, RV32F__NO }, 0, 1 },
	{ "blt" , RV32_OPCODE_BRANCH, RV32_F3_BLT , 0, 0, { RV32F__RS1, RV32F__RS2, RV32F__ISB, RV32F__NO }, 0, 1 },
	{ "bge" , RV32_OPCODE_BRANCH, RV32_F3_BGE , 0, 0, { RV32F__RS1, RV32F__RS2, RV32F__ISB, RV32F__NO }, 0, 1 },
	{ "bltu", RV32_OPCODE_BRANCH, RV32_F3_BLTU, 0, 0, { RV32F__RS1, RV32F__RS2, RV32F__ISB, RV32F__NO }, 0, 1 },
	{ "bgeu", RV32_OPCODE_BRANCH, RV32_F3_BGEU, 0, 0, { RV32F__RS1, RV32F__RS2, RV32F__ISB, RV32F__NO }, 0, 1 },
	{ "b"   , RV32_OPCODE_BRANCH, RV32_F3_BEQ , 0, 0, { RV32F__ISB, RV32F__NO , RV32F__NO , RV32F__NO }, 1, 1 },
	{ "beqz", RV32_OPCODE_BRANCH, RV32_F3_BEQ , 0, 0, { RV32F__RS1, RV32F__ISB, RV32F__NO , RV32F__NO }, 0, 1 },
	{ "bnez", RV32_OPCODE_BRANCH, RV32_F3_BNE , 0, 0, { RV32F__RS1, RV32F__ISB, RV32F__NO , RV32F__NO }, 0, 1 },
	{ "blez", RV32_OPCODE_BRANCH, RV32_F3_BGE , 0, 0, { RV32F__RS2, RV32F__ISB, RV32F__NO , RV32F__NO }, 0, 1 },
	{ "bgez", RV32_OPCODE_BRANCH, RV32_F3_BGE , 0, 0, { RV32F__RS1, RV32F__ISB, RV32F__NO , RV32F__NO }, 0, 1 },
	{ "bltz", RV32_OPCODE_BRANCH, RV32_F3_BLT , 0, 0, { RV32F__RS1, RV32F__ISB, RV32F__NO , RV32F__NO }, 0, 1 },
	{ "bgtz", RV32_OPCODE_BRANCH, RV32_F3_BLT , 0, 0, { RV32F__RS2, RV32F__ISB, RV32F__NO , RV32F__NO }, 0, 1 },

	{ "lb" , RV32_OPCODE_LOAD, RV32_F3_LB , 0, 0, { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "lh" , RV32_OPCODE_LOAD, RV32_F3_LH , 0, 0, { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "lw" , RV32_OPCODE_LOAD, RV32_F3_LW , 0, 0, { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "lbu", RV32_OPCODE_LOAD, RV32_F3_LBU, 0, 0, { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "lhu", RV32_OPCODE_LOAD, RV32_F3_LHU, 0, 0, { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },

	{ "sb", RV32_OPCODE_STORE, RV32_F3_SB , 0, 0, { RV32F__RS2, RV32F__RS1, RV32F__IS, RV32F__NO }, 0, 0 },
	{ "sh", RV32_OPCODE_STORE, RV32_F3_SH , 0, 0, { RV32F__RS2, RV32F__RS1, RV32F__IS, RV32F__NO }, 0, 0 },
	{ "sw", RV32_OPCODE_STORE, RV32_F3_SW , 0, 0, { RV32F__RS2, RV32F__RS1, RV32F__IS, RV32F__NO }, 0, 0 },

	{ "addi" , RV32_OPCODE_OP_IMM, RV32_F3_ADDI , 0, 0     , { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "nop"  , RV32_OPCODE_OP_IMM, RV32_F3_ADDI , 0, 0     , { RV32F__NO, RV32F__NO , RV32F__NO, RV32F__NO }, 0, 0 },
	{ "mv"   , RV32_OPCODE_OP_IMM, RV32_F3_ADDI , 0, 0     , { RV32F__RD, RV32F__RS1, RV32F__NO, RV32F__NO }, 0, 0 },
	{ "mvi"  , RV32_OPCODE_OP_IMM, RV32_F3_ADDI , 0, 0     , { RV32F__RD, RV32F__II , RV32F__NO, RV32F__NO }, 0, 0 },
	{ "slti" , RV32_OPCODE_OP_IMM, RV32_F3_SLTI , 0, 0     , { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "sltiu", RV32_OPCODE_OP_IMM, RV32_F3_SLTIU, 0, 0     , { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "seqz" , RV32_OPCODE_OP_IMM, RV32_F3_SLTIU, 0, 0x0001, { RV32F__RD, RV32F__RS1, RV32F__NO, RV32F__NO }, 0, 0 },
	{ "xori" , RV32_OPCODE_OP_IMM, RV32_F3_XORI , 0, 0     , { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "not"  , RV32_OPCODE_OP_IMM, RV32_F3_XORI , 0, 0x0FFF, { RV32F__RD, RV32F__RS1, RV32F__NO, RV32F__NO }, 0, 0 },
	{ "ori"  , RV32_OPCODE_OP_IMM, RV32_F3_ORI  , 0, 0     , { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },
	{ "andi" , RV32_OPCODE_OP_IMM, RV32_F3_ANDI , 0, 0     , { RV32F__RD, RV32F__RS1, RV32F__II, RV32F__NO }, 0, 0 },

	{ "slli", RV32_OPCODE_OP_IMM, RV32_F3_SLLI, 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__SHAMT, RV32F__NO }, 0, 0 },
	{ "srli", RV32_OPCODE_OP_IMM, RV32_F3_SRLI, 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__SHAMT, RV32F__NO }, 0, 0 },
	{ "srai", RV32_OPCODE_OP_IMM, RV32_F3_SRAI, 0b0100000, 0, { RV32F__RD, RV32F__RS1, RV32F__SHAMT, RV32F__NO }, 0, 0 },

	{ "add" , RV32_OPCODE_OP, RV32_F3_ADD , 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "sub" , RV32_OPCODE_OP, RV32_F3_SUB , 0b0100000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "neg" , RV32_OPCODE_OP, RV32_F3_SUB , 0b0100000, 0, { RV32F__RD, RV32F__RS2, RV32F__NO , RV32F__NO }, 0, 0 },
	{ "sll" , RV32_OPCODE_OP, RV32_F3_SLL , 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "slt" , RV32_OPCODE_OP, RV32_F3_SLT , 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "sltz", RV32_OPCODE_OP, RV32_F3_SLT , 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__NO , RV32F__NO }, 0, 0 },
	{ "sgtz", RV32_OPCODE_OP, RV32_F3_SLT , 0b0000000, 0, { RV32F__RD, RV32F__RS2, RV32F__NO , RV32F__NO }, 0, 0 },
	{ "sltu", RV32_OPCODE_OP, RV32_F3_SLTU, 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "snez", RV32_OPCODE_OP, RV32_F3_SLTU, 0b0000000, 0, { RV32F__RD, RV32F__RS2, RV32F__NO , RV32F__NO }, 0, 0 },
	{ "xor" , RV32_OPCODE_OP, RV32_F3_XOR , 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "srl" , RV32_OPCODE_OP, RV32_F3_SRL , 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "sra" , RV32_OPCODE_OP, RV32_F3_SRA , 0b0100000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "or"  , RV32_OPCODE_OP, RV32_F3_OR  , 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "and" , RV32_OPCODE_OP, RV32_F3_AND , 0b0000000, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },

	/* TODO - FENCE and SYSTEM instructions. */

	{ "mul"   , RV32_OPCODE_OP, RV32M_F3_MUL   , 0b0000001, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "mulh"  , RV32_OPCODE_OP, RV32M_F3_MULH  , 0b0000001, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "mulhsu", RV32_OPCODE_OP, RV32M_F3_MULHSU, 0b0000001, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "mulhu" , RV32_OPCODE_OP, RV32M_F3_MULHU , 0b0000001, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "div"   , RV32_OPCODE_OP, RV32M_F3_DIV   , 0b0000001, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "divu"  , RV32_OPCODE_OP, RV32M_F3_DIVU  , 0b0000001, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "rem"   , RV32_OPCODE_OP, RV32M_F3_REM   , 0b0000001, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },
	{ "remu"  , RV32_OPCODE_OP, RV32M_F3_REMU  , 0b0000001, 0, { RV32F__RD, RV32F__RS1, RV32F__RS2, RV32F__NO }, 0, 0 },

	{ NULL, 0, 0, 0, 0, { 0, 0, 0, 0 }, 0, 0 }
};



static bool rv32__assemble_psop(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (postfix)
		return false;

	if (fct_tok_keyword(mnem, "li"))
	{
		if ((field_count != 2)
			|| !field[0] || !field[1])
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Expected destination and immediate");
			return false;
		}

		if (field[0]->type != FCT_ASM_FIELD_TYPE_REGISTER)
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected destination register");
			return false;
		}

		if ((field[0]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[1]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Field transformations not supported in this intruction");
			return false;
		}

		if ((field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
			|| (field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED))
		{
			if (field[1]->immediate.size > 32)
			{
				FCT_DEBUG_ERROR_TOKEN(mnem,
					"Immediate size too large for architecture");
				return false;
			}

			fct_tok_t psmnem =
			{
				.type = FCT_TOK_IDENT,
				.base = "mvi",
				.size = 3,
				.file = mnem->file,
				.row  = mnem->row,
				.col  = mnem->col,
			};

			if ((field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
				&& (field[1]->immediate.size <= 11))
			{
				return rv32_assemble(
					context, &psmnem, NULL, field, field_count);
			}
			else if ((field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED)
				&& (field[1]->immediate.size <= 12))
			{
				return rv32_assemble(
					context, &psmnem, NULL, field, field_count);
			}
			else if ((field[1]->immediate.value & 0x00000FFF) == 0)
			{
				psmnem.base = "lui";
				psmnem.size = 3;

				uint64_t uui = (field[1]->immediate.value >> 12);
				fct_asm_field_t* ui = fct_asm_field_create_immediate(
					20, 1, &uui, false);
				if (!ui) return false;

				fct_asm_field_t* f[2] = { field[0], ui };

				bool success = rv32_assemble(
					context, &psmnem, NULL, f, 2);
				fct_asm_field_delete(ui);
				return success;
			}
		}

		rv32_instruction_t code[2];

		code[0].u.reserved = 0b11;
		code[0].u.opcode   = RV32_OPCODE_LUI;
		code[0].u.rd       = field[0]->reg.reg;

		code[1].i.reserved = 0b11;
		code[1].i.opcode   = RV32_OPCODE_OP_IMM;
		code[1].i.rd       = field[0]->reg.reg;
		code[1].i.funct3   = RV32_F3_ADDI;
		code[1].i.rs1      = field[0]->reg.reg;

		switch (field[1]->type)
		{
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
			{
				uint32_t hi, lo;
				hi = (field[0]->immediate.value & 0xFFFFF000) >> 12;
				lo = (field[0]->immediate.value & 0x00000FFF);
				if (lo & 0x800) hi +=1;

				code[0].u.imm = hi;
				code[1].i.imm = lo;
				if (!fct_asm_context_emit_code(
					context, code, 8, 4, 0))
					return false;
				break;
			}

			case FCT_ASM_FIELD_TYPE_SYMBOL:
				code[0].u.imm = 0;
				code[1].i.imm = 0;
				if (!fct_asm_context_emit(
					context, code, 8, 4,
					FCT_CHUNK_ACCESS_RX,
					FCT_CHUNK_CONTENT_DATA,
					field[1]->symbol.name,
					0, false, true, 0))
				{
					FCT_DEBUG_ERROR_TOKEN(mnem,
						"Failed to add architecture specific import.");
					return false;
				}
				break;

			default:
					FCT_DEBUG_ERROR_FIELD(field[1],
					"Expected immediate or symbol");
				return false;
		}

		return true;
	}
	else if (fct_tok_keyword(mnem, "la"))
	{
		if ((field_count != 2)
			|| !field[0] || !field[1])
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Expected destination and immediate");
			return false;
		}

		if (field[0]->type != FCT_ASM_FIELD_TYPE_REGISTER)
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected destination register");
			return false;
		}

		if ((field[0]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[1]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Field transformations not supported in this intruction");
			return false;
		}

		if ((field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
			|| (field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED))
		{
			if (field[1]->immediate.size > 32)
			{
				FCT_DEBUG_ERROR_TOKEN(mnem,
					"Address offset too large for architecture");
				return false;
			}

			if ((field[1]->immediate.value & 0x00000FFF) == 0)
			{
				fct_tok_t psmnem =
				{
					.type = FCT_TOK_IDENT,
					.base = "auipc",
					.size = 5,
					.file = mnem->file,
					.row  = mnem->row,
					.col  = mnem->col,
				};

				uint64_t uui = (field[1]->immediate.value >> 12);
				fct_asm_field_t* ui = fct_asm_field_create_immediate(
					20, 1, &uui, false);
				if (!ui) return false;

				fct_asm_field_t* f[2] = { field[0], ui };

				bool success = rv32_assemble(
					context, &psmnem, NULL, f, 2);
				fct_asm_field_delete(ui);
				return success;
			}
		}

		rv32_instruction_t code[2];

		code[0].u.reserved = 0b11;
		code[0].u.opcode   = RV32_OPCODE_AUIPC;
		code[0].u.rd       = field[0]->reg.reg;

		code[1].i.reserved = 0b11;
		code[1].i.opcode   = RV32_OPCODE_OP_IMM;
		code[1].i.rd       = field[0]->reg.reg;
		code[1].i.funct3   = RV32_F3_ADDI;
		code[1].i.rs1      = field[0]->reg.reg;

		switch (field[1]->type)
		{
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
			{
				uint32_t hi, lo;
				hi = (field[0]->immediate.value & 0xFFFFF000) >> 12;
				lo = (field[0]->immediate.value & 0x00000FFF);
				if (lo & 0x800) hi += 1;

				code[0].u.imm = hi;
				code[1].i.imm = lo;
				if (!fct_asm_context_emit_code(
					context, code, 8, 4, 0))
					return false;
				break;
			}

			case FCT_ASM_FIELD_TYPE_SYMBOL:
				code[0].u.imm = 0;
				code[1].i.imm = 0;
				if (!fct_asm_context_emit(
					context, code, 8, 4,
					FCT_CHUNK_ACCESS_RX,
					FCT_CHUNK_CONTENT_DATA,
					field[1]->symbol.name,
					8, true, true, 0))
				{
					FCT_DEBUG_ERROR_TOKEN(mnem,
						"Failed to add architecture specific import.");
					return false;
				}
				break;

			default:
					FCT_DEBUG_ERROR_FIELD(field[1],
					"Expected immediate or symbol");
				return false;
		}

		return true;
	}
	else if (fct_tok_keyword(mnem, "ljal"))
	{
		if ((field_count != 2)
			|| !field[0] || !field[1])
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Expected destination and immediate");
			return false;
		}

		if (field[0]->type != FCT_ASM_FIELD_TYPE_REGISTER)
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected destination register");
			return false;
		}
		if (field[0]->reg.reg == RV32_REG_ZERO)
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Long jump and link requires non-zero destination register");
			return false;
		}

		if ((field[0]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[1]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Field transformations not supported in this intruction");
			return false;
		}

		if ((field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
			|| (field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED))
		{
			if (field[1]->immediate.size > 32)
			{
				FCT_DEBUG_ERROR_TOKEN(mnem,
					"Jump target too large for architecture");
				return false;
			}
			if (field[1]->immediate.value & 1)
			{
				FCT_DEBUG_ERROR_TOKEN(mnem,
					"Jump target misaligned");
				return false;
			}
		}

		rv32_instruction_t code[2];

		code[0].u.reserved = 0b11;
		code[0].u.opcode   = RV32_OPCODE_AUIPC;
		code[0].u.rd       = field[0]->reg.reg;

		code[1].i.reserved = 0b11;
		code[1].i.opcode   = RV32_OPCODE_JALR;
		code[1].i.rd       = field[0]->reg.reg;
		code[1].i.rs1      = field[0]->reg.reg;

		switch (field[1]->type)
		{
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
			{
				uint32_t hi, lo;
				hi = (field[0]->immediate.value & 0xFFFFF000) >> 12;
				lo = (field[0]->immediate.value & 0x00000FFF);
				if (lo & 0x800) hi += 1;

				code[0].u.imm = hi;
				code[1].i.imm = lo;
				if (!fct_asm_context_emit_code(
					context, code, 8, 4, 0))
					return false;
				break;
			}

			case FCT_ASM_FIELD_TYPE_SYMBOL:
				code[0].u.imm = 0;
				code[1].i.imm = 0;
				if (!fct_asm_context_emit(
					context, code, 8, 4,
					FCT_CHUNK_ACCESS_RX,
					FCT_CHUNK_CONTENT_DATA,
					field[1]->symbol.name,
					8, true, true, 0))
				{
					FCT_DEBUG_ERROR_TOKEN(mnem,
						"Failed to add architecture specific import.");
					return false;
				}
				break;

			default:
					FCT_DEBUG_ERROR_FIELD(field[1],
					"Expected immediate or symbol");
				return false;
		}

		return true;
	}
	else if (fct_tok_keyword(mnem, "llb")
		|| fct_tok_keyword(mnem, "llbu")
		|| fct_tok_keyword(mnem, "llh")
		|| fct_tok_keyword(mnem, "llhu")
		|| fct_tok_keyword(mnem, "llw")
		|| fct_tok_keyword(mnem, "lrb")
		|| fct_tok_keyword(mnem, "lrbu")
		|| fct_tok_keyword(mnem, "lrh")
		|| fct_tok_keyword(mnem, "lrhu")
		|| fct_tok_keyword(mnem, "lrw"))
	{
		if ((field_count != 3)
			|| !field[0] || !field[1])
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Expected destination and address");
			return false;
		}

		if (field[0]->type != FCT_ASM_FIELD_TYPE_REGISTER)
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected destination register");
			return false;
		}
		if (field[0]->reg.reg == RV32_REG_ZERO)
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Long load requires non-zero destination register");
			return false;
		}

		if ((field[0]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[1]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Field transformations not supported in this intruction");
			return false;
		}

		if ((field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
			|| (field[1]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED))
		{
			if (field[1]->immediate.size > 32)
			{
				FCT_DEBUG_ERROR_TOKEN(mnem,
					"Address offset too large for architecture");
				return false;
			}
		}

		bool relative = (fct_tok_keyword(mnem, "lrb")
			|| fct_tok_keyword(mnem, "lrbu")
			|| fct_tok_keyword(mnem, "lrh")
			|| fct_tok_keyword(mnem, "lrhu")
			|| fct_tok_keyword(mnem, "lrw"));

		rv32_instruction_t code[2];

		code[0].u.reserved = 0b11;
		code[0].u.opcode   = (relative ? RV32_OPCODE_AUIPC : RV32_OPCODE_LUI);
		code[0].u.rd       = field[0]->reg.reg;

		code[1].i.reserved = 0b11;
		code[1].i.opcode   = RV32_OPCODE_LOAD;
		code[1].i.rd       = field[0]->reg.reg;
		code[1].i.rs1      = field[0]->reg.reg;

		if (fct_tok_keyword(mnem, "llb")
			|| fct_tok_keyword(mnem, "lrb"))
			code[1].i.funct3 = RV32_F3_LB;
		else if (fct_tok_keyword(mnem, "llbu")
			|| fct_tok_keyword(mnem, "lrbu"))
			code[1].i.funct3 = RV32_F3_LBU;
		else if (fct_tok_keyword(mnem, "llh")
			|| fct_tok_keyword(mnem, "lrh"))
			code[1].i.funct3 = RV32_F3_LH;
		else if (fct_tok_keyword(mnem, "llhu")
			|| fct_tok_keyword(mnem, "lrhu"))
			code[1].i.funct3 = RV32_F3_LHU;
		else
			code[1].i.funct3 = RV32_F3_LW;

		switch (field[1]->type)
		{
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
			{
				uint32_t hi, lo;
				hi = (field[0]->immediate.value & 0xFFFFF000) >> 12;
				lo = (field[0]->immediate.value & 0x00000FFF);
				if (lo & 0x800) hi += 1;

				code[0].u.imm = hi;
				code[1].i.imm = lo;
				if (!fct_asm_context_emit_code(
					context, code, 8, 4, 0))
					return false;
				break;
			}

			case FCT_ASM_FIELD_TYPE_SYMBOL:
				code[0].u.imm = 0;
				code[1].i.imm = 0;
				if (!fct_asm_context_emit(
					context, code, 8, 4,
					FCT_CHUNK_ACCESS_RX,
					FCT_CHUNK_CONTENT_DATA,
					field[1]->symbol.name,
					(relative ? 8 : 0), relative, true, 0))
				{
					FCT_DEBUG_ERROR_TOKEN(mnem,
						"Failed to add architecture specific import.");
					return false;
				}
				break;

			default:
					FCT_DEBUG_ERROR_FIELD(field[1],
					"Expected immediate or symbol");
				return false;
		}

		return true;
	}

	FCT_DEBUG_ERROR_TOKEN(mnem,
		"Operation not recognized.");
	return false;
}

bool rv32_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (!context)
		return false;

	rv32__op_t* op;
	for (op = rv32__op_name; op->mnem; op++)
	{
		if (fct_tok_keyword(mnem, op->mnem))
			break;
	}

	if (!op->mnem)
	{
		return rv32__assemble_psop(
			context,
			mnem, postfix,
			field, field_count);
	}

	rv32_instruction_t inst;
	inst.mask       = 0x00000000;
	inst.r.reserved = 0b11;
	inst.r.opcode   = op->opcode;
	inst.r.funct3   = op->funct3;
	inst.r.funct7   = op->funct7;

	if (op->imm)
		inst.i.imm = (op->imm & 0x0FFF);

	fct_asm_field_t* sym = NULL;

	unsigned f;
	for (f = 0; (f < 4) && (op->field[f] != RV32F__NO); f++)
	{
		if (f >= field_count)
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Not enough fields (%u) for instruction", field_count);
			return false;
		}

		if (field[f]->xform != FCT_ASM_FIELD_XFORM_NONE)
		{
			FCT_DEBUG_ERROR_FIELD(field[f],
				"Unsupported field transformation.");
			return false;
		}

		uint32_t val = 0;
		switch (op->field[f])
		{
			case RV32F__RD:
			case RV32F__RS1:
			case RV32F__RS2:
				if (field[f]->type != FCT_ASM_FIELD_TYPE_REGISTER)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Expected register");
					return false;
				}
				val = field[f]->reg.reg;
				break;

			case RV32F__II:
			case RV32F__IS:
			case RV32F__ISB:
			case RV32F__IU:
			case RV32F__IUJ:
			case RV32F__SHAMT:
			{
				switch (field[f]->type)
				{
					case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
					case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
						val = field[f]->immediate.value;
						break;

					case FCT_ASM_FIELD_TYPE_SYMBOL:
						if (sym)
						{
							FCT_DEBUG_ERROR_FIELD(field[f],
								"Multiple symbols per instruction not allowed.");
							return false;
						}

						sym = field[f];
						break;

					default:
						FCT_DEBUG_ERROR_FIELD(field[f],
							"Expected immediate or symbol");
						return false;
				}
			}
			break;

			default:
				return false;
		}

		switch (op->field[f])
		{
			case RV32F__II:
			case RV32F__IS:
				if ((val >> 31)
					? ((val & 0xFFFFF800) != 0xFFFFF800)
					: ((val & 0xFFFFF800) != 0x00000000))
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Signed immediate out of range");
					return false;
				}
				break;

			case RV32F__ISB:
				if (val & 1)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Branch target misaligned");
					return false;
				}
				if ((val >> 31)
					? ((val & 0xFFFFF000) != 0xFFFFF000)
					: ((val & 0xFFFFF000) != 0x00000000))
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Branch target out of range");
					return false;
				}
				break;

			case RV32F__IU:
				if (val >> 20)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Upper immediate out of range");
					return false;
				}
				break;

			case RV32F__IUJ:
				if (val & 1)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Jump target misaligned");
					return false;
				}
				if ((val >> 31)
					? ((val & 0xFFF00000) != 0xFFF00000)
					: ((val & 0xFFF00000) != 0x00000000))
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Jump target out of range");
					return false;
				}
				break;

			case RV32F__SHAMT:
				if (val >= 32)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Shift immediate out of range");
					return false;
				}
				break;

			default:
				break;
		}

		switch (op->field[f])
		{
			case RV32F__RD:
				inst.r.rd = (val & 0x1F);
				break;

			case RV32F__RS1:
				inst.r.rs1 = (val & 0x1F);
				break;

			case RV32F__RS2:
				inst.r.rs2 = (val & 0x1F);
				break;

			case RV32F__II:
				inst.i.imm = (val & 0x00000FFF);
				break;

			case RV32F__IS:
				inst.s.imm_4_0  = (val >> 0) & 0x1F;
				inst.s.imm_11_5 = (val >> 5) & 0x7F;
				break;

			case RV32F__ISB:
				inst.sb.imm_11   = (val >> 11) & 0x01;
				inst.sb.imm_4_1  = (val >>  1) & 0x0F;
				inst.sb.imm_10_5 = (val >>  5) & 0x3F;
				inst.sb.imm_12   = (val >> 12) & 0x01;
				break;

			case RV32F__IU:
				inst.u.imm = (val & 0x000FFFFF);
				break;

			case RV32F__IUJ:
				inst.uj.imm_19_12 = (val >> 12) & 0x0FF;
				inst.uj.imm_11    = (val >> 11) & 0x001;
				inst.uj.imm_10_1  = (val >>  1) & 0x3FF;
				inst.uj.imm_20    = (val >> 20) & 0x001;
				break;

			case RV32F__SHAMT:
				inst.r.rs2 = (val & 0x1F);
				break;

			default:
				break;
		}
	}

	if (f != field_count)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Too many fields (%u)", field_count);
		return false;
	}

	unsigned boundary = (op->boundary ? 4 : 0);
	if (sym)
	{
		if (!fct_asm_context_emit(
			context, &inst.mask, 4, 4,
			FCT_CHUNK_ACCESS_RX,
			FCT_CHUNK_CONTENT_DATA,
			sym->symbol.name, (op->symrel ? 4 : 0), op->symrel, true,
			boundary))
		{
			FCT_DEBUG_ERROR_FIELD(sym,
				"Failed to add architecture specific import.");
			return false;
		}
	}
	else
	{
		if (!fct_asm_context_emit_code(
			context, &inst.mask, 4, 4, boundary))
			return false;
	}

	return true;
}
