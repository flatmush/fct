#include <stdlib.h>
#include "rv32.h"
#include <fct/arch/emu.h>
#include <fct/debug.h>



typedef struct
{
	uint32_t pc;

	union
	__attribute__((__packed__))
	{
		uint32_t  r[32];
		 int32_t rs[32];
	};
} rv32_core_t;



static inline uint32_t rv32_sign_extend8(uint8_t x)
	{ return (((x >>  7) ? 0xFFFFFF00 : 0) | x); }

static inline uint32_t rv32_sign_extend12(uint16_t x)
	{ return (((x >> 11) ? 0xFFFFF000 : 0) | x); }

static inline uint32_t rv32_sign_extend13(uint16_t x)
	{ return (((x >> 12) ? 0xFFFFE000 : 0) | x); }

static inline uint32_t rv32_sign_extend16(uint16_t x)
	{ return (((x >> 15) ? 0xFFFF0000 : 0) | x); }

static inline uint32_t rv32_sign_extend20(uint32_t x)
	{ return (((x >> 19) ? 0xFFF00000 : 0) | x); }



static inline uint32_t rv32_reg_read(
	rv32_core_t* core, rv32_reg_e reg)
{
	return (reg == 0 ? 0 : core->r[reg]);
}

static inline int32_t rv32_reg_read_signed(
	rv32_core_t* core, rv32_reg_e reg)
{
	return (reg == 0 ? 0 : core->rs[reg]);
}



static inline bool rv32_mem_read32(
	fct_device_t* bus, uint32_t address, uint32_t* data)
{
	/* TODO - Handle unaligned read. */
	if (address & 3)
		return false;

	fct_device_read(bus, (address >> 2), data);
	return true;
}

static inline bool rv32_mem_read16(
	fct_device_t* bus, uint32_t address, uint16_t* data)
{
	/* TODO - Handle unaligned read. */
	if (address & 1)
		return false;

	uint16_t datah[2];
	fct_device_read(bus, (address >> 2), datah);

	if (data)
		*data = ((address & 2) ? datah[1] : datah[0]);
	return true;
}


static inline bool rv32_mem_read8(
	fct_device_t* bus, uint32_t address, uint8_t* data)
{
	uint8_t datab[4];
	fct_device_read(bus, (address >> 2), datab);

	if (data)
		*data = datab[address & 3];
	return true;
}



static inline bool rv32_mem_write32(
	fct_device_t* bus, uint32_t address, uint32_t data)
{
	/* TODO - Handle unaligned write. */
	if (address & 3)
		return false;

	fct_device_write(bus, (address >> 2), 0xF, &data);
	return true;
}

static inline bool rv32_mem_write16(
	fct_device_t* bus, uint32_t address, uint16_t data)
{
	/* TODO - Handle unaligned write. */
	if (address & 1)
		return false;

	uint16_t datah[2] = { data, data };
	fct_device_write(bus, (address >> 2), ((address & 2) ? 0xC : 0x3), datah);
	return true;
}


static inline bool rv32_mem_write8(
	fct_device_t* bus, uint32_t address, uint8_t data)
{
	uint8_t datab[4] = { data, data, data, data };
	fct_device_write(bus, (address >> 2), (1 << (address & 3)), datab);
	return true;
}



static rv32_core_t* rv32_core_create(void)
{
	rv32_core_t* core
		= (rv32_core_t*)malloc(
			sizeof(rv32_core_t));
	if (!core) return NULL;

	core->pc   = 0;
	return core;
}

static void rv32_core_delete(rv32_core_t* core)
{
	if (!core)
		return;

	free(core);
}

static void rv32_core_exec(rv32_core_t* core, fct_device_t* bus)
{
	if (!core)
		return;

	rv32_instruction_t inst;
	if (!rv32_mem_read32(bus, core->pc, (uint32_t*)&inst))
	{
		/* TODO - Handle as an exception. */
		FCT_DEBUG_ERROR(
			"Invalid instruction address 0x%08X", core->pc);
		return;
	}
	core->pc += 4;

	uint32_t ui;
	int32_t  si;
	switch (inst.r.opcode)
	{
		case RV32_OPCODE_LOAD:
		{
			ui = rv32_sign_extend12(inst.i.imm);
			uint32_t addr = rv32_reg_read(core, inst.r.rs1) + ui;
			uint32_t data = 0x00000000;
			switch (inst.i.funct3)
			{
				case RV32_F3_LB:
				case RV32_F3_LBU:
				{
					uint8_t byte = 0;
					if (!rv32_mem_read8(bus, addr, &byte))
					{
						/* TODO - Invalid read address exception. */
					}
					data = byte;
					break;
				}

				case RV32_F3_LH:
				case RV32_F3_LHU:
				{
					uint16_t half = 0;
					if (!rv32_mem_read16(bus, addr, &half))
					{
						/* TODO - Invalid read address exception. */
					}
					data = half;
					break;
				}

				case RV32_F3_LW:
					if (!rv32_mem_read32(bus, addr, &data))
					{
						/* TODO - Invalid read address exception. */
					}
					break;

				default:
					/* TODO - Undefind instruction exception. */
					break;
			}

			switch (inst.i.funct3)
			{
				case RV32_F3_LB:
					data = rv32_sign_extend8(data & 0xFF);
					break;

				case RV32_F3_LH:
					data = rv32_sign_extend16(data & 0xFFFF);
					break;

				default:
					break;
			}

			core->r[inst.i.rd] = data;
			break;
		}

		case RV32_OPCODE_OP_IMM:
			ui = rv32_sign_extend12(inst.i.imm);
			si = ui;

			switch (inst.i.funct3)
			{
				case RV32_F3_ADDI:
					core->r[inst.i.rd] = rv32_reg_read(core, inst.r.rs1) + ui;
					break;

				case RV32_F3_SLLI:
					if (inst.r.funct7 != 0b0000000)
					{
						/* TODO - Undefined instruction exception. */
					}
					core->r[inst.r.rd] = rv32_reg_read(core, inst.r.rs1) << inst.r.rs2;
					break;

				case RV32_F3_SLTI:
					core->r[inst.r.rd] = (rv32_reg_read_signed(core, inst.r.rs1) < si ? 1 : 0);
					break;

				case RV32_F3_SLTIU:
					core->r[inst.r.rd] = (rv32_reg_read(core, inst.r.rs1) < ui ? 1 : 0);
					break;

				case RV32_F3_XORI:
					core->r[inst.i.rd] = rv32_reg_read(core, inst.r.rs1) ^ ui;
					break;

				case RV32_F3_SRLI:
					if (inst.r.funct7 == 0b0000000)
					{
						core->r[inst.r.rd] = rv32_reg_read(core, inst.r.rs1) >> inst.r.rs2;
					}
					else if (inst.r.funct7 == 0b0100000)
					{
						uint32_t rs1 = rv32_reg_read(core, inst.r.rs1);
						if (inst.r.rs2 == 0)
							core->r[inst.r.rd] = rs1;
						else 
							core->r[inst.r.rd] = (rs1 >> inst.r.rs2)
								| (((rs1 >> 31) ? 0xFFFFFFFF : 0) >> (32 - inst.r.rs2));
					}
					else
					{
						/* TODO - Undefined instruction exception. */
					}
					break;

				case RV32_F3_ORI:
					core->r[inst.i.rd] = rv32_reg_read(core, inst.r.rs1) | ui;
					break;

				case RV32_F3_ANDI:
					core->r[inst.i.rd] = rv32_reg_read(core, inst.r.rs1) & ui;
					break;
			}
			break;

		case RV32_OPCODE_AUIPC:
			core->r[inst.u.rd] = core->pc + (inst.u.imm << 12);
			break;

		case RV32_OPCODE_STORE:
		{
			ui = rv32_sign_extend12((inst.s.imm_11_5 << 5) | inst.s.imm_4_0);
			uint32_t addr = rv32_reg_read(core, inst.r.rs1) + ui;
			uint32_t data = rv32_reg_read(core, inst.r.rs2);
			switch (inst.i.funct3)
			{
				case RV32_F3_SB:
					if (!rv32_mem_write8(bus, addr, (data & 0xFF)))
					{
						/* TODO - Invalid write address exception. */
					}
					break;

				case RV32_F3_SH:
					if (!rv32_mem_write16(bus, addr, (data & 0xFFFF)))
					{
						/* TODO - Invalid write address exception. */
					}
					break;

				case RV32_F3_SW:
					if (!rv32_mem_write32(bus, addr, data))
					{
						/* TODO - Invalid write address exception. */
					}
					break;

				default:
					/* TODO - Undefind instruction exception. */
					break;
			}
			break;
		}

		case RV32_OPCODE_OP:
			switch (inst.r.funct7)
			{
				case 0b0000000:
					switch (inst.i.funct3)
					{
						case RV32_F3_ADD:
							core->r[inst.r.rd] = rv32_reg_read(core, inst.r.rs1)
								+ rv32_reg_read(core, inst.r.rs2) + 0;
							break;

						case RV32_F3_SLL:
						{
							uint32_t a = rv32_reg_read(core, inst.r.rs1);
							int32_t  s = rv32_reg_read(core, inst.r.rs2);
							if ((s <= -32) || (s >= 32))
								core->r[inst.r.rd] = 0;
							else if (s < 0)
								core->r[inst.r.rd] = a >> -s;
							else
								core->r[inst.r.rd] = a << s;
							break;
						}

						case RV32_F3_SLT:
							core->r[inst.r.rd] = (rv32_reg_read_signed(core, inst.r.rs1)
								< rv32_reg_read_signed(core, inst.r.rs2) ? 1 : 0);
							break;

						case RV32_F3_SLTU:
							core->r[inst.r.rd] = (rv32_reg_read(core, inst.r.rs1)
								< rv32_reg_read(core, inst.r.rs2) ? 1 : 0);
							break;

						case RV32_F3_XOR:
							core->r[inst.r.rd] = rv32_reg_read(core, inst.r.rs1)
								^ rv32_reg_read(core, inst.r.rs2);
							break;

						case RV32_F3_SRL:
						{
							uint32_t a = rv32_reg_read(core, inst.r.rs1);
							int32_t  s = rv32_reg_read(core, inst.r.rs2);
							if ((s <= -32) || (s >= 32))
								core->r[inst.r.rd] = 0;
							else if (s < 0)
								core->r[inst.r.rd] = a << -s;
							else
								core->r[inst.r.rd] = a >> s;
							break;
						}

						case RV32_F3_OR:
							core->r[inst.r.rd] = rv32_reg_read(core, inst.r.rs1)
								| rv32_reg_read(core, inst.r.rs2);
							break;

						case RV32_F3_AND:
							core->r[inst.r.rd] = rv32_reg_read(core, inst.r.rs1)
								& rv32_reg_read(core, inst.r.rs2);
							break;

						default:
							/* TODO - Undefined instruction exception. */
							break;
					}
					break;

				case 0b0100000:
					switch (inst.i.funct3)
					{
						case RV32_F3_SUB:
							core->r[inst.r.rd] = rv32_reg_read(core, inst.r.rs1)
								+ ~rv32_reg_read(core, inst.r.rs2) + 1;
							break;

						case RV32_F3_SRA:
						{
							uint32_t a = rv32_reg_read(core, inst.r.rs1);
							int32_t  s = rv32_reg_read(core, inst.r.rs2);
							if ((s <= -32) || (s >= 32))
								core->r[inst.r.rd] = 0;
							else if (s < 0)
							{
								core->r[inst.r.rd] = a << -s;
							}
							else
							{
								core->r[inst.r.rd] = a >> s;
								if (a >> 31)
									core->r[inst.r.rd] |= (0xFFFFFFFF << s);
							}
							break;
						}

						default:
							/* TODO - Undefined instruction exception. */
							break;
					}
					break;

				case 0b0000001:
					switch (inst.i.funct3)
					{
						case RV32M_F3_MUL:
							core->r[inst.r.rd] = rv32_reg_read(core, inst.r.rs1)
								* rv32_reg_read(core, inst.r.rs2);
							break;

						case RV32M_F3_MULH:
						{
							int64_t m = rv32_reg_read_signed(core, inst.r.rs1)
								* rv32_reg_read_signed(core, inst.r.rs2);
							core->r[inst.r.rd] = (m >> 32);
							break;
						}

						case RV32M_F3_MULHSU:
						{
							int64_t m = rv32_reg_read_signed(core, inst.r.rs1)
								* rv32_reg_read(core, inst.r.rs2);
							core->r[inst.r.rd] = (m >> 32);
							break;
						}

						case RV32M_F3_MULHU:
						{
							uint64_t m = rv32_reg_read(core, inst.r.rs1)
								* rv32_reg_read(core, inst.r.rs2);
							core->r[inst.r.rd] = (m >> 32);
							break;
						}

						case RV32M_F3_DIV:
						{
							int32_t a = rv32_reg_read_signed(core, inst.r.rs1);
							int32_t b = rv32_reg_read_signed(core, inst.r.rs1);
							core->r[inst.r.rd] = (b == 0 ? 0xFFFFFFFF : (uint32_t)(a / b));
							break;
						}

						case RV32M_F3_DIVU:
						{
							uint32_t a = rv32_reg_read(core, inst.r.rs1);
							uint32_t b = rv32_reg_read(core, inst.r.rs1);
							core->r[inst.r.rd] = (b == 0 ? 0xFFFFFFFF : (a / b));
							break;
						}

						case RV32M_F3_REM:
						{
							int32_t a = rv32_reg_read_signed(core, inst.r.rs1);
							int32_t b = rv32_reg_read_signed(core, inst.r.rs1);
							core->r[inst.r.rd] = (uint32_t)(b == 0 ? b : (a % b));
							break;
						}

						case RV32M_F3_REMU:
						{
							uint32_t a = rv32_reg_read(core, inst.r.rs1);
							uint32_t b = rv32_reg_read(core, inst.r.rs1);
							core->r[inst.r.rd] = (b == 0 ? b : (a % b));
							break;
						}

						default:
							/* TODO - Undefined instruction exception. */
							break;
					}
					break;

				default:
					/* TODO - Undefined instruction exception. */
					break;
			}
			break;

		case RV32_OPCODE_LUI:
			core->r[inst.u.rd] = (inst.u.imm << 12);
			break;

		case RV32_OPCODE_BRANCH:
			ui = rv32_sign_extend13(
				(inst.sb.imm_11 << 11)
					| (inst.sb.imm_4_1 << 1)
					| (inst.sb.imm_10_5 << 5)
					| (inst.sb.imm_12 << 12));
			switch (inst.r.funct3)
			{
				case RV32_F3_BEQ:
					if (rv32_reg_read(core, inst.r.rs1)
						== rv32_reg_read(core, inst.r.rs2))
						core->pc = core->pc + ui;
					break;

				case RV32_F3_BNE:
					if (rv32_reg_read(core, inst.r.rs1)
						!= rv32_reg_read(core, inst.r.rs2))
						core->pc = core->pc + ui;
					break;

				case RV32_F3_BLT:
					if (rv32_reg_read_signed(core, inst.r.rs1)
						< rv32_reg_read_signed(core, inst.r.rs2))
						core->pc = core->pc + ui;
					break;

				case RV32_F3_BGE:
					if (rv32_reg_read_signed(core, inst.r.rs1)
						>= rv32_reg_read_signed(core, inst.r.rs2))
						core->pc = core->pc + ui;
					break;

				case RV32_F3_BLTU:
					if (rv32_reg_read(core, inst.r.rs1)
						< rv32_reg_read(core, inst.r.rs2))
						core->pc = core->pc + ui;
					break;

				case RV32_F3_BGEU:
					if (rv32_reg_read(core, inst.r.rs1)
						>= rv32_reg_read(core, inst.r.rs2))
						core->pc = core->pc + ui;
					break;

				default:
					/* TODO - Undefined instruction exception. */
					break;
			}
			break;

		case RV32_OPCODE_JALR:
			ui = rv32_sign_extend12(inst.i.imm);
			core->r[inst.i.rd] = core->pc;
			core->pc = rv32_reg_read(core, inst.r.rs1) + ui;
			break;

		case RV32_OPCODE_JAL:
			ui = rv32_sign_extend20(
				(inst.uj.imm_19_12 << 12)
					| (inst.uj.imm_11 << 11)
					| (inst.uj.imm_10_1 << 1)
					| (inst.uj.imm_20 << 20));
			core->r[inst.uj.rd] = core->pc;
			core->pc = core->pc + ui;
			break;

		case RV32_OPCODE_SYSTEM:
			/* TODO - Implement system. */
			break;

		default:
			/* TODO - Undefined instruction exception. */
			FCT_DEBUG_ERROR(
				"Unsupported opcode %u.", inst.r.opcode);
			break;
	}
}



fct_arch_emu_t fct_arch_emu__rv32 =
{
	.create_cb = (void*)rv32_core_create,
	.delete_cb = (void*)rv32_core_delete,
	.exec_cb   = (void*)rv32_core_exec,

	.mmap_read  = NULL,
	.mmap_write = NULL,

	.interrupt = NULL,
};
