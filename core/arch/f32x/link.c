#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "f32x.h"


bool f32x_link(
	f32x_inst_t* code,
	uint64_t size, uint64_t symbol)
{
	if (!code)
		return false;

	if (((symbol >> 32) != 0)
		&& ((symbol >> 32) != 0xFFFFFFFF))
		return false;
	symbol &= 0xFFFFFFFF;

	if ((size < 8) || (size % 4))
		return false;

	unsigned end = (size >> 2) - 1;
	unsigned i;
	for (i = 0; i < end; i++)
	{
		if (code[i].li.eimm
			|| !code[i].li.limm)
			return false;		
	}

	code[0].li.ui = (symbol >> 6);
	if (code[0].li.field == 0)
		code[end].r.a = (symbol & 0x3F);
	else
		code[end].r.b = (symbol & 0x3F);
	return true;
}
