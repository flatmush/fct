#include <stdlib.h>
#include "f32x.h"
#include <fct/arch/emu.h>



typedef struct
{
	f32x_reg_t  reg[64];
	uint32_t    program_counter;

	bool        was_system;
	bool        was_exception;
	f32x_mode_e mode;

	f32x_control_mem_t control;
} f32x_core_t;



static void f32x__exception(
	f32x_core_t* core, f32x_exception_e exception, uint32_t data)
{
	if (exception >= (F32X_EXCEPTION_COUNT >> 1))
		return;

	exception <<= 1;
	switch (core->mode)
	{
		case F32X_MODE_USER:
			core->control.user_return_addr = core->program_counter;
			break;

		case F32X_MODE_SYSTEM:
			core->control.system_return_addr = core->program_counter;
			core->was_system = true;
			exception += 1;
			break;

		default:
			return;
	}

	core->control.exception_data = data;
	core->program_counter = core->control.vector_exception[exception];
	core->mode = F32X_MODE_EXCEPTION;
}

static void f32x_core_interrupt(
	f32x_core_t* core, uint64_t interrupt)
{
	if (interrupt >= 32)
		return;

	uint32_t mask = (1U << interrupt);
	if (((core->control.interrupt_enable & mask) == 0)
		|| ((core->control.interrupt_mask & mask) != 0))
		return;

	core->control.interrupt_mask |= mask;

	switch (core->mode)
	{
		case F32X_MODE_USER:
			core->control.user_return_addr = core->program_counter;
			break;

		case F32X_MODE_SYSTEM:
			core->control.system_return_addr = core->program_counter;
			core->was_system = true;
			break;

		case F32X_MODE_EXCEPTION:
			core->control.exception_return_addr = core->program_counter;
			core->was_exception = true;
			break;

		default:
			return;
	}

	core->program_counter = core->control.vector_interrupt[interrupt];
	core->mode = F32X_MODE_INTERRUPT;
}

static void f32x__system(f32x_core_t* core)
{
	if (core->mode == F32X_MODE_USER)
	{
		core->control.user_return_addr = core->program_counter;
		core->program_counter = core->control.vector_syscall;
		core->mode = F32X_MODE_SYSTEM;
	}
	else if (core->was_exception)
	{
		core->program_counter = core->control.exception_return_addr;
		core->mode = F32X_MODE_SYSTEM;
		core->was_exception = false;
	}
	else if (core->was_system)
	{
		core->program_counter = core->control.system_return_addr;
		core->mode = F32X_MODE_SYSTEM;
		core->was_system = false;
	}
	else
	{
		core->program_counter = core->control.user_return_addr;
		core->mode = F32X_MODE_USER;
	}

	uint32_t mask = core->control.interrupt_mask;
	mask &= core->control.interrupt_enable;
	if (mask != 0)
	{
		unsigned i;
		for (i = 0; ~mask & (1U << i); i++);
		f32x_core_interrupt(core, i);
	}
}



static bool f32x__control_read(
	f32x_core_t* core, unsigned addr, uint32_t* data)
{
	if (addr >= 1024)
		return false;
	if (data) *data = core->control.data[addr];
	return true;
}

static bool f32x__control_write(
	f32x_core_t* core, unsigned addr, uint32_t data)
{
	if (addr >= 1024)
		return false;

	switch (addr)
	{
		case 0:
		case 1:
			return false;

		case 10:
			core->control.interrupt_mask |= data;
			break;

		case 11:
			core->control.interrupt_mask &= ~data;
			break;

		default:
			core->control.data[addr] = data;
			break;
	}

	if (core->mode != F32X_MODE_INTERRUPT)
	{
		uint32_t mask = core->control.interrupt_mask;
		mask &= core->control.interrupt_enable;
		if (mask != 0)
		{
			unsigned i;
			for (i = 0; ~mask & (1U << i); i++);
			f32x_core_interrupt(core, i);
		}
	}

	return true;
}



static bool f32x__mem_read(
	fct_device_t* bus, uint32_t addr,
	uint32_t* result)
{
	/* TODO - Implement TLB */

	unsigned offset = (addr & 3);
	addr >>= 2;

	if (offset)
	{
		uint32_t data[2];
		fct_device_read(bus, (addr + 0), &data[0]);
		fct_device_read(bus, (addr + 1), &data[1]);

		offset *= 8;
		if (*result)
			*result = (data[0] >> offset)
				| (data[1] << (32 - offset));
	}
	else
	{
		fct_device_read(bus, addr, result);
	}

	return true;
}

static bool f32x__mem_write(
	fct_device_t* bus, uint32_t addr,
	unsigned size, uint32_t result)
{
	/* TODO - Implement TLB */

	unsigned mask = (1U << size) - 1;

	unsigned offset = (addr & 3);
	addr >>= 2;

	if (offset != 0)
	{
		mask <<= offset;
		if ((mask >> 4) == 0)
		{
			result <<= (offset * 8);
			fct_device_write(bus, addr, mask, &result);
		}
		else
		{
			uint32_t data[2];
			fct_device_read(bus, (addr + 0), &data[0]);
			fct_device_read(bus, (addr + 1), &data[1]);

			uint32_t m[2] = { 0, 0 };
			if (mask &   1) m[0] |= (0xFF <<  0);
			if (mask &   2) m[0] |= (0xFF <<  8);
			if (mask &   4) m[0] |= (0xFF << 16);
			if (mask &   8) m[0] |= (0xFF << 24);
			if (mask &  16) m[1] |= (0xFF <<  0);
			if (mask &  32) m[1] |= (0xFF <<  8);
			if (mask &  64) m[1] |= (0xFF << 16);
			if (mask & 128) m[1] |= (0xFF << 24);

			data[0] &= ~m[0];
			data[1] &= ~m[1];

			offset *= 8;
			data[0] |= ((result <<       offset ) & m[0]);
			data[1] |= ((result >> (32 - offset)) & m[1]);

			fct_device_write(bus, (addr + 0), (mask & 0xF), &data[0]);
			fct_device_write(bus, (addr + 1), (mask >> 4) , &data[1]);
		}
	}
	else
	{
		fct_device_write(bus, addr, mask, &result);
	}

	return true;
}



static void f32x__reg_read(
	f32x_core_t* core, f32x_reg_e reg,
	f32x_reg_t* result)
{
	f32x_reg_t value;
	if (reg == F32X_REG_ZERO)
		value.s32.ux = 0;
	else
		value = core->reg[reg];

	if (result)
		*result = value;
}

static void f32x_reg_write(
	f32x_core_t* core, f32x_reg_e reg,
	f32x_reg_t result)
{
	if (reg == F32X_REG_ZERO)
		return;
	core->reg[reg] = result;
}



static bool f32x__cond_resolve(
	f32x_cond_e cond,
	bool sign, bool zero,
	bool overflow, bool carry)
{
	switch (cond)
	{
		case F32X_COND_LOWER:
			return !carry;
		case F32X_COND_LOWER_SAME:
			return (!carry || zero);
		case F32X_COND_LESS_EQUAL:
			return (zero || (sign != overflow));
		case F32X_COND_LESS_THAN:
			return (sign != overflow);
		default:
			break;
	}
	return false;
}

static uint32_t f32x__add32(
	f32x_cond_e* cond,
	uint32_t a, uint32_t b, uint32_t ci,
	uint32_t* co)
{
	uint32_t result = a + b + (ci & 1);

	bool carry = ((result < a) || (result < b));
	if (co) *co = (carry ? 1 : 0);

	if (cond)
	{
		bool sign, zero, overflow;
		sign     = (result >> 31);
		zero     = (result ==  0);
		overflow = (((a ^ ~b) & (a ^ result)) >> 31);

		result = (f32x__cond_resolve(
			*cond, sign, zero, overflow, carry) ? 1 : 0);
	}

	return result;
}

static uint16_t f32x__add16(
	f32x_cond_e* cond,
	uint16_t a, uint16_t b, uint16_t ci,
	uint16_t* co)
{
	uint16_t result = a + b + (ci & 1);

	bool carry = ((result < a) || (result < b));
	if (co) *co = (carry ? 1 : 0);

	if (cond)
	{
		bool sign, zero, overflow;
		sign     = (result >> 15);
		zero     = (result ==  0);
		overflow = (((a ^ ~b) & (a ^ result)) >> 15);

		result = (f32x__cond_resolve(
			*cond, sign, zero, overflow, carry) ? 1 : 0);
	}

	return result;
}

static uint8_t f32x__add8(
	f32x_cond_e* cond,
	uint8_t a, uint8_t b, uint8_t ci,
	uint8_t* co)
{
	uint8_t result = a + b + (ci & 1);

	bool carry = ((result < a) || (result < b));
	if (co) *co = (carry ? 1 : 0);

	if (cond)
	{
		bool sign, zero, overflow;
		sign     = (result >> 7);
		zero     = (result ==  0);
		overflow = (((a ^ ~b) & (a ^ result)) >> 7);

		result = (f32x__cond_resolve(
			*cond, sign, zero, overflow, carry) ? 1 : 0);
	}

	return result;
}

static f32x_reg_t f32x__add(
	f32x_msimd_e msimd,
	f32x_cond_e* cond,
	f32x_reg_t a, f32x_reg_t b, f32x_reg_t ci,
	f32x_reg_t* co)
{
	f32x_reg_t result;
	f32x_reg_t carry = { .s32.ux = 0 };
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
		{
			uint8_t carry_q8[4];
			result.q8.ux = f32x__add8(cond,
				a.q8.ux, b.q8.ux, ci.q8.ux,
				&carry_q8[0]);
			result.q8.uy = f32x__add8(cond,
				a.q8.uy, b.q8.uy, ci.q8.uy,
				&carry_q8[1]);
			result.q8.uz = f32x__add8(cond,
				a.q8.uz, b.q8.uz, ci.q8.uz,
				&carry_q8[2]);
			result.q8.uw = f32x__add8(cond,
				a.q8.uw, b.q8.uw, ci.q8.uw,
				&carry_q8[3]);
			carry.q8.ux = carry_q8[0];
			carry.q8.uy = carry_q8[1];
			carry.q8.uz = carry_q8[2];
			carry.q8.uw = carry_q8[3];
		}
		break;

		case F32X_MSIMD_D16:
		{
			uint16_t carry_d16[2];
			result.d16.ux = f32x__add16(cond,
				a.d16.ux, b.d16.ux, ci.d16.ux,
				&carry_d16[0]);
			result.d16.uy = f32x__add16(cond,
				a.d16.uy, b.d16.uy, ci.d16.uy,
				&carry_d16[1]);
			carry.d16.ux = carry_d16[0];
			carry.d16.uy = carry_d16[1];
		}
		break;

		default:
		{
			uint32_t carry_s32;
			result.s32.ux = f32x__add32(cond,
				a.s32.ux, b.s32.ux, ci.s32.ux,
				&carry_s32);
			carry.s32.ux = carry_s32;
		}
		break;
	}

	if (co) *co = carry;
	return result;
}

static uint32_t f32x__pop(uint32_t x)
{
	unsigned i, c;
	for (i = 0, c = 0; i < 32; i++, x >>= 1)
		c += (x & 1);
	return c;
}

static uint32_t f32x__ctz(uint32_t x)
{
	unsigned i;
	for (i = 0; ((x & 1) == 0) && (i < 32); i++, x >>= 1);
	return i;
}

static uint32_t f32x__clz(uint32_t x)
{
	unsigned i;
	for (i = 0; ((x >> 31) == 0) && (i < 32); i++, x <<= 1);
	return i;
}

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		f32x_reg_t a;
		f32x_reg_t b;
	} reg;

	uint64_t u;
	 int64_t s;
} f32x__reg64_t;

static uint32_t f32x__mul32(
	bool sign, uint32_t a, uint32_t b, uint32_t* co)
{
	f32x__reg64_t r;
	if (sign)
		r.s = (int64_t)a * (int64_t)b;
	else
		r.u = (uint64_t)a * (uint64_t)b;
	if (co) *co = r.reg.b.s32.ux;
	return r.reg.a.s32.ux;
}

static uint16_t f32x__mul16(
	bool sign, uint16_t a, uint16_t b, uint16_t* co)
{
	f32x_reg_t r;
	if (sign)
		r.s32.sx = (int32_t)a * (int32_t)b;
	else
		r.s32.ux = (uint32_t)a * (uint32_t)b;
	if (co) *co = r.d16.uy;
	return r.d16.ux;
}

static uint8_t f32x__mul8(
	bool sign, uint8_t a, uint8_t b, uint8_t* co)
{
	f32x_reg_t r;
	if (sign)
		r.d16.sx = (int16_t)a * (int16_t)b;
	else
		r.d16.ux = (uint16_t)a * (uint16_t)b;
	if (co) *co = r.q8.uy;
	return r.q8.ux;
}

static f32x_reg_t f32x__mul(
	bool sign, f32x_msimd_e msimd,
	f32x_reg_t a, f32x_reg_t b, f32x_reg_t* co)
{
	f32x_reg_t r, c;
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
			r.q8.ux = f32x__mul8(
				sign, a.q8.ux, b.q8.ux, &c.q8.ux);
			r.q8.uy = f32x__mul8(
				sign, a.q8.uy, b.q8.uy, &c.q8.uy);
			r.q8.uz = f32x__mul8(
				sign, a.q8.uz, b.q8.uz, &c.q8.uz);
			r.q8.uw = f32x__mul8(
				sign, a.q8.uw, b.q8.uw, &c.q8.uw);
			break;
		case F32X_MSIMD_D16:
			r.d16.ux = f32x__mul16(
				sign, a.d16.ux, b.d16.ux, &c.d16.ux);
			r.d16.uy = f32x__mul16(
				sign, a.d16.uy, b.d16.uy, &c.d16.uy);
			break;
		default:
			r.s32.ux = f32x__mul32(
				sign, a.s32.ux, b.s32.ux, &c.s32.ux);
			break;
	}

	if (co) *co = c;
	return r;
}

static uint32_t f32x__divmod32(
	bool sign, uint32_t a, uint32_t b, uint32_t* co)
{
	f32x_reg_t r, ra, rb, rc;
	ra.s32.ux = a;
	rb.s32.ux = b;

	if (sign)
	{
		r.s32.sx  = ra.s32.sx / rb.s32.sx;
		rc.s32.sx = ra.s32.sx % rb.s32.sx;
	}
	else
	{
		r.s32.ux  = ra.s32.ux / rb.s32.ux;
		rc.s32.ux = ra.s32.ux % rb.s32.ux;
	}
	*co = rc.s32.ux;
	return r.s32.ux;
}

static uint16_t f32x__divmod16(
	bool sign, uint16_t a, uint16_t b, uint16_t* co)
{
	f32x_reg_t r, ra, rb, rc;
	ra.d16.ux = a;
	rb.d16.ux = b;

	if (sign)
	{
		r.d16.sx  = ra.d16.sx / rb.d16.sx;
		rc.d16.sx = ra.d16.sx % rb.d16.sx;
	}
	else
	{
		r.d16.ux  = ra.d16.ux / rb.d16.ux;
		rc.d16.ux = ra.d16.ux % rb.d16.ux;
	}
	*co = rc.d16.ux;
	return r.d16.ux;
}

static uint8_t f32x__divmod8(
	bool sign, uint8_t a, uint8_t b, uint8_t* co)
{
	f32x_reg_t r, ra, rb, rc;
	ra.q8.ux = a;
	rb.q8.ux = b;

	if (sign)
	{
		r.q8.sx  = ra.q8.sx / rb.q8.sx;
		rc.q8.sx = ra.q8.sx % rb.q8.sx;
	}
	else
	{
		r.q8.ux  = ra.q8.ux / rb.q8.ux;
		rc.q8.ux = ra.q8.ux % rb.q8.ux;
	}
	*co = rc.q8.ux;
	return r.q8.ux;
}

static f32x_reg_t f32x__divmod(
	bool sign, f32x_msimd_e msimd,
	f32x_reg_t a, f32x_reg_t b, f32x_reg_t* co)
{
	f32x_reg_t r, c;
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
			r.q8.ux = f32x__divmod8(
				sign, a.q8.ux, b.q8.ux, &c.q8.ux);
			r.q8.uy = f32x__divmod8(
				sign, a.q8.uy, b.q8.uy, &c.q8.uy);
			r.q8.uz = f32x__divmod8(
				sign, a.q8.uz, b.q8.uz, &c.q8.uz);
			r.q8.uw = f32x__divmod8(
				sign, a.q8.uw, b.q8.uw, &c.q8.uw);
			break;
		case F32X_MSIMD_D16:
			r.d16.ux = f32x__divmod16(
				sign, a.d16.ux, b.d16.ux, &c.d16.ux);
			r.d16.uy = f32x__divmod16(
				sign, a.d16.uy, b.d16.uy, &c.d16.uy);
			break;
		default:
			r.s32.ux = f32x__divmod32(
				sign, a.s32.ux, b.s32.ux, &c.s32.ux);
			break;
	}

	if (co) *co = c;
	return r;
}

static f32x_reg_t f32x__min(
	bool sign, bool neg, f32x_msimd_e msimd,
	f32x_reg_t a, f32x_reg_t b)
{
	f32x_reg_t r;
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
			if (neg)
			{
				r.q8.sx = -r.q8.sx;
				r.q8.sy = -r.q8.sy;
				r.q8.sz = -r.q8.sz;
				r.q8.sw = -r.q8.sw;
			}
			if (sign)
			{
				r.q8.sx = (a.q8.sx < b.q8.sx ? a.q8.sx : b.q8.sx);
				r.q8.sy = (a.q8.sy < b.q8.sy ? a.q8.sy : b.q8.sy);
				r.q8.sz = (a.q8.sz < b.q8.sz ? a.q8.sz : b.q8.sz);
				r.q8.sw = (a.q8.sw < b.q8.sw ? a.q8.sw : b.q8.sw);
			}
			else
			{
				r.q8.ux = (a.q8.ux < b.q8.ux ? a.q8.ux : b.q8.ux);
				r.q8.uy = (a.q8.uy < b.q8.uy ? a.q8.uy : b.q8.uy);
				r.q8.uz = (a.q8.uz < b.q8.uz ? a.q8.uz : b.q8.uz);
				r.q8.uw = (a.q8.uw < b.q8.uw ? a.q8.uw : b.q8.uw);
			}
			return r;
		case F32X_MSIMD_D16:
			if (neg)
			{
				r.d16.sx = -r.d16.sx;
				r.d16.sy = -r.d16.sy;
			}
			if (sign)
			{
				r.d16.sx = (a.d16.sx < b.d16.sx ? a.d16.sx : b.d16.sx);
				r.d16.sy = (a.d16.sy < b.d16.sy ? a.d16.sy : b.d16.sy);
			}
			else
			{
				r.d16.ux = (a.d16.ux < b.d16.ux ? a.d16.ux : b.d16.ux);
				r.d16.uy = (a.d16.uy < b.d16.uy ? a.d16.uy : b.d16.uy);
			}
			return r;
		default:
			break;
	}

	if (neg)
		b.s32.sx = -b.s32.sx;
	if (sign)
		r.s32.sx = (a.s32.sx < b.s32.sx ? a.s32.sx : b.s32.sx);
	else
		r.s32.ux = (a.s32.ux < b.s32.ux ? a.s32.ux : b.s32.ux);
	return r;
}

static f32x_reg_t f32x__max(
	bool sign, bool neg, f32x_msimd_e msimd,
	f32x_reg_t a, f32x_reg_t b)
{
	f32x_reg_t r;
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
			if (neg)
			{
				r.q8.sx = -r.q8.sx;
				r.q8.sy = -r.q8.sy;
				r.q8.sz = -r.q8.sz;
				r.q8.sw = -r.q8.sw;
			}
			if (sign)
			{
				r.q8.sx = (a.q8.sx > b.q8.sx ? a.q8.sx : b.q8.sx);
				r.q8.sy = (a.q8.sy > b.q8.sy ? a.q8.sy : b.q8.sy);
				r.q8.sz = (a.q8.sz > b.q8.sz ? a.q8.sz : b.q8.sz);
				r.q8.sw = (a.q8.sw > b.q8.sw ? a.q8.sw : b.q8.sw);
			}
			else
			{
				r.q8.ux = (a.q8.ux > b.q8.ux ? a.q8.ux : b.q8.ux);
				r.q8.uy = (a.q8.uy > b.q8.uy ? a.q8.uy : b.q8.uy);
				r.q8.uz = (a.q8.uz > b.q8.uz ? a.q8.uz : b.q8.uz);
				r.q8.uw = (a.q8.uw > b.q8.uw ? a.q8.uw : b.q8.uw);
			}
			return r;
		case F32X_MSIMD_D16:
			if (neg)
			{
				r.d16.sx = -r.d16.sx;
				r.d16.sy = -r.d16.sy;
			}
			if (sign)
			{
				r.d16.sx = (a.d16.sx > b.d16.sx ? a.d16.sx : b.d16.sx);
				r.d16.sy = (a.d16.sy > b.d16.sy ? a.d16.sy : b.d16.sy);
			}
			else
			{
				r.d16.ux = (a.d16.ux > b.d16.ux ? a.d16.ux : b.d16.ux);
				r.d16.uy = (a.d16.uy > b.d16.uy ? a.d16.uy : b.d16.uy);
			}
			return r;
		default:
			break;
	}

	if (neg)
		b.s32.sx = -b.s32.sx;
	if (sign)
		r.s32.sx = (a.s32.sx > b.s32.sx ? a.s32.sx : b.s32.sx);
	else
		r.s32.ux = (a.s32.ux > b.s32.ux ? a.s32.ux : b.s32.ux);
	return r;
}


static uint8_t f32x__sll8(uint8_t a, uint8_t b)
	{ return (b >= 8 ? 0 : (a << b)); }
static uint16_t f32x__sll16(uint16_t a, uint16_t b)
	{ return (b >= 16 ? 0 : (a << b)); }
static uint32_t f32x__sll32(uint32_t a, uint32_t b)
	{ return (b >= 32 ? 0 : (a << b)); }

static uint8_t f32x__srl8(uint8_t a, uint8_t b)
	{ return (b >= 8 ? 0 : (a >> b)); }
static uint16_t f32x__srl16(uint16_t a, uint16_t b)
	{ return (b >= 16 ? 0 : (a >> b)); }
static uint32_t f32x__srl32(uint32_t a, uint32_t b)
	{ return (b >= 32 ? 0 : (a >> b)); }


static uint8_t f32x__sra8(uint8_t a, uint8_t b)
{
	uint8_t m = ((a >> 7) ? 0xFF : 0);
	if (b >= 8) return m;
	return (a >> b) | (m << (8 - b));
}

static uint16_t f32x__sra16(uint16_t a, uint16_t b)
{
	uint16_t m = ((a >> 15) ? 0xFFFF : 0);
	if (b >= 16) return m;
	return (a >> b) | (m << (16 - b));
}

static uint32_t f32x__sra32(uint32_t a, uint32_t b)
{
	uint32_t m = ((a >> 31) ? 0xFFFFFFFF : 0);
	if (b >= 32) return m;
	return (a >> b) | (m << (32 - b));
}


static uint8_t f32x__srr8(uint8_t a, uint8_t b)
	{ b %=  8; return (a >> b) | (a << ( 8 - b)); }
static uint16_t f32x__srr16(uint16_t a, uint16_t b)
	{ b %= 16; return (a >> b) | (a << (16 - b)); }
static uint32_t f32x__srr32(uint32_t a, uint32_t b)
	{ b %= 32; return (a >> b) | (a << (32 - b)); }


static f32x_reg_t f32x__sll(
	f32x_msimd_e msimd,
	f32x_reg_t a, f32x_reg_t b)
{
	f32x_reg_t r;
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
			r.q8.ux = f32x__sll8(a.q8.ux, b.q8.ux);
			r.q8.uy = f32x__sll8(a.q8.uy, b.q8.uy);
			r.q8.uz = f32x__sll8(a.q8.uz, b.q8.uz);
			r.q8.uw = f32x__sll8(a.q8.uw, b.q8.uw);
			return r;
		case F32X_MSIMD_D16:
			r.d16.ux = f32x__sll16(a.d16.ux, b.d16.ux);
			r.d16.uy = f32x__sll16(a.d16.uy, b.d16.uy);
			return r;
		default:
			r.s32.ux = f32x__sll32(a.s32.ux, b.s32.ux);
			break;
	}
	return r;
}

static f32x_reg_t f32x__srl(
	f32x_msimd_e msimd,
	f32x_reg_t a, f32x_reg_t b)
{
	f32x_reg_t r;
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
			r.q8.ux = f32x__srl8(a.q8.ux, b.q8.ux);
			r.q8.uy = f32x__srl8(a.q8.uy, b.q8.uy);
			r.q8.uz = f32x__srl8(a.q8.uz, b.q8.uz);
			r.q8.uw = f32x__srl8(a.q8.uw, b.q8.uw);
			return r;
		case F32X_MSIMD_D16:
			r.d16.ux = f32x__srl16(a.d16.ux, b.d16.ux);
			r.d16.uy = f32x__srl16(a.d16.uy, b.d16.uy);
			return r;
		default:
			r.s32.ux = f32x__srl32(a.s32.ux, b.s32.ux);
			break;
	}
	return r;
}

static f32x_reg_t f32x__sra(
	f32x_msimd_e msimd,
	f32x_reg_t a, f32x_reg_t b)
{
	f32x_reg_t r;
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
			r.q8.ux = f32x__sra8(a.q8.ux, b.q8.ux);
			r.q8.uy = f32x__sra8(a.q8.uy, b.q8.uy);
			r.q8.uz = f32x__sra8(a.q8.uz, b.q8.uz);
			r.q8.uw = f32x__sra8(a.q8.uw, b.q8.uw);
			return r;
		case F32X_MSIMD_D16:
			r.d16.ux = f32x__sra16(a.d16.ux, b.d16.ux);
			r.d16.uy = f32x__sra16(a.d16.uy, b.d16.uy);
			return r;
		default:
			r.s32.ux = f32x__sra32(a.s32.ux, b.s32.ux);
			break;
	}
	return r;
}

static f32x_reg_t f32x__srr(
	f32x_msimd_e msimd,
	f32x_reg_t a, f32x_reg_t b)
{
	f32x_reg_t r;
	switch (msimd)
	{
		case F32X_MSIMD_Q8:
			r.q8.ux = f32x__srr8(a.q8.ux, b.q8.ux);
			r.q8.uy = f32x__srr8(a.q8.uy, b.q8.uy);
			r.q8.uz = f32x__srr8(a.q8.uz, b.q8.uz);
			r.q8.uw = f32x__srr8(a.q8.uw, b.q8.uw);
			return r;
		case F32X_MSIMD_D16:
			r.d16.ux = f32x__srr16(a.d16.ux, b.d16.ux);
			r.d16.uy = f32x__srr16(a.d16.uy, b.d16.uy);
			return r;
		default:
			r.s32.ux = f32x__srr32(a.s32.ux, b.s32.ux);
			break;
	}
	return r;
}

static inline uint32_t f32x__signex13(uint32_t x)
	{ return ((x >> 12) ? 0xFFFFE000 : 0) | x; }
static inline uint32_t f32x__signex16(uint32_t x)
	{ return ((x >> 15) ? 0xFFFF0000 : 0) | x; }
static inline uint32_t f32x__signex8(uint32_t x)
	{ return ((x >>  7) ? 0xFFFFFF00 : 0) | x; }

static inline uint32_t f32x__mem_addr_decode(unsigned addr, unsigned shift)
	{ return (addr & (0x1FFF ^ ((1U << shift) - 1))) | ((addr & ((1U << shift) - 1)) << 13); }



static f32x_core_t* f32x_core_create(void)
{
	f32x_core_t* core
		= (f32x_core_t*)malloc(
			sizeof(f32x_core_t));
	if (!core) return NULL;

	core->control.version  = 0;
	core->control.features = 0x00000000;

	core->control.vector_reset = 0;

	core->program_counter = core->control.vector_reset;

	core->was_system = false;
	core->was_exception = false;
	core->mode = F32X_MODE_SYSTEM;

	return core;
}

static void f32x_core_delete(f32x_core_t* core)
{
	if (!core)
		return;

	free(core);
}

static void f32x_core_exec(f32x_core_t* core, fct_device_t* bus)
{
	if (!core)
		return;

	/* Fetch */
	uint32_t pc = core->program_counter;

	f32x_inst_t inst;
	bool     li_set[2] = { false, false };
	uint32_t li_imm[2];

	bool skip = false;
	while (true)
	{
		if (skip)
		{
			skip = false;
			pc += 4;
			continue;
		}

		if (!f32x__mem_read(bus, pc, &inst.mask))
		{
			f32x__exception(core, F32X_EXCEPTION_INVALID_INST_ADDR, pc);
			return;
		}
		pc += 4;

		if (!inst.li.eimm && inst.li.limm)
		{
			li_set[inst.li.field] = true;
			li_imm[inst.li.field] = (inst.li.ui << 6);
			skip = (inst.li.type == F32X_LI_U64);
		}
		else
		{
			li_imm[0] |= inst.r.a;
			li_imm[1] |= inst.r.b;
			break;
		}
	}

	/* Decode */
	f32x_reg_t a, b, c;

	bool is_store = ((inst.ui.op == F32X_OPI_CST)
		|| F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_STORE));

	uint32_t edimm = (inst.ui.ui & 0xFFFFFFC0) | inst.ui.d;

	if (li_set[0])
		a.s32.ux = li_imm[0];
	else
		f32x__reg_read(core, inst.r.a, &a);

	if (li_set[1])
		b.s32.ux = li_imm[1];
	else if (inst.ui.eimm && !is_store)
		b.s32.ux = inst.ui.ui;
	else
		f32x__reg_read(core, inst.r.b, &b);

	f32x__reg_read(core, F32X_REG_CARRY, &c);

	/* Execute */
	f32x_reg_t result = { .s32.ux = 0 };
	f32x_reg_t carry  = { .s32.ux = 0 };
	bool       writeback = true;
	bool carry_writeback = false;
	bool       system    = false;

	if (inst.ui.eimm)
	{
		if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_MEMORY))
		{
			unsigned shift;
			switch (inst.mem.op)
			{
				case F32X_MEMORY_OP_LB:
				case F32X_MEMORY_OP_LBS:
				case F32X_MEMORY_OP_SB:
				case F32X_MEMORY_OP_SCB:
					shift = 0;
					break;
				case F32X_MEMORY_OP_LH:
				case F32X_MEMORY_OP_LHS:
				case F32X_MEMORY_OP_SH:
				case F32X_MEMORY_OP_SCH:
					shift = 1;
					break;
				case F32X_MEMORY_OP_LW:
				case F32X_MEMORY_OP_LWS:
				case F32X_MEMORY_OP_SW:
				case F32X_MEMORY_OP_SCW:
					shift = 2;
					break;
				default:
					shift = 3;
					break;
			}
			unsigned size = (1U << shift);
			if (size > 4) size = 4;

			f32x_reg_t data;
			if (inst.mem.op >= F32X_MEMORY_OP_SW)
			{
				result.s32.ux = (a.s32.ux + f32x__mem_addr_decode(edimm, shift));

				if (!f32x__mem_write(
					bus, result.s32.ux, size, b.s32.ux))
				{
					f32x__exception(core,
						F32X_EXCEPTION_INVALID_DATA_WR_ADDR,
						result.s32.ux);
					return;
				}
				writeback = false;
			}
			else
			{
				if (!li_set[1]) b.s32.ux = f32x__mem_addr_decode(b.s32.ux, shift);
				result.s32.ux = (a.s32.ux + b.s32.ux);

				if (!f32x__mem_read(bus, result.s32.ux, &data.s32.ux))
				{
					f32x__exception(core,
						F32X_EXCEPTION_INVALID_DATA_WR_ADDR,
						result.s32.ux);
					return;
				}
				uint32_t mask = 0xFFFFFFFF;
				if (size < 4) mask >>= ((4 - size) * 8);
				result.s32.ux = data.s32.ux & mask;

				if (inst.mem.op & 1)
				{
					switch (size)
					{
						case 1:
							result.s32.ux = f32x__signex8(result.s32.ux);
							break;
						case 2:
							result.s32.ux = f32x__signex16(result.s32.ux);
							break;
						default:
							break;
					}
				}
			}
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_BIT))
		{
			switch (inst.biti.op)
			{
				case F32X_BIT_OP_AND:
					result.s32.ux = a.s32.ux & b.s32.ux;
					break;
				case F32X_BIT_OP_OR:
					result.s32.ux = a.s32.ux | b.s32.ux;
					break;
				case F32X_BIT_OP_NAND:
					result.s32.ux = ~(a.s32.ux & b.s32.ux);
					break;
				case F32X_BIT_OP_NOR:
					result.s32.ux = ~(a.s32.ux | b.s32.ux);
					break;
				case F32X_BIT_OP_ANDN:
					result.s32.ux = a.s32.ux & ~b.s32.ux;
					break;
				case F32X_BIT_OP_ORN:
					result.s32.ux = a.s32.ux | ~b.s32.ux;
					break;
				case F32X_BIT_OP_XOR:
					result.s32.ux = a.s32.ux ^ b.s32.ux;
					break;
				default:
					result.s32.ux = ~(a.s32.ux ^ b.s32.ux);
					break;
			}
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_CMOV))
		{
			b.s32.ux = f32x__signex13(b.s32.ux);
			if (inst.cmovi.op == F32X_CMOV_OP_BRANCH_LINK)
				b.s32.ux <<= 2;
			result.s32.ux = pc + b.s32.ux;
			switch (inst.cmovi.cond)
			{
				case F32X_CMOV_COND_EQUAL_ZERO:
					writeback = (a.s32.ux == 0);
					break;
				case F32X_CMOV_COND_NON_ZERO:
					writeback = (a.s32.ux != 0);
					break;
				case F32X_CMOV_COND_LESS_THAN_ZERO:
					writeback = (a.s32.sx < 0);
					break;
				default:
					writeback = (a.s32.sx <= 0);
					break;
			}

			if (writeback)
			{
				uint32_t swap;
				switch (inst.cmovi.op)
				{
					case F32X_CMOV_OP_BRANCH_LINK:
						swap = result.s32.ux;
						result.s32.ux = pc;
						pc = swap;
						break;
					default:
						break;
				}
			}
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_DIVMOD))
		{
			bool sign = ((inst.divmodi.op == F32X_DIVMOD_OP_SDIV)
				|| (inst.divmodi.op == F32X_DIVMOD_OP_SDIVMOD));
			if (sign) b.s32.ux = f32x__signex13(b.s32.ux);
			result = f32x__divmod(
				sign, F32X_MSIMD_S32,
				(inst.divmodi.swap ? b : a),
				(inst.divmodi.swap ? a : b),
				&carry);
			carry_writeback
				= ((inst.divmodi.op == F32X_DIVMOD_OP_UDIVMOD)
				|| (inst.divmodi.op == F32X_DIVMOD_OP_SDIVMOD));
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_SHIFT))
		{
			switch (inst.shifti.op)
			{
				case F32X_SHIFT_OP_LEFT_LOGICAL:
					result = f32x__sll(F32X_MSIMD_S32, b, a);
					break;
				case F32X_SHIFT_OP_RIGHT_LOGICAL:
					result = f32x__srl(F32X_MSIMD_S32, b, a);
					break;
				case F32X_SHIFT_OP_RIGHT_ARITHMETIC:
					b.s32.ux = f32x__signex13(b.s32.ux);
					result = f32x__sra(F32X_MSIMD_S32, b, a);
					break;
				default:
					result = f32x__srr(F32X_MSIMD_S32, b, a);
					break;
			}
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_CLAMP))
		{
			bool sign = ((inst.clampi.op == F32X_CLAMP_OP_SMIN)
				|| (inst.clampi.op == F32X_CLAMP_OP_SMAX));
			if (sign) b.s32.ux = f32x__signex13(b.s32.ux);
			if ((inst.clampi.op == F32X_CLAMP_OP_SMIN)
				|| (inst.clampi.op == F32X_CLAMP_OP_UMIN))
				result = f32x__min(sign, false, F32X_MSIMD_S32, a, b);
			else
				result = f32x__max(sign, false, F32X_MSIMD_S32, a, b);
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_SETCOND))
		{
			bool sign = ((inst.setcondi.cond == F32X_COND_LESS_THAN)
				|| (inst.setcondi.cond == F32X_COND_LESS_EQUAL));
			if (sign) b.s32.ux = f32x__signex13(b.s32.ux);
			f32x_cond_e cond = inst.setcondi.cond;
			if (inst.setcondi.swap)
				a.s32.ux = ~a.s32.ux;
			else
				b.s32.ux = ~b.s32.ux;
			c.s32.ux = 0x01010101;
			f32x__add(F32X_MSIMD_S32, &cond, a, b, c, NULL);
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_ADDSUB))
		{
			b.s32.ux = f32x__signex13(b.s32.ux);
			c.s32.ux = (inst.addi.a_neg ? 1 : 0);
			if (inst.addi.a_neg)
				a.s32.ux = ~a.s32.ux;
			result = f32x__add(F32X_MSIMD_S32, NULL, a, b, c, &carry);
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_MUL))
		{
			bool sign = (inst.muli.op == F32X_MULTIPLY_OP_SMUL);
			if (sign) b.s32.ux = f32x__signex13(b.s32.ux);
			result = f32x__mul(sign, F32X_MSIMD_S32, a, b, &carry);
		}
		else if (inst.ui.op ==  F32X_OPI_CST)
		{
			result.s32.ux = a.s32.ux + edimm;
			f32x__control_write(core, result.s32.ux, b.s32.ux);
			writeback = false;
		}
		else if (inst.ui.op ==  F32X_OPI_CLD)
		{
			result.s32.ux = (a.s32.ux + b.s32.ux);
			f32x__control_read(core, result.s32.ux, &result.s32.ux);
		}
		else
		{
			f32x__exception(core,
				F32X_EXCEPTION_INVALID_INST,
				inst.mask);
			return;
		}
	}
	else if (inst.r.reserved != 0)
	{
		f32x__exception(core,
			F32X_EXCEPTION_INVALID_INST,
			inst.mask);
		return;
	}
	else
	{
		if (inst.add.prefix == F32X_OPR_PREFIX_ADD)
		{
			if (inst.add.a_inv)
				a.s32.ux = ~a.s32.ux;
			if (!inst.add.c_reg)
				c.s32.ux = 0;
			if (inst.add.c_inv)
				c.s32.ux = ~c.s32.ux;
			carry_writeback = inst.add.c_set;
			result = f32x__add(inst.add.msimd, NULL, a, b, c, &carry);
		}
		else if (inst.bit.prefix == F32X_OPR_PREFIX_BIT)
		{
			if (inst.bit.bit_imm)
				b.s32.ux = (1U << inst.bit.b);
			switch (inst.bit.op)
			{
				case F32X_BIT_OP_AND:
					result.s32.ux = a.s32.ux & b.s32.ux;
					break;
				case F32X_BIT_OP_OR:
					result.s32.ux = a.s32.ux | b.s32.ux;
					break;
				case F32X_BIT_OP_NAND:
					result.s32.ux = ~(a.s32.ux & b.s32.ux);
					break;
				case F32X_BIT_OP_NOR:
					result.s32.ux = ~(a.s32.ux | b.s32.ux);
					break;
				case F32X_BIT_OP_ANDN:
					result.s32.ux = a.s32.ux & ~b.s32.ux;
					break;
				case F32X_BIT_OP_ORN:
					result.s32.ux = a.s32.ux | ~b.s32.ux;
					break;
				case F32X_BIT_OP_XOR:
					result.s32.ux = a.s32.ux ^ b.s32.ux;
					break;
				default:
					result.s32.ux = ~(a.s32.ux ^ b.s32.ux);
					break;
			}
			switch (inst.bit.post_op)
			{
				case F32X_BIT_POST_OP_POP:
					result.s32.ux = f32x__pop(result.s32.ux);
					break;
				case F32X_BIT_POST_OP_CLZ:
					result.s32.ux = f32x__clz(result.s32.ux);
					break;
				case F32X_BIT_POST_OP_CTZ:
					result.s32.ux = f32x__ctz(result.s32.ux);
					break;
				default:
					break;
			}
		}
		else if (inst.clamp.prefix == F32X_OPR_PREFIX_CLAMP)
		{
			bool sign = ((inst.clamp.op == F32X_CLAMP_OP_SMIN)
				|| (inst.clamp.op == F32X_CLAMP_OP_SMAX));
			if ((inst.clamp.op == F32X_CLAMP_OP_SMIN)
				|| (inst.clamp.op == F32X_CLAMP_OP_UMIN))
				result = f32x__min(sign, inst.clamp.a_neg,
					inst.clamp.msimd, b, a);
			else
				result = f32x__max(sign, inst.clamp.a_neg,
					inst.clamp.msimd, b, a);
		}
		else if (inst.shift.prefix == F32X_OPR_PREFIX_SHIFT)
		{
			switch (inst.shift.op)
			{
				case F32X_SHIFT_OP_LEFT_LOGICAL:
					result = f32x__sll(inst.shift.msimd, a, b);
					break;
				case F32X_SHIFT_OP_RIGHT_LOGICAL:
					result = f32x__srl(inst.shift.msimd, a, b);
					break;
				case F32X_SHIFT_OP_RIGHT_ARITHMETIC:
					result = f32x__sra(inst.shift.msimd, a, b);
					break;
				default:
					result = f32x__srr(inst.shift.msimd, a, b);
					break;
			}
		}
		else if (inst.cmov.prefix == F32X_OPR_PREFIX_CMOV)
		{
			if (inst.r.a == F32X_REG_ZERO)
				a.s32.ux = 0;

			result = b;
			switch (inst.cmov.op)
			{
				case F32X_CMOV_OP_MOVE_RELATIVE:
				case F32X_CMOV_OP_BRANCH_LINK:
					result.s32.ux += pc;
					break;
				default:
					break;
			}
			switch (inst.cmov.cond)
			{
				case F32X_CMOV_COND_EQUAL_ZERO:
					writeback = (a.s32.ux == 0);
					break;
				case F32X_CMOV_COND_NON_ZERO:
					writeback = (a.s32.ux != 0);
					break;
				case F32X_CMOV_COND_LESS_THAN_ZERO:
					writeback = (a.s32.sx < 0);
					break;
				default:
					writeback = (a.s32.sx <= 0);
					break;
			}

			if (writeback)
			{
				uint32_t swap;
				switch (inst.cmov.op)
				{
					case F32X_CMOV_OP_JUMP_LINK:
					case F32X_CMOV_OP_BRANCH_LINK:
						swap = result.s32.ux;
						result.s32.ux = pc;
						pc = swap;
						break;
					default:
						break;
				}
			}

			system = inst.cmov.system;
		}
		else if (inst.setcond.prefix == F32X_OPR_PREFIX_SETCOND)
		{
			f32x_cond_e cond = inst.setcond.cond;
			b.s32.ux = ~b.s32.ux;
			c.s32.ux = 0x01010101;
			f32x__add(inst.add.msimd, &cond, a, b, c, NULL);
		}
		else if (inst.mul.prefix == F32X_OPR_PREFIX_MUL)
		{
			bool sign = ((inst.mul.op == F32X_MULTIPLY_OP_SMUL)
				|| (inst.mul.op == F32X_MULTIPLY_OP_SLMUL));
			result = f32x__mul(sign,
				inst.mul.msimd, a, b, &carry);
			carry_writeback
				= ((inst.mul.op == F32X_MULTIPLY_OP_ULMUL)
				|| (inst.mul.op == F32X_MULTIPLY_OP_SLMUL));
		}
		else if (inst.divmod.prefix == F32X_OPR_PREFIX_DIVMOD)
		{
			bool sign = ((inst.divmod.op == F32X_DIVMOD_OP_SDIV)
				|| (inst.divmod.op == F32X_DIVMOD_OP_SDIVMOD));
			result = f32x__divmod(sign,
				inst.divmod.msimd, a, b, &carry);
			carry_writeback
				= ((inst.divmod.op == F32X_DIVMOD_OP_UDIVMOD)
				|| (inst.divmod.op == F32X_DIVMOD_OP_SDIVMOD));
		}
		else
		{
			f32x__exception(core,
				F32X_EXCEPTION_INVALID_INST,
				inst.mask);
			return;
		}
	}

	/* Writeback */
	if (writeback && (inst.r.d == F32X_REG_CARRY))
	{
		carry = result;
		carry_writeback = writeback;
		writeback = false;
	}

	if (writeback)
		f32x_reg_write(core, inst.r.d, result);
	if (carry_writeback)
		f32x_reg_write(core, F32X_REG_CARRY, carry);
	core->program_counter = pc;
	if (system) f32x__system(core);
}





fct_arch_emu_t fct_arch_emu__f32x =
{
	.create_cb = (void*)f32x_core_create,
	.delete_cb = (void*)f32x_core_delete,
	.exec_cb   = (void*)f32x_core_exec,

	.mmap_read  = NULL,
	.mmap_write = NULL,

	.interrupt = (void*)f32x_core_interrupt,
};
