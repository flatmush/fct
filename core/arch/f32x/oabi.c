#include <fct/debug.h>
#include <fct/asm/context.h>
#include <fct/arch/arch.h>
#include <fct/object/format/elf.h>
#include "f32x.h"


#define F32X_OABI_REG_SP 2
#define F32X_OABI_REG_LR 3
#define F32X_OABI_REG_A0 8


static bool f32x__gen_call(
	fct_asm_context_t* context,
	fct_asm_field_t* field)
{
	if (!field)
		return false;

	fct_asm_field_t* dst
		= fct_asm_field_create_register(
			F32X_OABI_REG_LR, NULL);
	if (!dst) return false;

	fct_asm_field_t* f[2] = { dst, field };

	fct_tok_t token =
	{
		.type = FCT_TOK_IDENT,
		.base = "jl",
		.size = 2,
		.file = field->file,
		.row  = field->row,
		.col  = field->col,
	};

	bool success = f32x_assemble(
		context, &token, NULL, f, 2);
	fct_asm_field_delete(dst);

	return success;
}

static bool f32x__gen_return(fct_asm_context_t* context)
{
	fct_asm_field_t* link
		= fct_asm_field_create_register(
			F32X_OABI_REG_LR, NULL);
	if (!link) return false;

	fct_tok_t token =
	{
		.type = FCT_TOK_IDENT,
		.base = "j",
		.size = 1,
		.file = NULL,
		.row  = 0,
		.col  = 0,
	};

	bool success = f32x_assemble(
		context, &token, NULL, &link, 1);
	fct_asm_field_delete(link);

	return success;
}

static bool f32x__gen_push(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (field_count == 0)
		return true;

	if (field_count >> 10)
		return false;

	fct_asm_field_t* sp
		= fct_asm_field_create_register(
			F32X_OABI_REG_SP, NULL);
	if (!sp) return false;

	uint64_t entry_size = ((field_count * 4) ^ 0xFFFFFFFF) + 1;
	fct_asm_field_t* immediate
		= fct_asm_field_create_immediate(32, 1, &entry_size, true);
	if (!immediate)
	{
		fct_asm_field_delete(sp);
		return false;
	}

	fct_tok_t tok_add =
	{
		.type = FCT_TOK_IDENT,
		.base = "add",
		.size = 3,
		.file = (field[0] ? field[0]->file : NULL),
		.row  = (field[0] ? field[0]->row  : 0   ),
		.col  = 0,
	};

	fct_asm_field_t* f[3] = { sp, sp, immediate };
	bool success = f32x_assemble(
		context, &tok_add, NULL, f, 3);
	if (!success)
	{
		fct_asm_field_delete(immediate);
		fct_asm_field_delete(sp);
		return false;
	}

	immediate->type = FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED;
	immediate->immediate.value = (field_count - 1) * 4;

	unsigned i;
	for (i = 0; i < field_count; i++, immediate->immediate.value -= 4)
	{
		if (!field[i]
			|| (field[i]->type != FCT_ASM_FIELD_TYPE_REGISTER)
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Only untransformed registers may be pushed onto the stack.");
			fct_asm_field_delete(immediate);
			fct_asm_field_delete(sp);
			return false;
		}

		if (field[i]->reg.reg == F32X_OABI_REG_SP)
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Can't push stack pointer onto stack.");
			fct_asm_field_delete(immediate);
			fct_asm_field_delete(sp);
			return false;
		}

		fct_tok_t tok_store =
		{
			.type = FCT_TOK_IDENT,
			.base = "sw",
			.size = 2,
			.file = field[i]->file,
			.row  = field[i]->row,
			.col  = field[i]->col,
		};

		fct_asm_field_t* f[3] = { field[i], sp, immediate };
		if (!f32x_assemble(
			context, &tok_store, NULL, f, 3))
		{
			fct_asm_field_delete(immediate);
			fct_asm_field_delete(sp);
			return false;
		}
	}

	fct_asm_field_delete(immediate);
	fct_asm_field_delete(sp);
	return true;
}

static bool f32x__gen_pop(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (field_count == 0)
		return true;

	if (field_count >> 10)
		return false;

	fct_asm_field_t* sp
		= fct_asm_field_create_register(
			F32X_OABI_REG_SP, NULL);
	if (!sp) return false;

	uint64_t zero = 0;
	fct_asm_field_t* immediate
		= fct_asm_field_create_immediate(32, 1, &zero, false);
	if (!immediate)
	{
		fct_asm_field_delete(sp);
		return false;
	}

	unsigned i;
	for (i = 0; i < field_count; i++, immediate->immediate.value += 4)
	{
		if (!field[i]
			|| (field[i]->type != FCT_ASM_FIELD_TYPE_REGISTER)
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Only untransformed registers may be popped off the stack.");
			fct_asm_field_delete(immediate);
			fct_asm_field_delete(sp);
			return false;
		}

		if (field[i]->reg.reg == F32X_OABI_REG_SP)
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Can't pop stack pointer off stack.");
			fct_asm_field_delete(immediate);
			fct_asm_field_delete(sp);
			return false;
		}

		fct_tok_t tok_load =
		{
			.type = FCT_TOK_IDENT,
			.base = "lw",
			.size = 2,
			.file = field[i]->file,
			.row  = field[i]->row,
			.col  = field[i]->col,
		};

		fct_asm_field_t* f[3] = { field[i], sp, immediate };
		if (!f32x_assemble(
			context, &tok_load, NULL, f, 3))
		{
			fct_asm_field_delete(immediate);
			fct_asm_field_delete(sp);
			return false;
		}
	}

	fct_tok_t tok_add =
	{
		.type = FCT_TOK_IDENT,
		.base = "add",
		.size = 3,
		.file = field[0]->file,
		.row  = field[0]->row,
		.col  = 0,
	};

	immediate->immediate.value = (field_count * 4);

	fct_asm_field_t* f[3] = { sp, sp, immediate };
	bool success = f32x_assemble(
		context, &tok_add, NULL, f, 3);
	fct_asm_field_delete(immediate);
	fct_asm_field_delete(sp);
	return success;
}

static bool f32x__gen_syscall(
	fct_asm_context_t* context,
	fct_asm_field_t* field)
{
	if (!field)
		return false;

	fct_asm_field_t* a0
		= fct_asm_field_create_register(
			F32X_OABI_REG_A0, NULL);
	if (!a0) return false;

	fct_asm_field_t* f[2] = { a0, field };

	fct_tok_t mnem =
	{
		.type = FCT_TOK_IDENT,
		.base = "sysmov",
		.size = 6,
		.file = field->file,
		.row  = field->row,
		.col  = field->col,
	};

	bool success = f32x_assemble(
		context, &mnem, NULL, f, 2);
	fct_asm_field_delete(a0);
	return success;
}

static fct_keyval_t fct_arch_f32x__oabi_reg_names[] =
{
	{ "sp", F32X_OABI_REG_SP },
	{ "lr", F32X_OABI_REG_LR },

	{ "a0",  8 }, { "a1",  9 }, { "a2", 10 }, { "a3", 11 },
	{ "a4", 12 }, { "a5", 13 }, { "a6", 14 }, { "a7", 15 },

	{ "t0" , 16 }, { "t1" , 17 }, { "t2" , 18 }, { "t3" , 19 },
	{ "t4" , 20 }, { "t5" , 21 }, { "t6" , 22 }, { "t7" , 23 },
	{ "t8" , 24 }, { "t9" , 25 }, { "t10", 26 }, { "t11", 27 },
	{ "t12", 28 }, { "t13", 29 }, { "t14", 30 }, { "t15", 31 },

	{ "s0" , 32 }, { "s1" , 33 }, { "s2" , 34 }, { "s3" , 35 },
	{ "s4" , 36 }, { "s5" , 37 }, { "s6" , 38 }, { "s7" , 39 },
	{ "s8" , 40 }, { "s9" , 41 }, { "s10", 42 }, { "s11", 43 },
	{ "s12", 44 }, { "s13", 45 }, { "s14", 46 }, { "s15", 47 },

	{ NULL, 0 }
};

fct_arch_t fct_arch_f32x__oabi =
{
	.name      = "oabi",
	.base      = FCT_ARCH_F32X_STATIC,
	.spec      = NULL,
	.reg_names = fct_arch_f32x__oabi_reg_names,

	.elf_osabi      = ELF_OSABI_NONE,
	.elf_abiversion = 0,

	.gen_call    = f32x__gen_call,
	.gen_return  = f32x__gen_return,
	.gen_push    = f32x__gen_push,
	.gen_pop     = f32x__gen_pop,
	.gen_syscall = f32x__gen_syscall,
};

fct_arch_t* fct_arch_f32x_oabi
	= &fct_arch_f32x__oabi;
