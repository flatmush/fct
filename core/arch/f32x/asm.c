#include <stdlib.h>

#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include <fct/arch/arch.h>
#include "f32x.h"



static bool uint32_is_pow2(uint32_t x)
{
	return ((x & (x - 1)) == 0);
}



typedef enum
{
	F32X__PSATTR_NONE = 0,
	F32X__PSATTR_X64,
	F32X__PSATTR_S32,
	F32X__PSATTR_D16,
	F32X__PSATTR_Q8,
	F32X__PSATTR_POP,
	F32X__PSATTR_CLZ,
	F32X__PSATTR_CTZ,
	F32X__PSATTR_IPOP,
	F32X__PSATTR_CLO,
	F32X__PSATTR_CTO,
	F32X__PSATTR_INV,
} f32x__psattr_e;

static unsigned f32x__psattr_parse(
	const fct_tok_t* postfix,
	f32x__psattr_e* psattr)
{
	static const f32x__psattr_e attr[] =
	{
		F32X__PSATTR_X64,
		F32X__PSATTR_S32,
		F32X__PSATTR_D16,
		F32X__PSATTR_Q8,
		F32X__PSATTR_POP,
		F32X__PSATTR_CLZ,
		F32X__PSATTR_CTZ,
		F32X__PSATTR_IPOP,
		F32X__PSATTR_CLO,
		F32X__PSATTR_CTO,
		F32X__PSATTR_INV,
		F32X__PSATTR_NONE
	};

	static const char* name[] =
	{
		"x64",
		"s32",
		"d16",
		"q8",
		"pop",
		"clz",
		"ctz",
		"ipop",
		"clo",
		"cto",
		"inv",
		NULL
	};

	unsigned i;
	for (i = 0; name[i]; i++)
	{
		if (fct_tok_keyword(postfix, name[i]))
			break;
	}
	if (!name[i])
		return false;

	if (psattr)
		*psattr = attr[i];
	return true;
}

static bool f32x__psattr_to_msimd(
	f32x__psattr_e attr, f32x_msimd_e* msimd)
{
	switch (attr)
	{
		case F32X__PSATTR_X64:
			*msimd = F32X_MSIMD_X64;
			break;
		case F32X__PSATTR_NONE:
		case F32X__PSATTR_S32:
			*msimd = F32X_MSIMD_S32;
			break;
		case F32X__PSATTR_D16:
			*msimd = F32X_MSIMD_D16;
			break;
		case F32X__PSATTR_Q8:
			*msimd = F32X_MSIMD_Q8;
			break;
		default:
			return false;
	}
	return true;
}

static bool f32x__psattr_to_bit_post_op(
	f32x__psattr_e attr, f32x_bit_post_op_e* op, bool* invert)
{
	switch (attr)
	{
		case F32X__PSATTR_NONE:
		case F32X__PSATTR_X64:
		case F32X__PSATTR_S32:
		case F32X__PSATTR_D16:
		case F32X__PSATTR_Q8:
			*op = F32X_BIT_POST_OP_NOP;
			*invert = false;
			break;
		case F32X__PSATTR_INV:
			*op = F32X_BIT_POST_OP_NOP;
			*invert = true;
			break;
		case F32X__PSATTR_POP:
			*op = F32X_BIT_POST_OP_POP;
			*invert = false;
			break;
		case F32X__PSATTR_IPOP:
			*op = F32X_BIT_POST_OP_POP;
			*invert = true;
			break;
		case F32X__PSATTR_CLZ:
			*op = F32X_BIT_POST_OP_CLZ;
			*invert = false;
			break;
		case F32X__PSATTR_CLO:
			*op = F32X_BIT_POST_OP_CLZ;
			*invert = true;
			break;
		case F32X__PSATTR_CTZ:
			*op = F32X_BIT_POST_OP_CTZ;
			*invert = false;
			break;
		case F32X__PSATTR_CTO:
			*op = F32X_BIT_POST_OP_CTZ;
			*invert = true;
			break;
		default:
			return false;
	}
	return true;
}



typedef struct
{
	const fct_asm_field_t* base;

	bool          immediate;
	bool          invert, negate;
	bool          is_signed;
	bool          embed;
	f32x_msimd_e  msimd;
	f32x_reg_t    value;
	f32x_reg_e    reg;
	fct_string_t* symbol;
	bool          symbol_relative;
} f32x__field_t;

static const f32x__field_t f32x__field_default
	= { NULL, false, false, false, false, false, F32X_MSIMD_S32, { .s32.ux = 0 }, F32X_REG_ZERO, NULL, false };

static bool f32x__field_empty(f32x__field_t* field)
{
	return ((field->reg == F32X_REG_ZERO)
		&& (!field->immediate || (field->value.s32.ux == 0))
		&& !field->invert && !field->negate
		&& (field->symbol == NULL));
}

static bool f32x__field_is_long_immediate(f32x__field_t* field)
{
	if (!field)
		return false;
	return (field->immediate && !field->embed);
}

static void f32x__field_invert(f32x__field_t* field)
{
	field->is_signed = false;

	if (field->invert)
	{
		field->invert = false;
		return;
	}

	if (field->immediate
		&& !field->symbol && !field->embed)
	{
		if (field->value.s32.ux == 0xFFFFFFFF)
		{
			field->reg = F32X_REG_ZERO;
			field->immediate = false;
			return;
		}

		field->value.s32.ux = ~field->value.s32.ux;
		field->reg = (field->value.s32.ux % F32X_REG_COUNT);
		return;
	}

	field->invert = !field->invert;
}

static void f32x__field_negate(f32x__field_t* field)
{
	field->is_signed = true;

	if (field->negate)
	{
		field->negate = false;
		return;
	}

	if (field->immediate
		&& !field->symbol && !field->embed)
	{
		if (field->invert)
		{
			switch (field->msimd)
			{
				case F32X_MSIMD_Q8:
					field->value.q8.ux += 1;
					field->value.q8.uy += 1;
					field->value.q8.uz += 1;
					field->value.q8.uw += 1;
					break;
				case F32X_MSIMD_D16:
					field->value.d16.ux += 1;
					field->value.d16.uy += 1;
					break;
				default:
					field->value.s32.ux += 1;
					break;

			}
			field->reg = (field->value.s32.ux % F32X_REG_COUNT);
			field->invert = false;

			if (field->value.s32.ux == 0)
			{
				field->reg = F32X_REG_ZERO;
				field->immediate = false;
			}

			return;
		}

		switch (field->msimd)
		{
			case F32X_MSIMD_Q8:
				field->value.q8.ux = ~field->value.q8.ux + 1;
				field->value.q8.uy = ~field->value.q8.uy + 1;
				field->value.q8.uz = ~field->value.q8.uz + 1;
				field->value.q8.uw = ~field->value.q8.uw + 1;
				break;
			case F32X_MSIMD_D16:
				field->value.d16.ux = ~field->value.d16.ux + 1;
				field->value.d16.uy = ~field->value.d16.uy + 1;
				break;
			default:
				field->value.s32.ux = ~field->value.s32.ux + 1;
				break;
		}
		field->reg = (field->value.s32.ux % F32X_REG_COUNT);

		if (field->value.s32.ux == 0)
		{
			field->reg = F32X_REG_ZERO;
			field->immediate = false;
		}

		return;
	}

	field->negate = !field->negate;
}

static bool f32x__field_convert(
	fct_asm_context_t* context,
	const fct_asm_field_t* afield,
	const f32x_msimd_e* pmsimd,
	bool relative, f32x__field_t* field)
{
	if (!afield)
		return false;

	switch (afield->xform)
	{
		case FCT_ASM_FIELD_XFORM_NONE:
		case FCT_ASM_FIELD_XFORM_INVERT:
		case FCT_ASM_FIELD_XFORM_NEGATE:
			break;
		default:
			return false;
	}

	bool          immediate = false;
	bool          is_signed = false;
	f32x_msimd_e  msimd     = (pmsimd ? *pmsimd : F32X_MSIMD_S32);
	bool          invert    = (afield->xform == FCT_ASM_FIELD_XFORM_INVERT);
	bool          negate    = (afield->xform == FCT_ASM_FIELD_XFORM_NEGATE);
	f32x_reg_e    reg       = F32X_REG_ZERO;
	f32x_reg_t    value     = { .s32.ux = 0 };
	fct_string_t* symbol    = NULL;

	switch (afield->type)
	{
		case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
		case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
			switch (msimd)
			{
				case F32X_MSIMD_Q8:
					if (afield->immediate.size > 8)
						return false;
					value.q8.ux = afield->immediate.value;
					value.q8.uy = afield->immediate.value;
					value.q8.uz = afield->immediate.value;
					value.q8.uw = afield->immediate.value;
					break;
				case F32X_MSIMD_D16:
					if (afield->immediate.size > 16)
						return false;
					value.d16.ux = afield->immediate.value;
					value.d16.uy = afield->immediate.value;
					break;
				default:
					if (afield->immediate.size > 32)
						return false;
					value.s32.ux = afield->immediate.value;
					break;
			}
			immediate = (value.s32.ux != 0);
			is_signed = (afield->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED);
			break;

		case FCT_ASM_FIELD_TYPE_VECTOR_UNSIGNED:
		case FCT_ASM_FIELD_TYPE_VECTOR_SIGNED:
			if (!pmsimd)
			{
				switch (afield->vector.count)
				{
					case 2:
						msimd = F32X_MSIMD_D16;
						break;
					case 3:
					case 4:
						msimd = F32X_MSIMD_Q8;
						break;
					default:
						return false;
				}
			}

			switch (msimd)
			{
				case F32X_MSIMD_Q8:
					if ((afield->vector.size > 8)
						|| (afield->vector.count < 3)
						|| (afield->vector.count > 4))
						return false;

					value.q8.ux = afield->vector.value[0];
					value.q8.uy = afield->vector.value[1];
					value.q8.uz = afield->vector.value[2];

					value.q8.uw = 0;
					if (afield->vector.count > 3)
						value.q8.uw = afield->vector.value[3];
					break;
				case F32X_MSIMD_D16:
					if ((afield->vector.size > 16)
						|| (afield->vector.count != 2))
						return false;
					value.d16.ux = afield->vector.value[0];
					value.d16.uy = afield->vector.value[1];
					break;
				default:
					return false;
			}
			immediate = (value.s32.ux != 0);
			is_signed = (afield->type == FCT_ASM_FIELD_TYPE_VECTOR_SIGNED);
			break;

		case FCT_ASM_FIELD_TYPE_REGISTER:
			reg = afield->reg.reg;
			immediate = false;

			if (reg == F32X_REG_ZERO)
			{
				if (invert)
				{
					immediate = true;
					reg = F32X_REG_ZERO;
					value.s32.ux = 0xFFFFFFFF;
				}
				negate = false;
			}
			break;

		case FCT_ASM_FIELD_TYPE_SYMBOL:
			{
				uint64_t sval, size;
				if (fct_asm_context_symbol_resolve(
					context, afield->symbol.name, relative,
					(relative ? 4 : 0),
					&sval, &size) && (size == 4))
				{
					value.s32.ux = sval;
				}
				else
				{
					symbol = fct_string_reference(
						afield->symbol.name);
					if (!symbol) return false;
				}
			}

			immediate = true;
			break;

		default:
			return false;
	}

	if (immediate)
		reg = (value.s32.ux % F32X_REG_COUNT);

	if (field)
	{
		field->base            = afield;
		field->immediate       = immediate;
		field->embed           = false;
		field->msimd           = msimd;
		field->invert          = invert;
		field->negate          = negate;
		field->is_signed       = is_signed;
		field->value           = value;
		field->reg             = reg;
		field->symbol          = symbol;
		field->symbol_relative = relative;
	}

	return true;
}



static void f32x__bit_op_resolve_invert(
	unsigned* op, f32x__field_t* a, f32x__field_t* b)
{
	if (a->invert)
	{
		switch (*op)
		{
			case F32X_BIT_OP_AND:
				{ f32x__field_t s = *a; *a = *b; *b = s; }
				*op = F32X_BIT_OP_ANDN;
				break;
			case F32X_BIT_OP_OR:
				{ f32x__field_t s = *a; *a = *b; *b = s; }
				*op = F32X_BIT_OP_ORN;
				break;
			case F32X_BIT_OP_ANDN:
				*op = F32X_BIT_OP_OR;
				break;
			case F32X_BIT_OP_ORN:
				*op = F32X_BIT_OP_AND;
				break;
			case F32X_BIT_OP_NAND:
				*op = F32X_BIT_OP_ORN;
				break;
			case F32X_BIT_OP_NOR:
				*op = F32X_BIT_OP_ANDN;
				break;
			case F32X_BIT_OP_XOR:
				*op = F32X_BIT_OP_NXOR;
				break;
			case F32X_BIT_OP_NXOR:
				*op = F32X_BIT_OP_XOR;
				break;
			default:
				break;
		}
		a->invert = false;
	}

	if (b->invert)
	{
		switch (*op)
		{
			case F32X_BIT_OP_AND:
				*op = F32X_BIT_OP_ANDN;
				break;
			case F32X_BIT_OP_OR:
				*op = F32X_BIT_OP_ORN;
				break;
			case F32X_BIT_OP_ANDN:
				*op = F32X_BIT_OP_AND;
				break;
			case F32X_BIT_OP_ORN:
				*op = F32X_BIT_OP_OR;
				break;
			case F32X_BIT_OP_NAND:
				{ f32x__field_t s = *a; *a = *b; *b = s; }
				*op = F32X_BIT_OP_ORN;
				break;
			case F32X_BIT_OP_NOR:
				{ f32x__field_t s = *a; *a = *b; *b = s; }
				*op = F32X_BIT_OP_ANDN;
				break;
			case F32X_BIT_OP_XOR:
				*op = F32X_BIT_OP_NXOR;
				break;
			case F32X_BIT_OP_NXOR:
				*op = F32X_BIT_OP_XOR;
				break;
			default:
				break;
		}
		b->invert = false;
	}
}

static void f32x__bit_op_resolve_invert_output(
	unsigned* op, f32x__field_t* a, f32x__field_t* b)
{
	switch (*op)
	{
		case F32X_BIT_OP_AND:
			*op = F32X_BIT_OP_NAND;
			break;
		case F32X_BIT_OP_OR:
			*op = F32X_BIT_OP_NOR;
			break;
		case F32X_BIT_OP_ANDN:
			{ f32x__field_t s = *a; *a = *b; *b = s; }
			*op = F32X_BIT_OP_ORN;
			break;
		case F32X_BIT_OP_ORN:
			{ f32x__field_t s = *a; *a = *b; *b = s; }
			*op = F32X_BIT_OP_ANDN;
			break;
		case F32X_BIT_OP_NAND:
			*op = F32X_BIT_OP_AND;
			break;
		case F32X_BIT_OP_NOR:
			*op = F32X_BIT_OP_OR;
			break;
		case F32X_BIT_OP_XOR:
			*op = F32X_BIT_OP_NXOR;
			break;
		case F32X_BIT_OP_NXOR:
			*op = F32X_BIT_OP_XOR;
			break;
		default:
			break;
	}
}



static bool f32x__encode_nop(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_mov(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_inv(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_neg(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_add(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_sub(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_setcond(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_memory(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_control(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_clamp(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_abs(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_bit(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_bit_unary(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_cmov(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_ucmov(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_shift(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_shift_slr(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_multiply(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);
static bool f32x__encode_divide(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary);


typedef struct
{
	const char* mnem;
	bool        dest;
	unsigned    args_min, args_max;
	bool        relative;
	unsigned    op;

	bool (*encode)(
		const fct_tok_t* mnem,
		const fct_tok_t* postfix,
		unsigned op, f32x__psattr_e psattr,
		f32x_reg_e* d, f32x__field_t* a,
		f32x__field_t* b, f32x__field_t* c,
		f32x_inst_t* inst, bool* boundary);
} f32x__asm_inst_t;

static f32x__asm_inst_t f32x__asm_inst[] =
{
	{ "nop" , 0, 0, 0, 0, 0, f32x__encode_nop },

	{ "add" , 1, 2, 3, 0, 0, f32x__encode_add },
	{ "adds", 1, 2, 3, 0, 1, f32x__encode_add },
	{ "neg" , 1, 1, 1, 0, 0, f32x__encode_neg },
	{ "sub" , 1, 2, 3, 0, 0, f32x__encode_sub },
	{ "subs", 1, 2, 3, 0, 1, f32x__encode_sub },

	{ "slo" , 1, 2, 2, 0, 0 + F32X_COND_LOWER	  , f32x__encode_setcond },
	{ "sls" , 1, 2, 2, 0, 0 + F32X_COND_LOWER_SAME, f32x__encode_setcond },
	{ "slt" , 1, 2, 2, 0, 0 + F32X_COND_LESS_THAN , f32x__encode_setcond },
	{ "sle" , 1, 2, 2, 0, 0 + F32X_COND_LESS_EQUAL, f32x__encode_setcond },
	{ "shi" , 1, 2, 2, 0, 4 + F32X_COND_LOWER	  , f32x__encode_setcond },
	{ "shs" , 1, 2, 2, 0, 4 + F32X_COND_LOWER_SAME, f32x__encode_setcond },
	{ "sgt" , 1, 2, 2, 0, 4 + F32X_COND_LESS_THAN , f32x__encode_setcond },
	{ "sge" , 1, 2, 2, 0, 4 + F32X_COND_LESS_EQUAL, f32x__encode_setcond },

	{ "ld" , 1, 1, 2, 0, F32X_MEMORY_OP_LD , f32x__encode_memory },
	{ "lld", 1, 1, 2, 0, F32X_MEMORY_OP_LLD, f32x__encode_memory },
	{ "lw" , 1, 1, 2, 0, F32X_MEMORY_OP_LW , f32x__encode_memory },
	{ "lws", 1, 1, 2, 0, F32X_MEMORY_OP_LWS, f32x__encode_memory },
	{ "lh" , 1, 1, 2, 0, F32X_MEMORY_OP_LH , f32x__encode_memory },
	{ "lhs", 1, 1, 2, 0, F32X_MEMORY_OP_LHS, f32x__encode_memory },
	{ "lb" , 1, 1, 2, 0, F32X_MEMORY_OP_LB , f32x__encode_memory },
	{ "lbs", 1, 1, 2, 0, F32X_MEMORY_OP_LBS, f32x__encode_memory },
	{ "sd" , 0, 2, 3, 0, F32X_MEMORY_OP_SD , f32x__encode_memory },
	{ "sw" , 0, 2, 3, 0, F32X_MEMORY_OP_SW , f32x__encode_memory },
	{ "sh" , 0, 2, 3, 0, F32X_MEMORY_OP_SH , f32x__encode_memory },
	{ "sb" , 0, 2, 3, 0, F32X_MEMORY_OP_SB , f32x__encode_memory },
	{ "scd", 0, 2, 3, 0, F32X_MEMORY_OP_SCD, f32x__encode_memory },
	{ "scw", 0, 2, 3, 0, F32X_MEMORY_OP_SCW, f32x__encode_memory },
	{ "sch", 0, 2, 3, 0, F32X_MEMORY_OP_SCH, f32x__encode_memory },
	{ "scb", 0, 2, 3, 0, F32X_MEMORY_OP_SCB, f32x__encode_memory },

	{ "cld" , 1, 1, 2, 0, 0 , f32x__encode_control },
	{ "cst" , 0, 2, 3, 0, 1 , f32x__encode_control },

	{ "umin", 1, 2, 2, 0, F32X_CLAMP_OP_UMIN, f32x__encode_clamp },
	{ "umax", 1, 2, 2, 0, F32X_CLAMP_OP_UMAX, f32x__encode_clamp },
	{ "smin", 1, 2, 2, 0, F32X_CLAMP_OP_SMIN, f32x__encode_clamp },
	{ "smax", 1, 2, 2, 0, F32X_CLAMP_OP_SMAX, f32x__encode_clamp },
	{ "abs" , 1, 1, 1, 0, F32X_CLAMP_OP_SMAX, f32x__encode_abs   },
	{ "nabs", 1, 1, 1, 0, F32X_CLAMP_OP_SMIN, f32x__encode_abs   },

	{ "and"   , 1, 2, 2, 0, F32X_BIT_OP_AND , f32x__encode_bit       },
	{ "or"    , 1, 2, 2, 0, F32X_BIT_OP_OR  , f32x__encode_bit       },
	{ "andn"  , 1, 2, 2, 0, F32X_BIT_OP_ANDN, f32x__encode_bit       },
	{ "bic"   , 1, 2, 2, 0, F32X_BIT_OP_ANDN, f32x__encode_bit       },
	{ "orn"   , 1, 2, 2, 0, F32X_BIT_OP_ORN , f32x__encode_bit       },
	{ "nand"  , 1, 2, 2, 0, F32X_BIT_OP_NAND, f32x__encode_bit       },
	{ "nor"   , 1, 2, 2, 0, F32X_BIT_OP_NOR , f32x__encode_bit       },
	{ "xor"   , 1, 2, 2, 0, F32X_BIT_OP_XOR , f32x__encode_bit       },
	{ "nxor"  , 1, 2, 2, 0, F32X_BIT_OP_NXOR, f32x__encode_bit       },
	{ "xorn"  , 1, 2, 2, 0, F32X_BIT_OP_NXOR, f32x__encode_bit       },
	{ "inv"   , 1, 1, 1, 0, F32X__PSATTR_INV, f32x__encode_bit_unary },
	{ "popcnt", 1, 1, 1, 0, F32X__PSATTR_POP, f32x__encode_bit_unary },
	{ "clz"   , 1, 1, 1, 0, F32X__PSATTR_CLZ, f32x__encode_bit_unary },
	{ "clo"   , 1, 1, 1, 0, F32X__PSATTR_CLO, f32x__encode_bit_unary },
	{ "ctz"   , 1, 1, 1, 0, F32X__PSATTR_CTZ, f32x__encode_bit_unary },
	{ "cto"   , 1, 1, 1, 0, F32X__PSATTR_CTO, f32x__encode_bit_unary },

	{ "mov"   , 1, 1, 1, 0, 0                                              , f32x__encode_mov	},
	{ "moveqz", 1, 2, 2, 0, F32X_CMOV_OP_COND_MOVE_EQUAL_ZERO              , f32x__encode_cmov  },
	{ "movnez", 1, 2, 2, 0, F32X_CMOV_OP_COND_MOVE_NON_ZERO                , f32x__encode_cmov  },
	{ "movltz", 1, 2, 2, 0, F32X_CMOV_OP_COND_MOVE_LESS_THAN_ZERO          , f32x__encode_cmov  },
	{ "movgez", 1, 2, 2, 0, F32X_CMOV_OP_COND_MOVE_LESS_EQUAL_ZERO         , f32x__encode_cmov  },
	{ "adr"   , 1, 1, 1, 1, F32X_CMOV_OP_COND_MOVE_RELATIVE_EQUAL_ZERO     , f32x__encode_ucmov },
	{ "adreqz", 1, 2, 2, 1, F32X_CMOV_OP_COND_MOVE_RELATIVE_EQUAL_ZERO     , f32x__encode_cmov  },
	{ "adrnez", 1, 2, 2, 1, F32X_CMOV_OP_COND_MOVE_RELATIVE_NON_ZERO       , f32x__encode_cmov  },
	{ "adrltz", 1, 2, 2, 1, F32X_CMOV_OP_COND_MOVE_RELATIVE_LESS_THAN_ZERO , f32x__encode_cmov  },
	{ "adrgez", 1, 2, 2, 1, F32X_CMOV_OP_COND_MOVE_RELATIVE_LESS_EQUAL_ZERO, f32x__encode_cmov  },
	{ "bl"    , 1, 1, 1, 1, F32X_CMOV_OP_COND_BRANCH_LINK_EQUAL_ZERO       , f32x__encode_ucmov },
	{ "bleqz" , 1, 2, 2, 1, F32X_CMOV_OP_COND_BRANCH_LINK_EQUAL_ZERO       , f32x__encode_cmov  },
	{ "blnez" , 1, 2, 2, 1, F32X_CMOV_OP_COND_BRANCH_LINK_NON_ZERO         , f32x__encode_cmov  },
	{ "blltz" , 1, 2, 2, 1, F32X_CMOV_OP_COND_BRANCH_LINK_LESS_THAN_ZERO   , f32x__encode_cmov  },
	{ "blgez" , 1, 2, 2, 1, F32X_CMOV_OP_COND_BRANCH_LINK_LESS_EQUAL_ZERO  , f32x__encode_cmov  },
	{ "jl"    , 1, 1, 1, 0, F32X_CMOV_OP_COND_JUMP_LINK_EQUAL_ZERO         , f32x__encode_ucmov },
	{ "jleqz" , 1, 2, 2, 0, F32X_CMOV_OP_COND_JUMP_LINK_EQUAL_ZERO         , f32x__encode_cmov  },
	{ "jlnez" , 1, 2, 2, 0, F32X_CMOV_OP_COND_JUMP_LINK_NON_ZERO           , f32x__encode_cmov  },
	{ "jlltz" , 1, 2, 2, 0, F32X_CMOV_OP_COND_JUMP_LINK_LESS_THAN_ZERO     , f32x__encode_cmov  },
	{ "jlgez" , 1, 2, 2, 0, F32X_CMOV_OP_COND_JUMP_LINK_LESS_EQUAL_ZERO    , f32x__encode_cmov  },
	{ "b"     , 0, 1, 1, 1, F32X_CMOV_OP_COND_BRANCH_LINK_EQUAL_ZERO       , f32x__encode_ucmov },
	{ "beqz"  , 0, 2, 2, 1, F32X_CMOV_OP_COND_BRANCH_LINK_EQUAL_ZERO       , f32x__encode_cmov  },
	{ "bnez"  , 0, 2, 2, 1, F32X_CMOV_OP_COND_BRANCH_LINK_NON_ZERO         , f32x__encode_cmov  },
	{ "bltz"  , 0, 2, 2, 1, F32X_CMOV_OP_COND_BRANCH_LINK_LESS_THAN_ZERO   , f32x__encode_cmov  },
	{ "bgez"  , 0, 2, 2, 1, F32X_CMOV_OP_COND_BRANCH_LINK_LESS_EQUAL_ZERO  , f32x__encode_cmov  },
	{ "j"     , 0, 1, 1, 0, F32X_CMOV_OP_COND_JUMP_LINK_EQUAL_ZERO         , f32x__encode_ucmov },
	{ "jeqz"  , 0, 2, 2, 0, F32X_CMOV_OP_COND_JUMP_LINK_EQUAL_ZERO         , f32x__encode_cmov  },
	{ "jnez"  , 0, 2, 2, 0, F32X_CMOV_OP_COND_JUMP_LINK_NON_ZERO           , f32x__encode_cmov  },
	{ "jltz"  , 0, 2, 2, 0, F32X_CMOV_OP_COND_JUMP_LINK_LESS_THAN_ZERO     , f32x__encode_cmov  },
	{ "jgez"  , 0, 2, 2, 0, F32X_CMOV_OP_COND_JUMP_LINK_LESS_EQUAL_ZERO    , f32x__encode_cmov  },

	{ "sysmov"   , 1, 1, 1, 0, F32X_CMOV_OP_COND_SYS_MOVE_EQUAL_ZERO			  , f32x__encode_cmov  },
	{ "sysmoveqz", 1, 2, 2, 0, F32X_CMOV_OP_COND_SYS_MOVE_EQUAL_ZERO			  , f32x__encode_cmov  },
	{ "sysmovnez", 1, 2, 2, 0, F32X_CMOV_OP_COND_SYS_MOVE_NON_ZERO  			  , f32x__encode_cmov  },
	{ "sysmovltz", 1, 2, 2, 0, F32X_CMOV_OP_COND_SYS_MOVE_LESS_THAN_ZERO		  , f32x__encode_cmov  },
	{ "sysmovgez", 1, 2, 2, 0, F32X_CMOV_OP_COND_SYS_MOVE_LESS_EQUAL_ZERO		  , f32x__encode_cmov  },
	{ "sysadr"   , 1, 1, 1, 1, F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_EQUAL_ZERO	  , f32x__encode_ucmov },
	{ "sysadreqz", 1, 2, 2, 1, F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_EQUAL_ZERO	  , f32x__encode_cmov  },
	{ "sysadrnez", 1, 2, 2, 1, F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_NON_ZERO 	  , f32x__encode_cmov  },
	{ "sysadrltz", 1, 2, 2, 1, F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_LESS_THAN_ZERO , f32x__encode_cmov  },
	{ "sysadrgez", 1, 2, 2, 1, F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_LESS_EQUAL_ZERO, f32x__encode_cmov  },
	{ "sysbl"    , 1, 1, 1, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_EQUAL_ZERO 	  , f32x__encode_ucmov },
	{ "sysbleqz" , 1, 2, 2, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_EQUAL_ZERO 	  , f32x__encode_cmov  },
	{ "sysblnez" , 1, 2, 2, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_NON_ZERO		  , f32x__encode_cmov  },
	{ "sysblltz" , 1, 2, 2, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_LESS_THAN_ZERO   , f32x__encode_cmov  },
	{ "sysblgez" , 1, 2, 2, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_LESS_EQUAL_ZERO  , f32x__encode_cmov  },
	{ "sysjl"    , 1, 1, 1, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_EQUAL_ZERO		  , f32x__encode_ucmov },
	{ "sysjleqz" , 1, 2, 2, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_EQUAL_ZERO		  , f32x__encode_cmov  },
	{ "sysjlnez" , 1, 2, 2, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_NON_ZERO 		  , f32x__encode_cmov  },
	{ "sysjlltz" , 1, 2, 2, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_LESS_THAN_ZERO	  , f32x__encode_cmov  },
	{ "sysjlgez" , 1, 2, 2, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_LESS_EQUAL_ZERO    , f32x__encode_cmov  },
	{ "sysb"     , 0, 1, 1, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_EQUAL_ZERO 	  , f32x__encode_ucmov },
	{ "sysbeqz"  , 0, 2, 2, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_EQUAL_ZERO 	  , f32x__encode_cmov  },
	{ "sysbnez"  , 0, 2, 2, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_NON_ZERO		  , f32x__encode_cmov  },
	{ "sysbltz"  , 0, 2, 2, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_LESS_THAN_ZERO   , f32x__encode_cmov  },
	{ "sysbgez"  , 0, 2, 2, 1, F32X_CMOV_OP_COND_SYS_BRANCH_LINK_LESS_EQUAL_ZERO  , f32x__encode_cmov  },
	{ "sysj"     , 0, 1, 1, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_EQUAL_ZERO		  , f32x__encode_ucmov },
	{ "sysjeqz"  , 0, 2, 2, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_EQUAL_ZERO		  , f32x__encode_cmov  },
	{ "sysjnez"  , 0, 2, 2, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_NON_ZERO 		  , f32x__encode_cmov  },
	{ "sysjltz"  , 0, 2, 2, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_LESS_THAN_ZERO	  , f32x__encode_cmov  },
	{ "sysjgez"  , 0, 2, 2, 0, F32X_CMOV_OP_COND_SYS_JUMP_LINK_LESS_EQUAL_ZERO    , f32x__encode_cmov  },

	{ "sll" , 1, 2, 2, 0, F32X_SHIFT_OP_LEFT_LOGICAL	, f32x__encode_shift	 },
	{ "srl" , 1, 2, 2, 0, F32X_SHIFT_OP_RIGHT_LOGICAL	, f32x__encode_shift	 },
	{ "sra" , 1, 2, 2, 0, F32X_SHIFT_OP_RIGHT_ARITHMETIC, f32x__encode_shift	 },
	{ "srr" , 1, 2, 2, 0, F32X_SHIFT_OP_RIGHT_ROTATIONAL, f32x__encode_shift	 },
	{ "slr ", 1, 2, 2, 0, F32X_SHIFT_OP_RIGHT_ROTATIONAL, f32x__encode_shift_slr },

	{ "umul"   , 1, 2, 2, 0, F32X_MULTIPLY_OP_UMUL , f32x__encode_multiply },
	{ "smul"   , 1, 2, 2, 0, F32X_MULTIPLY_OP_SMUL , f32x__encode_multiply },
	{ "ulmul"  , 1, 2, 2, 0, F32X_MULTIPLY_OP_ULMUL, f32x__encode_multiply },
	{ "slmul"  , 1, 2, 2, 0, F32X_MULTIPLY_OP_SLMUL, f32x__encode_multiply },

	{ "udiv"   , 1, 2, 2, 0, F32X_DIVMOD_OP_UDIV   , f32x__encode_divide },
	{ "udivmod", 1, 2, 2, 0, F32X_DIVMOD_OP_UDIVMOD, f32x__encode_divide },
	{ "sdiv"   , 1, 2, 2, 0, F32X_DIVMOD_OP_SDIV   , f32x__encode_divide },
	{ "sdivmod", 1, 2, 2, 0, F32X_DIVMOD_OP_SDIVMOD, f32x__encode_divide },

	{ NULL, 0, 0, 0, 0, 0, NULL }
};



static bool f32x__encode_nop(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	(void)mnem;
	(void)postfix;
	(void)op;
	(void)psattr;
	(void)boundary;

	*d = F32X_REG_ZERO;
	*a = f32x__field_default;
	*b = f32x__field_default;
	*c = f32x__field_default;

	inst->mask = 0;
	return true;
}

static bool f32x__encode_mov(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (op != 0)
		return false;

	if (*d == F32X_REG_ZERO)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	if (a->immediate && !a->symbol)
	{
		if (a->invert)
			f32x__field_invert(a);
		if (a->negate)
			f32x__field_negate(a);
	}

	if (a->invert && !a->negate)
		return f32x__encode_inv(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	else if (!a->invert && a->negate)
		return f32x__encode_neg(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	else if (a->invert && a->negate)
	{
		a->invert       = false;
		a->negate       = false;
		c->immediate    = true;
		c->reg          = F32X_REG_ZERO;
		c->value.s32.ux = 1;
		return f32x__encode_add(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	}

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
	{
		FCT_DEBUG_ERROR_TOKEN(postfix,
			"Invalid attribute for instruction");
		return false;
	}

	if (!f32x__field_empty(b)
		|| !f32x__field_empty(c))
		return false;

	{ f32x__field_t swap = *a; *a = *b; *b = swap; }

	if (b->immediate && !b->symbol)
	{
		if ((b->value.s32.ux >> 13) == 0)
		{
			inst->ui.d    = *d;
			inst->ui.a    = F32X_REG_ZERO;
			inst->ui.ui   = b->value.s32.ux;
			inst->ui.op   = F32X_OPI_OR;
			inst->ui.eimm = true;
			b->embed = true;
			return true;
		}

		if ((~b->value.s32.ux >> 13) == 0)
		{
			inst->ui.d    = *d;
			inst->ui.a    = F32X_REG_ZERO;
			inst->ui.ui   = b->value.s32.ux;
			inst->ui.op   = F32X_OPI_NOR;
			inst->ui.eimm = true;
			b->embed = true;
			return true;
		}

		if ((b->value.s32.ux >> 12) == 0x000FFFFF)
		{
			inst->ui.d    = *d;
			inst->ui.a    = F32X_REG_ZERO;
			inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
			inst->ui.op   = F32X_OPI_ADD;
			inst->ui.eimm = true;
			b->embed = true;
			return true;
		}

		if (uint32_is_pow2(b->value.s32.ux))
		{
			unsigned i;
			for (i = 0; (1U << i) < b->value.s32.ux; i++);

			inst->r.d        = *d;
			inst->r.a        = F32X_REG_ZERO;
			inst->r.b        = i;
			inst->r.op       = F32X_OPR_BIT;
			inst->r.reserved = 0;
			inst->r.limm     = false;
			inst->r.eimm     = false;

			inst->bit.bit_imm = true;
			inst->bit.post_op = F32X_BIT_POST_OP_NOP;
			inst->bit.op      = F32X_BIT_OP_OR;

			b->embed = true;
			return true;
		}

		if (uint32_is_pow2(~b->value.s32.ux))
		{
			unsigned i;
			for (i = 0; (1U << i) < ~b->value.s32.ux; i++);

			inst->r.d        = *d;
			inst->r.a        = F32X_REG_ZERO;
			inst->r.b        = i;
			inst->r.op       = F32X_OPR_BIT;
			inst->r.reserved = 0;
			inst->r.limm     = false;
			inst->r.eimm     = false;

			inst->bit.bit_imm = true;
			inst->bit.post_op = F32X_BIT_POST_OP_NOP;
			inst->bit.op      = F32X_BIT_OP_NOR;

			b->embed = true;
			return true;
		}

		if ((b->value.s32.ux == 0x01010101)
			|| (b->value.s32.ux == 0x00010001)
			|| (b->value.s32.ux == 0x00000001))
		{
			inst->r.d        = *d;
			inst->r.a        = F32X_REG_ZERO;
			inst->r.b        = F32X_REG_ZERO;
			inst->r.op       = F32X_OPR_BIT;
			inst->r.reserved = 0;
			inst->r.limm     = false;
			inst->r.eimm     = false;

			inst->add.a_inv = false;
			inst->add.c_inv = true;
			inst->add.c_reg = false;
			inst->add.c_set = false;

			if (b->value.s32.ux == 0x01010101)
				inst->add.msimd = F32X_MSIMD_Q8;
			if (b->value.s32.ux == 0x00010001)
				inst->add.msimd = F32X_MSIMD_D16;
			else
				inst->add.msimd = F32X_MSIMD_S32;

			b->embed = true;
			return true;
		}

		if (b->value.s32.ux == 0x01010100)
		{
			inst->r.d        = *d;
			inst->r.a        = F32X_REG_ZERO;
			inst->r.b        = F32X_REG_ZERO;
			inst->r.op       = F32X_OPR_BIT;
			inst->r.reserved = 0;
			inst->r.limm     = false;
			inst->r.eimm     = false;

			inst->add.a_inv = true;
			inst->add.c_inv = true;
			inst->add.c_reg = false;
			inst->add.c_set = false;
			inst->add.msimd = F32X_MSIMD_Q8;

			b->embed = true;
			return true;
		}
	}

	inst->r.d        = *d;
	inst->r.a        = F32X_REG_ZERO;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_CMOV;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->cmov.cond = F32X_CMOV_COND_EQUAL_ZERO;
	inst->cmov.op   = F32X_CMOV_OP_MOVE;

	return true;
}

static bool f32x__encode_inv(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (op != 0)
		return false;

	if (*d == F32X_REG_ZERO)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	if (!f32x__field_empty(b)
		|| !f32x__field_empty(c))
		return false;

	f32x__field_invert(a);
	if (a->immediate && !a->symbol)
		return f32x__encode_mov(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);

	if (!a->invert && !a->negate)
		return f32x__encode_mov(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	else if (!a->invert && a->negate)
		return f32x__encode_neg(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	else if (a->invert && a->negate)
	{
		a->invert       = false;
		a->negate       = false;
		c->reg          = F32X_REG_ZERO;
		c->immediate    = true;
		c->value.s32.ux = 1;
		return f32x__encode_add(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	}

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = a->reg;
	inst->r.op       = F32X_OPR_BIT;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->bit.op      = F32X_BIT_OP_NAND;
	inst->bit.post_op = F32X_BIT_POST_OP_NOP;
	inst->bit.bit_imm = false;

	return true;
}

static bool f32x__encode_add(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (op >> 1)
		return false;

	if ((op == 0) && (*d == F32X_REG_ZERO))
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	bool carry_set = (op != 0);
	bool carry_reg = false;
	bool carry_inv = false;
	if (c)
	{
		if (c->negate)
		{
			FCT_DEBUG_ERROR_FIELD(c->base,
				"Can't negate carry field");
			return false;
		}

		if (c->symbol)
		{
			FCT_DEBUG_ERROR_FIELD(c->base,
				"Carry field can't be a symbol");
			return false;
		}

		if (c->reg == F32X_REG_CARRY)
		{
			carry_reg = true;
		}
		else if (c->immediate)
		{
			if (c->value.s32.ux > 1)
			{
				FCT_DEBUG_ERROR_FIELD(c->base,
					"Carry field immediate can only be 0 or 1");
				return false;
			}
			carry_inv = (c->value.s32.ux != 0);
			c->embed = true;
		}
		else if (!f32x__field_empty(c))
		{
			FCT_DEBUG_ERROR_FIELD(c->base,
				"Only $carry register can be used in carry field");
			return false;
		}

		if (c->invert)
			carry_inv = !carry_inv;
	}

	if (!carry_reg && carry_inv)
	{
		if (a->immediate && !a->symbol)
		{
			switch (msimd)
			{
				case F32X_MSIMD_Q8:
					a->value.q8.ux += 1;
					a->value.q8.uy += 1;
					a->value.q8.uz += 1;
					a->value.q8.uw += 1;
					break;
				case F32X_MSIMD_D16:
					a->value.d16.ux += 1;
					a->value.d16.uy += 1;
					break;
				default:
					a->value.s32.ux += 1;
					break;
			}
			carry_inv = false;
		}
		else if (a->immediate && !b->symbol)
		{
			switch (msimd)
			{
				case F32X_MSIMD_Q8:
					b->value.q8.ux += 1;
					b->value.q8.uy += 1;
					b->value.q8.uz += 1;
					b->value.q8.uw += 1;
					break;
				case F32X_MSIMD_D16:
					b->value.d16.ux += 1;
					b->value.d16.uy += 1;
					break;
				default:
					b->value.s32.ux += 1;
					break;
			}
			carry_inv = false;
		}
	}

	if (b->negate || b->invert)
	{
		if (a->negate || a->invert)
			return false;

		f32x__field_t swap = *a;
		*a = *b;
		*b = swap;
	}

	if (a->negate)
	{
		if (a->immediate && !a->symbol)
		{
			f32x__field_negate(a);
		}
		else
		{
			if (carry_reg || carry_inv)
				return false;
			carry_inv = true;
			a->negate = false;
			a->invert = true;
		}
	}

	if (a->immediate && !a->symbol)
	{
		if (a->invert)
			f32x__field_invert(a);

		if (b->immediate && !b->symbol)
		{
			switch (msimd)
			{
				case F32X_MSIMD_Q8:
					a->value.q8.ux += b->value.q8.ux;
					a->value.q8.uy += b->value.q8.uy;
					a->value.q8.uz += b->value.q8.uz;
					a->value.q8.uw += b->value.q8.uw;
					break;
				case F32X_MSIMD_D16:
					a->value.d16.ux += b->value.d16.ux;
					a->value.d16.uy += b->value.d16.uy;
					break;
				default:
					a->value.s32.ux += b->value.s32.ux;
					break;
			}
			*b = f32x__field_default;
		}
		else
		{
			f32x__field_t swap = *a;
			*a = *b;
			*b = swap;
		}
	}

	if (!carry_reg && !carry_inv)
	{
		if (f32x__field_empty(b))
		{
			if (a->invert)
				return f32x__encode_inv(
					mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
			else
				return f32x__encode_mov(
					mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
		}
		if (f32x__field_empty(a))
		{
			{ f32x__field_t swap = *a; *a = *b; *b = swap; }

			if (a->invert)
				return f32x__encode_inv(
					mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
			else
				return f32x__encode_mov(
					mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
		}
	}

	if (b->immediate
		&& (msimd == F32X_MSIMD_S32)
		&& !carry_set && !b->symbol && !carry_reg
		&& (carry_inv == a->invert)
		&& (((b->value.s32.ux >> 12) == 0)
			|| ((b->value.s32.ux >> 12) == 0x000FFFFF)))
	{
		inst->ui.d    = *d;
		inst->ui.a    = a->reg;
		inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
		inst->ui.op   = F32X_OPI_ADDSUB;
		inst->ui.eimm = true;

		inst->addi.a_neg = a->invert;

		b->embed = true;
		return true;
	}

	if ((a->immediate || b->immediate)
		&& ((!a->immediate && (a->reg == F32X_REG_ZERO))
			|| (!b->immediate && (b->reg == F32X_REG_ZERO))))
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Can't reference $zero and long immediate"
			" in the same instruction");
		return false;
	}

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_ADD;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->add.msimd = msimd;
	inst->add.a_inv = a->invert;
	inst->add.c_inv = carry_inv;
	inst->add.c_reg = carry_reg;
	inst->add.c_set = carry_set;

	return true;
}

static bool f32x__encode_sub(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	f32x__field_negate(b);
	return f32x__encode_add(
		mnem, postfix, op, psattr, d, a, b, c, inst, boundary);
}

static bool f32x__encode_neg(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (op > 0)
		return false;

	f32x__field_negate(a);
	if (a->immediate && !a->symbol)
		return f32x__encode_mov(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);

	if (!a->invert && !a->negate)
		return f32x__encode_mov(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	else if (!a->invert && a->negate)
		return f32x__encode_neg(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	else if (a->invert && a->negate)
	{
		a->invert = false;
		a->negate = false;
		c->immediate = true;
		c->reg = F32X_REG_ZERO;
		c->value.s32.ux = 1;
		return f32x__encode_add(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	}

	return f32x__encode_add(
		mnem, postfix, op, psattr, d, a, b, c, inst, boundary);
}

static bool f32x__encode_setcond(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (op >> 3)
		return false;

	if (op >> 2)
	{
		f32x__field_t swap = *a;
		*a = *b;
		*b = swap;
		op &= 3;
	}

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	if (a->negate || a->invert
		|| b->negate || b->invert)
		return false;

	if (!f32x__field_empty(c))
		return false;

	if (a->immediate && !a->symbol
		&& b->immediate && !b->symbol)
	{
		f32x_reg_t fi = { .s32.ux = 0 };
		switch (op)
		{
			case F32X_COND_LOWER:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = (a->value.q8.ux < b->value.q8.ux ? 1 : 0);
						fi.q8.uy = (a->value.q8.uy < b->value.q8.uy ? 1 : 0);
						fi.q8.uz = (a->value.q8.uz < b->value.q8.uz ? 1 : 0);
						fi.q8.uw = (a->value.q8.uw < b->value.q8.uw ? 1 : 0);
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = (a->value.d16.ux < b->value.d16.ux ? 1 : 0);
						fi.d16.uy = (a->value.d16.uy < b->value.d16.uy ? 1 : 0);
						break;
					default:
						fi.s32.ux = (a->value.s32.ux < b->value.s32.ux ? 1 : 0);
						break;
				}
			}
			break;

			case F32X_COND_LOWER_SAME:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = (a->value.q8.ux <= b->value.q8.ux ? 1 : 0);
						fi.q8.uy = (a->value.q8.uy <= b->value.q8.uy ? 1 : 0);
						fi.q8.uz = (a->value.q8.uz <= b->value.q8.uz ? 1 : 0);
						fi.q8.uw = (a->value.q8.uw <= b->value.q8.uw ? 1 : 0);
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = (a->value.d16.ux <= b->value.d16.ux ? 1 : 0);
						fi.d16.uy = (a->value.d16.uy <= b->value.d16.uy ? 1 : 0);
						break;
					default:
						fi.s32.ux = (a->value.s32.ux <= b->value.s32.ux ? 1 : 0);
						break;
				}
			}
			break;

			case F32X_COND_LESS_THAN:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = (a->value.q8.sx < b->value.q8.sx ? 1 : 0);
						fi.q8.uy = (a->value.q8.sy < b->value.q8.sy ? 1 : 0);
						fi.q8.uz = (a->value.q8.sz < b->value.q8.sz ? 1 : 0);
						fi.q8.uw = (a->value.q8.sw < b->value.q8.sw ? 1 : 0);
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = (a->value.d16.sx < b->value.d16.sx ? 1 : 0);
						fi.d16.uy = (a->value.d16.sy < b->value.d16.sy ? 1 : 0);
						break;
					default:
						fi.s32.ux = (a->value.s32.sx < b->value.s32.sx ? 1 : 0);
						break;
				}
			}
			break;

			default:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = (a->value.q8.sx <= b->value.q8.sx ? 1 : 0);
						fi.q8.uy = (a->value.q8.sy <= b->value.q8.sy ? 1 : 0);
						fi.q8.uz = (a->value.q8.sz <= b->value.q8.sz ? 1 : 0);
						fi.q8.uw = (a->value.q8.sw <= b->value.q8.sw ? 1 : 0);
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = (a->value.d16.sx <= b->value.d16.sx ? 1 : 0);
						fi.d16.uy = (a->value.d16.sy <= b->value.d16.sy ? 1 : 0);
						break;
					default:
						fi.s32.ux = (a->value.s32.sx <= b->value.s32.sx ? 1 : 0);
						break;
				}
			}
			break;
		}

		a->value = fi;
		*b = f32x__field_default;
		return f32x__encode_mov(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	}

	if (msimd == F32X_MSIMD_S32)
	{
		if (a->immediate && !a->symbol)
		{
			bool embed;
			switch (op)
			{
				case F32X_COND_LESS_THAN:
				case F32X_COND_LESS_EQUAL:
					embed = (((a->value.s32.ux >> 12) == 0)
							|| ((a->value.s32.ux >> 12) == 0x000FFFFF));
					break;
				default:
					embed = ((a->value.s32.ux >> 13) == 0);
					break;
			}

			if (embed)
			{
				inst->ui.d    = *d;
				inst->ui.a    = b->reg;
				inst->ui.ui   = (a->value.s32.ux & 0x1FFF);
				inst->ui.op   = F32X_OPI_SETCOND;
				inst->ui.eimm = true;

				inst->setcondi.cond = op;
				inst->setcondi.swap = true;

				a->embed = true;
				return true;
			}
		}

		if (b->immediate && !b->symbol)
		{
			bool embed;
			switch (op)
			{
				case F32X_COND_LESS_THAN:
				case F32X_COND_LESS_EQUAL:
					embed = (((b->value.s32.ux >> 12) == 0)
							|| ((b->value.s32.ux >> 12) == 0x000FFFFF));
					break;
				default:
					embed = ((b->value.s32.ux >> 13) == 0);
					break;
			}

			if (embed)
			{
				inst->ui.d    = *d;
				inst->ui.a    = a->reg;
				inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
				inst->ui.op   = F32X_OPI_SETCOND;
				inst->ui.eimm = true;

				inst->setcondi.cond = op;
				inst->setcondi.swap = false;

				b->embed = true;
				return true;
			}
		}
	}

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_SETCOND;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->setcond.cond  = op;
	inst->setcond.msimd = msimd;

	return true;
}

static bool f32x__encode_memory(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	(void)mnem;
	(void)postfix;
	(void)boundary;

	if (op > F32X_MEMORY_OP_SCB)
		return false;

	if (psattr != F32X__PSATTR_NONE)
		return false;

	bool is_store = (op >= F32X_MEMORY_OP_SW);
	if (!is_store && !f32x__field_empty(c))
		return false;
	f32x__field_t* fi = (is_store ? c : b);

	const char* lsname = (is_store ? "Store" : "Load");

	/* TODO - Handle long/relative immediates properly. */

	uint32_t offset = 0;
	if (fi->immediate)
	{
		if (fi->symbol)
		{
			FCT_DEBUG_ERROR_FIELD(fi->base,
				"%s offset may not be a symbol", lsname);
			return false;
		}

		offset = fi->value.s32.ux;
		switch (op)
		{
			case F32X_MEMORY_OP_LB:
			case F32X_MEMORY_OP_LBS:
			case F32X_MEMORY_OP_SB:
			case F32X_MEMORY_OP_SCB:
				if (offset >> 13)
				{
					FCT_DEBUG_ERROR_FIELD(fi->base,
						"%s 8-bit is out of range (%08" PRIX32 ")", lsname, offset);
					return false;
				}
				break;

			case F32X_MEMORY_OP_LH:
			case F32X_MEMORY_OP_LHS:
			case F32X_MEMORY_OP_SH:
			case F32X_MEMORY_OP_SCH:
				if (offset & 1)
				{
					FCT_DEBUG_ERROR_FIELD(fi->base,
						"%s 16-bit is unaligned (%08" PRIX32 ")", lsname, offset);
					return false;
				}
				if (offset >> 14)
				{
					FCT_DEBUG_ERROR_FIELD(fi->base,
						"%s 16-bit is out of range (%08" PRIX32 ")", lsname, offset);
					return false;
				}
				break;

			case F32X_MEMORY_OP_LW:
			case F32X_MEMORY_OP_LWS:
			case F32X_MEMORY_OP_SW:
			case F32X_MEMORY_OP_SCW:
				if (offset & 3)
				{
					FCT_DEBUG_ERROR_FIELD(fi->base,
						"%s 32-bit is unaligned (%08" PRIX32 ")", lsname, offset);
					return false;
				}
				if (offset >> 15)
				{
					FCT_DEBUG_ERROR_FIELD(fi->base,
						"%s 32-bit is out of range (%08" PRIX32 ")", lsname, offset);
					return false;
				}
				break;

			default:
				if (offset & 7)
				{
					FCT_DEBUG_ERROR_FIELD(fi->base,
						"%s 64-bit is unaligned (%08" PRIX32 ")", lsname, offset);
					return false;
				}
				if (offset >> 16)
				{
					FCT_DEBUG_ERROR_FIELD(fi->base,
						"%s 64-bit is out of range (%08" PRIX32 ")", lsname, offset);
					return false;
				}
				break;
		}
		offset |= (offset >> 13);
		offset &= 0x1FFF;

		fi->embed = true;
	}
	else if (fi->reg != F32X_REG_ZERO)
	{
		FCT_DEBUG_ERROR_FIELD(fi->base,
			"Can't use register as offset in %s",
				(is_store ? "store" : "load"));
		return false;
	}

	inst->ui.op   = F32X_OPI_MEMORY;
	inst->ui.eimm = true;
	inst->mem.op  = op;

	if (is_store)
	{
		{ f32x__field_t swap = *a; *a = *b; *b = swap; }

		inst->store.a     = a->reg;
		inst->store.b     = b->reg;
		inst->store.ui_lo = (offset & 0x3F);
		inst->store.ui_hi = (offset >> 6);
	}
	else
	{
		inst->ui.d  = *d;
		inst->ui.a  = a->reg;
		inst->ui.ui = offset;
	}

	return true;
}

static bool f32x__encode_control(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	(void)mnem;
	(void)postfix;
	(void)boundary;

	if (psattr != F32X__PSATTR_NONE)
		return false;

	if (op == 0)
	{
		uint32_t v = 0;
		if (!f32x__field_empty(c))
		{
			if (!c->immediate)
			{
				FCT_DEBUG_ERROR_FIELD(c->base,
					"Can't use register as offset in cld");
				return false;
			}

			v = c->value.s32.ux;
			if (v >> 13)
			{
				FCT_DEBUG_ERROR_FIELD(c->base,
					"Offset out of range in cld");
				return false;
			}
			c->embed = true;
		}

		inst->ui.d    = *d;
		inst->ui.a    = a->reg;
		inst->ui.ui   = (v & 0x1FFF);
		inst->ui.op   = F32X_OPI_CLD;
		inst->ui.eimm = true;

		return true;
	}
	else if (op == 1)
	{
		uint32_t v = 0;
		if (!f32x__field_empty(c))
		{
			if (!c->immediate)
			{
				FCT_DEBUG_ERROR_FIELD(c->base,
					"Can't use register as offset in cst");
				return false;
			}

			v = c->value.s32.ux;
			if (v >> 13)
			{
				FCT_DEBUG_ERROR_FIELD(c->base,
					"Offset out of range in cst");
				return false;
			}
			c->embed = true;
		}

		{ f32x__field_t swap = *a; *a = *b; *b = swap; }

		inst->store.ui_lo = v & 0x3F;
		inst->store.a     = a->reg;
		inst->store.b     = b->reg;
		inst->store.ui_hi = (v >> 6) & 0x7F;

		inst->ui.op   = F32X_OPI_CST;
		inst->ui.eimm = true;

		return true;
	}

	return false;
}

static bool f32x__encode_clamp(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	(void)boundary;

	if (op >> 2)
		return false;

	if (!f32x__field_empty(c))
		return false;

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	if (*d == F32X_REG_ZERO)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	if (a->immediate && !a->symbol
		&& b->immediate && !b->symbol)
	{
		f32x_reg_t fi;
		switch (op)
		{
			case F32X_CLAMP_OP_SMIN:
			case F32X_CLAMP_OP_SMAX:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.sx = (a->value.q8.sx < b->value.q8.sx
							? a->value.q8.sx : b->value.q8.sx);
						fi.q8.sy = (a->value.q8.sy < b->value.q8.sy
							? a->value.q8.sy : b->value.q8.sy);
						fi.q8.sz = (a->value.q8.sz < b->value.q8.sz
							? a->value.q8.sz : b->value.q8.sz);
						fi.q8.sw = (a->value.q8.sw < b->value.q8.sw
							? a->value.q8.sw : b->value.q8.sw);
						break;
					case F32X_MSIMD_D16:
						fi.d16.sx = (a->value.d16.sx < b->value.d16.sx
							? a->value.d16.sx : b->value.d16.sx);
						fi.d16.sy = (a->value.d16.sy < b->value.d16.sy
							? a->value.d16.sy : b->value.d16.sy);
						break;
					default:
						fi.s32.sx = (a->value.s32.sx < b->value.s32.sx
							? a->value.s32.sx : b->value.s32.sx);
						break;
				}
			}
			break;

			default:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = (a->value.q8.ux < b->value.q8.ux
							? a->value.q8.ux : b->value.q8.ux);
						fi.q8.uy = (a->value.q8.uy < b->value.q8.uy
							? a->value.q8.uy : b->value.q8.uy);
						fi.q8.uz = (a->value.q8.uz < b->value.q8.uz
							? a->value.q8.uz : b->value.q8.uz);
						fi.q8.uw = (a->value.q8.uw < b->value.q8.uw
							? a->value.q8.uw : b->value.q8.uw);
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = (a->value.d16.ux < b->value.d16.ux
							? a->value.d16.ux : b->value.d16.ux);
						fi.d16.uy = (a->value.d16.uy < b->value.d16.uy
							? a->value.d16.uy : b->value.d16.uy);
						break;
					default:
						fi.s32.ux = (a->value.s32.ux < b->value.s32.ux
							? a->value.s32.ux : b->value.s32.ux);
						break;
				}
			}
			break;
		}

		a->value = fi;
		a->msimd = msimd;
		*b = f32x__field_default;
		return f32x__encode_mov(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	}

	if (a->invert || b->invert)
		return false;

	if (a->negate && b->negate)
		return false;

	if (b->negate)
	{
		if (a->negate)
			return false;

		f32x__field_t swap = *a;
		*a = *b;
		*b = swap;
	}

	if (a->immediate && !a->symbol
		&& (!b->immediate || b->symbol))
	{
		f32x__field_t swap = *a;
		*a = *b;
		*b = swap;
	}

	bool embed = (b->immediate
		&& (msimd == F32X_MSIMD_S32) && !b->symbol && !a->negate);

	if (embed)
	{
		switch (op)
		{
			case F32X_CLAMP_OP_SMIN:
			case F32X_CLAMP_OP_SMAX:
				embed = (((b->value.s32.ux >> 12) == 0)
						|| ((b->value.s32.ux >> 12) == 0x000FFFFF));
				break;
			default:
				embed = ((b->value.s32.ux >> 13) == 0);
				break;
		}
	}

	if (embed)
	{
		inst->ui.d    = *d;
		inst->ui.a    = a->reg;
		inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
		inst->ui.op   = F32X_OPI_CLAMP;
		inst->ui.eimm = true;

		inst->clampi.op = op;

		b->embed = true;
		return true;
	}

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_CLAMP;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->clamp.msimd = msimd;
	inst->clamp.a_neg = a->negate;
	inst->clamp.op    = op;

	return true;
}

static bool f32x__encode_abs(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (!f32x__field_empty(b)
		|| !f32x__field_empty(c))
		return false;

	*b = *a;
	f32x__field_negate(b);

	return f32x__encode_clamp(
		mnem, postfix, op, psattr, d, a, b, c, inst, boundary);
}

static bool f32x__encode_bit(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (op >> 3)
		return false;

	if ((op == 0) && (*d == F32X_REG_ZERO))
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	if (!f32x__field_empty(c))
		return false;

	bool invert;
	f32x_bit_post_op_e post_op;
	if (!f32x__psattr_to_bit_post_op(psattr, &post_op, &invert))
		return false;

	if (a->negate || b->negate)
		return false;

	if (a->immediate && !a->symbol
		&& (~a->value.s32.ux < a->value.s32.ux))
	{
		a->value.s32.ux = ~a->value.s32.ux;
		a->invert = !a->invert;
	}

	if (b->immediate && !b->symbol
		&& (~b->value.s32.ux < b->value.s32.ux))
	{
		b->value.s32.ux = ~b->value.s32.ux;
		b->invert = !b->invert;
	}

	if (a->immediate && !a->symbol
		&& uint32_is_pow2(~a->value.s32.ux))
	{
		a->value.s32.ux = ~a->value.s32.ux;
		a->invert = !a->invert;
	}

	if (b->immediate && !b->symbol
		&& uint32_is_pow2(~b->value.s32.ux))
	{
		b->value.s32.ux = ~b->value.s32.ux;
		b->invert = !b->invert;
	}

	f32x__bit_op_resolve_invert(&op, a, b);
	if (invert)
		f32x__bit_op_resolve_invert_output(&op, a, b);

	if (f32x__field_empty(b))
		*b = f32x__field_default;

	if (f32x__field_empty(a))
	{
		switch (op)
		{
			case F32X_BIT_OP_AND:
				*a = f32x__field_default;
				return f32x__encode_mov(
					mnem, postfix, 0, F32X__PSATTR_NONE, d, a, b, c, inst, boundary);
			case F32X_BIT_OP_ORN:
			case F32X_BIT_OP_NAND:
				*a = f32x__field_default;
				a->immediate = true;
				a->reg = F32X_REG_ZERO;
				a->value.s32.ux = 0xFFFFFFFF;
				return f32x__encode_mov(
					mnem, postfix, 0, F32X__PSATTR_NONE, d, a, b, c, inst, boundary);
			case F32X_BIT_OP_NOR:
			case F32X_BIT_OP_NXOR:
				return f32x__encode_inv(
					mnem, postfix, 0, F32X__PSATTR_NONE, d, a, b, c, inst, boundary);
			default:
				return f32x__encode_mov(
					mnem, postfix, 0, F32X__PSATTR_NONE, d, a, b, c, inst, boundary);
		}
	}

	if (a->immediate && !a->symbol)
	{
		if (b->immediate && !b->symbol)
		{
			switch (op)
			{
				case F32X_BIT_OP_AND:
					a->value.s32.ux =  (a->value.s32.ux &  b->value.s32.ux);
					break;
				case F32X_BIT_OP_OR:
					a->value.s32.ux =  (a->value.s32.ux |  b->value.s32.ux);
					break;
				case F32X_BIT_OP_ANDN:
					a->value.s32.ux =  (a->value.s32.ux & ~b->value.s32.ux);
					break;
				case F32X_BIT_OP_ORN:
					a->value.s32.ux =  (a->value.s32.ux | ~b->value.s32.ux);
					break;
				case F32X_BIT_OP_NAND:
					a->value.s32.ux = ~(a->value.s32.ux &  b->value.s32.ux);
					break;
				case F32X_BIT_OP_NOR:
					a->value.s32.ux = ~(a->value.s32.ux |  b->value.s32.ux);
					break;
				case F32X_BIT_OP_XOR:
					a->value.s32.ux =  (a->value.s32.ux ^  b->value.s32.ux);
					break;
				default:
					a->value.s32.ux = ~(a->value.s32.ux ^  b->value.s32.ux);
					break;
			}
			*b = f32x__field_default;
		}
		else
		{
			f32x__field_t swap = *a;
			*a = *b;
			*b = swap;
		}
	}

	bool field_swap = false;
	if (a->immediate && !a->symbol)
	{
		if (b->immediate && !b->symbol)
			field_swap = (a->value.s32.ux < b->value.s32.ux);
		else
			field_swap = true;
	}

	switch (op)
	{
		case F32X_BIT_OP_ANDN:
		case F32X_BIT_OP_ORN:
			field_swap = false;
			break;
		default:
			break;
	}

	if (field_swap)
	{
		f32x__field_t swap = *a;
		*a = *b;
		*b = swap;
	}

	if (b->immediate && !b->symbol)
	{
		if ((post_op == F32X_BIT_POST_OP_NOP)
			&& ((b->value.s32.ux >> 13) == 0))
		{
			inst->ui.d    = *d;
			inst->ui.a    = a->reg;
			inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
			inst->ui.op   = F32X_OPI_BIT;
			inst->ui.eimm = true;

			inst->biti.op = op;

			b->embed = true;
			return true;
		}

		if (uint32_is_pow2(b->value.s32.ux))
		{
			unsigned i;
			for (i = 0; (i < 32) && ((1U << i) < b->value.s32.ux); i++);

			inst->r.d        = *d;
			inst->r.a        = a->reg;
			inst->r.b        = i;
			inst->r.op       = F32X_OPR_BIT;
			inst->r.reserved = 0;
			inst->r.limm     = false;
			inst->r.eimm     = false;

			inst->bit.op      = op;
			inst->bit.post_op = post_op;
			inst->bit.bit_imm = true;

			return true;
		}
	}

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_BIT;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->bit.op      = op;
	inst->bit.post_op = post_op;
	inst->bit.bit_imm = false;

	return true;
}

static bool f32x__encode_bit_unary(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (!f32x__field_empty(b)
		|| !f32x__field_empty(c))
		return false;

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	bool invert;
	f32x_bit_post_op_e post_op;
	if (!f32x__psattr_to_bit_post_op(op, &post_op, &invert))
		return false;

	return f32x__encode_bit(
		mnem, postfix,
		(invert ? F32X_BIT_OP_NOR : F32X_BIT_OP_OR),
		op, d, a, b, c, inst, boundary);
}

static bool f32x__encode_cmov(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (op >> 5)
		return false;

	f32x_cmov_cond_e cond = (op & 3);
	op >>= 2;

	bool system = ((op & 4) != 0);
	op &= 3;

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	if (!f32x__field_empty(c))
		return false;

	if (f32x__field_empty(b)
		&& !system
		&& (op == F32X_CMOV_OP_BRANCH_LINK)
		&& (*d == F32X_REG_ZERO))
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	if (a->symbol)
	{
		FCT_DEBUG_ERROR_FIELD(a->base,
			"Can't use symbol in condition since boundaries can't be resolved");
		return false;
	}

	bool is_nop = false;
	bool unconditional = false;
	if (f32x__field_empty(a))
	{
		unconditional = true;
		is_nop = ((cond & 1) == 0);
	}
	else if (a->immediate)
	{
		unconditional = true;
		switch (cond)
		{
			case F32X_CMOV_COND_NON_ZERO:
				is_nop = (a->value.s32.ux == 0);
				break;
			case F32X_CMOV_COND_EQUAL_ZERO:
				is_nop = (a->value.s32.ux != 0);
				break;
			case F32X_CMOV_COND_LESS_THAN_ZERO:
				is_nop = (a->value.s32.sx >= 0);
				break;
			default:
				is_nop = (a->value.s32.sx > 0);
				break;
		}
	}

	if (unconditional)
		*a = f32x__field_default;

	if (is_nop && !system)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect, condition is never true");
		return false;
	}
	if (unconditional && !system)
	{
		*a = f32x__field_default;

		if (op == F32X_CMOV_OP_MOVE)
		{
			{ f32x__field_t swap = *a; *a = *b; *b = swap; }
			return f32x__encode_mov(
				mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
		}
	}

	if (a->invert || a->negate
		|| b->invert || b->negate)
		return false;

	if (msimd != F32X_MSIMD_S32)
	{
		FCT_DEBUG_ERROR_TOKEN(postfix,
			"Can't conditionally move vectors");
		return false;
	}

	if (b->symbol && ((op >> 1) == 0))
		b->symbol_relative = true;

	bool has_link = false;
	switch (op)
	{
		case F32X_CMOV_OP_BRANCH_LINK:
		case F32X_CMOV_OP_JUMP_LINK:
			has_link = (*d != F32X_REG_ZERO);
			break;
		default:
			break;
	}

	if (unconditional && !has_link && boundary)
		*boundary = true;

	bool embed = !system && (b->immediate && !b->symbol);
	if (embed)
	{
		switch (op)
		{
			case F32X_CMOV_OP_MOVE_RELATIVE:
				embed = (((b->value.s32.ux >> 12) == 0)
					|| ((b->value.s32.ux >> 12) == 0x000FFFFF));
				break;
			case F32X_CMOV_OP_BRANCH_LINK:
				if (((b->value.s32.ux & 3) == 0)
					&& (((b->value.s32.ux >> 14) == 0)
						|| ((b->value.s32.ux >> 14) == 0x0003FFFF)))
					b->value.s32.ux >>= 2;
				else
					embed = false;
				break;
			default:
				embed = false;
				break;
		}
	}
	if (embed)
	{
		inst->ui.d    = *d;
		inst->ui.a    = a->reg;
		inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
		inst->ui.op   = F32X_OPI_CMOV;
		inst->ui.eimm = true;
		inst->cmovi.op   = op;
		inst->cmovi.cond = cond;

		b->embed = true;
		return true;
	}

	if (f32x__field_is_long_immediate(b)
		&& ((op == F32X_CMOV_OP_MOVE_RELATIVE)
			|| (op == F32X_CMOV_OP_BRANCH_LINK)))
		b->value.s32.ux -= 4;

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_CMOV;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->cmov.op     = op;
	inst->cmov.cond   = cond;
	inst->cmov.system = system;

	return true;
}

static bool f32x__encode_ucmov(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (!f32x__field_empty(b)
		|| !f32x__field_empty(c))
		return false;

	*b = *a;
	*a = f32x__field_default;
	return f32x__encode_cmov(
		mnem, postfix, op, psattr, d, a, b, c, inst, boundary);
}

static bool f32x__encode_shift(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	if (op >> 2)
		return false;

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	if (*d == F32X_REG_ZERO)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	if (b->immediate && !b->symbol)
	{
		switch (op)
		{
			case F32X_SHIFT_OP_LEFT_LOGICAL:
			case F32X_SHIFT_OP_RIGHT_LOGICAL:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						if ((b->value.q8.ux >= 8)
							&& (b->value.q8.uy >= 8)
							&& (b->value.q8.uz >= 8)
							&& (b->value.q8.uw >= 8))
						{
							*a = f32x__field_default;
							*b = f32x__field_default;
							return f32x__encode_mov(
								mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
						}
						break;
					case F32X_MSIMD_D16:
						if ((b->value.d16.ux >= 16)
							&& (b->value.d16.uy >= 16))
						{
							*a = f32x__field_default;
							*b = f32x__field_default;
							return f32x__encode_mov(
								mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
						}
						break;
					default:
						if (b->value.s32.ux >= 32)
						{
							*a = f32x__field_default;
							*b = f32x__field_default;
							return f32x__encode_mov(
								mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
						}
						break;
				}
			}
			break;

			case F32X_SHIFT_OP_RIGHT_ARITHMETIC:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						if (b->value.q8.ux > 7)
							b->value.q8.ux = 7;
						if (b->value.q8.uy > 7)
							b->value.q8.uy = 7;
						if (b->value.q8.uz > 7)
							b->value.q8.uz = 7;
						if (b->value.q8.uw > 7)
							b->value.q8.uw = 7;
						break;
					case F32X_MSIMD_D16:
						if (b->value.d16.ux > 15)
							b->value.d16.ux = 15;
						if (b->value.d16.uy > 15)
							b->value.d16.uy = 15;
						break;
					default:
						if (b->value.s32.ux > 31)
							b->value.s32.ux = 31;
						break;
				}
			}
			break;

			default:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						b->value.q8.ux %= 8;
						b->value.q8.uy %= 8;
						b->value.q8.uz %= 8;
						b->value.q8.uw %= 8;
						break;
					case F32X_MSIMD_D16:
						b->value.d16.ux %= 16;
						b->value.d16.uy %= 16;
						break;
					default:
						b->value.s32.ux %= 32;
						break;
				}
			}
			break;
		}
	}

	if (a->immediate && !a->symbol
		&& b->immediate && !b->symbol)
	{
		f32x_reg_t fi;
		switch (op)
		{
			case F32X_SHIFT_OP_LEFT_LOGICAL:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = a->value.q8.ux << b->value.q8.ux;
						fi.q8.uy = a->value.q8.uy << b->value.q8.uy;
						fi.q8.uz = a->value.q8.uz << b->value.q8.uz;
						fi.q8.uw = a->value.q8.uw << b->value.q8.uw;
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = a->value.d16.ux << b->value.d16.ux;
						fi.d16.uy = a->value.d16.uy << b->value.d16.uy;
						break;
					default:
						fi.s32.ux = a->value.s32.ux << b->value.s32.ux;
						break;
				}
			}
			break;

			case F32X_SHIFT_OP_RIGHT_LOGICAL:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = a->value.q8.ux >> b->value.q8.ux;
						fi.q8.uy = a->value.q8.uy >> b->value.q8.uy;
						fi.q8.uz = a->value.q8.uz >> b->value.q8.uz;
						fi.q8.uw = a->value.q8.uw >> b->value.q8.uw;
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = a->value.d16.ux >> b->value.d16.ux;
						fi.d16.uy = a->value.d16.uy >> b->value.d16.uy;
						break;
					default:
						fi.s32.ux = a->value.s32.ux >> b->value.s32.ux;
						break;
				}
			}
			break;

			case F32X_SHIFT_OP_RIGHT_ARITHMETIC:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.sx = a->value.q8.sx >> b->value.q8.sx;
						fi.q8.sy = a->value.q8.sy >> b->value.q8.sy;
						fi.q8.sz = a->value.q8.sz >> b->value.q8.sz;
						fi.q8.sw = a->value.q8.sw >> b->value.q8.sw;
						break;
					case F32X_MSIMD_D16:
						fi.d16.sx = a->value.d16.sx >> b->value.d16.sx;
						fi.d16.sy = a->value.d16.sy >> b->value.d16.sy;
						break;
					default:
						fi.s32.sx = a->value.s32.sx >> b->value.s32.sx;
						break;
				}
			}
			break;

			default:
			{
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux  = a->value.q8.ux >> b->value.q8.ux;
						fi.q8.uy  = a->value.q8.uy >> b->value.q8.uy;
						fi.q8.uz  = a->value.q8.uz >> b->value.q8.uz;
						fi.q8.uw  = a->value.q8.uw >> b->value.q8.uw;
						fi.q8.ux |= a->value.q8.ux << (8 - b->value.q8.ux);
						fi.q8.uy |= a->value.q8.uy << (8 - b->value.q8.uy);
						fi.q8.uz |= a->value.q8.uz << (8 - b->value.q8.uz);
						fi.q8.uw |= a->value.q8.uw << (8 - b->value.q8.uw);
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux  = a->value.d16.ux >> b->value.d16.ux;
						fi.d16.uy  = a->value.d16.uy >> b->value.d16.uy;
						fi.d16.ux |= a->value.d16.ux << (16 - b->value.d16.ux);
						fi.d16.uy |= a->value.d16.uy << (16 - b->value.d16.uy);
						break;
					default:
						fi.s32.ux  = a->value.s32.ux >> b->value.s32.ux;
						fi.s32.ux |= a->value.s32.ux << (32 - b->value.s32.ux);
						break;
				}
			}
			break;
		}

		a->value = fi;
		a->msimd = (f32x_msimd_e)msimd;
		*b = f32x__field_default;
		return f32x__encode_mov(
			mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
	}

	bool embed = (b->immediate && !b->symbol);
	if (embed)
	{
		switch (msimd)
		{
			case F32X_MSIMD_Q8:
				embed = ((b->value.q8.ux == b->value.q8.uy)
					&& (b->value.q8.ux == b->value.q8.uz)
					&& (b->value.q8.ux == b->value.q8.uw));
				break;
			case F32X_MSIMD_D16:
				embed = (b->value.d16.ux == b->value.d16.uy);
				break;
			default:
				break;
		}
	}

	if (embed)
	{
		if (((b->reg == F32X_REG_ZERO)
			|| (b->value.s32.ux == 0)))
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Instruction has no effect");
			return false;
		}

		inst->r.d        = *d;
		inst->r.a        = a->reg;
		inst->r.b        = (b->value.s32.ux % F32X_REG_COUNT);
		inst->r.op       = F32X_OPR_SHIFT;
		inst->r.reserved = 0;
	    inst->r.limm     = false;
		inst->r.eimm     = false;

		inst->shift.msimd  = msimd;
		inst->shift.b_imm = true;
		inst->shift.op    = op;

		b->embed = true;
		return true;
	}

	embed = (a->immediate
		&& !a->symbol && (msimd == F32X_MSIMD_S32));
	if (embed)
	{
		switch (op)
		{
			case F32X_SHIFT_OP_RIGHT_ARITHMETIC:
				embed = (((a->value.s32.ux >> 12) == 0)
					|| ((a->value.s32.ux >> 12) == 0x000FFFFF));
				break;
			default:
				embed = ((a->value.s32.ux >> 13) == 0);
				break;
		}
	}

	if (embed)
	{
		inst->ui.d    = *d;
		inst->ui.a    = b->reg;
		inst->ui.ui   = (a->value.s32.ux & 0x1FFF);
		inst->ui.op   = F32X_OPI_SHIFT;
		inst->ui.eimm = true;

		inst->shift.op = op;

		a->embed = true;
		return true;
	}

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_SHIFT;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->shift.msimd = msimd;
	inst->shift.b_imm = false;
	inst->shift.op    = op;

	return true;
}

static bool f32x__encode_shift_slr(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	if (b->immediate)
	{
		if (b->symbol)
			return false;

		switch (msimd)
		{
			case F32X_MSIMD_Q8:
				b->value.q8.ux = (8 - b->value.q8.ux) % 8;
				b->value.q8.uy = (8 - b->value.q8.uy) % 8;
				b->value.q8.uz = (8 - b->value.q8.uz) % 8;
				b->value.q8.uw = (8 - b->value.q8.uw) % 8;
				break;
			case F32X_MSIMD_D16:
				b->value.d16.ux = (16 - b->value.d16.ux) % 16;
				b->value.d16.uy = (16 - b->value.d16.ux) % 16;
				break;
			default:
				b->value.s32.ux = (32 - b->value.s32.ux) % 32;
				break;
		}
	}
	else if (b->reg != F32X_REG_ZERO)
		return false;

	return f32x__encode_shift(
		mnem, postfix, op, psattr, d, a, b, c, inst, boundary);
}

static bool f32x__encode_multiply(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	(void)boundary;

	if (op >> 2)
		return false;

	if (!f32x__field_empty(c))
		return false;

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	if (((op >> 1) == 0) && (*d == F32X_REG_ZERO))
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	if (a->immediate && !a->symbol
		&& b->immediate && !b->symbol)
	{
		switch (op)
		{
			case F32X_MULTIPLY_OP_UMUL:
			{
				f32x_reg_t fi;
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = a->value.q8.ux * b->value.q8.ux;
						fi.q8.uy = a->value.q8.uy * b->value.q8.uy;
						fi.q8.uz = a->value.q8.uz * b->value.q8.uz;
						fi.q8.uw = a->value.q8.uw * b->value.q8.uw;
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = a->value.d16.ux * b->value.d16.ux;
						fi.d16.uy = a->value.d16.uy * b->value.d16.uy;
						break;
					default:
						fi.s32.ux = a->value.s32.ux * b->value.s32.ux;
						break;
				}

				a->value = fi;
				a->msimd = msimd;
				*b = f32x__field_default;
				return f32x__encode_mov(
					mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
			}
			break;

			case F32X_MULTIPLY_OP_SMUL:
			{
				f32x_reg_t fi;
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.sx = a->value.q8.sx * b->value.q8.sx;
						fi.q8.sy = a->value.q8.sy * b->value.q8.sy;
						fi.q8.sz = a->value.q8.sz * b->value.q8.sz;
						fi.q8.sw = a->value.q8.sw * b->value.q8.sw;
						break;
					case F32X_MSIMD_D16:
						fi.d16.sx = a->value.d16.sx * b->value.d16.sx;
						fi.d16.sy = a->value.d16.sy * b->value.d16.sy;
						break;
					default:
						fi.s32.sx = a->value.s32.sx * b->value.s32.sx;
						break;
				}

				a->value = fi;
				a->msimd = msimd;
				*b = f32x__field_default;
				return f32x__encode_mov(
					mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
			}
			break;

			default:
				break;
		}
	}

	if (a->invert || a->negate
		|| b->invert || b->negate)
		return false;

	if (a->immediate && !a->symbol
		&& (!b->immediate || b->symbol))
	{
		f32x__field_t swap = *a;
		*a = *b;
		*b = swap;
	}

	bool embed = (b->immediate
		&& (msimd == F32X_MSIMD_S32) && !b->symbol);
	if (embed)
	{
		switch (op)
		{
			case F32X_MULTIPLY_OP_UMUL:
				embed = ((b->value.s32.ux >> 13) == 0);
				break;
			case F32X_MULTIPLY_OP_SMUL:
				embed = (((b->value.s32.ux >> 12) == 0)
						|| ((b->value.s32.ux >> 12) == 0x000FFFFF));
				break;
			default:
				embed = false;
				break;
		}
	}

	if (embed)
	{
		inst->ui.d    = *d;
		inst->ui.a    = a->reg;
		inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
		inst->ui.op   = F32X_OPI_MUL;
		inst->ui.eimm = true;

		inst->muli.op = op;

		b->embed = true;
		return true;
	}

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_MUL;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->mul.msimd = msimd;
	inst->mul.op    = op;

	return true;
}

static bool f32x__encode_divide(
	const fct_tok_t* mnem,
	const fct_tok_t* postfix,
	unsigned op, f32x__psattr_e psattr,
	f32x_reg_e* d, f32x__field_t* a, f32x__field_t* b, f32x__field_t* c,
	f32x_inst_t* inst, bool* boundary)
{
	(void)boundary;

	if (op >> 2)
		return false;

	if (!f32x__field_empty(c))
		return false;

	f32x_msimd_e msimd;
	if (!f32x__psattr_to_msimd(psattr, &msimd))
		return false;

	if (((op >> 1) == 0) && (*d == F32X_REG_ZERO))
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Instruction has no effect");
		return false;
	}

	if (a->immediate && !a->symbol
		&& b->immediate && !b->symbol)
	{
		switch (op)
		{
			case F32X_DIVMOD_OP_UDIV:
			{
				f32x_reg_t fi;
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.ux = a->value.q8.ux / b->value.q8.ux;
						fi.q8.uy = a->value.q8.uy / b->value.q8.uy;
						fi.q8.uz = a->value.q8.uz / b->value.q8.uz;
						fi.q8.uw = a->value.q8.uw / b->value.q8.uw;
						break;
					case F32X_MSIMD_D16:
						fi.d16.ux = a->value.d16.ux / b->value.d16.ux;
						fi.d16.uy = a->value.d16.uy / b->value.d16.uy;
						break;
					default:
						fi.s32.ux = a->value.s32.ux / b->value.s32.ux;
						break;
				}

				a->value = fi;
				a->msimd = msimd;
				*b = f32x__field_default;
				return f32x__encode_mov(
					mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
			}
			break;

			case F32X_DIVMOD_OP_SDIV:
			{
				f32x_reg_t fi;
				switch (msimd)
				{
					case F32X_MSIMD_Q8:
						fi.q8.sx = a->value.q8.sx / b->value.q8.sx;
						fi.q8.sy = a->value.q8.sy / b->value.q8.sy;
						fi.q8.sz = a->value.q8.sz / b->value.q8.sz;
						fi.q8.sw = a->value.q8.sw / b->value.q8.sw;
						break;
					case F32X_MSIMD_D16:
						fi.d16.sx = a->value.d16.sx / b->value.d16.sx;
						fi.d16.sy = a->value.d16.sy / b->value.d16.sy;
						break;
					default:
						fi.s32.sx = a->value.s32.sx / b->value.s32.sx;
						break;
				}

				a->value = fi;
				a->msimd = msimd;
				*b = f32x__field_default;
				return f32x__encode_mov(
					mnem, postfix, 0, psattr, d, a, b, c, inst, boundary);
			}
			break;

			default:
				break;
		}
	}

	if (a->invert || a->negate
		|| b->invert || b->negate)
		return false;

	bool embed = (b->immediate
		&& (msimd == F32X_MSIMD_S32) && !b->symbol);
	if (embed)
	{
		switch (op)
		{
			case F32X_DIVMOD_OP_SDIV:
			case F32X_DIVMOD_OP_SDIVMOD:
				embed = (((b->value.s32.ux >> 12) == 0)
						|| ((b->value.s32.ux >> 12) == 0x000FFFFF));
				break;
			default:
				embed = ((b->value.s32.ux >> 13) == 0);
				break;
		}
	}

	if (embed)
	{
		inst->ui.d    = *d;
		inst->ui.a    = a->reg;
		inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
		inst->ui.op   = F32X_OPI_DIVMOD;
		inst->ui.eimm = true;

		inst->divmodi.op   = op;
		inst->divmodi.swap = false;

		b->embed = true;
		return true;
	}

	embed = (a->immediate
		&& (msimd == F32X_MSIMD_S32) && !a->symbol);
	if (embed)
	{
		switch (op)
		{
			case F32X_DIVMOD_OP_SDIV:
			case F32X_DIVMOD_OP_SDIVMOD:
				embed = (((a->value.s32.ux >> 12) == 0)
						|| ((a->value.s32.ux >> 12) == 0x000FFFFF));
				break;
			default:
				embed = ((a->value.s32.ux >> 13) == 0);
				break;
		}
	}

	if (embed)
	{
		{ f32x__field_t swap = *a; *a = *b; *b = swap; }

		inst->ui.d    = *d;
		inst->ui.a    = a->reg;
		inst->ui.ui   = (b->value.s32.ux & 0x1FFF);
		inst->ui.op   = F32X_OPI_DIVMOD;
		inst->ui.eimm = true;

		inst->divmodi.op   = op;
		inst->divmodi.swap = true;

		a->embed = true;
		return true;
	}

	inst->r.d        = *d;
	inst->r.a        = a->reg;
	inst->r.b        = b->reg;
	inst->r.op       = F32X_OPR_DIVMOD;
	inst->r.reserved = 0;
	inst->r.limm     = false;
	inst->r.eimm     = false;

	inst->divmod.msimd = msimd;
	inst->divmod.op    = op;

	return true;
}



bool f32x_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (!context)
		return false;

	unsigned i;
	for (i = 0; f32x__asm_inst[i].mnem; i++)
	{
		if (fct_tok_keyword(mnem, f32x__asm_inst[i].mnem))
			break;
	}
	if (!f32x__asm_inst[i].mnem)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Unrecognized instruction mnemonic");
		return false;
	}
	f32x__asm_inst_t* asm_inst
		= &f32x__asm_inst[i];

	f32x__psattr_e psattr
		= F32X__PSATTR_NONE;
	if (postfix && !f32x__psattr_parse(
		postfix, &psattr))
	{
		FCT_DEBUG_ERROR_TOKEN(postfix,
			"Unrecognized instruction postfix");
		return false;
	}

	f32x_reg_e d = F32X_REG_ZERO;
	if (asm_inst->dest)
	{
		if ((field_count < 1) || !field[0]
			|| (field[0]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[0]->type  != FCT_ASM_FIELD_TYPE_REGISTER))
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected untransformed register as destination");
			return false;
		}
		d = field[0]->reg.reg;
	}

	f32x_msimd_e   msimd;
	f32x_msimd_e* pmsimd = &msimd;
	switch (psattr)
	{
		case F32X__PSATTR_Q8:
			msimd = F32X_MSIMD_Q8;
			break;
		case F32X__PSATTR_D16:
			msimd = F32X_MSIMD_D16;
			break;
		case F32X__PSATTR_S32:
			msimd = F32X_MSIMD_S32;
			break;
		case F32X__PSATTR_X64:
			msimd = F32X_MSIMD_X64;
			break;
		default:
			pmsimd = NULL;
			break;
	}

	unsigned arg_count = field_count;
	if (asm_inst->dest)
		arg_count--;

	if (arg_count > asm_inst->args_max)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Too many operands (%u)", field_count);
		return false;
	}

	if (arg_count < asm_inst->args_min)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Too few operands (%u)", field_count);
		return false;
	}

	f32x__field_t a = f32x__field_default,
		b = f32x__field_default,
		c = f32x__field_default;

	for (i = 0; i < arg_count; i++)
	{
		f32x__field_t* f[3] = { &a, &b, &c };

		unsigned j = i + (field_count - arg_count);
		if (!f32x__field_convert(context,
			field[j], pmsimd, asm_inst->relative, f[i]))
		{
			FCT_DEBUG_ERROR_FIELD(field[j],
				"Invalid operand");
			return false;
		}
	}

	f32x_inst_t inst;
	bool boundary = false;
	if (!asm_inst->encode(
		mnem, postfix, asm_inst->op, psattr,
		&d, &a, &b, &c, &inst, &boundary))
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Failed to encode instruction");
		return false;
	}

	if (c.immediate && !c.embed)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Immediate out of range for third operand");
		return false;
	}

	if (a.symbol && b.symbol)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Can't have two symbols in one instruction");
		return false;
	}

	f32x_inst_t code[3];
	unsigned ci = 0;

	if (a.immediate && !a.embed)
	{
		code[ci].li.ui       = (a.value.s32.ux >> 6);
		code[ci].li.type     = (a.is_signed ? F32X_LI_S32 : F32X_LI_U32);
		code[ci].li.field    = 0;
		code[ci].li.reserved = 0;
		code[ci].li.limm     = true;
		code[ci].li.eimm     = false;

		if (a.symbol)
		{
			ci++;
		}
		else
		{
			if (!fct_asm_context_emit_code(
				context, &code[ci].mask,
				sizeof(code[ci]), sizeof(code[ci]), 0))
				return false;
		}
	}

	if (b.immediate && !b.embed)
	{
		code[ci].li.ui       = (b.value.s32.ux >> 6);
		code[ci].li.type     = (b.is_signed ? F32X_LI_S32 : F32X_LI_U32);
		code[ci].li.field    = 1;
		code[ci].li.reserved = 0;
		code[ci].li.limm     = true;
		code[ci].li.eimm     = false;

		if (a.symbol || b.symbol)
		{
			ci++;
		}
		else
		{
			if (!fct_asm_context_emit_code(
				context, &code[ci].mask,
				sizeof(code[ci]), sizeof(code[ci]), 0))
				return false;
		}
	}

	code[ci++] = inst;

	unsigned code_size = (sizeof(inst) * ci);
	unsigned boundary_size = (boundary ? code_size : 0);

	fct_string_t* symbol = (a.symbol ? a.symbol : b.symbol);
	bool symrel = (a.symbol ? a.symbol_relative : b.symbol_relative);
	uint32_t symbol_offset = ((symbol && symrel) ? code_size : 0);

	return fct_asm_context_emit(
		context, code,
		code_size, sizeof(inst),
		FCT_CHUNK_ACCESS_RX,
		FCT_CHUNK_CONTENT_DATA,
		symbol, symbol_offset, symrel, true,
		boundary_size);
}



static const char* f32x_bit_op_name[8]
	= { "and", "or", "andn", "orn", "nand", "nor", "xor", "nxor" };
static const char* f32x_bit_post_op_name[4]
	= { NULL, "pop", "clz", "ctz" };
static const char* f32x_clamp_op_name[4]
	= { "umin", "smin", "umax", "smax" };
static const char* f32x_memory_op_name[16] =
{
	"ld", "ll" , "lw", "lws", "lh", "lhs", "lb", "lbs",
	"sd", "scd", "sw", "scw", "sh", "sch", "sb", "scb",
};
static const char* f32x_cond_name[4]
	= { "lo", "ls", "lt", "le" };
static const char* f32x_shift_op_name[4]
	= { "sll", "srl", "sra", "srr" };
static const char* f32x_cmov_cond_name[4]
	= { "nez", "eqz", "ltz", "lez" };
static const char* f32x_cmov_op_name[4]
	= { "adr", "bl", "mov", "jl" };
static const char* f32x_multiply_op_name[4]
	= { "umul", "smul", "ulmul", "slmul" };
static const char* f32x_divmod_op_name[4]
	= { "udiv", "udivmod", "sdiv", "sdivmod" };
static const char* f32x_msimd_name[4]
	= { "x64", "s32", "d16", "q8" };

static bool f32x_disassemble__field(
	fct_arch_t* arch,
	f32x__field_t field, bool is_src,
	bool immediate_mask,
	fct_string_t* str)
{
	if (field.invert
		&& !fct_string_append_static_format(str, "~"))
		return false;

	if (field.negate
		&& !fct_string_append_static_format(str, "-"))
		return false;

	if (!field.immediate && (field.reg == F32X_REG_ZERO))
	{
		field.value.s32.ux = (field.invert ? 0xFFFFFFFF : 0);
		field.immediate    = true;
		field.reg          = F32X_REG_ZERO;
		field.negate       = false;
		field.invert       = false;
	}

	if (is_src && field.immediate)
	{
		if (field.symbol && !fct_string_append(str, field.symbol))
			return false;

		if (immediate_mask)
		{
			return fct_string_append_static_format(
				str, "0x%08" PRIX32, field.value.s32.ux);
		}
		else if (field.is_signed)
		{
			switch (field.msimd)
			{
				case F32X_MSIMD_Q8:
					if ((field.value.q8.sx == field.value.q8.sy)
						&& (field.value.q8.sx == field.value.q8.sz)
						&& (field.value.q8.sx == field.value.q8.sw))
						return fct_string_append_static_format(
							str, "%" PRId8, field.value.q8.sx);
					return fct_string_append_static_format(
						str, "q8(%" PRId8 ", %" PRId8 ", %" PRId8 ", %" PRId8 ")",
						field.value.q8.sx, field.value.q8.sy,
						field.value.q8.sz, field.value.q8.sw);
				case F32X_MSIMD_D16:
					if (field.value.d16.sx == field.value.d16.sy)
						return fct_string_append_static_format(
							str, "%" PRId16, field.value.d16.sx);
					return fct_string_append_static_format(
						str, "d16(%" PRId16 ", %" PRId16 ")",
						field.value.d16.sx, field.value.d16.sy);
				default:
					break;
			}
			return fct_string_append_static_format(
				str, "%" PRId32, field.value.s32.sx);
		}

		switch (field.msimd)
		{
			case F32X_MSIMD_Q8:
				if ((field.value.q8.ux == field.value.q8.uy)
					&& (field.value.q8.ux == field.value.q8.uz)
					&& (field.value.q8.ux == field.value.q8.uw))
					return fct_string_append_static_format(
						str, "%" PRIu8, field.value.q8.ux);
				return fct_string_append_static_format(
					str, "q8(%" PRIu8 ", %" PRIu8 ", %" PRIu8 ", %" PRIu8 ")",
					field.value.q8.ux, field.value.q8.uy,
					field.value.q8.uz, field.value.q8.uw);
			case F32X_MSIMD_D16:
				if (field.value.d16.ux == field.value.d16.uy)
					return fct_string_append_static_format(
						str, "%" PRIu16, field.value.d16.ux);
				return fct_string_append_static_format(
					str, "d16(%" PRIu16 ", %" PRIu16 ")",
					field.value.d16.ux, field.value.d16.uy);
			default:
				break;
		}
		return fct_string_append_static_format(
			str, "%" PRIu32, field.value.s32.ux);
	}

	char reg_sigil = '$';
	if (!fct_string_append_static_format(
			str, "%c", reg_sigil))
		return false;

	const char* rname
		= fct_arch_reg_name_from_number(arch, field.reg);
	if (rname)
		return fct_string_append_static_format(str, "%s", rname);

	return fct_string_append_static_format(str, "%u", (unsigned)field.reg);
}

unsigned f32x_disassemble(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned offset,
	fct_string_t* str)
{
	if (!arch)
		arch = &fct_arch__f32x;

	const char* op_name[2] = { NULL, NULL };
	const char* psattr_name = NULL;

	f32x__field_t a = f32x__field_default;
	f32x__field_t b = f32x__field_default;
	f32x__field_t c = f32x__field_default;

	f32x_inst_t inst;
	if (!fct_maubuff_read(code, offset, 4, (void*)&inst))
		return 0;
	unsigned size = 4;

	bool a_li = false;
	bool b_li = false;

	while (!inst.li.eimm && inst.li.limm)
	{
		switch (inst.li.type)
		{
			case F32X_LI_U32:
			case F32X_LI_S32:
				break;

			default:
				return 0;
		}

		if (inst.li.field == 0)
		{
			a_li = true;
			a.value.s32.ux = (inst.li.ui << 6);
			a.immediate = true;
			a.is_signed = (inst.li.type == F32X_LI_S32);
		}
		else
		{
			b_li = true;
			b.value.s32.ux = (inst.li.ui << 6);
			b.immediate = true;
			b.is_signed = (inst.li.type == F32X_LI_S32);
		}

		if (!fct_maubuff_read(code, (offset + size), 4, (void*)&inst))
			return 0;
		size += 4;
	}

	if (inst.mask == 0)
		return (fct_string_append_static_format(str, "nop") ? size : 0);

	a.reg = inst.r.a;
	b.reg = inst.r.b;
	a.value.s32.ux |= inst.r.a;
	b.value.s32.ux |= inst.r.b;

	bool has_d = true;
	bool has_a = true;
	bool has_b = true;
	bool has_c = false;

	bool a_mask = false;
	bool b_mask = false;

	bool operand_swap = false;

	if (inst.ui.eimm)
	{
		bool is_store = ((inst.ui.op == F32X_OPI_CST)
			|| F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_STORE));

		if (is_store)
		{
			c.immediate = true;
			c.value.s32.ux = (inst.store.ui_lo | (inst.store.ui_hi << 6));
			has_d = false;
			operand_swap = true;
			has_c = true;
		}
		else if (!b_li)
		{
			b.immediate = true;
			b.value.s32.ux = inst.ui.ui;
		}

		if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_MEMORY))
		{
			op_name[0] = f32x_memory_op_name[inst.mem.op];

			if (is_store)
			{
				c.value.s32.ux <<= (3 - ((inst.mem.op >> 1) & 3));
				b_mask = true;
			}
			else
			{
				if (!b_li) b.value.s32.ux <<= (3 - ((inst.mem.op >> 1) & 3));
				a_mask = true;
			}
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_BIT))
		{
			op_name[0] = f32x_bit_op_name[inst.biti.op];
			a_mask = true;
			b_mask = true;
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_CMOV))
		{
			op_name[0] = f32x_cmov_op_name[inst.cmovi.op];
			op_name[1] = f32x_cmov_cond_name[inst.cmovi.cond];

			if ((inst.cmovi.op == F32X_CMOV_OP_BRANCH_LINK)
				&& (inst.cmovi.d == F32X_REG_ZERO))
			{
				b.value.s32.ux <<= 2;
				op_name[0] = "b";
				has_d = false;
			}
			if ((inst.cmovi.cond == F32X_CMOV_COND_EQUAL_ZERO)
				&& (inst.cmovi.a == F32X_REG_ZERO))
			{
				op_name[1] = NULL;
				has_a = false;
			}

			a.is_signed = true;
			b.is_signed = true;
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_DIVMOD))
		{
			op_name[0] = f32x_divmod_op_name[inst.divmodi.op];
			a.is_signed = ((inst.divmodi.op & 2) != 0);
			b.is_signed = a.is_signed;
			operand_swap = inst.divmodi.swap;
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_SHIFT))
		{
			op_name[0] = f32x_shift_op_name[inst.shifti.op];
			b.is_signed = (inst.shifti.op == F32X_SHIFT_OP_RIGHT_ARITHMETIC);
			operand_swap = true;
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_CLAMP))
		{
			op_name[0] = f32x_clamp_op_name[inst.clampi.op];
			a.is_signed = ((inst.clampi.op & 1) != 0);
			b.is_signed = a.is_signed;
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_SETCOND))
		{
			op_name[0] = "s";
			op_name[1] = f32x_cond_name[inst.setcondi.cond];
			a.is_signed = ((inst.clampi.op & 2) != 0);
			b.is_signed = a.is_signed;
			operand_swap = inst.setcondi.swap;
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_ADDSUB))
		{
			op_name[0] = (inst.addi.a_neg ? "sub" : "add");
			a.is_signed = true;
			b.is_signed = true;
			operand_swap = inst.addi.a_neg;
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_MUL))
		{
			op_name[0] = f32x_multiply_op_name[inst.muli.op];
			a.is_signed = (inst.muli.op != 0);
			b.is_signed = a.is_signed;
		}
		else if (F32X_OPI_IS_GROUP(inst.ui.op, F32X_OPI_CONTROL))
		{
			op_name[0] = (inst.control.store ? "cst" : "cld");
			if (!a_li) a.is_signed = false;
			if (!b_li) b.is_signed = false;
		}
		else
		{
			return 0;
		}

		if (!b_li && b.is_signed
			&& ((b.value.s32.ux >> 12) != 0))
			b.value.s32.ux |= 0xFFFFE000;
	}
	else if (inst.r.reserved == 0)
	{
		if (inst.add.prefix == F32X_OPR_PREFIX_ADD)
		{
			op_name[0] = "add";
			if (inst.add.c_set)
				op_name[1] = "s";

			a.msimd = inst.add.msimd;
			b.msimd = inst.add.msimd;
			c.msimd = inst.add.msimd;
			psattr_name = f32x_msimd_name[inst.add.msimd];
			if (inst.add.msimd == F32X_MSIMD_S32)
				psattr_name = NULL;

			if (inst.add.a_inv && inst.add.c_inv
				&& !inst.add.c_reg && !inst.add.c_set)
			{
				op_name[0] = "sub";
				operand_swap = true;
			}
			else
			{
				a.invert = inst.add.a_inv;
				if (inst.add.c_reg)
				{
					c.reg = F32X_REG_CARRY;
					c.invert = inst.add.c_inv;
				}
				else if (inst.add.c_inv)
				{
					c.immediate = true;
					c.reg = F32X_REG_ZERO;
					switch (inst.add.msimd)
					{
						case F32X_MSIMD_Q8:
							c.value.q8.ux = 1;
							c.value.q8.uy = 1;
							c.value.q8.uz = 1;
							c.value.q8.uw = 1;
							break;
						case F32X_MSIMD_D16:
							c.value.d16.ux = 1;
							c.value.d16.uy = 1;
							break;
						default:
							c.value.s32.ux = 1;
							break;
					}
				}
			}

			a.is_signed = true;
			b.is_signed = true;
		}
		else if (inst.bit.prefix == F32X_OPR_PREFIX_BIT)
		{
			op_name[0] = f32x_bit_op_name[inst.bit.op];
			psattr_name = f32x_bit_post_op_name[inst.bit.post_op];

			if (inst.bit.bit_imm)
			{
				b.immediate = true;
				b.reg = F32X_REG_ZERO;
				b.value.s32.ux = (1U << (inst.bit.b & 0x1F));
			}

			a_mask = true;
			b_mask = true;
		}
		else if (inst.clamp.prefix == F32X_OPR_PREFIX_CLAMP)
		{
			op_name[0] = f32x_clamp_op_name[inst.clamp.op];

			a.msimd = inst.clamp.msimd;
			b.msimd = inst.clamp.msimd;
			psattr_name = f32x_msimd_name[inst.clamp.msimd];
			if (inst.clamp.msimd == F32X_MSIMD_S32)
				psattr_name = NULL;

			a.invert = inst.clamp.a_neg;

			a.is_signed = ((inst.clamp.op & 1) != 0);
			b.is_signed = a.is_signed;
		}
		else if (inst.shift.prefix == F32X_OPR_PREFIX_SHIFT)
		{
			op_name[0] = f32x_shift_op_name[inst.shift.op];

			a.msimd = inst.shift.msimd;
			b.msimd = inst.shift.msimd;
			psattr_name = f32x_msimd_name[inst.shift.msimd];
			if (inst.shift.msimd == F32X_MSIMD_S32)
				psattr_name = NULL;

			if (inst.shift.b_imm)
			{
				b.immediate = true;
				b.reg = F32X_REG_ZERO;
				switch (inst.add.msimd)
				{
					case F32X_MSIMD_Q8:
						b.value.q8.ux = inst.shift.b;
						b.value.q8.uy = inst.shift.b;
						b.value.q8.uz = inst.shift.b;
						b.value.q8.uw = inst.shift.b;
						break;
					case F32X_MSIMD_D16:
						b.value.d16.ux = inst.shift.b;
						b.value.d16.uy = inst.shift.b;
						break;
					default:
						b.value.s32.ux = inst.shift.b;
						break;
				}
			}

			a.is_signed = (inst.shift.op == F32X_SHIFT_OP_RIGHT_ARITHMETIC);
		}
		else if (inst.cmov.prefix == F32X_OPR_PREFIX_CMOV)
		{
			if (inst.cmov.a == F32X_REG_ZERO)
				a.value.s32.ux = 0;

			op_name[0] = f32x_cmov_op_name[inst.cmov.op];
			op_name[1] = f32x_cmov_cond_name[inst.cmov.cond];

			if (inst.cmov.op == F32X_CMOV_OP_JUMP_LINK)
				b_mask = true;
			else if ((inst.cmov.op == F32X_CMOV_OP_BRANCH_LINK)
				|| (inst.cmov.op == F32X_CMOV_OP_MOVE_RELATIVE))
				b.is_signed = true;

			a.is_signed = ((inst.cmov.cond & 2) != 0);

			if (inst.cmov.d == F32X_REG_ZERO)
			{
				if (inst.cmov.op == F32X_CMOV_OP_BRANCH_LINK)
				{
					op_name[0] = "b";
					has_d = false;
				}
				else if (inst.cmov.op == F32X_CMOV_OP_JUMP_LINK)
				{
					op_name[0] = "j";
					has_d = false;
				}
			}

			if ((inst.cmov.cond == F32X_CMOV_COND_EQUAL_ZERO)
				&& (inst.cmov.a == F32X_REG_ZERO))
			{
				op_name[1] = NULL;
				has_a = false;
			}
		}
		else if (inst.setcond.prefix == F32X_OPR_PREFIX_SETCOND)
		{
			op_name[0] = "s";
			op_name[1] = f32x_cond_name[inst.setcond.cond];

			a.msimd = inst.setcond.msimd;
			b.msimd = inst.setcond.msimd;
			psattr_name = f32x_msimd_name[inst.setcond.msimd];
			if (inst.setcond.msimd == F32X_MSIMD_S32)
				psattr_name = NULL;

			a.is_signed = ((inst.setcond.cond & 2) != 0);
			b.is_signed = a.is_signed;
		}
		else if (inst.mul.prefix == F32X_OPR_PREFIX_MUL)
		{
			op_name[0] = f32x_multiply_op_name[inst.mul.op];

			a.msimd = inst.mul.msimd;
			b.msimd = inst.mul.msimd;
			psattr_name = f32x_msimd_name[inst.mul.msimd];
			if (inst.mul.msimd == F32X_MSIMD_S32)
				psattr_name = NULL;

			a.is_signed = ((inst.divmod.op & 1) != 0);
			b.is_signed = a.is_signed;
		}
		else if (inst.divmod.prefix == F32X_OPR_PREFIX_DIVMOD)
		{
			op_name[0] = f32x_divmod_op_name[inst.divmod.op];

			a.msimd = inst.divmod.msimd;
			b.msimd = inst.divmod.msimd;
			psattr_name = f32x_msimd_name[inst.divmod.msimd];
			if (inst.divmod.msimd == F32X_MSIMD_S32)
				psattr_name = NULL;

			a.is_signed = ((inst.divmod.op & 2) != 0);
			b.is_signed = a.is_signed;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}

	if (operand_swap)
	{
		f32x__field_t swap = a;
		a = b;
		b = swap;

		bool swap_mask = a_mask;
		a_mask = b_mask;
		b_mask = swap_mask;
	}

	fct_string_t* istr
		= fct_string_create(op_name[0]);
	if (!istr)
		return 0;

	if (op_name[1]
		&& !fct_string_append_static_format(
			istr, "%s", op_name[1]))
	{
		fct_string_delete(istr);
		return 0;
	}

	if (psattr_name
		&& !fct_string_append_static_format(
			istr, ".%s", psattr_name))
	{
		fct_string_delete(istr);
		return 0;
	}

	if (has_d)
	{
		if(!fct_string_append_static_format(istr, " "))
		{
			fct_string_delete(istr);
			return 0;
		}

		f32x__field_t fd = f32x__field_default;
		fd.reg = inst.r.d;

		if (!f32x_disassemble__field(
			arch, fd, false, false, istr))
		{
			fct_string_delete(istr);
			return 0;
		}
	}

	if (has_a)
	{
		if(!fct_string_append_static_format(
			istr, (has_d ? ", " : " ")))
		{
			fct_string_delete(istr);
			return 0;
		}

		if (!f32x_disassemble__field(
			arch, a, true, a_mask, istr))
		{
			fct_string_delete(istr);
			return 0;
		}
	}

	if (has_b)
	{
		if(!fct_string_append_static_format(
			istr, (has_d || has_a ? ", " : " ")))
		{
			fct_string_delete(istr);
			return 0;
		}

		if (!f32x_disassemble__field(
			arch, b, true, b_mask, istr))
		{
			fct_string_delete(istr);
			return 0;
		}
	}

	if (has_c || !f32x__field_empty(&c))
	{
		if(!fct_string_append_static_format(
			istr, (has_d || has_a || has_b ? ", " : " ")))
		{
			fct_string_delete(istr);
			return 0;
		}

		if (!f32x_disassemble__field(
			arch, c, true, false, istr))
		{
			fct_string_delete(istr);
			return 0;
		}
	}

	bool success = fct_string_append(str, istr);
	fct_string_delete(istr);
	return (success ? size : 0);
}
