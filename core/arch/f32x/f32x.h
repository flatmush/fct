#ifndef __FCT_ARCH_F32X_H__
#define __FCT_ARCH_F32X_H__

#include <fct/arch/arch.h>


typedef enum
{
	F32X_REG_ZERO  = 0,
	F32X_REG_CARRY = 1,
} f32x_reg_e;
static const unsigned F32X_REG_COUNT = 64;


typedef enum
{
	F32X_OPI_LD  = 0x00,
	F32X_OPI_LL  = 0x01,
	F32X_OPI_LW  = 0x02,
	F32X_OPI_LWS = 0x03,
	F32X_OPI_LH  = 0x04,
	F32X_OPI_LHS = 0x05,
	F32X_OPI_LB  = 0x06,
	F32X_OPI_LBS = 0x07,
	F32X_OPI_SD  = 0x08,
	F32X_OPI_SCD = 0x09,
	F32X_OPI_SW  = 0x0A,
	F32X_OPI_SCW = 0x0B,
	F32X_OPI_SH  = 0x0C,
	F32X_OPI_SCH = 0x0D,
	F32X_OPI_SB  = 0x0E,
	F32X_OPI_SCB = 0x0F,

	F32X_OPI_AND  = 0x10,
	F32X_OPI_OR   = 0x11,
	F32X_OPI_ANDN = 0x12,
	F32X_OPI_ORN  = 0x13,
	F32X_OPI_NAND = 0x14,
	F32X_OPI_NOR  = 0x15,
	F32X_OPI_XOR  = 0x16,
	F32x_OPI_NXOR = 0x17,

	F32X_OPI_ADRNEZ = 0x18,
	F32X_OPI_ADREQZ = 0x19,
	F32X_OPI_ADRLTZ = 0x1A,
	F32X_OPI_ADRLEZ = 0x1B,
	F32X_OPI_BLNEZ  = 0x1C,
	F32X_OPI_BLEQZ  = 0x1D,
	F32X_OPI_BLLTZ  = 0x1E,
	F32X_OPI_BLLEZ  = 0x1F,

	F32X_OPI_SLO = 0x20,
	F32X_OPI_SLS = 0x21,
	F32X_OPI_SLT = 0x22,
	F32X_OPI_SLE = 0x23,
	F32X_OPI_SHI = 0x24,
	F32X_OPI_SHS = 0x25,
	F32X_OPI_SGT = 0x26,
	F32X_OPI_SGE = 0x27,

	F32X_OPI_UDIV     = 0x28,
	F32X_OPI_UDIVMOD  = 0x29,
	F32X_OPI_SDIV     = 0x2A,
	F32X_OPI_SDIVMOD  = 0x2B,
	F32X_OPI_UDIVR    = 0x2C,
	F32X_OPI_UDIVMODR = 0x2D,
	F32X_OPI_SDIVR    = 0x2E,
	F32X_OPI_SDIVMODR = 0x2F,

	F32X_OPI_SLL = 0x30,
	F32X_OPI_SRL = 0x31,
	F32X_OPI_SRA = 0x32,
	F32X_OPI_SRR = 0x33,

	F32X_OPI_UMIN = 0x34,
	F32X_OPI_SMIN = 0x35,
	F32X_OPI_UMAX = 0x36,
	F32X_OPI_SMAX = 0x37,

	F32X_OPI_ADD = 0x38,
	F32X_OPI_SUB = 0x39,

	F32X_OPI_UMUL = 0x3A,
	F32X_OPI_SMUL = 0x3B,

	F32X_OPI_CLD = 0x3C,
	F32X_OPI_CST = 0x3D,

	F32X_OPI_UNDEF0 = 0x3E,
	F32X_OPI_UNDEF1 = 0x3F,
} f32x_opi_e;

#define F32X_OPI_GROUP(name, base, size) static const f32x_opi_e F32X_OPI_##name = F32X_OPI_##base; static const unsigned F32X_OPI_##name##_SIZE = size

F32X_OPI_GROUP(MEMORY , LD    , 16);
F32X_OPI_GROUP(LOAD   , LD    ,  8);
F32X_OPI_GROUP(STORE  , SD    ,  8);
F32X_OPI_GROUP(BIT    , AND   ,  8);
F32X_OPI_GROUP(CMOV   , ADRNEZ,  8);
F32X_OPI_GROUP(SETCOND, SLO   ,  8);
F32X_OPI_GROUP(DIVMOD , UDIV  ,  8);
F32X_OPI_GROUP(SHIFT  , SLL   ,  4);
F32X_OPI_GROUP(CLAMP  , UMIN  ,  4);
F32X_OPI_GROUP(ADDSUB , ADD   ,  2);
F32X_OPI_GROUP(MUL    , UMUL  ,  2);
F32X_OPI_GROUP(CONTROL, CLD   ,  2);

static inline bool f32x_opi_is_group(f32x_opi_e op, f32x_opi_e group, unsigned size)
	{ return ((op >= group) && (op < (group + size))); }
#define F32X_OPI_IS_GROUP(op, group) f32x_opi_is_group(op, group, group##_SIZE)


typedef enum
{
	F32X_OPR_ADD     =   0,
	F32X_OPR_CLAMP   = 128,
	F32X_OPR_SETCOND = 160,
	F32X_OPR_UNDEF_0 = 176,
	F32X_OPR_MUL     = 192,
	F32X_OPR_DIVMOD  = 224,
	F32X_OPR_SHIFT   = 256,
	F32X_OPR_BIT     = 384,
	F32X_OPR_CMOV    = 448,
	F32X_OPR_UNDEF_1 = 480,
} f32x_opr_e;

typedef enum
{
	F32X_OPR_PREFIX_ADD     =  0,
	F32X_OPR_PREFIX_CLAMP   =  4,
	F32X_OPR_PREFIX_SETCOND = 10,
	F32X_OPR_PREFIX_MUL     =  6,
	F32X_OPR_PREFIX_DIVMOD  =  7,
	F32X_OPR_PREFIX_SHIFT   =  2,
	F32X_OPR_PREFIX_BIT     =  6,
	F32X_OPR_PREFIX_CMOV    = 14,
} f32x_opr_prefix_e;

typedef enum
{
	F32X_BIT_OP_AND  = 0,
	F32X_BIT_OP_OR   = 1,
	F32X_BIT_OP_ANDN = 2,
	F32X_BIT_OP_ORN  = 3,
	F32X_BIT_OP_NAND = 4,
	F32X_BIT_OP_NOR  = 5,
	F32X_BIT_OP_XOR  = 6,
	F32X_BIT_OP_NXOR = 7,
} f32x_bit_op_e;

typedef enum
{
	F32X_BIT_POST_OP_NOP = 0,
	F32X_BIT_POST_OP_POP = 1,
	F32X_BIT_POST_OP_CLZ = 2,
	F32X_BIT_POST_OP_CTZ = 3,
} f32x_bit_post_op_e;

typedef enum
{
	F32X_CLAMP_OP_UMIN = 0,
	F32X_CLAMP_OP_SMIN = 1,
	F32X_CLAMP_OP_UMAX = 2,
	F32X_CLAMP_OP_SMAX = 3,
} f32x_clamp_op_e;

typedef enum
{
	F32X_MEMORY_OP_LD  =  0,
	F32X_MEMORY_OP_LLD =  1,
	F32X_MEMORY_OP_LW  =  2,
	F32X_MEMORY_OP_LWS =  3,
	F32X_MEMORY_OP_LH  =  4,
	F32X_MEMORY_OP_LHS =  5,
	F32X_MEMORY_OP_LB  =  6,
	F32X_MEMORY_OP_LBS =  7,
	F32X_MEMORY_OP_SD  =  8,
	F32X_MEMORY_OP_SCD =  9,
	F32X_MEMORY_OP_SW  = 10,
	F32X_MEMORY_OP_SCW = 11,
	F32X_MEMORY_OP_SH  = 12,
	F32X_MEMORY_OP_SCH = 13,
	F32X_MEMORY_OP_SB  = 14,
	F32X_MEMORY_OP_SCB = 15,
} f32x_memory_op_e;

typedef enum
{
	F32X_COND_LOWER      = 0,
	F32X_COND_LOWER_SAME = 1,
	F32X_COND_LESS_THAN  = 2,
	F32X_COND_LESS_EQUAL = 3,
} f32x_cond_e;

typedef enum
{
	F32X_SHIFT_OP_LEFT_LOGICAL     = 0,
	F32X_SHIFT_OP_RIGHT_LOGICAL    = 1,
	F32X_SHIFT_OP_RIGHT_ARITHMETIC = 2,
	F32X_SHIFT_OP_RIGHT_ROTATIONAL = 3,
} f32x_shift_op_e;

typedef enum
{
	F32X_CMOV_COND_NON_ZERO        =  0,
	F32X_CMOV_COND_EQUAL_ZERO      =  1,
	F32X_CMOV_COND_LESS_THAN_ZERO  =  2,
	F32X_CMOV_COND_LESS_EQUAL_ZERO =  3,
} f32x_cmov_cond_e;

typedef enum
{
	F32X_CMOV_OP_MOVE_RELATIVE =  0,
	F32X_CMOV_OP_BRANCH_LINK   =  1,
	F32X_CMOV_OP_MOVE          =  2,
	F32X_CMOV_OP_JUMP_LINK     =  3,
} f32x_cmov_op_e;

typedef enum
{
	F32X_CMOV_OP_COND_MOVE_RELATIVE_NON_ZERO            =  0,
	F32X_CMOV_OP_COND_MOVE_RELATIVE_EQUAL_ZERO          =  1,
	F32X_CMOV_OP_COND_MOVE_RELATIVE_LESS_THAN_ZERO      =  2,
	F32X_CMOV_OP_COND_MOVE_RELATIVE_LESS_EQUAL_ZERO     =  3,
	F32X_CMOV_OP_COND_BRANCH_LINK_NON_ZERO              =  4,
	F32X_CMOV_OP_COND_BRANCH_LINK_EQUAL_ZERO            =  5,
	F32X_CMOV_OP_COND_BRANCH_LINK_LESS_THAN_ZERO        =  6,
	F32X_CMOV_OP_COND_BRANCH_LINK_LESS_EQUAL_ZERO       =  7,
	F32X_CMOV_OP_COND_MOVE_NON_ZERO                     =  8,
	F32X_CMOV_OP_COND_MOVE_EQUAL_ZERO                   =  9,
	F32X_CMOV_OP_COND_MOVE_LESS_THAN_ZERO               = 10,
	F32X_CMOV_OP_COND_MOVE_LESS_EQUAL_ZERO              = 11,
	F32X_CMOV_OP_COND_JUMP_LINK_NON_ZERO                = 12,
	F32X_CMOV_OP_COND_JUMP_LINK_EQUAL_ZERO              = 13,
	F32X_CMOV_OP_COND_JUMP_LINK_LESS_THAN_ZERO          = 14,
	F32X_CMOV_OP_COND_JUMP_LINK_LESS_EQUAL_ZERO         = 15,
	F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_NON_ZERO        = 16,
	F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_EQUAL_ZERO      = 17,
	F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_LESS_THAN_ZERO  = 18,
	F32X_CMOV_OP_COND_SYS_MOVE_RELATIVE_LESS_EQUAL_ZERO = 19,
	F32X_CMOV_OP_COND_SYS_BRANCH_LINK_NON_ZERO          = 20,
	F32X_CMOV_OP_COND_SYS_BRANCH_LINK_EQUAL_ZERO        = 21,
	F32X_CMOV_OP_COND_SYS_BRANCH_LINK_LESS_THAN_ZERO    = 22,
	F32X_CMOV_OP_COND_SYS_BRANCH_LINK_LESS_EQUAL_ZERO   = 23,
	F32X_CMOV_OP_COND_SYS_MOVE_NON_ZERO                 = 24,
	F32X_CMOV_OP_COND_SYS_MOVE_EQUAL_ZERO               = 25,
	F32X_CMOV_OP_COND_SYS_MOVE_LESS_THAN_ZERO           = 26,
	F32X_CMOV_OP_COND_SYS_MOVE_LESS_EQUAL_ZERO          = 27,
	F32X_CMOV_OP_COND_SYS_JUMP_LINK_NON_ZERO            = 28,
	F32X_CMOV_OP_COND_SYS_JUMP_LINK_EQUAL_ZERO          = 29,
	F32X_CMOV_OP_COND_SYS_JUMP_LINK_LESS_THAN_ZERO      = 30,
	F32X_CMOV_OP_COND_SYS_JUMP_LINK_LESS_EQUAL_ZERO     = 31,
} f32x_cmov_op_cond_e;

typedef enum
{
	F32X_MULTIPLY_OP_UMUL  = 0,
	F32X_MULTIPLY_OP_SMUL  = 1,
	F32X_MULTIPLY_OP_ULMUL = 2,
	F32X_MULTIPLY_OP_SLMUL = 3,
} f32x_multiply_op_e;

typedef enum
{
	F32X_DIVMOD_OP_UDIV    = 0,
	F32X_DIVMOD_OP_UDIVMOD = 1,
	F32X_DIVMOD_OP_SDIV    = 2,
	F32X_DIVMOD_OP_SDIVMOD = 3,
} f32x_divmod_op_e;

typedef enum
{
	F32X_MSIMD_X64 = 0,
	F32X_MSIMD_S32 = 1,
	F32X_MSIMD_D16 = 2,
	F32X_MSIMD_Q8  = 3,
} f32x_msimd_e;

typedef enum
{
	F32X_LI_U32        = 0,
	F32X_LI_S32        = 1,
	F32X_LI_U32_REPEAT = 2,
	F32X_LI_U64        = 3,
} f32x_li_e;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		unsigned   ui       : 26;
		f32x_li_e  type     :  2;
		unsigned   field    :  1;
		unsigned   reserved :  1;
		bool       limm     :  1;
		bool       eimm     :  1;
	} li;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e d    :  6;
		f32x_reg_e a    :  6;
		unsigned   ui   : 13;
		f32x_opi_e op   :  6;
		bool       eimm :  1;
	} ui;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e d    :  6;
		f32x_reg_e a    :  6;
		signed     si   : 13;
		f32x_opi_e op   :  6;
		bool       eimm :  1;
	} si;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e d        : 6;
		f32x_reg_e a        : 6;
		f32x_reg_e b        : 6;
		f32x_opr_e op       : 9;
		unsigned   reserved : 3;
		bool       limm     : 1;
		bool       eimm     : 1;
	} r;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e       d      :  6;
		f32x_reg_e       a      :  6;
		unsigned         ui     : 13;
		f32x_memory_op_e op     :  4;
		unsigned         prefix :  2;
		bool             eimm   :  1;
	} mem;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e    d      :  6;
		f32x_reg_e    a      :  6;
		unsigned      ui     : 13;
		f32x_bit_op_e op     :  3;
		unsigned      prefix :  3;
		bool          eimm   :  1;
	} biti;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e       d      :  6;
		f32x_reg_e       a      :  6;
		signed           si     : 13;
		f32x_cmov_cond_e cond   :  2;
		unsigned         op     :  1;
		unsigned         prefix :  3;
		bool             eimm   :  1;
	} cmovi;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e       d      :  6;
		f32x_reg_e       a      :  6;
		unsigned         ui     : 13;
		f32x_divmod_op_e op     :  2;
		bool             swap   :  1;
		unsigned         prefix :  3;
		bool             eimm   :  1;
	} divmodi;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e      d      :  6;
		f32x_reg_e      a      :  6;
		unsigned        ui     : 13;
		f32x_shift_op_e op     :  2;
		unsigned        prefix :  4;
		bool            eimm   :  1;
	} shifti;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e      d      :  6;
		f32x_reg_e      a      :  6;
		unsigned        ui     : 13;
		f32x_clamp_op_e op     :  2;
		unsigned        prefix :  4;
		bool            eimm   :  1;
	} clampi;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e  d      :  6;
		f32x_reg_e  a      :  6;
		unsigned    ui     : 13;
		f32x_cond_e cond   :  2;
		bool        swap   :  1;
		unsigned    prefix :  3;
		bool        eimm   :  1;
	} setcondi;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e  d      :  6;
		f32x_reg_e  a      :  6;
		signed      si     : 13;
		bool        a_neg  :  1;
		unsigned    prefix :  5;
		bool        eimm   :  1;
	} addi;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e d      :  6;
		f32x_reg_e a      :  6;
		unsigned   ui     : 13;
		unsigned   op     :  1;
		unsigned   prefix :  5;
		bool       eimm   :  1;
	} muli;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e d	  :  6;
		f32x_reg_e a	  :  6;
		unsigned   ui	  : 13;
		bool       store  :  1;
		unsigned   prefix :  5;
		bool       eimm   :  1;
	} control;

	struct
	__attribute__((__packed__))
	{
		unsigned   ui_lo	: 6;
		f32x_reg_e a		: 6;
		f32x_reg_e b		: 6;
		unsigned   ui_hi	: 7;
		f32x_opi_e op		: 6;
		bool       eimm 	: 1;
	} store;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e   d        : 6;
		f32x_reg_e   a        : 6;
		f32x_reg_e   b        : 6;
		f32x_msimd_e msimd    : 2;
		bool         a_inv    : 1;
		bool         c_inv    : 1;
		bool         c_reg    : 1;
        bool         c_set    : 1;
		unsigned     prefix   : 3;
		unsigned     reserved : 3;
		bool         limm     : 1;
		bool         eimm     : 1;
	} add;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e         d        : 6;
		f32x_reg_e         a        : 6;
		f32x_reg_e         b        : 6;
		bool               bit_imm  : 1;
		f32x_bit_post_op_e post_op  : 2;
		f32x_bit_op_e      op       : 3;
		unsigned           prefix   : 3;
		unsigned           reserved : 3;
		bool               limm     : 1;
		bool               eimm     : 1;
	} bit;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e      d        : 6;
		f32x_reg_e      a        : 6;
		f32x_reg_e      b        : 6;
		f32x_msimd_e    msimd    : 2;
		bool            a_neg    : 1;
		f32x_clamp_op_e op       : 2;
		unsigned        prefix   : 3;
		unsigned        reserved : 4;
		bool            limm     : 1;
		bool            eimm     : 1;
	} clamp;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e      d        : 6;
		f32x_reg_e      a        : 6;
		f32x_reg_e      b        : 6;
		f32x_msimd_e    msimd    : 2;
		bool            b_imm    : 1;
		f32x_shift_op_e op       : 2;
		unsigned        prefix   : 4;
		unsigned        reserved : 3;
		bool            limm     : 1;
		bool            eimm     : 1;
	} shift;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e       d        : 6;
		f32x_reg_e       a        : 6;
		f32x_reg_e       b        : 6;
		f32x_cmov_cond_e cond     : 2;
		f32x_cmov_op_e   op       : 2;
		bool             system   : 1;
		unsigned         prefix   : 4;
		unsigned         reserved : 3;
		bool             limm     : 1;
		bool             eimm     : 1;
	} cmov;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e   d        : 6;
		f32x_reg_e   a        : 6;
		f32x_reg_e   b        : 6;
		f32x_msimd_e msimd    : 2;
		f32x_cond_e  cond     : 2;
		unsigned     prefix   : 5;
		unsigned     reserved : 3;
		bool         limm     : 1;
		bool         eimm     : 1;
	} setcond;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e         d        : 6;
		f32x_reg_e         a        : 6;
		f32x_reg_e         b        : 6;
		f32x_msimd_e       msimd    : 2;
		f32x_multiply_op_e op       : 2;
		unsigned           prefix   : 5;
		unsigned           reserved : 3;
		bool               limm     : 1;
		bool               eimm     : 1;
	} mul;

	struct
	__attribute__((__packed__))
	{
		f32x_reg_e       d        : 6;
		f32x_reg_e       a        : 6;
		f32x_reg_e       b        : 6;
		f32x_msimd_e     msimd    : 2;
		f32x_divmod_op_e op       : 2;
		unsigned         prefix   : 5;
		unsigned         reserved : 3;
		bool             limm     : 1;
		bool             eimm     : 1;
	} divmod;

	uint32_t mask;
} f32x_inst_t;

typedef enum
{
	F32X_MODE_USER      = 0,
	F32X_MODE_SYSTEM    = 1,
	F32X_MODE_EXCEPTION = 2,
	F32X_MODE_INTERRUPT = 3,
} f32x_mode_e;
static const unsigned F32X_MODE_COUNT = 4;

typedef enum
{
	F32X_EXCEPTION_INVALID_INST         = 0,
	F32X_EXCEPTION_INVALID_INST_ADDR    = 1,
	F32X_EXCEPTION_INVALID_DATA_RD_ADDR = 2,
	F32X_EXCEPTION_INVALID_DATA_WR_ADDR = 3,
} f32x_exception_e;
static const unsigned F32X_EXCEPTION_COUNT = 32;

typedef union
__attribute__((__packed__))
{
	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			uint8_t ux, uy, uz, uw;
		};

		struct
		__attribute__((__packed__))
		{
			int8_t sx, sy, sz, sw;
		};
	} q8;

	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			uint16_t ux, uy;
		};

		struct
		__attribute__((__packed__))
		{
			int16_t sx, sy;
		};
	} d16;

	union
	__attribute__((__packed__))
	{
		uint32_t ux;
		 int32_t sx;
	} s32;
} f32x_reg_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t version;
		uint32_t features;
		uint32_t vector_reset;
		uint32_t vector_syscall;
		uint32_t user_return_addr;
		uint32_t system_return_addr;
		uint32_t exception_return_addr;
		uint32_t exception_data;
		uint32_t interrupt_mask;
		uint32_t interrupt_enable;
		uint32_t interrupt_mask_set;
		uint32_t interrupt_mask_clear;
		uint32_t reserved[20];
		uint32_t vector_exception[32];
		uint32_t vector_interrupt[64];
	};
	uint32_t data[1024];
} f32x_control_mem_t;


bool f32x_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count);

unsigned f32x_disassemble(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned offset,
	fct_string_t* str);

bool f32x_link(
	f32x_inst_t* code,
	uint64_t size, uint64_t symbol);

fct_arch_t fct_arch__f32x;
#define FCT_ARCH_F32X_STATIC &fct_arch__f32x
fct_arch_t* fct_arch_f32x;

fct_arch_t fct_arch_f32x__oabi;
#define FCT_ARCH_F32X_OABI_STATIC &fct_arch_f32x__oabi
fct_arch_t* fct_arch_f32x_oabi;

#endif
