#include <fct/arch/arch.h>
#include <fct/arch/emu.h>
#include <fct/object/format/elf.h>
#include "f32x.h"


static fct_arch_spec_t fct_arch_spec__f32x =
{
	.mau       =  8,
	.addr_size =  4,
	.word_size =  4,
	.inst_size =  4,

	.reg_count = 64,

	.interrupt_count = 32,

	.elf_machine = ELF_MACHINE_NONE,
};

static fct_keyval_t fct_arch__f32x_reg_names[] =
{
	{ "zero" , F32X_REG_ZERO  },
	{ "carry", F32X_REG_CARRY },
	{ NULL, 0 }
};

fct_arch_emu_t fct_arch_emu__f32x;

fct_arch_t fct_arch__f32x =
{
	.name = "f32x",
	.base = NULL,
	.emu  = &fct_arch_emu__f32x,
	.spec = &fct_arch_spec__f32x,

	.reg_names = fct_arch__f32x_reg_names,

	.elf_osabi      = ELF_OSABI_STANDALONE,
	.elf_abiversion = 0,

	.gen_call    = NULL,
	.gen_return  = NULL,
	.gen_push    = NULL,
	.gen_pop     = NULL,
	.gen_syscall = NULL,

	.assemble    =        f32x_assemble,
	.disassemble = (void*)f32x_disassemble,
	.link        = (void*)f32x_link,
};

fct_arch_t* fct_arch_f32x
	= &fct_arch__f32x;
