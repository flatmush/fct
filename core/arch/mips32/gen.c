#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "mips32.h"


bool mips32_gen_call(
	fct_asm_context_t* context, fct_asm_field_t* field)
{
	if (!field)
		return false;

	if ((field->xform != FCT_ASM_FIELD_XFORM_NONE)
		|| (field->type != FCT_ASM_FIELD_TYPE_SYMBOL))
	{
		FCT_DEBUG_ERROR_FIELD(field,
			"Expected untransformed symbol.");
		return false;
	}

	mips32_instruction_t inst;
	inst.op   = mips32_op_jal;
	inst.addr = 0;
	return fct_asm_context_emit(
			context, &inst.mask,
			sizeof(inst), sizeof(inst),
			FCT_CHUNK_ACCESS_RX,
			FCT_CHUNK_CONTENT_DATA,
			field->symbol.name, 0, false, true, 0);
}

bool mips32_gen_return(fct_asm_context_t* context)
{
	mips32_instruction_t inst;
	inst.op   = mips32_op_func;
	inst.func = mips32_func_jr;
	inst.h    = 0;
	inst.d    = 0;
	inst.t    = 0;
	inst.s    = mips32_reg_ra;
	return fct_asm_context_emit_code(
		context, &inst, sizeof(inst), sizeof(inst), (sizeof(inst) * 2));
}

bool mips32_gen_syscall(
	fct_asm_context_t* context, fct_asm_field_t* field)
{
	if (!field)
		return false;

	if ((field->xform != FCT_ASM_FIELD_XFORM_NONE)
		|| (field->type != FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED))
	{
		FCT_DEBUG_ERROR_FIELD(field,
			"Expected be untransformed unsigned immediate.");
		return false;
	}

	if (field->immediate.size > 20)
	{
		FCT_DEBUG_ERROR_FIELD(field,
			"System call (%u) out of range.", field->immediate.value);
		return false;
	}

	mips32_instruction_t inst;
	inst.op   = mips32_op_func;
	inst.func = mips32_func_syscall;
	inst.code = field->immediate.value;
	return fct_asm_context_emit_code(
		context, &inst, sizeof(inst), sizeof(inst), 0);
}
