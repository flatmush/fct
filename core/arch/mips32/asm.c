#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "mips32.h"


typedef enum
{
	_mf_no,
	_mf_rs, _mf_rt, _mf_rd, _mf_rh,
	_mf_is, _mf_it, _mf_id, _mf_ih,
	_mf_co,
	_mf_sb, _mf_sh, _mf_sw,
	_mf_la, _mf_lw,
	_mf_ui, _mf_si,
} mips32__field_t;

typedef struct
{
	const char* mnem;
	unsigned op, func;
	unsigned h, d, t, s;
	mips32__field_t field[4];
	bool boundary;
} mips32__op_t;

static mips32__op_t mips32__op_name[] = {
	{ "sll"    , 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_ih, _mf_no }, 0 },
	{ "srl"    , 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_ih, _mf_no }, 0 },
	{ "rotr"   , 0x00, 0x02, 0x00, 0x00, 0x00, 0x01, { _mf_rd, _mf_rt, _mf_ih, _mf_no }, 0 },
	{ "sra"    , 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_ih, _mf_no }, 0 },
	{ "sllv"   , 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_rs, _mf_no }, 0 },
	{ "srlv"   , 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_rs, _mf_no }, 0 },
	{ "rotrv"  , 0x00, 0x06, 0x01, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_rs, _mf_no }, 0 },
	{ "srav"   , 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_rs, _mf_no }, 0 },
	{ "jr"     , 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_no, _mf_no, _mf_no }, 1 },
	{ "jalr"   , 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_no, _mf_no }, 0 },
	{ "movz"   , 0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_rs, _mf_no }, 0 },
	{ "movn"   , 0x00, 0x0B, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_rs, _mf_no }, 0 },
	{ "syscall", 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, { _mf_co, _mf_no, _mf_no, _mf_no }, 0 },
	{ "break"  , 0x00, 0x0D, 0x00, 0x00, 0x00, 0x00, { _mf_co, _mf_no, _mf_no, _mf_no }, 0 },
	{ "sync"   , 0x00, 0x0F, 0x00, 0x00, 0x00, 0x00, { _mf_ih, _mf_no, _mf_no, _mf_no }, 0 },
	{ "mfhi"   , 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_no, _mf_no, _mf_no }, 0 },
	{ "mthi"   , 0x00, 0x11, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_no, _mf_no, _mf_no }, 0 },
	{ "mflo"   , 0x00, 0x12, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_no, _mf_no, _mf_no }, 0 },
	{ "mtlo"   , 0x00, 0x13, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_no, _mf_no, _mf_no }, 0 },
	{ "mult"   , 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_rt, _mf_no, _mf_no }, 0 },
	{ "multu"  , 0x00, 0x19, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_rt, _mf_no, _mf_no }, 0 },
	{ "div"    , 0x00, 0x1A, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_rt, _mf_no, _mf_no }, 0 },
	{ "divu"   , 0x00, 0x1B, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_rt, _mf_no, _mf_no }, 0 },
	{ "add"    , 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "addu"   , 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "sub"    , 0x00, 0x22, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "subu"   , 0x00, 0x23, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "and"    , 0x00, 0x24, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "or"     , 0x00, 0x25, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "xor"    , 0x00, 0x26, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "nor"    , 0x00, 0x27, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "slt"    , 0x00, 0x2A, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },
	{ "sltu"   , 0x00, 0x2B, 0x00, 0x00, 0x00, 0x00, { _mf_rd, _mf_rs, _mf_rt, _mf_no }, 0 },

	{ "bltz"   , 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_sw, _mf_no, _mf_no }, 0 },
	{ "bgez"   , 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, { _mf_rs, _mf_sw, _mf_no, _mf_no }, 0 },
	{ "synci"  , 0x01, 0x00, 0x00, 0x00, 0x1F, 0x00, { _mf_rs, _mf_sw, _mf_no, _mf_no }, 0 },

	{ "j"      , 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_la, _mf_no, _mf_no, _mf_no }, 1 },
	{ "jal"    , 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_la, _mf_no, _mf_no, _mf_no }, 0 },

	{ "beq"    , 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_rt, _mf_sw, _mf_no }, 0 },
	{ "bne"    , 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_rt, _mf_sw, _mf_no }, 0 },
	{ "blez"   , 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_sw, _mf_no, _mf_no }, 0 },
	{ "bgtz"   , 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rs, _mf_sw, _mf_no, _mf_no }, 0 },

	{ "addi"   , 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_is, _mf_no }, 0 },
	{ "addiu"  , 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_ui, _mf_no }, 0 },
	{ "slti"   , 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_is, _mf_no }, 0 },
	{ "sltiu"  , 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_ui, _mf_no }, 0 },
	{ "andi"   , 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_ui, _mf_no }, 0 },
	{ "ori"    , 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_ui, _mf_no }, 0 },
	{ "xori"   , 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_ui, _mf_no }, 0 },
	{ "lui"    , 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_ui, _mf_no, _mf_no }, 0 },

	{ "tlbr"   , 0x10, 0x01, 0x00, 0x00, 0x00, 0x10, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },
	{ "tlbwi"  , 0x10, 0x02, 0x00, 0x00, 0x00, 0x10, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },
	{ "tlbwr"  , 0x10, 0x06, 0x00, 0x00, 0x00, 0x10, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },
	{ "tlbwp"  , 0x10, 0x08, 0x00, 0x00, 0x00, 0x10, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },
	{ "eret"   , 0x10, 0x18, 0x00, 0x00, 0x00, 0x10, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },
	{ "deret"  , 0x10, 0x1F, 0x00, 0x00, 0x00, 0x10, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },
	{ "wait"   , 0x10, 0x18, 0x00, 0x00, 0x00, 0x10, { _mf_lw, _mf_no, _mf_no, _mf_no }, 0 },

	/* TODO - Encode co-processor operations. */

	{ "ei"     , 0x10, 0x00, 0x00, 0x0C, 0x00, 0x0B, { _mf_rt, _mf_no, _mf_no, _mf_no }, 0 },
	{ "di"     , 0x10, 0x10, 0x00, 0x0C, 0x00, 0x0B, { _mf_rt, _mf_no, _mf_no, _mf_no }, 0 },
	{ "wrpgpr" , 0x10, 0x00, 0x00, 0x00, 0x00, 0x0E, { _mf_rd, _mf_rt, _mf_no, _mf_no }, 0 },

	{ "wsbh"   , 0x1C, 0x20, 0x02, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_no, _mf_no }, 0 },
	{ "seb"    , 0x1C, 0x20, 0x10, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_no, _mf_no }, 0 },
	{ "seh"    , 0x1C, 0x20, 0x18, 0x00, 0x00, 0x00, { _mf_rd, _mf_rt, _mf_no, _mf_no }, 0 },

	{ "lb"     , 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sb, _mf_no }, 0 },
	{ "lh"     , 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sh, _mf_no }, 0 },
	{ "lw"     , 0x23, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sw, _mf_no }, 0 },
	{ "lbu"    , 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sb, _mf_no }, 0 },
	{ "lhu"    , 0x25, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sh, _mf_no }, 0 },
	{ "sb"     , 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sb, _mf_no }, 0 },
	{ "sh"     , 0x29, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sh, _mf_no }, 0 },
	{ "sw"     , 0x2B, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sw, _mf_no }, 0 },
	{ "cache"  , 0x2F, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_si, _mf_no }, 0 },
	{ "lwc1"   , 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sw, _mf_no }, 0 },
	{ "lwc2"   , 0x32, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sw, _mf_no }, 0 },
	{ "swc1"   , 0x39, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sw, _mf_no }, 0 },
	{ "swc2"   , 0x3A, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_rt, _mf_rs, _mf_sw, _mf_no }, 0 },

	{ "nop"    , 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },
	{ "ssnop"  , 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },
	{ "pause"  , 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, { _mf_no, _mf_no, _mf_no, _mf_no }, 0 },

	{ NULL, 0, 0, 0, 0, 0, 0, { 0, 0, 0, 0 }, 0 }
	};



static bool mips32__assemble_psop(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (postfix)
		return false;

	if (fct_tok_keyword(mnem, "li"))
	{
		if ((field_count != 2)
			|| !field[0] || !field[1])
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Expected destination and immediate");
			return false;
		}

		if (field[0]->type != FCT_ASM_FIELD_TYPE_REGISTER)
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected destination register");
			return false;
		}

		if ((field[0]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[1]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Field transformations not supported in this intruction");
			return false;
		}

		mips32_instruction_t code[2];
		code[0].op = mips32_op_lui;
		code[0].t  = field[0]->reg.reg;
		code[0].s  = 0;
		code[1].op = mips32_op_ori;
		code[1].t  = field[0]->reg.reg;
		code[1].s  = mips32_reg_zero;

		switch (field[1]->type)
		{
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
				code[0].ui = (field[1]->immediate.value >> 16);
				code[1].ui = (field[1]->immediate.value & 0xFFFF);

				if (!fct_asm_context_emit_code(
					context, code, 8, 4, 0))
					return false;
				break;

			case FCT_ASM_FIELD_TYPE_SYMBOL:
				code[0].ui = 0;
				code[1].ui = 0;

				if (!fct_asm_context_emit(
					context, code, 8, 4,
					FCT_CHUNK_ACCESS_RX,
					FCT_CHUNK_CONTENT_DATA,
					field[1]->symbol.name,
					0, false, true, 0))
				{
					FCT_DEBUG_ERROR_TOKEN(mnem,
						"Failed to add architecture specific import.");
					return false;
				}
				break;

			default:
					FCT_DEBUG_ERROR_FIELD(field[1],
					"Expected immediate or symbol");
				return false;
		}

		return true;
	}

	FCT_DEBUG_ERROR_TOKEN(mnem,
		"Operation not recognized.");
	return false;
}

bool mips32_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (!context)
		return false;

	mips32__op_t* op;
	for (op = mips32__op_name; op->mnem; op++)
	{
		if (fct_tok_keyword(mnem, op->mnem))
			break;
	}

	if (!op->mnem)
	{
		return mips32__assemble_psop(
			context,
			mnem, postfix,
			field, field_count);
	}

	mips32_instruction_t inst;
	inst.op   = op->op;
	inst.func = op->func;
	inst.h = op->h;
	inst.d = op->d;
	inst.t = op->t;
	inst.s = op->s;

	fct_asm_field_t* sym = NULL;

	unsigned f;
	for (f = 0; (f < 4) && (op->field[f] != _mf_no); f++)
	{
		if (f >= field_count)
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Not enough fields (%u) for instruction", field_count);
			return false;
		}

		if (field[f]->xform != FCT_ASM_FIELD_XFORM_NONE)
		{
			FCT_DEBUG_ERROR_FIELD(field[f],
				"Unsupported field transformation.");
			return false;
		}

		uint32_t val = 0;
		switch (op->field[f])
		{
			case _mf_rs:
			case _mf_rt:
			case _mf_rd:
			case _mf_rh:
				if (field[f]->type != FCT_ASM_FIELD_TYPE_REGISTER)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Expected register");
					return false;
				}
				val = field[f]->reg.reg;
				break;
			case _mf_is:
			case _mf_it:
			case _mf_id:
			case _mf_ih:
			case _mf_co:
			case _mf_la:
			case _mf_lw:
			case _mf_sb:
			case _mf_sh:
			case _mf_sw:
			case _mf_ui:
			case _mf_si:
			{
				switch (field[f]->type)
				{
					case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
					case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
						val = field[f]->immediate.value;
						break;

					case FCT_ASM_FIELD_TYPE_SYMBOL:
						if (sym)
						{
							FCT_DEBUG_ERROR_FIELD(field[f],
								"Multiple symbols per instruction not allowed.");
							return false;
						}

						sym = field[f];
						break;

					default:
						FCT_DEBUG_ERROR_FIELD(field[f],
							"Expected immediate or symbol");
						return false;
				}
			}
			break;

			default:
				break;
		}

		if (!sym)
		{
			switch (op->field[f])
			{
				case _mf_la:
					if (val & 3)
					{
						FCT_DEBUG_ERROR_FIELD(field[f],
							"Long address unaligned");
						return false;
					}
					val >>= 2;
					break;
				case _mf_sh:
					if (val & 1)
					{
						FCT_DEBUG_ERROR_FIELD(field[f],
							"Signed half offset unaligned");
						return false;
					}
					if (val >> 31)
					{
						val >>= 1;
						val  |= (1 << 31);
					} else {
						val >>= 1;
					}
					break;
				case _mf_sw:
					if (val & 3)
					{
						FCT_DEBUG_ERROR_FIELD(field[f],
							"Signed word offset unaligned");
						return false;
					}
					if (val >> 31)
					{
						val >>= 2;
						val  |= (3 << 30);
					} else {
						val >>= 2;
					}
					break;
				default:
					break;
			}
		}

		switch (op->field[f])
		{
			case _mf_is:
			case _mf_it:
			case _mf_id:
			case _mf_ih:
				if (val >= 32) /* TODO - Handle modulo immediates (e.g. srl) */
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Immediate out of range");
					return false;
				}
				break;
			case _mf_co:
				if (val >> 20)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Code out of range");
					return false;
				}
				break;
			case _mf_la:
				if (val >> 26)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Long address out of range");
					return false;
				}
				break;
			case _mf_lw:
				if (val >> 19)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Long wait out of range");
					return false;
				}
				break;
			case _mf_ui:
				if (val >> 16)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Unsigned immediate out of range");
					return false;
				}
				break;
			case _mf_si:
			case _mf_sb:
			case _mf_sh:
			case _mf_sw:
				if ((val >> 31)
					? ((val & 0xFFFF8000) != 0xFFFF8000)
					: (val >> 15))
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Signed immediate out of range");
					return false;
				}
				val &= 0xFFFF;
				break;
			default:
				break;
		}

		switch (op->field[f])
		{
			case _mf_rs:
			case _mf_is:
				inst.s |= val;
				break;
			case _mf_rt:
			case _mf_it:
				inst.t |= val;
				break;
			case _mf_rd:
			case _mf_id:
				inst.d |= val;
				break;
			case _mf_rh:
			case _mf_ih:
				inst.h |= val;
				break;
			case _mf_co:
				inst.code |= val;
				break;
			case _mf_lw:
				inst.wait |= val;
				break;
			case _mf_la:
				inst.addr |= val;
				break;
			case _mf_ui:
			case _mf_si:
			case _mf_sb:
			case _mf_sh:
			case _mf_sw:
				inst.ui |= val;
				break;
			default:
				break;
		}
	}

	if (f != field_count)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Too many fields (%u)", field_count);
		return false;
	}

	unsigned boundary
		= (op->boundary ? 8 : 0);

	if (sym)
	{
		bool symrel = false;
		switch (inst.op)
		{
			case mips32_op_beq:
			case mips32_op_bne:
			case mips32_op_bgtz:
			case mips32_op_blez:
				symrel = true;
				break;
			case mips32_op_regimm:
				switch (inst.t)
				{
					case mips32_regimm_bgez:
					case mips32_regimm_bltzal:
					case mips32_regimm_bgezal:
					case mips32_regimm_bltzall:
					case mips32_regimm_bgezall:
						symrel = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}

		if (!fct_asm_context_emit(
			context, &inst.mask, 4, 4,
			FCT_CHUNK_ACCESS_RX,
			FCT_CHUNK_CONTENT_DATA,
			sym->symbol.name, (symrel ? 4 : 0), symrel, true,
			boundary))
		{
			FCT_DEBUG_ERROR_FIELD(sym,
				"Failed to add architecture specific import.");
			return false;
		}
	}
	else
	{
		if (!fct_asm_context_emit_code(
			context, &inst.mask, 4, 4, boundary))
			return false;
	}

	return true;
}
