#ifndef __FCT_ARCH_MIPS32_H__
#define __FCT_ARCH_MIPS32_H__

#include <fct/arch/arch.h>


typedef union __attribute__((__packed__))
{
	struct __attribute__((__packed__))
	{
		unsigned func : 6;
		unsigned h    : 5;
		unsigned d    : 5;
		unsigned t    : 5;
		unsigned s    : 5;
		unsigned op   : 6;
	};
	struct __attribute__((__packed__))
	{
		unsigned ui    : 16;
		unsigned _res0 : 16;
	};
	struct __attribute__((__packed__))
	{
		signed   si    : 16;
		unsigned _res1 : 16;
	};
	struct __attribute__((__packed__))
	{
		unsigned addr  : 26;
		unsigned _res2 :  6;
	};
	struct __attribute__((__packed__))
	{
		unsigned _res3 :  6;
		unsigned code  : 20;
		unsigned _res4 :  6;
	};
	struct __attribute__((__packed__))
	{
		unsigned _res5 :  6;
		signed   wait  : 19;
		unsigned _res6 :  7;
	};
	uint32_t mask;
} mips32_instruction_t;

typedef enum
{
	mips32_reg_zero =  0,
	mips32_reg_ra   = 31,
} mips32_reg_e;

typedef enum
{
	mips32_op_func   =  0,
	mips32_op_regimm =  1,
	mips32_op_j      =  2,
	mips32_op_jal    =  3,
	mips32_op_beq    =  4,
	mips32_op_bne    =  5,
	mips32_op_blez   =  6,
	mips32_op_bgtz   =  7,
	mips32_op_addiu  =  9,
	mips32_op_slti   = 10,
	mips32_op_sltiu  = 11,
	mips32_op_andi   = 12,
	mips32_op_ori    = 13,
	mips32_op_xori   = 14,
	mips32_op_lui    = 15,
	mips32_op_spec2  = 28,
	mips32_op_lb     = 32,
	mips32_op_lh     = 33,
	mips32_op_lw     = 35,
	mips32_op_lbu    = 36,
	mips32_op_lhu    = 37,
	mips32_op_sb     = 40,
	mips32_op_sh     = 41,
	mips32_op_sw     = 43,
	mips32_op_cache  = 47,
} mips32_op_e;

typedef enum
{
	mips32_func_sll     =  0,
	mips32_func_srl     =  2,
	mips32_func_sra     =  3,
	mips32_func_sllv    =  4,
	mips32_func_srlv    =  6,
	mips32_func_srav    =  7,
	mips32_func_jr      =  8,
	mips32_func_movz    = 10,
	mips32_func_movn    = 11,
	mips32_func_syscall = 12,
	mips32_func_sync    = 15,
	mips32_func_mfhi    = 16,
	mips32_func_mthi    = 17,
	mips32_func_mflo    = 18,
	mips32_func_mtlo    = 19,
	mips32_func_mult    = 24,
	mips32_func_multu   = 25,
	mips32_func_divu    = 27,
	mips32_func_add     = 32,
	mips32_func_addu    = 33,
	mips32_func_subu    = 35,
	mips32_func_and     = 36,
	mips32_func_or      = 37,
	mips32_func_xor     = 38,
	mips32_func_nor     = 39,
	mips32_func_slt     = 42,
	mips32_func_sltu    = 43,
} mips32_func_e;

typedef enum
{
	mips32_regimm_bltz    =  0,
	mips32_regimm_bgez    =  1,
	mips32_regimm_bltzal  = 16,
	mips32_regimm_bgezal  = 17,
	mips32_regimm_bltzall = 18,
	mips32_regimm_bgezall = 19,
} mips32_regimm_e;

typedef enum
{
	mips32_spec2_mul = 2,
} mips32_spec2_e;



bool mips32_gen_call(
	fct_asm_context_t* context, fct_asm_field_t* field);
bool mips32_gen_return(fct_asm_context_t* context);
bool mips32_gen_syscall(
	fct_asm_context_t* context, fct_asm_field_t* field);

bool mips32_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count);

bool mips32_link(
	mips32_instruction_t* code,
	uint64_t size, uint64_t symbol);


fct_arch_t fct_arch__mips32;
#define FCT_ARCH_MIPS32_STATIC &fct_arch__mips32
fct_arch_t* fct_arch_mips32;

fct_arch_t fct_arch_mips__o32;
#define FCT_ARCH_MIPS_O32_STATIC &fct_arch_mips__o32
fct_arch_t* fct_arch_mips_o32;

fct_arch_t fct_arch_mips__n32;
#define FCT_ARCH_MIPS_N32_STATIC &fct_arch_mips__n32
fct_arch_t* fct_arch_mips_n32;

#define ELF_MACHINE_MIPS (uint16_t)0x08

#endif
