#include <fct/arch/arch.h>
#include <fct/arch/emu.h>
#include <fct/object/format/elf.h>
#include "mips32.h"


static fct_arch_spec_t fct_arch_spec__mips32 =
{
	.mau       =  8,
	.addr_size =  4,
	.word_size =  4,
	.inst_size =  4,

	.reg_count = 32,

	.interrupt_count = 0,

	.elf_machine = ELF_MACHINE_MIPS,
};

static fct_keyval_t fct_arch__mips32_reg_names[] =
{
	{ "zero", mips32_reg_zero },
	{ "ra"  , mips32_reg_ra   },
	{ NULL, 0 }
};

fct_arch_emu_t fct_arch_emu__mips32;

fct_arch_t fct_arch__mips32 =
{
	.name = "mips32",
	.base = NULL,
	.emu  = &fct_arch_emu__mips32,
	.spec = &fct_arch_spec__mips32,

	.reg_names = fct_arch__mips32_reg_names,

	.elf_osabi      = ELF_OSABI_STANDALONE,
	.elf_abiversion = 0,

	.gen_call    = mips32_gen_call,
	.gen_return  = mips32_gen_return,
	.gen_push    = NULL,
	.gen_pop     = NULL,
	.gen_syscall = mips32_gen_syscall,

	.assemble    =        mips32_assemble,
	.disassemble =        NULL,
	.link        = (void*)mips32_link,
};

fct_arch_t* fct_arch_mips32
	= &fct_arch__mips32;
