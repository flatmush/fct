#include <stdlib.h>
#include "mips32.h"
#include <fct/arch/emu.h>
#include <fct/debug.h>



#ifdef MIPS32_DEBUG
	#define MIPS32_DEBUG_ASM(x...) FCT_DEBUG_INFO("Asm: " x)
#else
	#define MIPS32_DEBUG_ASM(x...)
#endif



typedef struct
{
	uint32_t pc, next;
	union
	__attribute__((__packed__))
	{
		uint32_t  r[32];
		 int32_t rs[32];
	};
	uint32_t hi, lo;
} mips32_core_t;



static inline uint32_t mips32_sign_extend8(uint8_t x)
	{ return (((x >> 7) ? 0xFFFFFF00 : 0) | x); }

static inline uint32_t mips32_sign_extend16(uint16_t x)
	{ return (((x >> 15) ? 0xFFFF0000 : 0) | x); }



static inline uint32_t mips32_reg_read(
	mips32_core_t* core, mips32_reg_e reg)
{
	return (reg == 0 ? 0 : core->r[reg]);
}

static inline int32_t mips32_reg_read_signed(
	mips32_core_t* core, mips32_reg_e reg)
{
	return (reg == 0 ? 0 : core->rs[reg]);
}



static inline bool mips32_mem_read32(
	fct_device_t* bus, uint32_t address, uint32_t* data)
{
	/* TODO - Handle unaligned read. */
	if (address & 3)
		return false;

	fct_device_read(bus, (address >> 2), data);
	return true;
}

static inline bool mips32_mem_read16(
	fct_device_t* bus, uint32_t address, uint16_t* data)
{
	/* TODO - Handle unaligned read. */
	if (address & 1)
		return false;

	uint16_t datah[2];
	fct_device_read(bus, (address >> 2), datah);

	if (data)
		*data = ((address & 2) ? datah[1] : datah[0]);
	return true;
}


static inline bool mips32_mem_read8(
	fct_device_t* bus, uint32_t address, uint8_t* data)
{
	uint8_t datab[4];
	fct_device_read(bus, (address >> 2), datab);

	if (data)
		*data = datab[address & 3];
	return true;
}



static inline bool mips32_mem_write32(
	fct_device_t* bus, uint32_t address, uint32_t data)
{
	/* TODO - Handle unaligned write. */
	if (address & 3)
		return false;

	fct_device_write(bus, (address >> 2), 0xF, &data);
	return true;
}

static inline bool mips32_mem_write16(
	fct_device_t* bus, uint32_t address, uint16_t data)
{
	/* TODO - Handle unaligned write. */
	if (address & 1)
		return false;

	uint16_t datah[2] = { data, data };
	fct_device_write(bus, (address >> 2), ((address & 2) ? 0xC : 0x3), datah);
	return true;
}


static inline bool mips32_mem_write8(
	fct_device_t* bus, uint32_t address, uint8_t data)
{
	uint8_t datab[4] = { data, data, data, data };
	fct_device_write(bus, (address >> 2), (1 << (address & 3)), datab);
	return true;
}



static mips32_core_t* mips32_core_create(void)
{
	mips32_core_t* core
		= (mips32_core_t*)malloc(
			sizeof(mips32_core_t));
	if (!core) return NULL;

	#ifdef MIPS32_DEBUG
	memset(core, 0x00, sizeof(mips32_core_t));
	#endif

	core->pc   = 0;
	core->next = core->pc + 4;
	return core;
}

static void mips32_core_delete(mips32_core_t* core)
{
	if (!core)
		return;

	free(core);
}

static void mips32_core_exec(mips32_core_t* core, fct_device_t* bus)
{
	if (!core)
		return;

	mips32_instruction_t inst;
	if (!mips32_mem_read32(bus, core->pc, (uint32_t*)&inst))
	{
		/* TODO - Handle as an exception. */
		FCT_DEBUG_ERROR(
			"Invalid instruction address 0x%08X", core->pc);
		return;
	}

	#ifdef MIPS32_DEBUG
	FCT_DEBUG_INFO(
		"Code: 0x%08" PRIX32 ": 0x%08" PRIX32,
		core->pc, inst.mask);
	#endif

	uint32_t next = (core->next + 4);
	switch (inst.op)
	{
		case mips32_op_func:
			switch (inst.func)
			{
				case mips32_func_sll:
					core->r[inst.d] = mips32_reg_read(core, inst.t) << inst.h;
					break;
				case mips32_func_srl:
					core->r[inst.d] = mips32_reg_read(core, inst.t) >> inst.h;
					break;
				case mips32_func_sra:
					core->rs[inst.d] = mips32_reg_read_signed(core, inst.t) >> inst.h;
					break;
				case mips32_func_sllv:
					core->r[inst.d] = mips32_reg_read(core, inst.t)
						<< mips32_reg_read(core, inst.s);
					break;
				case mips32_func_srlv:
					core->r[inst.d] = mips32_reg_read(core, inst.t)
						>> mips32_reg_read(core, inst.s);
					break;
				case mips32_func_srav:
					core->rs[inst.d] = mips32_reg_read_signed(core, inst.t)
						>> mips32_reg_read_signed(core, inst.s);
					break;
				case mips32_func_jr:
					MIPS32_DEBUG_ASM("JR $%" PRIu32,
						inst.s);
					next = mips32_reg_read(core, inst.s);
					break;
				case mips32_func_movz:
					if (mips32_reg_read(core, inst.t) == 0)
						core->r[inst.d] = mips32_reg_read(core, inst.s);
					break;
				case mips32_func_movn:
					if (mips32_reg_read(core, inst.t) != 0)
						core->r[inst.d] = mips32_reg_read(core, inst.s);
					break;
				case mips32_func_sync:
					MIPS32_DEBUG_ASM("SYNC %" PRIu32, inst.h);
					/* TODO - Implement in some way */
					break;
				case mips32_func_mfhi:
					core->r[inst.d] = core->hi;
					break;
				case mips32_func_mthi:
					core->hi = mips32_reg_read(core, inst.s);
					break;
				case mips32_func_mflo:
					core->r[inst.d] = core->lo;
					break;
				case mips32_func_mtlo:
					core->lo = mips32_reg_read(core, inst.s);
					break;
				case mips32_func_mult:
					{
						int64_t r = (int64_t)mips32_reg_read_signed(core, inst.s)
							* (int64_t)mips32_reg_read_signed(core, inst.t);
						core->lo = (r & 0xFFFFFFFF);
						core->hi = ((uint64_t)r >> 32);
					}
					break;
				case mips32_func_multu:
					{
						uint64_t r = (uint64_t)mips32_reg_read(core, inst.s)
							* (uint64_t)mips32_reg_read(core, inst.t);
						core->lo = (r & 0xFFFFFFFF);
						core->hi = (r >> 32);
					}
					break;
				case mips32_func_divu:
					core->lo = mips32_reg_read(core, inst.s)
						/ mips32_reg_read(core, inst.t);
					core->hi = mips32_reg_read(core, inst.s)
						% mips32_reg_read(core, inst.t);
					break;
				case mips32_func_add:
					/* TODO - Exception properly */
					core->r[inst.d] = mips32_reg_read(core, inst.s)
						+ mips32_reg_read(core, inst.t);
					break;
				case mips32_func_addu:
					core->r[inst.d] = mips32_reg_read(core, inst.s)
						+ mips32_reg_read(core, inst.t);
					break;
				case mips32_func_subu:
					core->r[inst.d] = mips32_reg_read(core, inst.s)
						+ ~mips32_reg_read(core, inst.t) + 1;
					break;
				case mips32_func_and:
					core->r[inst.d] = mips32_reg_read(core, inst.s)
						& mips32_reg_read(core, inst.t);
					break;
				case mips32_func_or:
					core->r[inst.d] = mips32_reg_read(core, inst.s)
						| mips32_reg_read(core, inst.t);
					break;
				case mips32_func_xor:
					core->r[inst.d] = mips32_reg_read(core, inst.s)
						^ mips32_reg_read(core, inst.t);
					break;
				case mips32_func_nor:
					core->r[inst.d] = ~(mips32_reg_read(core, inst.s)
						| mips32_reg_read(core, inst.t));
					break;
				case mips32_func_slt:
					core->r[inst.d] = (mips32_reg_read_signed(core, inst.s)
						< mips32_reg_read_signed(core, inst.t));
				case mips32_func_sltu:
					core->r[inst.d] = (mips32_reg_read(core, inst.s)
						< mips32_reg_read(core, inst.t));
					break;
				default:
					FCT_DEBUG_ERROR(
						"Unrecognized function %u", inst.func);
					return;
			}
			break;
		case mips32_op_regimm:
			switch (inst.t)
			{
				case mips32_regimm_bltz:
					MIPS32_DEBUG_ASM("BLTZ $%u, %" PRId32,
						inst.s, (inst.si << 2));
					if (mips32_reg_read_signed(core, inst.s) < 0)
						next = (core->next + (inst.si << 2));
					break;
				case mips32_regimm_bgez:
					MIPS32_DEBUG_ASM("BGEZ $%u, %" PRId32,
						inst.s, (inst.si << 2));
					if (mips32_reg_read_signed(core, inst.s) >= 0)
						next = (core->next + (inst.si << 2));
					break;
				case mips32_regimm_bltzal:
				case mips32_regimm_bltzall:
					MIPS32_DEBUG_ASM("BLTZAL $%u, %" PRId32,
						inst.s, (inst.si << 2));
					if (mips32_reg_read_signed(core, inst.s) < 0)
					{
						core->r[mips32_reg_ra] = next;
						next = (core->next + (inst.si << 2));
					}
					break;
				case mips32_regimm_bgezal:
				case mips32_regimm_bgezall:
					MIPS32_DEBUG_ASM("BGEZAL $%u, %" PRId32,
						inst.s, (inst.si << 2));
					if (mips32_reg_read_signed(core, inst.s) >= 0)
					{
						core->r[mips32_reg_ra] = next;
						next = (core->next + (inst.si << 2));
					}
					break;
				default:
					FCT_DEBUG_ERROR(
						"Unrecognised regimm %u", inst.t);
					return;
			}
			break;
		case mips32_op_spec2:
			switch (inst.func)
			{
				case mips32_spec2_mul:
					core->r[inst.d] = (mips32_reg_read(core, inst.s)
						* mips32_reg_read(core, inst.t));
					break;
				default:
					FCT_DEBUG_ERROR(
						"Unrecognized SPECIAL2 %u", inst.func);
					return;
			}
			break;
		case mips32_op_j:
			MIPS32_DEBUG_ASM("J 0x%08" PRIX32,
				(core->pc & 0xF0000000) | (inst.addr << 2));
			next = ((core->pc & 0xF0000000)
				| (inst.addr << 2));
			break;
		case mips32_op_jal:
			MIPS32_DEBUG_ASM("JAL 0x%08" PRIX32,
				(core->pc & 0xF0000000) | (inst.addr << 2));
			core->r[mips32_reg_ra] = next;
			next = ((core->pc & 0xF0000000) | (inst.addr << 2));
			break;
		case mips32_op_beq:
			MIPS32_DEBUG_ASM("BEQ $%u, $%u, %" PRId32,
				inst.t, inst.s, (inst.si << 2));
			if (mips32_reg_read(core, inst.s) == mips32_reg_read(core, inst.t))
				next = (core->next + (inst.si << 2));
			break;
		case mips32_op_bne:
			MIPS32_DEBUG_ASM("BNE $%u, $%u, %" PRId32,
				inst.t, inst.s, (inst.si << 2));
			if (mips32_reg_read(core, inst.s) != mips32_reg_read(core, inst.t))
				next = (core->next + (inst.si << 2));
			break;
		case mips32_op_blez:
			MIPS32_DEBUG_ASM("BLEZ $%u, %" PRId32,
				inst.s, (inst.si << 2));
			if (mips32_reg_read_signed(core, inst.s) <= 0)
				next = (core->next + (inst.si << 2));
			break;
		case mips32_op_bgtz:
			MIPS32_DEBUG_ASM("BGTZ $%u, %" PRId32,
				inst.s, (inst.si << 2));
			if (mips32_reg_read_signed(core, inst.s) > 0)
				next = (core->next + (inst.si << 2));
			break;
		case mips32_op_lui:
			MIPS32_DEBUG_ASM("LUI $%u, %" PRIX32,
				inst.t, inst.ui);
			core->r[inst.t] = (inst.ui << 16);
			break;
		case mips32_op_slti:
			MIPS32_DEBUG_ASM("SLTI $%u, %" PRId32,
				inst.t, inst.si);
			core->r[inst.t] = (mips32_reg_read_signed(core, inst.s) < inst.si);
			break;
		case mips32_op_sltiu:
			MIPS32_DEBUG_ASM("SLTIU $%u, %" PRIu32,
				inst.t, inst.ui);
			core->r[inst.t] = (mips32_reg_read(core, inst.s) < inst.ui);
			break;
		case mips32_op_addiu:
			MIPS32_DEBUG_ASM("ADDIU $%u, %" PRId32,
				inst.t, inst.si);
			core->r[inst.t] = mips32_reg_read(core, inst.s) + inst.si;
			break;
		case mips32_op_andi:
			MIPS32_DEBUG_ASM("ANDI $%u, 0x%04" PRIX32,
				inst.s, inst.ui);
			core->r[inst.t] = mips32_reg_read(core, inst.s) & inst.ui;
			break;
		case mips32_op_ori:
			MIPS32_DEBUG_ASM("ORI $%u, 0x%04" PRIX32,
				inst.s, inst.ui);
			core->r[inst.t] = mips32_reg_read(core, inst.s) | inst.ui;
			break;
		case mips32_op_xori:
			MIPS32_DEBUG_ASM("XORI $%u, 0x%04" PRIX32,
				inst.s, inst.ui);
			core->r[inst.t] = mips32_reg_read(core, inst.s) ^ inst.ui;
			break;
		case mips32_op_lb:
			MIPS32_DEBUG_ASM("LB $%u, %d($%u)",
				inst.t, inst.si, inst.s);
			{
				uint8_t data = 0;
				mips32_mem_read8(bus,
					(mips32_reg_read(core, inst.s) + inst.si),
					&data);
				core->rs[inst.t] = mips32_sign_extend8(data);
			}
			break;
		case mips32_op_lh:
			MIPS32_DEBUG_ASM("LH $%u, %d($%u)",
				inst.t, inst.si, inst.s);
			{
				uint16_t data = 0;
				mips32_mem_read16(bus,
					(mips32_reg_read(core, inst.s) + inst.si),
					&data);
				core->rs[inst.t] = mips32_sign_extend16(data);
			}
			break;
		case mips32_op_lw:
			MIPS32_DEBUG_ASM("LW $%u, %d($%u)",
				inst.t, inst.si, inst.s);
			mips32_mem_read32(bus,
				(mips32_reg_read(core, inst.s) + inst.si),
				&core->r[inst.t]);
			break;
		case mips32_op_lbu:
			MIPS32_DEBUG_ASM("LBU $%u, %d($%u)",
				inst.t, inst.si, inst.s);
			core->r[inst.t] = 0;
			mips32_mem_read8(bus,
				(mips32_reg_read(core, inst.s) + inst.si),
				(uint8_t*)&core->r[inst.t]);
			break;
		case mips32_op_lhu:
			MIPS32_DEBUG_ASM("LHU $%u, %d($%u)",
				inst.t, inst.si, inst.s);
			core->r[inst.t] = 0;
			mips32_mem_read16(bus,
				(mips32_reg_read(core, inst.s) + inst.si),
				(uint16_t*)&core->r[inst.t]);
			break;
		case mips32_op_sb:
			MIPS32_DEBUG_ASM("SB $%u, %d($%u)",
				inst.t, inst.si, inst.s);
			mips32_mem_write8(bus,
				(mips32_reg_read(core, inst.s) + inst.si),
				(mips32_reg_read(core, inst.t) & 0xFF));
			break;
		case mips32_op_sh:
			MIPS32_DEBUG_ASM("SH $%u, %d($%u)",
				inst.t, inst.si, inst.s);
			mips32_mem_write16(bus,
				(mips32_reg_read(core, inst.s) + inst.si),
				(mips32_reg_read(core, inst.t) & 0xFFFF));
			break;
		case mips32_op_sw:
			MIPS32_DEBUG_ASM("SW $%u, %d($%u)",
				inst.t, inst.si, inst.s);
			mips32_mem_write32(bus,
				(mips32_reg_read(core, inst.s) + inst.si),
				mips32_reg_read(core, inst.t));
			break;
		case mips32_op_cache:
			MIPS32_DEBUG_ASM("CACHE %u, %d($%u)",
				inst.t, inst.si, inst.s);
			/* TODO - Implement some of these */
			break;
		default:
			FCT_DEBUG_ERROR(
				"Unrecognized operation %u.", inst.op);
			return;
	}

	core->pc   = core->next;
	core->next = next;
}



fct_arch_emu_t fct_arch_emu__mips32 =
{
	.create_cb = (void*)mips32_core_create,
	.delete_cb = (void*)mips32_core_delete,
	.exec_cb   = (void*)mips32_core_exec,

	.mmap_read  = NULL,
	.mmap_write = NULL,

	.interrupt = NULL,
};
