#include <fct/debug.h>
#include <fct/asm/context.h>
#include <fct/arch/arch.h>
#include <fct/object/format/elf.h>
#include "mips32.h"



#define MIPS32_ABI_REG_SP 29


static bool mips__gen_push(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (field_count == 0)
		return true;
	if (field_count >= (1U << 13))
		return false;

	mips32_instruction_t inst[field_count + 1];

	inst[0].op = mips32_op_addiu;
	inst[0].t  = MIPS32_ABI_REG_SP;
	inst[0].s  = MIPS32_ABI_REG_SP;
	inst[0].si = -(field_count * 4);

	unsigned i, o;
	for (i = 1, o = (field_count - 1);
		i <= field_count; i++, o--)
	{
		if (!field[i]
			|| (field[i]->type != FCT_ASM_FIELD_TYPE_REGISTER)
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Only untransformed registers may be pushed onto the stack.");
			return false;
		}

		if (field[i]->reg.reg == MIPS32_ABI_REG_SP)
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Can't push stack pointer onto stack.");
			return false;
		}

		inst[i].op = mips32_op_sw;
		inst[i].t  = field[i]->reg.reg;
		inst[i].s  = MIPS32_ABI_REG_SP;
		inst[i].si = (o * 4);
	}

	return fct_asm_context_emit_code(
		context, inst, ((field_count + 1) * sizeof(inst)), sizeof(inst), 0);
}

static bool mips__gen_pop(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (field_count == 0)
		return true;
	if (field_count >= (1U << 13))
		return false;

	mips32_instruction_t inst[field_count + 1];

	unsigned i;
	for (i = 0; i < field_count; i++)
	{
		if (!field[i]
			|| (field[i]->type != FCT_ASM_FIELD_TYPE_REGISTER)
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Only untransformed registers may be popped off the stack.");
			return false;
		}

		if (field[i]->reg.reg == MIPS32_ABI_REG_SP)
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Can't pop stack pointer off stack.");
			return false;
		}

		inst[i].op = mips32_op_lw;
		inst[i].t  = field[i]->reg.reg;
		inst[i].s  = MIPS32_ABI_REG_SP;
		inst[i].si = (i * 4);
	}

	inst[field_count].op = mips32_op_addiu;
	inst[field_count].t  = MIPS32_ABI_REG_SP;
	inst[field_count].s  = MIPS32_ABI_REG_SP;
	inst[field_count].si = (field_count * 4);

	return fct_asm_context_emit_code(
		context, inst, ((field_count + 1) * sizeof(inst)), sizeof(inst), 0);
}



static fct_keyval_t fct_arch_mips__o32_reg_names[] =
{
	{ "at",  1 },
	{ "v0",  2 }, { "v1",  3 },
	{ "a0",  4 }, { "a1",  5 }, { "a2",  6 }, { "a3",  7 },
	{ "t0",  8 }, { "t1",  9 }, { "t2", 10 }, { "t3", 11 },
	{ "t4", 12 }, { "t5", 13 }, { "t6", 14 }, { "t7", 15 },
	{ "s0", 16 }, { "s1", 17 }, { "s2", 18 }, { "s3", 19 },
	{ "s4", 20 }, { "s5", 21 }, { "s6", 22 }, { "s7", 23 },
	{ "t8", 24 }, { "t9", 25 },
	{ "k0", 26 }, { "k1", 27 },
	{ "gp", 28 },
	{ "sp", MIPS32_ABI_REG_SP },
	{ "fp", 30 },
	{ NULL, 0 }
};

fct_arch_t fct_arch_mips__o32 =
{
	.name      = "o32",
	.base      = FCT_ARCH_MIPS32_STATIC,
	.spec      = NULL,
	.reg_names = fct_arch_mips__o32_reg_names,

	.elf_osabi      = ELF_OSABI_NONE,
	.elf_abiversion = 0,

	.gen_call    = NULL,
	.gen_return  = NULL,
	.gen_push    = mips__gen_push,
	.gen_pop     = mips__gen_pop,
	.gen_syscall = NULL,
};

fct_arch_t* fct_arch_mips_o32
	= &fct_arch_mips__o32;



static fct_keyval_t fct_arch_mips__n32_reg_names[] =
{
	{ "at",  1 },
	{ "v0",  2 }, { "v1",  3 },
	{ "a0",  4 }, { "a1",  5 }, { "a2",  6 }, { "a3",  7 },
	{ "a4",  8 }, { "a5",  9 }, { "a6", 10 }, { "a7", 11 },
	{ "t0",  8 }, { "t1",  9 }, { "t2", 10 }, { "t3", 11 },
	{ "t4", 12 }, { "t5", 13 }, { "t6", 14 }, { "t7", 15 },
	{ "s0", 16 }, { "s1", 17 }, { "s2", 18 }, { "s3", 19 },
	{ "s4", 20 }, { "s5", 21 }, { "s6", 22 }, { "s7", 23 },
	{ "t8", 24 }, { "t9", 25 },
	{ "k0", 26 }, { "k1", 27 },
	{ "gp", 28 },
	{ "sp", MIPS32_ABI_REG_SP },
	{ "fp", 30 },
	{ NULL, 0 }
};

fct_arch_t fct_arch_mips__n32 =
{
	.name      = "n32",
	.base      = FCT_ARCH_MIPS32_STATIC,
	.spec      = NULL,
	.reg_names = fct_arch_mips__n32_reg_names,

	.elf_osabi      = ELF_OSABI_NONE,
	.elf_abiversion = 0,

	.gen_call    = NULL,
	.gen_return  = NULL,
	.gen_push    = mips__gen_push,
	.gen_pop     = mips__gen_pop,
	.gen_syscall = NULL,
};

fct_arch_t* fct_arch_mips_n32
	= &fct_arch_mips__n32;
