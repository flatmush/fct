#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "mips32.h"


bool mips32_link(
	mips32_instruction_t* code,
	uint64_t size, uint64_t symbol)
{
	if (!code)
		return NULL;

	if (size == 4)
	{
		if ((code[0].op == mips32_op_j)
			|| (code[0].op == mips32_op_jal))
		{
			if ((symbol & 3)
				|| (symbol >> 28))
				return false;
			/* TODO - Check that code address is correct. */
			code[0].addr = (symbol >> 2);
			return true;
		}

		if ((code[0].op == mips32_op_func)
			&& (code[0].func == mips32_func_syscall))
		{
			if (symbol >> 20)
				return false;
			code[0].code = symbol;
			return true;
		}

		if ((code[0].op == mips32_op_beq)
			|| (code[0].op == mips32_op_bne)
			|| (code[0].op == mips32_op_bgtz)
			|| (code[0].op == mips32_op_blez)
			|| ((code[0].op == mips32_op_regimm)
				&& ((code[0].t ==  mips32_regimm_bltz)
					|| (code[0].t == mips32_regimm_bgez)
					|| (code[0].t == mips32_regimm_bltzal)
					|| (code[0].t == mips32_regimm_bgezal)
					|| (code[0].t == mips32_regimm_bltzall)
					|| (code[0].t == mips32_regimm_bgezall))))
		{
			if (symbol & 3)
				return false;

			int32_t ssymbol = ((symbol >> 31)
				? -(~symbol + 1) : symbol);
			ssymbol >>= 2;

			if ((ssymbol >= 32768)
				|| (ssymbol < -32768))
				return false;

			code[0].si = ssymbol;
			return true;
		}
	}
	else if (size == 8)
	{
		if ((code[0].op == mips32_op_lui)
			&& (code[1].op == mips32_op_ori)
			&& (code[0].t == code[1].t)
			&& (code[1].s == mips32_reg_zero))
		{
			code[0].ui = (symbol >> 16);
			code[1].ui = (symbol & 0xFFFF);
			return true;
		}
	}

	return false;
}
