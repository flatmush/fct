#ifndef __FCT_ARCH_ARCH_H__
#define __FCT_ARCH_ARCH_H__

#include <fct/arch.h>
#include <fct/lex/tok.h>

struct fct_arch_spec_s
{
	unsigned mau;
	unsigned addr_size;
	unsigned word_size;
	unsigned inst_size;

	unsigned reg_count;

	unsigned interrupt_count;

	uint16_t elf_machine;
};

struct fct_arch_s
{
	const char*      name;
	fct_arch_t*      base;
	fct_arch_spec_t* spec;
	fct_arch_emu_t*  emu;
	fct_keyval_t*    reg_names;

	uint8_t elf_osabi;
	uint8_t elf_abiversion;

	bool (*gen_call   )(fct_asm_context_t* context, fct_asm_field_t* field);
	bool (*gen_return )(fct_asm_context_t* context);
	bool (*gen_push   )(fct_asm_context_t* context, fct_asm_field_t** field, unsigned field_count);
	bool (*gen_pop    )(fct_asm_context_t* context, fct_asm_field_t** field, unsigned field_count);
	bool (*gen_syscall)(fct_asm_context_t* context, fct_asm_field_t* field);

	bool (*assemble)(
		fct_asm_context_t* context,
		const fct_tok_t*   mnem,
		const fct_tok_t*   postfix,
		fct_asm_field_t**  field,
		unsigned           field_count);

	unsigned (*disassemble)(
		fct_arch_t* arch,
		fct_maubuff_t* code, unsigned offset,
        fct_string_t* str);

	bool (*link)(void* code, uint64_t size, uint64_t symbol);
};

#endif
