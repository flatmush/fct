#ifndef __FCT_ARCH_EMU_H__
#define __FCT_ARCH_EMU_H__

#include <fct/arch.h>
#include <fct/device.h>

struct fct_arch_emu_s
{
	void* (*create_cb)(void);
	void  (*delete_cb)(void* core);
	void  (*exec_cb)(void* core, fct_device_t* bus);

	bool (*mmap_read )(
		void* core, uint64_t addr, void* data);
	bool (*mmap_write)(
		void* core, fct_device_t* bus,
		uint64_t addr, uint32_t mask, void* data);

	void (*interrupt)(void* core, uint64_t number);
};

#endif
