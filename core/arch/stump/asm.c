#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "stump.h"



typedef struct
{
	const char*  mnem;
	stump_op_e   op;
	stump_cond_e cond;
	bool         ldcc;
} stump__asm_inst_t;

static stump__asm_inst_t stump__asm_inst[] =
{
	{ "add" , stump_op_add, stump_cond_al, 0 },
	{ "adc" , stump_op_adc, stump_cond_al, 0 },
	{ "sub" , stump_op_sub, stump_cond_al, 0 },
	{ "sbc" , stump_op_sbc, stump_cond_al, 0 },
	{ "and" , stump_op_and, stump_cond_al, 0 },
	{ "or"  , stump_op_or , stump_cond_al, 0 },
	{ "orr" , stump_op_or , stump_cond_al, 0 },

	{ "adds", stump_op_add, stump_cond_al, 1 },
	{ "adcs", stump_op_adc, stump_cond_al, 1 },
	{ "subs", stump_op_sub, stump_cond_al, 1 },
	{ "sbcs", stump_op_sbc, stump_cond_al, 1 },
	{ "ands", stump_op_and, stump_cond_al, 1 },
	{ "ors" , stump_op_or , stump_cond_al, 1 },
	{ "orrs", stump_op_or , stump_cond_al, 1 },

	{ "st"  , stump_op_mem, stump_cond_al, 0 },
	{ "str" , stump_op_mem, stump_cond_al, 0 },
	{ "ld"  , stump_op_mem, stump_cond_al, 1 },
	{ "ldr" , stump_op_mem, stump_cond_al, 1 },

	{ "bal" , stump_op_b  , stump_cond_al, 0 },
	{ "bnv" , stump_op_b  , stump_cond_nv, 0 },
	{ "bhi" , stump_op_b  , stump_cond_hi, 0 },
	{ "bls" , stump_op_b  , stump_cond_ls, 0 },
	{ "bcc" , stump_op_b  , stump_cond_cc, 0 },
	{ "blo" , stump_op_b  , stump_cond_cc, 0 },
	{ "bcs" , stump_op_b  , stump_cond_cs, 0 },
	{ "bhs" , stump_op_b  , stump_cond_cs, 0 },
	{ "bne" , stump_op_b  , stump_cond_ne, 0 },
	{ "bzc" , stump_op_b  , stump_cond_ne, 0 },
	{ "beq" , stump_op_b  , stump_cond_eq, 0 },
	{ "bzs" , stump_op_b  , stump_cond_eq, 0 },
	{ "bvc" , stump_op_b  , stump_cond_vc, 0 },
	{ "bvs" , stump_op_b  , stump_cond_vs, 0 },
	{ "bpl" , stump_op_b  , stump_cond_pl, 0 },
	{ "bnc" , stump_op_b  , stump_cond_pl, 0 },
	{ "bmi" , stump_op_b  , stump_cond_mi, 0 },
	{ "bns" , stump_op_b  , stump_cond_mi, 0 },
	{ "bge" , stump_op_b  , stump_cond_ge, 0 },
	{ "blt" , stump_op_b  , stump_cond_lt, 0 },
	{ "bgt" , stump_op_b  , stump_cond_gt, 0 },
	{ "ble" , stump_op_b  , stump_cond_le, 0 },

	{ NULL, 0, 0, 0 }
};

fct_keyval_t stump__shift_name[] =
{
	{ "asr", stump_shift_asr },
	{ "sra", stump_shift_asr },
	{ "ror", stump_shift_ror },
	{ "rsr", stump_shift_ror },
	{ "rrc", stump_shift_rrc },
	{ "rrx", stump_shift_rrc },
	{ NULL, 0 }
};



bool stump_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (!context)
		return false;

	if (postfix)
	{
		FCT_DEBUG_ERROR_TOKEN(postfix,
			"stump operations shouldn't have a postfix");
		return false;
	}

	unsigned i;
	for (i = 0; stump__asm_inst[i].mnem; i++)
	{
		if (fct_tok_keyword(mnem, stump__asm_inst[i].mnem))
			break;
	}
	if (!stump__asm_inst[i].mnem)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Operation not recognized");
		return false;
	}

	stump__asm_inst_t* asm_inst
		= &stump__asm_inst[i];

	stump_instruction_t inst = { .mask = 0 };
	inst.r.op   = asm_inst->op;
	inst.r.ldcc = asm_inst->ldcc;

	fct_asm_field_t* sym    = NULL;
	bool             symrel = false;

	if (asm_inst->op == stump_op_b)
	{
		inst.b.cond = asm_inst->cond;
		inst.b.type = true;

		if ((field_count != 1) || !field[0]
			|| (field[0]->xform != FCT_ASM_FIELD_XFORM_NONE))
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected single branch target field.");
			return false;
		}

		switch (field[0]->type)
		{
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
				inst.b.offset = field[0]->immediate.value;
				break;
			case FCT_ASM_FIELD_TYPE_SYMBOL:
				sym = field[0];
				inst.b.offset = 0;
				break;
			default:
				FCT_DEBUG_ERROR_FIELD(field[0],
					"Branch target must be symbol or unsigned immediate.");
				return false;
		}
	} else {
		if (field_count < 3)
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Not enough operands");
			return false;
		}

		for (i = 0; i < field_count; i++)
		{
			if (!field[i]
				|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE))
			{
				FCT_DEBUG_ERROR_FIELD(field[i],
					"Field transformations not supported.");
				return false;
			}
		}

		if (field[0]->type != FCT_ASM_FIELD_TYPE_REGISTER)
		{
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected destination register.");
			return false;
		}

		if (field[1]->type != FCT_ASM_FIELD_TYPE_REGISTER)
		{
			FCT_DEBUG_ERROR_FIELD(field[1],
				"Expected register.");
			return false;
		}

		inst.r.dst   = field[0]->reg.reg;
		inst.r.src_a = field[1]->reg.reg;

		if (field[2]->type == FCT_ASM_FIELD_TYPE_REGISTER)
		{
			if (field_count > 4)
			{
				FCT_DEBUG_ERROR_TOKEN(mnem,
					"Too many fields for register type instruction.");
				return false;
			}

			inst.r.shift = stump_shift_none;
			if (field_count == 4)
			{
				if (field[3]->type != FCT_ASM_FIELD_TYPE_SYMBOL)
				{
					FCT_DEBUG_ERROR_FIELD(field[3],
						"Expected shift command.");
					return false;
				}

				unsigned shift;
				if (!fct_keyval_list_resolven(
					stump__shift_name,
					field[3]->symbol.name->data,
					field[3]->symbol.name->size,
					&shift))
				{
					FCT_DEBUG_ERROR_FIELD(field[3],
						"Unrecognized shift command '%.*s'.",
						field[3]->symbol.name->size,
						field[3]->symbol.name->data);
					return false;
				}

				inst.r.shift = shift;
			}

			inst.r.src_b = field[2]->reg.reg;
		}
		else if (field[2]->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
		{
			if (field_count > 3)
			{
				FCT_DEBUG_ERROR_TOKEN(mnem,
					"Too many fields for immediate type instruction.");
				return false;
			}

			inst.i.type  = true;
			inst.i.immed = field[2]->immediate.value;
		}
		else
		{
			FCT_DEBUG_ERROR_FIELD(field[2],
				"Expected register or unsigned immediate.");
			return false;
		}
	}

	if (sym)
	{
		if (!fct_asm_context_emit(
			context, &inst.mask, 1, 0,
			FCT_CHUNK_ACCESS_RX,
			FCT_CHUNK_CONTENT_DATA,
			sym->symbol.name, (symrel ? 1 : 0), symrel, true,
			0))
		{
			FCT_DEBUG_ERROR_FIELD(sym,
				"Failed to add architecture specific import.");
			return 0;
		}
	}

	if (!fct_asm_context_emit_code(
		context, &inst.mask, 1, 0, 0))
		return 0;
	return i;
}
