#ifndef __FCT_ARCH_STUMP_H__
#define __FCT_ARCH_STUMP_H__

#include <fct/arch/arch.h>


typedef enum
{
	stump_op_add = 0,
	stump_op_adc = 1,
	stump_op_sub = 2,
	stump_op_sbc = 3,
	stump_op_and = 4,
	stump_op_or  = 5,
	stump_op_mem = 6,
	stump_op_b   = 7,
} stump_op_e;
#define stump_op_count 8

typedef enum
{
	stump_shift_none = 0,
	stump_shift_asr  = 1,
	stump_shift_ror  = 2,
	stump_shift_rrc  = 3,
} stump_shift_e;

typedef enum
{
	stump_cond_al = 0x0,
	stump_cond_nv = 0x1,
	stump_cond_hi = 0x2,
	stump_cond_ls = 0x3,
	stump_cond_cc = 0x4,
	stump_cond_cs = 0x5,
	stump_cond_ne = 0x6,
	stump_cond_eq = 0x7,
	stump_cond_vc = 0x8,
	stump_cond_vs = 0x9,
	stump_cond_pl = 0xA,
	stump_cond_mi = 0xB,
	stump_cond_ge = 0xC,
	stump_cond_lt = 0xD,
	stump_cond_gt = 0xE,
	stump_cond_le = 0xF,
} stump_cond_e;

typedef enum
{
	stump_reg_pc = 7,
} stump_reg_e;


typedef union __attribute__((__packed__))
{
	struct __attribute__((__packed__))
	{
		stump_shift_e shift : 2;
		stump_reg_e   src_b : 3;
		stump_reg_e   src_a : 3;
		stump_reg_e   dst   : 3;
		bool          ldcc  : 1;
		unsigned      type  : 1; // =0
		stump_op_e    op    : 3;
	} r;
	struct __attribute__((__packed__))
	{
		unsigned      immed : 5;
		stump_reg_e   src_a : 3;
		stump_reg_e   dst   : 3;
		bool          ldcc  : 1;
		unsigned      type  : 1; // =1
		stump_op_e    op    : 3;
	} i;
	struct __attribute__((__packed__))
	{
		unsigned      offset : 8;
		stump_cond_e  cond   : 4;
		unsigned      type   : 1; // =1
		stump_op_e    op     : 3; // =7
	} b;
	uint16_t mask;
} stump_instruction_t;


bool stump_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count);

bool stump_link(
	stump_instruction_t* code,
	uint64_t size, uint64_t symbol);


fct_arch_t fct_arch__stump;
#define FCT_ARCH_STUMP_STATIC &fct_arch__stump
fct_arch_t* fct_arch_stump;

#endif
