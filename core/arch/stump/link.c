#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "stump.h"



bool stump_link(
	stump_instruction_t* code,
	uint64_t size, uint64_t symbol)
{
	if (!code)
		return false;

	if (size == 1)
	{
		if ((code[0].b.op == 7)
			&& code[0].b.type)
		{
			if (symbol >> 8)
				return false;
			code[0].b.offset = symbol;
			return true;
		}
		else if (code[0].i.type)
		{
			if (symbol >> 5)
				return false;
			code[0].i.immed = symbol;
			return true;
		}
	}

	return false;
}
