#include <fct/arch/arch.h>
#include <fct/object/format/elf.h>
#include "stump.h"


static fct_arch_spec_t fct_arch_spec__stump =
{
	.mau       = 16,
	.addr_size =  1,
	.word_size =  1,
	.inst_size =  1,

	.reg_count =  8,

	.interrupt_count = 0,

	.elf_machine = ELF_MACHINE_NONE,
};

static fct_keyval_t fct_arch__stump_reg_names[] =
{
	{ "pc"  , stump_reg_pc },
	{ NULL, 0 }
};

fct_arch_t fct_arch__stump =
{
	.name = "stump",
	.base = NULL,
	.emu  = NULL,
	.spec = &fct_arch_spec__stump,

	.reg_names = fct_arch__stump_reg_names,

	.elf_osabi      = ELF_OSABI_STANDALONE,
	.elf_abiversion = 0,

	.gen_call    = NULL,
	.gen_return  = NULL,
	.gen_push    = NULL,
	.gen_pop     = NULL,
	.gen_syscall = NULL,

	.assemble    =        stump_assemble,
	.disassemble =        NULL,
	.link        = (void*)stump_link,
};

fct_arch_t* fct_arch_stump
	= &fct_arch__stump;
