#include "arch.h"

#include <fct/types.h>
#include <fct/object/format/elf.h>

#include "arm/arm.h"
#include "f32x/f32x.h"
#include "fx16/fx16.h"
#include "mips32/mips32.h"
#include "rv32/rv32.h"
#include "mu0/mu0.h"
#include "stump/stump.h"



static fct_arch_t* fct_arch__list[] =
{
	FCT_ARCH_ARM_STATIC,
	FCT_ARCH_F32X_STATIC,
	FCT_ARCH_FX16_STATIC,
	FCT_ARCH_MIPS32_STATIC,
	FCT_ARCH_RV32_STATIC,
	FCT_ARCH_MU0_STATIC,
	FCT_ARCH_STUMP_STATIC,

	FCT_ARCH_F32X_OABI_STATIC,
	FCT_ARCH_MIPS_O32_STATIC,
	FCT_ARCH_MIPS_N32_STATIC,
	FCT_ARCH_RV32_RVG_STATIC,

	NULL
};



static fct_arch_t* fct_arch_by_name__base(
	const char* name, unsigned nlen, fct_arch_t* base)
{
	if (!name || (nlen == 0))
		return NULL;

	unsigned i;
	for (i = 0; i < nlen; i++)
	{
		if (name[i] == '.')
		{
			fct_arch_t* nbase
				= fct_arch_by_name__base(name, i, base);
			if (!nbase) return NULL;
			return fct_arch_by_name__base(
				&name[i + 1], (nlen - (i + 1)), nbase);
		}
	}

	for (i = 0;  fct_arch__list[i]; i++)
	{
		fct_arch_t* arch = fct_arch__list[i];
		if (!arch->name) continue;

		if ((nlen == strlen(arch->name))
			&& (strncmp(name, arch->name, nlen) == 0)
			&& (arch->base == base))
			return arch;
	}

	return NULL;
}

fct_arch_t* fct_arch_by_name(fct_string_t* name)
{
	if (fct_string_is_empty(name))
		return NULL;
	return fct_arch_by_name__base(
		name->data, name->size, NULL);
}



bool fct_arch_is_ancestor(fct_arch_t* base, fct_arch_t* arch)
{
	if (!base || !arch)
		return false;

	if (base == arch)
		return true;

	if (!arch->base)
		return false;

	if (arch->base == base)
		return true;

	return fct_arch_is_ancestor(
		base, arch->base);
}



const char* fct_arch_reg_name_from_number(
	fct_arch_t* arch, unsigned number)
{
	if (!arch)
		return NULL;

	if (arch->reg_names)
	{
		unsigned i;
		for (i = 0; arch->reg_names[i].key != NULL; i++)
		{
			if (arch->reg_names[i].val == number)
				return arch->reg_names[i].key;
		}
	}

	if (arch->base)
		return fct_arch_reg_name_from_number(arch->base, number);

	return NULL;
}

bool fct_arch_number_from_reg_name(
	fct_arch_t* arch, const char* name, unsigned* number)
{
	if (!arch || !name)
		return false;

	uint32_t reg32;
	if (fct_keyval_list_resolve(
		arch->reg_names, name, &reg32))
	{
		unsigned reg = reg32;
		if (reg != reg32)
			return false;
		if (number) *number = reg;
		return true;
	}

	return fct_arch_number_from_reg_name(
		arch->base, name, number);
}



bool fct_arch_gen_call(
	fct_arch_t* arch, fct_asm_context_t* context,
	fct_asm_field_t* field)
{
	if (!arch || !field)
		return false;

	if (field->xform)
		return false;

	switch (field->type)
	{
		case FCT_ASM_FIELD_TYPE_REGISTER:
		case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
		case FCT_ASM_FIELD_TYPE_SYMBOL:
			break;
		default:
			return false;
	}

	if (arch->gen_call
		&& arch->gen_call(context, field))
		return true;

	return fct_arch_gen_call(
		arch->base, context, field);
}

bool fct_arch_gen_return(
	fct_arch_t* arch, fct_asm_context_t* context)
{
	if (!arch)
		return false;

	if (arch->gen_return
		&& arch->gen_return(context))
		return true;

	return fct_arch_gen_return(
		arch->base, context);
}

bool fct_arch_gen_push(
	fct_arch_t* arch, fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (!arch)
		return false;

	if (!field || (field_count == 0))
		return false;

	if (arch->gen_push
		&& arch->gen_push(context, field, field_count))
		return true;

	return fct_arch_gen_push(
		arch->base, context, field, field_count);
}

bool fct_arch_gen_pop(
	fct_arch_t* arch, fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (!arch)
		return false;

	if (!field || (field_count == 0))
		return false;

	if (arch->gen_pop
		&& arch->gen_pop(context, field, field_count))
		return true;

	return fct_arch_gen_pop(
		arch->base, context, field, field_count);
}

bool fct_arch_gen_syscall(
	fct_arch_t* arch, fct_asm_context_t* context,
	fct_asm_field_t* field)
{
	if (!arch || !field)
		return false;

	if (field->xform)
		return false;

	switch (field->type)
	{
		case FCT_ASM_FIELD_TYPE_REGISTER:
		case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
		case FCT_ASM_FIELD_TYPE_SYMBOL:
			break;
		default:
			return false;
	}

	if (arch->gen_syscall
		&& arch->gen_syscall(context, field))
		return true;

	return fct_arch_gen_syscall(
		arch->base, context, field);
}



bool fct_arch_assemble(
	fct_arch_t*        arch,
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (!arch || !context)
		return false;

	if (!postfix)
	{
		if (fct_tok_keyword(mnem, "call")
			&& (field_count == 1)
			&& fct_arch_gen_call(
				context->arch, context, field[0]))
			return true;

		if (fct_tok_keyword(mnem, "return")
			&& (field_count == 0)
			&& fct_arch_gen_return(
				context->arch, context))
			return true;

		if (fct_tok_keyword(mnem, "push")
			&& fct_arch_gen_push(
				context->arch, context,
				field, field_count))
			return true;

		if (fct_tok_keyword(mnem, "pop")
			&& fct_arch_gen_pop(
				context->arch, context,
				field, field_count))
			return true;

		if (fct_tok_keyword(mnem, "syscall")
			&& (field_count == 1)
			&& fct_arch_gen_syscall(
				context->arch, context, field[0]))
			return true;
	}

	if (arch->assemble
		&& arch->assemble(
			context,
			mnem, postfix,
			field, field_count))
		return true;

	return fct_arch_assemble(
		arch->base, context,
		mnem, postfix,
		field, field_count);
}



unsigned fct_arch_disassemble(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned offset,
	fct_string_t* str)
{
	if (!arch)
		return 0;

	fct_arch_t* base;
	for (base = arch; base; base = base->base)
	{
		if (base->disassemble)
		{
			unsigned len = base->disassemble(arch, code, offset, str);
			if (len > 0) return len;
		}
	}

	return 0;
}

fct_string_t* fct_arch_disassemble_buffer(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned indent)
{
	char indentstr[indent + 1];
	memset(indentstr, '\t', indent);
	indentstr[indent] = '\0';

	fct_string_t* str = fct_string_create("");
	if (!str) return NULL;

	uint32_t size = fct_maubuff_size_get(code);
	unsigned i = 0;
	while (i < size)
	{
		if (!fct_string_append_static_format(str, "%s", indentstr))
		{
			fct_string_delete(str);
			return NULL;
		}

		unsigned offset
			= fct_arch_disassemble(
				arch, code, i, str);
		if ((offset == 0)
			|| !fct_string_append_static_format(str, "\n"))
		{
			fct_string_delete(str);
			return NULL;
		}
		i += offset;
	}

	if (i != size)
	{
		fct_string_delete(str);
		return NULL;
	}

	return str;
}



bool fct_arch_link(
	fct_arch_t* arch, void* code, uint64_t size, uint64_t symbol)
{
	if (!arch)
		return false;

	if (arch->link && arch->link(code, size, symbol))
		return true;

	return fct_arch_link(
		arch->base, code, size, symbol);
}


bool fct_arch__name(
	fct_arch_t* arch, fct_string_t** name)
{
	if (!arch || !name)
		return false;

	if (arch->base && !fct_arch__name(arch->base, name))
		return false;

	if (!arch->name)
		return true;

	if (!*name)
	{
		fct_string_t* sname
			= fct_string_create(arch->name);
		if (!sname)
			return false;

		*name = sname;
		return true;
	}

	if (fct_string_append_static_format(
		*name, ".%s", arch->name))
		return true;

	fct_string_delete(*name);
	*name = NULL;
	return false;
}

fct_string_t* fct_arch_name(fct_arch_t* arch)
{
	fct_string_t* name = NULL;
	return (fct_arch__name(arch, &name) ? name : NULL);
}


fct_arch_emu_t* fct_arch_emu(fct_arch_t* arch)
{
	if (!arch)
		return NULL;
	if (arch->emu)
		return arch->emu;
	return fct_arch_emu(arch->base);
}

const fct_arch_spec_t* fct_arch_spec(
	const fct_arch_t* arch)
{
	if (!arch)
		return NULL;
	if (arch->spec)
		return arch->spec;
	return fct_arch_spec(arch->base);
}



unsigned fct_arch_mau(const fct_arch_t* arch)
{
	const fct_arch_spec_t* arch_spec
		= fct_arch_spec(arch);
	return (arch_spec ? arch_spec->mau : 0);
}

unsigned fct_arch_addr_size(const fct_arch_t* arch)
{
	const fct_arch_spec_t* arch_spec
		= fct_arch_spec(arch);
	return (arch_spec ? arch_spec->addr_size : 0);
}

unsigned fct_arch_word_size(const fct_arch_t* arch)
{
	const fct_arch_spec_t* arch_spec
		= fct_arch_spec(arch);
	return (arch_spec ? arch_spec->word_size : 0);
}

unsigned fct_arch_inst_size(const fct_arch_t* arch)
{
	const fct_arch_spec_t* arch_spec
		= fct_arch_spec(arch);
	return (arch_spec ? arch_spec->inst_size : 0);
}

unsigned fct_arch_reg_count(const fct_arch_t* arch)
{
	const fct_arch_spec_t* arch_spec
		= fct_arch_spec(arch);
	return (arch_spec ? arch_spec->reg_count : 0);
}

uint16_t fct_arch_elf_machine(const fct_arch_t* arch)
{
	if (!arch) return ELF_MACHINE_NONE;

	const fct_arch_spec_t* arch_spec
		= fct_arch_spec(arch);
	if (arch_spec
		&& (arch_spec->elf_machine != ELF_MACHINE_NONE))
		return arch_spec->elf_machine;

	return fct_arch_elf_machine(arch->base);
}

bool fct_arch_elf_abi(
	const fct_arch_t* arch,
	uint8_t* osabi,
	uint8_t* abiversion)
{
	if (!arch) return false;

	if (arch->elf_osabi != ELF_OSABI_NONE)
	{
		if (osabi     ) *osabi      = arch->elf_osabi;
		if (abiversion) *abiversion = arch->elf_abiversion;
		return true;
	}

	return fct_arch_elf_abi(
		arch->base, osabi, abiversion);
}
