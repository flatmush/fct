#include <stdlib.h>
#include "fx16.h"
#include <fct/arch/emu.h>



typedef struct
{
	uint16_t           addr, link, pc, sp;
	fx16_config_t      config;
	uint16_t           reg[8];
	fx16_instruction_t ir;
	bool               immed;
} fx16_core_t;



static void fx16_excep(fx16_core_t* core, uint16_t excep)
{
	core->link = core->pc;
	core->pc = (excep << 1);
	core->config.kernel = true;
	core->config.interrupt = false;
}



static bool fx16_mem_read(
	fct_device_t* bus, uint16_t addr, uint16_t* result)
{
	fct_device_read(bus, addr, result);
	return true;
}

static bool fx16_mem_write(
	fct_device_t* bus, uint16_t addr, uint16_t result)
{
	fct_device_write(bus, addr, 0x1, &result);
	return true;
}



static bool fx16_reg_read(
	fx16_core_t* core, fct_device_t* bus,
	unsigned reg, uint16_t immediate, uint16_t* result)
{
	uint16_t value;
	switch (reg)
	{
		case fx16_reg_imm:
			value = immediate;
			break;
		case fx16_reg_addr:
			value = core->addr;
			break;
		case fx16_reg_data:
			if (!fx16_mem_read(bus, core->addr, &value))
				return false;
			break;
		case fx16_reg_sp:
			value = core->sp;
			break;
		case fx16_reg_stack:
			if (!fx16_mem_read(bus, core->sp++, &value))
				return false;
			break;
		case fx16_reg_link:
			value = core->link;
			break;
		case fx16_reg_conf:
			value = core->config.mask;
			break;
		case fx16_reg_pc:
			value = core->pc;
			break;
		default:
			value = core->reg[reg & 0x7];
			break;
	}

	if (result)
		*result = value;
	return true;
}

static bool fx16_reg_write(
	fx16_core_t* core, fct_device_t* bus,
	unsigned reg, uint16_t result)
{
	switch (reg)
	{
		case fx16_reg_imm:
			return true;
		case fx16_reg_addr:
			core->addr = result;
			return true;
		case fx16_reg_data:
			return fx16_mem_write(bus, core->addr, result);
		case fx16_reg_sp:
			core->sp = result;
			return true;
		case fx16_reg_stack:
			return fx16_mem_write(bus, --core->sp, result);
		case fx16_reg_link:
			core->link = result;
			return true;
		case fx16_reg_conf:
		{
			fx16_config_t new = { .mask = result };
			if (!core->config.kernel && new.kernel)
				fx16_excep(core, fx16_excep_system);
			core->config.mask = result;
			return true;
		}
		case fx16_reg_pc:
			core->pc = result;
			return true;
		default:
			break;
	}
	core->reg[reg & 0x7] = result;
	return true;
}



static bool fx16_cond(fx16_core_t* core, fx16_cond_e cond)
{
	switch (cond)
	{
		case fx16_cond_nv:
			return false;
		case fx16_cond_al:
			return true;
		case fx16_cond_hi:
			return (core->config.carry && !core->config.zero);
		case fx16_cond_ls:
			return (!core->config.carry || core->config.zero);
		case fx16_cond_gt:
			return (!core->config.zero && (core->config.sign == core->config.overflow));
		case fx16_cond_le:
			return (core->config.zero || (core->config.sign != core->config.overflow));
		case fx16_cond_ge:
			return (core->config.sign == core->config.overflow);
		case fx16_cond_lt:
			return (core->config.sign != core->config.overflow);
		case fx16_cond_cc:
			return !core->config.carry;
		case fx16_cond_cs:
			return core->config.carry;
		case fx16_cond_vc:
			return !core->config.overflow;
		case fx16_cond_vs:
			return core->config.overflow;
		case fx16_cond_zc:
			return !core->config.zero;
		case fx16_cond_zs:
			return core->config.zero;
		case fx16_cond_nc:
			return !core->config.sign;
		case fx16_cond_ns:
			return core->config.sign;
		default:
			break;
	}
	return false;
}

static uint16_t _fx16_add(fx16_core_t* core, uint16_t src0, uint16_t src1, unsigned carry)
{
	uint16_t result = src0 + src1 + (carry ? 1 : 0);
	if (core->config.flag_set)
	{
		core->config.sign = (result >> 15);
		core->config.zero = (result == 0);
		core->config.overflow = (((src0 ^ ~src1) & (src0 ^ result)) >> 15);
		core->config.carry    = ((result < src0) || (result < src1));
	}
	return result;
}



static fx16_core_t* fx16_core_create(void)
{
	fx16_core_t* core = (fx16_core_t*)malloc(sizeof(fx16_core_t));
	if (!core) return NULL;
	core->immed = false;
	fx16_excep(core, fx16_excep_reset);
	return core;
}

static void fx16_core_delete(fx16_core_t* core)
{
	if (!core)
		return;

	free(core);
}

static void fx16_core_exec(fx16_core_t* core, fct_device_t* bus)
{
	if (!core)
		return;

	/* Fetch */
	uint16_t fetch;
	if (!fx16_mem_read(bus, core->pc++, &fetch))
	{
		fx16_excep(core, fx16_excep_bad_iaddr);
		return;
	}

	if (!core->immed)
	{
		core->ir.mask = fetch;
		if ((core->ir.src0 == fx16_reg_imm)
			|| (core->ir.src1 == fx16_reg_imm))
		{
			core->immed = true;
			return;
		}
	} else {
		core->immed = false;
	}

	/* Decode */
	uint16_t src[2];
	if (!fx16_reg_read(core, bus, core->ir.src0, fetch, &src[0]))
	{
		fx16_excep(core, fx16_excep_bad_daddr);
		return;
	}

	if (core->ir.src1 == core->ir.src0)
	{
		src[1] = src[0];
	} else if(!fx16_reg_read(core, bus, core->ir.src1, fetch, &src[1]))
	{
		fx16_excep(core, fx16_excep_bad_daddr);
		return;
	}

	/* Execute */
	uint16_t result = 0;
	bool     writeback = true;
	switch (core->ir.op)
	{
		case fx16_op_and:
			result = src[0] & src[1];
			break;
		case fx16_op_orr:
			result = src[0] | src[1];
			break;
		case fx16_op_bic:
			result = src[0] | ~src[1];
			break;
		case fx16_op_xor:
			result = src[0] ^ src[1];
			break;
		case fx16_op_add:
			result = _fx16_add(core, src[0], src[1], 0);
			break;
		case fx16_op_sub:
			result = _fx16_add(core, src[0], ~src[1], 1);
			break;
		case fx16_op_adc:
			result = _fx16_add(core, src[0], src[1], core->config.carry);
			break;
		case fx16_op_sbc:
			result = _fx16_add(core, src[0], ~src[1], ~core->config.carry);
			break;
		case fx16_op_lsl:
			result = src[0] << src[1];
			break;
		case fx16_op_lsr:
			result = src[0] >> src[1];
			break;
		case fx16_op_asr:
			result = src[0] >> src[1];
			if (src[0] >> 16)
				result |= 0xFFFF << (src[1] >= 16 ? 0 : (16 - src[1]));
			break;
		case fx16_op_ror:
			result = (src[0] >> (src[1] & 0xF)) | (src[0] << (16 - (src[1] & 0xF)));
			break;
		case fx16_op_cmb:
			result    = src[0];
			writeback = (src[1] != 0);
			break;
		case fx16_op_cmv:
			result = src[0];
			writeback = fx16_cond(core, core->ir.src1);
			break;
		case fx16_op_una:
			switch (src[1])
			{
				case fx16_op_una_neg:
					result = ~src[0] + 1;
					break;
				case fx16_op_una_abs:
					result = ((src[0] >> 15) ? (~src[0] + 1) : src[0]);
					break;
				case fx16_op_una_nbs:
					result = ((src[0] >> 15) ? src[0] : (~src[0] + 1));
					break;
				default:
					fx16_excep(core, fx16_excep_instruction);
					return;
			}
			break;
		default:
			fx16_excep(core, fx16_excep_instruction);
			return;
	}

	/* Conditional Writeback */
	if (writeback
		&& !fx16_reg_write(core, bus, core->ir.dest, result))
		fx16_excep(core, fx16_excep_bad_daddr);
}





fct_arch_emu_t fct_arch_emu__fx16 =
{
	.create_cb = (void*)fx16_core_create,
	.delete_cb = (void*)fx16_core_delete,
	.exec_cb   = (void*)fx16_core_exec,

	.mmap_read  = NULL,
	.mmap_write = NULL,

	.interrupt = NULL,
};
