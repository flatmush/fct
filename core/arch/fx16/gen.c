#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "fx16.h"


bool fx16_gen_push(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (field_count == 0)
		return true;
	if (!field)
		return false;

	fx16_instruction_t inst[field_count];

	unsigned i;
	for (i = 0; i < field_count; i++)
	{
		if (!field[i]
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[i]->type != FCT_ASM_FIELD_TYPE_REGISTER))
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Expected untransformed register");
			return false;
		}

		inst[i].op   = fx16_op_and;
		inst[i].dest = fx16_reg_stack;
		inst[i].src0 = field[i]->reg.reg;
		inst[i].src1 = field[i]->reg.reg;
	}

	return fct_asm_context_emit_code(
		context, inst, field_count, 0, 0);
}

bool fx16_gen_pop(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count)
{
	if (field_count == 0)
		return true;
	if (!field)
		return false;

	fx16_instruction_t inst[field_count];

	unsigned i;
	for (i = 0; i < field_count; i++)
	{
		if (!field[i]
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[i]->type != FCT_ASM_FIELD_TYPE_REGISTER))
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Expected untransformed register");
			return false;
		}

		inst[i].op   = fx16_op_and;
		inst[i].dest = field[i]->reg.reg;
		inst[i].src0 = fx16_reg_stack;
		inst[i].src1 = fx16_reg_stack;
	}

	return fct_asm_context_emit_code(
		context, inst, field_count, 0, 0);
}
