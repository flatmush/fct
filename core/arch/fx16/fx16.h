#ifndef __FCT_ARCH_FX16_H__
#define __FCT_ARCH_FX16_H__

#include <fct/arch/arch.h>


typedef enum
{
	fx16_op_and = 0x0,
	fx16_op_orr = 0x1,
	fx16_op_bic = 0x2,
	fx16_op_xor = 0x3,
	fx16_op_add = 0x4,
	fx16_op_sub = 0x5,
	fx16_op_adc = 0x6,
	fx16_op_sbc = 0x7,
	fx16_op_lsl = 0x8,
	fx16_op_lsr = 0x9,
	fx16_op_ror = 0xA,
	fx16_op_asr = 0xB,
	fx16_op_cmv = 0xC,
	fx16_op_cmb = 0xD,
	fx16_op_una = 0xE,
	fx16_op_exp = 0xF
} fx16_op_e;

typedef enum
{
	fx16_op_una_clz = 0x0,
	fx16_op_una_ctz = 0x1,
	fx16_op_una_clo = 0x2,
	fx16_op_una_cto = 0x3,
	fx16_op_una_pop = 0x4,
	fx16_op_una_neg = 0x5,
	fx16_op_una_abs = 0x6,
	fx16_op_una_nbs = 0x7,
	fx16_op_una_sx8 = 0x8,
} fx16_op_una_e;

typedef enum
{
	fx16_psop_nop,
	fx16_psop_mov,
	fx16_psop_inv,
	fx16_psop_zero,
	fx16_psop_ldr,
	fx16_psop_str,
	fx16_psop_push,
	fx16_psop_pop,
	fx16_psop_jmp,
	fx16_psop_cmp
} fx16_psop_e;

typedef enum
{
	fx16_reg_imm   = 0x8,
	fx16_reg_conf  = 0x9,
	fx16_reg_addr  = 0xA,
	fx16_reg_data  = 0xB,
	fx16_reg_sp    = 0xC,
	fx16_reg_stack = 0xD,
	fx16_reg_link  = 0xE,
	fx16_reg_pc    = 0xF,
} fx16_reg_e;
static const unsigned fx16_reg_count = 16;

typedef enum
{
	fx16_cond_nv = 0x0,
	fx16_cond_al = 0x1,
	fx16_cond_hi = 0x2,
	fx16_cond_ls = 0x3,
	fx16_cond_gt = 0x4,
	fx16_cond_le = 0x5,
	fx16_cond_ge = 0x6,
	fx16_cond_lt = 0x7,
	fx16_cond_cc = 0x8,
	fx16_cond_cs = 0x9,
	fx16_cond_vc = 0xA,
	fx16_cond_vs = 0xB,
	fx16_cond_zc = 0xC,
	fx16_cond_zs = 0xD,
	fx16_cond_nc = 0xE,
	fx16_cond_ns = 0xF,
} fx16_cond_e;

typedef enum
{
	fx16_excep_reset       = 0,
	fx16_excep_bad_iaddr   = 1,
	fx16_excep_bad_daddr   = 2,
	fx16_excep_instruction = 3,
	fx16_excep_reserved_0  = 4,
	fx16_excep_reserved_1  = 5,
	fx16_excep_system      = 6,
	fx16_excep_interrupt   = 7,
} fx16_excep_e;



typedef union __attribute__((__packed__))
{
	struct __attribute__((__packed__))
	{
		unsigned  src1 : 4;
		unsigned  src0 : 4;
		unsigned  dest : 4;
		fx16_op_e op   : 4;
	};
	uint16_t mask;
} fx16_instruction_t;

typedef union __attribute__((__packed__))
{
	struct __attribute__((__packed__))
	{
		unsigned carry      : 1;
		bool     overflow   : 1;
		bool     zero       : 1;
		bool     sign       : 1;
		bool     carry_set  : 1;
		bool     flag_set   : 1;
		bool     interrupt  : 1;
		bool     kernel     : 1;
		unsigned reserved   : 6;
		bool     byte_sel_0 : 1;
		bool     byte_sel_1 : 1;
	};
	uint16_t mask;
} fx16_config_t;


bool fx16_gen_push(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count);
bool fx16_gen_pop(
	fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count);

bool fx16_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count);

unsigned fx16_disassemble(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned offset,
	fct_string_t* str);


fct_arch_t fct_arch__fx16;
#define FCT_ARCH_FX16_STATIC &fct_arch__fx16
fct_arch_t* fct_arch_fx16;

#endif
