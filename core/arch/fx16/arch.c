#include <fct/arch/arch.h>
#include <fct/arch/emu.h>
#include <fct/object/format/elf.h>
#include "fx16.h"


static fct_arch_spec_t fct_arch_spec__fx16 =
{
	.mau       = 16,
	.addr_size =  1,
	.word_size =  1,
	.inst_size =  1,

	.reg_count = 16,

	.interrupt_count = 0,

	.elf_machine = ELF_MACHINE_NONE,
};

static fct_keyval_t fct_arch__fx16_reg_names[] =
{
	{ "imm"   , fx16_reg_imm   },
	{ "config", fx16_reg_conf  },
	{ "addr"  , fx16_reg_addr  },
	{ "data"  , fx16_reg_data  },
	{ "sp"    , fx16_reg_sp    },
	{ "stack" , fx16_reg_stack },
	{ "link"  , fx16_reg_link  }, /* TODO - Move to ABI? */
	{ "pc"    , fx16_reg_pc    },
	{ NULL, 0 }
};

fct_arch_emu_t fct_arch_emu__fx16;

fct_arch_t fct_arch__fx16 =
{
	.name = "fx16",
	.base = NULL,
	.emu  = &fct_arch_emu__fx16,
	.spec = &fct_arch_spec__fx16,

	.reg_names = fct_arch__fx16_reg_names,

	.elf_osabi      = ELF_OSABI_STANDALONE,
	.elf_abiversion = 0,

	.gen_call    = NULL,
	.gen_return  = NULL,
	.gen_push    = fx16_gen_push,
	.gen_pop     = fx16_gen_pop,
	.gen_syscall = NULL,

	.assemble    =        fx16_assemble,
	.disassemble = (void*)fx16_disassemble,
	.link        =        NULL,
};

fct_arch_t* fct_arch_fx16
	= &fct_arch__fx16;
