#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "fx16.h"


static char* fx16__op_name[] =
	{
		"and", "orr", "bic", "xor",
		"add", "sub", "adc", "sbc",
		"lsl", "lsr", "ror", "asr",
		"cmv", "cmb", "", "",
		"", "or", "orn", "eor",
		"", "", "", "",
		"sll", "srl", "ror", "sra",
		"cmov", "cmovb", "", ""
	};
static unsigned fx16__op_name_count
	= sizeof(fx16__op_name) / sizeof(char*);



static char* fx16__op_una_name[] =
	{
		"clz", "ctz", "clo", "cto"
		"popcnt", "neg", "abs", "nbs",
		"sx8", "", "", "",
		"", "", "", "",
		"", "", "", ""
		"", "", "", "nabs",
		"", "", "", "",
		"", "", "", "",
	};
static unsigned fx16__op_una_name_count
	= sizeof(fx16__op_una_name) / sizeof(char*);



static char* fx16__psop_name[] =
	{
		"nop", "mov", "inv", "zero",
		"ldr", "str", "push", "pop",
		"jmp", "cmp"
	};
static unsigned fx16__psop_name_count
	= sizeof(fx16__psop_name) / sizeof(char*);

typedef struct
{
	bool cond, dst, src0, src1;
} fx16_field_t;

static fx16_field_t fx16__psop_field[] =
	{
		{ 0, 0, 0, 0 },
		{ 1, 1, 1, 0 },
		{ 0, 1, 1, 0 },
		{ 0, 1, 0, 0 },
		{ 1, 1, 1, 1 },
		{ 1, 1, 1, 1 },
		{ 1, 0, 1, 0 },
		{ 1, 1, 0, 0 },
		{ 0, 0, 1, 0 },
		{ 0, 0, 1, 1 },
	};



static char* fx16__cond_name[] =
	{
		"nv", "al", "hi", "ls",
		"gt", "le", "ge", "lt",
		"cc", "cs", "vc", "vs",
		"zc", "zs", "nc", "ns",
		""  , ""  , ""  , ""  ,
		""  , ""  , ""  , ""  ,
		"lo", "hs", ""  , ""  ,
		"ne", "eq", "pl", "mi"
	};
static unsigned fx16__cond_name_count
	= sizeof(fx16__cond_name) / sizeof(char*);



static inline bool fx16_parse_op(
	const fct_tok_t* mnem,
	fx16_op_e* op, fx16_op_una_e* una)
{
	unsigned i;
	for (i = 0; i < fx16__op_name_count; i++)
	{
		if (fct_tok_keyword(mnem, fx16__op_name[i]))
		{
			if (op)
				*op = (i & 0xF);
			return true;
		}
	}

	for (i = 0; i < fx16__op_una_name_count; i++)
	{
		if (fct_tok_keyword(mnem, fx16__op_una_name[i]))
		{
			if (op)
				*op = fx16_op_una;
			if (una)
				*una = (i & 0xF);
			return true;
		}
	}

	return false;
}

static inline bool fx16_parse_psop(
	const fct_tok_t* mnem,
	fx16_psop_e* op)
{
	unsigned i;
	for (i = 0; i < fx16__psop_name_count; i++)
	{
		if (fct_tok_keyword(mnem, fx16__psop_name[i]))
		{
			if (op)
				*op = (i & 0xF);
			return true;
		}
	}

	return false;
}

static inline bool fx16_parse_cond(
	const fct_tok_t* postfix,
	fx16_cond_e* cond)
{
	if (!postfix)
	{
		if (cond)
			*cond = fx16_cond_al;
		return true;
	}

	unsigned i;
	for (i = 0; i < fx16__cond_name_count; i++)
	{
		if (fct_tok_keyword(postfix, fx16__cond_name[i]))
		{
			if (cond)
				*cond = (i & 0xF);
			return true;
		}
	}

	return false;
}



static bool fx16_emit(
	fct_asm_context_t* context,
	fx16_instruction_t inst, uint16_t imm,
	fct_string_t* symbol,
	unsigned boundary)
{
	bool src1Imm = false;
	switch (inst.op)
	{
		case fx16_op_cmv:
		case fx16_op_una:
		case fx16_op_exp:
			break;
		default:
			src1Imm = (inst.src1 == fx16_reg_imm);
			break;
	}

	bool imm_ex = ((inst.src0 == fx16_reg_imm) || src1Imm);

	if (!fct_asm_context_emit_code(
		context, &inst.mask, 1, 0, (boundary && !imm_ex)))
		return false;

	if (imm_ex)
	{
		if (symbol)
		{
			return fct_asm_context_emit(
				context, &imm, 1, 0,
				FCT_CHUNK_ACCESS_RX,
				FCT_CHUNK_CONTENT_DATA,
				symbol, 0, false, false,
				boundary);
		}
		else
		{
			return fct_asm_context_emit_code(
				context, &imm, 1, 0, boundary);
		}
	}

	return true;
}

bool fx16_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (!context)
		return false;

	fx16_op_e     op   = 0;
	fx16_op_una_e una  = 0;
	fx16_psop_e   psop = 0;
	fx16_cond_e   cond = fx16_cond_al;
	fx16_reg_e    dst  = fx16_reg_imm, src0 = 0, src1 = 0;

	bool pseudo = false;
	uint64_t imm = 0;
	fct_string_t* sym = NULL;

	fx16_field_t fields;

	if (fx16_parse_op(mnem, &op, &una))
	{
		fields = (fx16_field_t) { 0, 1, 1, 1 };
		switch (op)
		{
			case fx16_op_una:
				fields.src1 = 0;
				break;
			case fx16_op_cmv:
				fields.src1 = 0;
				fields.cond = 1;
				break;
			default:
				break;
		}
	}
	else
	{
		if (!fx16_parse_psop(mnem, &psop))
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"Operation not recognized");
			return false;
		}
		fields = fx16__psop_field[psop];
		pseudo = true;
	}

	if (!fx16_parse_cond(postfix, &cond))
	{
		FCT_DEBUG_ERROR_TOKEN(postfix,
			"Condition not recognized");
		return false;
	}

	unsigned i = 0;
	if (fields.dst)
	{
		if ((i >= field_count) || !field[i]
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE)
			|| (field[i]->type != FCT_ASM_FIELD_TYPE_REGISTER))
		{
			FCT_DEBUG_ERROR_FIELD(field[i],
				"Destination field must be untransformed register");
			return false;
		}
		dst = field[i++]->reg.reg;
	}

	if (fields.src0)
	{
		if ((i >= field_count) || !field[i]
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE))
			return false;

		switch (field[i]->type)
		{
			case FCT_ASM_FIELD_TYPE_REGISTER:
				src0 = field[i]->reg.reg;
				break;
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
				if (field[i]->immediate.size > 16)
				{
					FCT_DEBUG_ERROR_FIELD(field[i],
						"Immediate must be 16-bits or less");
					return false;
				}
				src0 = fx16_reg_imm;
				imm = field[i]->immediate.value;
				break;
			case FCT_ASM_FIELD_TYPE_SYMBOL:
				src0 = fx16_reg_imm;
				sym = field[i]->symbol.name;
				break;
			default:
				FCT_DEBUG_ERROR_FIELD(field[i],
					"Source must be register, immediate or symbol");
				return false;
		}
		i++;
	}

	if (fields.src1)
	{
		if ((i >= field_count) || !field[i]
			|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE))
			return false;

		switch (field[i]->type)
		{
			case FCT_ASM_FIELD_TYPE_REGISTER:
				src1 = field[i]->reg.reg;
				break;
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
				if (field[i]->immediate.size > 16)
				{
					FCT_DEBUG_ERROR_FIELD(field[i],
						"Immediate must be 16-bits or less.");
					return false;
				}
				if ((src0 == fx16_reg_imm)
					&& (sym || (imm != field[i]->immediate.value)))
				{
					FCT_DEBUG_ERROR_FIELD(field[i],
						"Only one unique immediate or symbol export allowed per instruction");
					return false;
				}
				src1 = fx16_reg_imm;
				imm = field[i]->immediate.value;
				break;
			case FCT_ASM_FIELD_TYPE_SYMBOL:
				if ((src0 == fx16_reg_imm) && sym
					&& !fct_string_compare(sym, field[i]->symbol.name))
				{
					FCT_DEBUG_ERROR_FIELD(field[i],
						"Only one unique immediate or symbol export allowed per instruction");
					return false;
				}
				src1 = fx16_reg_imm;
				sym = field[i]->symbol.name;
				break;
			default:
				FCT_DEBUG_ERROR_FIELD(field[i],
					"Source must be register, immediate or symbol");
				return false;
		}
		i++;
	}

	if (i != field_count)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Too many fields");
		return false;
	}

	fx16_instruction_t inst;
	if (!pseudo)
	{
		inst.op   = op;
		inst.dest = dst;
		inst.src0 = src0;
		switch (op)
		{
			case fx16_op_cmv:
				inst.src1 = cond;
				break;
			case fx16_op_una:
				inst.src1 = una;
				break;
			default:
				inst.src1 = src1;
				break;
		}
		fx16_emit(context, inst, imm, sym, 0);
	}
	else
	{
		switch (psop)
		{
			case fx16_psop_nop:
				inst.mask = 0;
				fx16_emit(context, inst, 0, NULL, 0);
				break;
			case fx16_psop_mov:
				inst.dest = dst;
				inst.src0 = src0;
				if (cond != fx16_cond_al)
				{
					inst.op = fx16_op_cmv;
					inst.src1 = cond;
				}
				else
				{
					inst.op = fx16_op_and;
					inst.src1 = src0;
				}
				fx16_emit(context, inst, imm, sym, 0);
				break;
			case fx16_psop_zero:
				inst.op   = fx16_op_xor;
				inst.dest = dst;
				inst.src0 = 0;
				inst.src1 = 0;
				fx16_emit(context, inst, 0, NULL, 0);
				break;
			case fx16_psop_inv:
				inst.op   = fx16_op_bic;
				inst.dest = dst;
				inst.src0 = 0;
				inst.src1 = 0;
				fx16_emit(context, inst, 0, NULL, 0);
				inst.op   = fx16_op_xor;
				inst.dest = dst;
				inst.src0 = src0;
				inst.src1 = dst;
				fx16_emit(context, inst, imm, sym, 0);
				break;
			case fx16_psop_ldr:
			case fx16_psop_str:
				inst.op = fx16_op_add;
				inst.dest = fx16_reg_addr;
				inst.src0 = src0;
				inst.src1 = src1;
				fx16_emit(context, inst, imm, sym, 0);
				if (psop == fx16_psop_ldr)
				{
					inst.dest = dst;
					inst.src0 = fx16_reg_data;
				}
				else
				{
					inst.dest = fx16_reg_data;
					inst.src0 = dst;
				}
				if (cond != fx16_cond_al)
				{
					inst.op = fx16_op_cmv;
					inst.src1 = cond;
				}
				else
				{
					inst.op = fx16_op_and;
					inst.src1 = inst.src0;
				}
				fx16_emit(context, inst, 0, NULL, 0);
				break;
			case fx16_psop_push:
				inst.op   = fx16_op_and;
				inst.dest = fx16_reg_stack;
				inst.src0 = src0;
				inst.src1 = src0;
				fx16_emit(context, inst, imm, sym, 0);
				break;
			case fx16_psop_pop:
				inst.op   = fx16_op_and;
				inst.dest = dst;
				inst.src0 = fx16_reg_stack;
				inst.src1 = fx16_reg_stack;
				fx16_emit(context, inst, 0, NULL, 0);
				break;
			case fx16_psop_jmp:
				inst.op   = fx16_op_and;
				inst.dest = fx16_reg_pc;
				inst.src0 = src0;
				inst.src1 = src0;
				fx16_emit(context, inst, imm, sym, 1);
				break;
			case fx16_psop_cmp:
				inst.op   = fx16_op_sub;
				inst.dest = fx16_reg_imm;
				inst.src0 = src0;
				inst.src1 = src1;
				fx16_emit(context, inst, imm, sym, 0);
				break;
			default:
				FCT_DEBUG_ERROR_TOKEN(mnem,
					"Unknown opcode encountered during instruction encoding");
				return false;
		}
	}
	return true;
}



static bool fx16__disassemble_field(
	fct_arch_t* arch,
	fx16_reg_e reg, bool is_src, uint16_t imm,
	fct_string_t* str)
{
	if (is_src && (reg == fx16_reg_imm))
		return fct_string_append_static_format(str, "%" PRIu16, imm);

	char reg_sigil = '$';
	if (!fct_string_append_static_format(
			str, "%c", reg_sigil))
		return false;

	const char* rname
		= fct_arch_reg_name_from_number(arch, reg);
	if (rname)
		return fct_string_append_static_format(str, "%s", rname);

	return fct_string_append_static_format(str, "%u", (unsigned)reg);
}

unsigned fx16_disassemble(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned offset,
	fct_string_t* str)
{
	if (!arch)
		arch = &fct_arch__fx16;

	fx16_instruction_t inst;
	if (!fct_maubuff_read(code, offset, 1, (void*)&inst))
		return 0;

	if (inst.mask == 0)
		return (fct_string_append_static_format(str, "nop") ? 1 : 0);

	if (inst.op == fx16_op_exp)
		return 0;

	bool fetch_imm = (inst.src0 == fx16_reg_imm);
	switch (inst.op)
	{
		case fx16_op_una:
			if (inst.src1 > fx16_op_una_sx8)
				return 0;
			break;
		case fx16_op_cmv:
		case fx16_op_exp:
			break;
		default:
			if (inst.src1 == fx16_reg_imm)
				fetch_imm = true;
			break;
	}

	uint16_t immediate = 0;
	if (fetch_imm
		&& !fct_maubuff_read(
			code, (offset + 1), 1, (void*)&immediate))
		return 0;

	fct_string_t* istr
		= fct_string_create("");
	if (!istr) return 0;

	bool has_dest = true;
	bool has_src0 = true;
	bool has_src1 = true;
	bool has_cond = false;

	bool is_psop = false;
	fx16_psop_e psop;
	if ((inst.op == fx16_op_and)
		&& (inst.src0 == inst.src1))
	{
		psop = fx16_psop_mov;
		is_psop = true;
		has_src1 = false;
	}

	if (is_psop && (psop == fx16_psop_mov))
	{
		if (inst.dest == fx16_reg_stack)
		{
			psop = fx16_psop_push;
			has_dest = false;
		}
		else if (inst.src0 == fx16_reg_stack)
		{
			psop = fx16_psop_pop;
			has_src0 = false;
		}
		else if (inst.dest == fx16_reg_pc)
		{
			psop = fx16_psop_jmp;
			has_dest = false;
		}
	}
	/* TODO - Disassemble all psops. */

	if (is_psop)
	{
		if (!fct_string_append_static_format(
			istr, "%s", fx16__psop_name[psop]))
		{
			fct_string_delete(istr);
			return 0;
		}
	}
	else if (inst.op == fx16_op_una)
	{
		if (!fct_string_append_static_format(
			istr, "%s", fx16__op_una_name[inst.src1]))
		{
			fct_string_delete(istr);
			return 0;
		}
		has_src1 = false;
	}
	else
	{
		if (!fct_string_append_static_format(
			istr, "%s", fx16__op_name[inst.op]))
		{
			fct_string_delete(istr);
			return 0;
		}
		if (inst.op == fx16_op_cmv)
		{
			has_cond = true;
			has_src1 = false;
		}
	}

	if (has_cond)
	{
		if (!fct_string_append_static_format(
			istr, ".%s", fx16__cond_name[inst.src1]))
		{
			fct_string_delete(istr);
			return 0;
		}
	}

	if (has_dest)
	{
		if (!fct_string_append_static_format(
			istr, " ") || !fx16__disassemble_field(
				arch, inst.dest, false, immediate, istr))
		{
			fct_string_delete(istr);
			return 0;
		}
	}

	if (has_src0)
	{
		if (!fct_string_append_static_format(
			istr, (has_dest ? ", " : " "))
			|| !fx16__disassemble_field(
				arch, inst.src0, true, immediate, istr))
		{
			fct_string_delete(istr);
			return 0;
		}
	}

	if (has_src1)
	{
		if (!fct_string_append_static_format(
			istr, (has_dest || has_src0 ? ", " : " "))
			|| !fx16__disassemble_field(
				arch, inst.src1, true, immediate, istr))
		{
			fct_string_delete(istr);
			return 0;
		}
	}

	bool success = fct_string_append(str, istr);
	fct_string_delete(istr);
	return (success ? (fetch_imm ? 2 : 1) : 0);
}
