#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "mu0.h"


bool mu0_link(
	mu0_instruction_t* code,
	uint64_t size, uint64_t symbol)
{
	if (!code
		|| (symbol > 0x1FFF)
		|| (size != 1))
		return false;
	code[0].addr = symbol;
	return true;
}
