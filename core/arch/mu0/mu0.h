#ifndef __FCT_ARCH_MU0_H__
#define __FCT_ARCH_MU0_H__

#include <fct/arch/arch.h>



typedef enum
{
	mu0_op_lda = 0,
	mu0_op_sto = 1,
	mu0_op_add = 2,
	mu0_op_sub = 3,
	mu0_op_jmp = 4,
	mu0_op_jge = 5,
	mu0_op_jne = 6,
	mu0_op_stp = 7,
} mu0_op_e;

typedef union __attribute__((__packed__))
{
	struct __attribute__((__packed__))
	{
		mu0_op_e op   :  3;
		unsigned addr : 13;
	};
	uint16_t mask;
} mu0_instruction_t;


bool mu0_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count);

unsigned mu0_disassemble(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned offset,
	fct_string_t* str);

bool mu0_link(
	mu0_instruction_t* code,
	uint64_t size, uint64_t symbol);


fct_arch_t fct_arch__mu0;
#define FCT_ARCH_MU0_STATIC &fct_arch__mu0
fct_arch_t* fct_arch_mu0;

#endif
