#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "mu0.h"

static fct_keyval_t mu0__op_name[] =
{
	{ "lda", mu0_op_lda },
	{ "sto", mu0_op_sto },
	{ "add", mu0_op_add },
	{ "sub", mu0_op_sub },
	{ "jmp", mu0_op_jmp },
	{ "jge", mu0_op_jge },
	{ "jne", mu0_op_jne },
	{ "stp", mu0_op_stp },
};



bool mu0_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (!context)
		return false;

	uint32_t op;
	if (!fct_keyval_list_resolven(
		mu0__op_name, mnem->base, mnem->size, &op))
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Operation not recognized");
		return false;
	}

	if (postfix)
	{
		FCT_DEBUG_ERROR_TOKEN(postfix,
			"mu0 operations shouldn't have a postfix");
		return false;
	}

	mu0_instruction_t inst = { .mask = 0 };
	inst.op = op;

	unsigned boundary = 0;
	if ((inst.op == mu0_op_jmp)
		|| (inst.op == mu0_op_stp))
		boundary = 1;

	if (op == mu0_op_stp)
	{
		if (field_count != 0)
		{
			FCT_DEBUG_ERROR_TOKEN(mnem,
				"No fields expected.");
			return false;
		}

		inst.addr = 0;

		return fct_asm_context_emit_code(
			context, &inst.mask, 1, 0, boundary);
	}

	if ((field_count != 1)
		|| !field[0])
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Exactly one field expected.");
		return false;
	}

	if (field[0]->xform != FCT_ASM_FIELD_XFORM_NONE)
	{
		FCT_DEBUG_ERROR_FIELD(field[0],
			"Unsupported transform in field.");
		return false;
	}

	switch (field[0]->type)
	{
		case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			if (field[0]->immediate.value >> 13)
			{
				FCT_DEBUG_ERROR_FIELD(field[0],
					"Immediate '%" PRIu64 " out of range.",
					field[0]->immediate.value);
				return false;
			}

			inst.addr = field[0]->immediate.value;

			if (!fct_asm_context_emit_code(
				context, &inst.mask, 1, 0, boundary))
				return false;
			break;

		case FCT_ASM_FIELD_TYPE_SYMBOL:
			if (!fct_asm_context_emit(
				context, &inst.mask, 1, 0,
				FCT_CHUNK_ACCESS_RX,
				FCT_CHUNK_CONTENT_DATA,
				field[0]->symbol.name, 0, false, true,
				boundary))
			{
				FCT_DEBUG_ERROR_FIELD(field[0],
					"Failed to add architecture specific import.");
				return false;
			}
			break;

		default:
			FCT_DEBUG_ERROR_FIELD(field[0],
				"Expected symbol or immediate.");
			return false;
	}

	return true;
}

unsigned mu0_disassemble(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned offset,
	fct_string_t* str)
{
	(void)arch;

	mu0_instruction_t inst;
	if (!fct_maubuff_read(code, offset, 1, (void*)&inst))
		return 0;

	return (fct_string_append_static_format(
		str, "%s %u", mu0__op_name[inst.op], inst.addr) ? 1 : 0);
}
