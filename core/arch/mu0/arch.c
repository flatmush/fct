#include <fct/arch/arch.h>
#include <fct/object/format/elf.h>
#include "mu0.h"


static fct_arch_spec_t fct_arch_spec__mu0 =
{
	.mau       = 16,
	.addr_size =  1,
	.word_size =  1,
	.inst_size =  1,

	.reg_count =  0,

	.interrupt_count = 0,

	.elf_machine = ELF_MACHINE_NONE,
};

fct_arch_t fct_arch__mu0 =
{
	.name = "mu0",
	.base = NULL,
	.emu  = NULL,
	.spec = &fct_arch_spec__mu0,

	.reg_names = NULL,

	.elf_osabi      = ELF_OSABI_STANDALONE,
	.elf_abiversion = 0,

	.gen_call    = NULL,
	.gen_return  = NULL,
	.gen_push    = NULL,
	.gen_pop     = NULL,
	.gen_syscall = NULL,

	.assemble    =        mu0_assemble,
	.disassemble = (void*)mu0_disassemble,
	.link        = (void*)mu0_link,
};

fct_arch_t* fct_arch_mu0
	= &fct_arch__mu0;
