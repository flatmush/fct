#include <fct/types.h>
#include <fct/debug.h>
#include "arm.h"


bool arm_link(
	arm_instruction_t* code,
	uint64_t size, uint64_t symbol)
{
	if (!code)
		return false;

	if (size == 4)
	{
		if (code[0].branch.hole0 == 5)
		{
			if ((symbol & 3)
				|| (symbol >> 26))
				return false;
			code[0].branch.off = (symbol >> 2);
			return true;
		}

		if (code[0].swi.hole0 == 0xF)
		{
			if (symbol >> 24)
				return false;
			code[0].swi.code = symbol;
			return true;
		}
	}

	return false;
}
