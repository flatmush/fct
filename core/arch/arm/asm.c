#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "arm.h"

typedef enum
{
	arm_inst_type_data,
	arm_inst_type_mul,
	arm_inst_type_lmul,
	arm_inst_type_swap,
	arm_inst_type_mem,
	arm_inst_type_memm,
	arm_inst_type_memhi,
	arm_inst_type_memhr,
	arm_inst_type_branch,
	arm_inst_type_bx,
	arm_inst_type_copdt,
	arm_inst_type_copdo,
	arm_inst_type_coprt,
	arm_inst_type_swi,
} arm_inst_type_e;

typedef enum
{
	_af_no,
	_af_rn, _af_rd, _af_rs, _af_rm,
	_af_sh,
	_af_rl,  /* Register List */
	_af_swi, _af_br
} arm__field_e;

typedef struct
{
	const char*     mnem;
	unsigned        opcode;
	uint32_t        mask;
	arm_inst_type_e type;
	bool            set_flags;
	arm__field_e    fields[4];
	bool            boundary;
	bool            postfix;
} arm__op_t;

static arm__op_t arm__op_name[] =
{
	{ "nop"  , 0         , 0x00000000, arm_inst_type_data  , 0, { _af_no , _af_no, _af_no, _af_no }, 0, 0 },

	{ "and"  , arm_op_and, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "xor"  , arm_op_xor, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "sub"  , arm_op_sub, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "rsb"  , arm_op_rsb, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "add"  , arm_op_add, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "adc"  , arm_op_adc, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "sbc"  , arm_op_sbc, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "rsc"  , arm_op_rsc, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "tst"  , arm_op_tst, 0x00100000, arm_inst_type_data  , 0, { _af_rn , _af_sh, _af_no, _af_no }, 0, 0 },
	{ "teq"  , arm_op_teq, 0x00100000, arm_inst_type_data  , 0, { _af_rn , _af_sh, _af_no, _af_no }, 0, 0 },
	{ "cmp"  , arm_op_cmp, 0x00100000, arm_inst_type_data  , 0, { _af_rn , _af_sh, _af_no, _af_no }, 0, 0 },
	{ "cmn"  , arm_op_cmn, 0x00100000, arm_inst_type_data  , 0, { _af_rn , _af_sh, _af_no, _af_no }, 0, 0 },
	{ "orr"  , arm_op_orr, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "mov"  , arm_op_mov, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_sh, _af_no, _af_no }, 0, 0 },
	{ "bic"  , arm_op_bic, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_rn, _af_sh, _af_no }, 0, 0 },
	{ "mvn"  , arm_op_mvn, 0x00000000, arm_inst_type_data  , 1, { _af_rd , _af_sh, _af_no, _af_no }, 0, 0 },

	{ "mul"  , 0         , 0x00000000, arm_inst_type_mul   , 1, { _af_rd , _af_rm, _af_rs, _af_no }, 0, 0 },
	{ "mla"  , 0         , 0x00200000, arm_inst_type_mul   , 1, { _af_rd , _af_rm, _af_rs, _af_rn }, 0, 0 },
	{ "umull", 0         , 0x00400000, arm_inst_type_lmul  , 1, { _af_rn , _af_rd, _af_rm, _af_rs }, 0, 0 },
	{ "umlal", 0         , 0x00600000, arm_inst_type_lmul  , 1, { _af_rn , _af_rd, _af_rm, _af_rs }, 0, 0 },
	{ "smull", 0         , 0x00000000, arm_inst_type_lmul  , 1, { _af_rn , _af_rd, _af_rm, _af_rs }, 0, 0 },
	{ "smlal", 0         , 0x00200000, arm_inst_type_lmul  , 1, { _af_rn , _af_rd, _af_rm, _af_rs }, 0, 0 },

	{ "ldm"  , 0         , 0x00000000, arm_inst_type_memm  , 0, { _af_rn , _af_rl, _af_no, _af_no }, 0, 1 },
	{ "stm"  , 0         , 0x00100000, arm_inst_type_memm  , 0, { _af_rn , _af_rl, _af_no, _af_no }, 0, 1 },

	{ "b"    , 0         , 0x00000000, arm_inst_type_branch, 0, { _af_br , _af_no, _af_no, _af_no }, 1, 0 },
	{ "bl"   , 0         , 0x01000000, arm_inst_type_branch, 0, { _af_br , _af_no, _af_no, _af_no }, 0, 0 },
	{ "bx"   , 0         , 0x00000000, arm_inst_type_bx    , 0, { _af_rm , _af_no, _af_no, _af_no }, 1, 0 },

	{ "swi"  , 0         , 0x00000000, arm_inst_type_swi   , 0, { _af_swi, _af_no, _af_no, _af_no }, 0, 0 },

	{ NULL, 0, 0, 0, 0, { 0, 0, 0, 0 }, 0, 0 }
};

static fct_keyval_t arm__cond_name[] =
{
	{ "zs", arm_cond_zs },
	{ "zc", arm_cond_zc },
	{ "cs", arm_cond_cs },
	{ "cc", arm_cond_cc },
	{ "ns", arm_cond_ns },
	{ "nc", arm_cond_nc },
	{ "vs", arm_cond_vs },
	{ "vc", arm_cond_vc },
	{ "hi", arm_cond_hi },
	{ "ls", arm_cond_ls },
	{ "ge", arm_cond_ge },
	{ "lt", arm_cond_lt },
	{ "gt", arm_cond_gt },
	{ "le", arm_cond_le },
	{ "al", arm_cond_al },
	{ "nv", arm_cond_nv },

	{ "eq", arm_cond_zs },
	{ "ne", arm_cond_zc },
	{ "hs", arm_cond_cs },
	{ "lo", arm_cond_cc },
	{ "mi", arm_cond_ns },
	{ "pl", arm_cond_nc },

	{ NULL, 0 }
};

static fct_keyval_t arm__modifier_name[] =
{
	{ "ia", arm_modifier_ia },
	{ "ib", arm_modifier_ib },
	{ "da", arm_modifier_da },
	{ "db", arm_modifier_db },

	{ "ea", (arm_modifier_ia | 4) },
	{ "fa", (arm_modifier_ib | 4) },
	{ "ed", (arm_modifier_da | 4) },
	{ "fd", (arm_modifier_db | 4) },

	{ NULL, 0 }
};

static arm__op_t* arm__parse_mnem(
	const fct_tok_t* mnem,
	arm_cond_e*      cond,
	arm_modifier_e*  modifier,
	bool*            set_flags)
{
	if (!mnem)
		return NULL;

	unsigned remain = mnem->size;
	unsigned offset = 0;

	arm__op_t*     op;
	arm_cond_e     c = arm_cond_al;
	arm_modifier_e m = 0;
	bool           s = false;

	{
		unsigned i;
		for (i = 0; arm__op_name[i].mnem; i++)
		{
			unsigned mlen = strlen(arm__op_name[i].mnem);
			if (remain < mlen) continue;

			if (strncasecmp(arm__op_name[i].mnem, mnem->base, mlen) == 0)
			{
				remain -= mlen;
				offset += mlen;
				break;
			}
		}
		if (!arm__op_name[i].mnem)
			return NULL;
		op = &arm__op_name[i];
	}

	if (remain >= 2)
	{
		fct_keyval_t* i;
		for (i = arm__cond_name; i->key; i++)
		{
			if (strncasecmp(i->key, &mnem->base[offset], 2) == 0)
				break;
		}
		if (i->key)
		{
			c  = i->val;
			remain -= 2;
			offset += 2;
		}
	}

	if (op->type == arm_inst_type_memm)
	{
		if (remain < 2)
			return NULL;

		fct_keyval_t* i;
		for (i = arm__modifier_name; i->key; i++)
		{
			if (strncasecmp(i->key, &mnem->base[offset], 2) == 0)
				break;
		}
		if (!i->key)
			return NULL;

		m = i->val;
		remain -= 2;
		offset += 2;
	}
	else if (op->set_flags)
	{
		if ((remain >= 1)
			&& (tolower(mnem->base[offset]) == 's'))
		{
			remain--;
			offset++;
			s = true;
		}
	}

	if (remain != 0)
		return NULL;

	if (cond     ) *cond      = c;
	if (modifier ) *modifier  = m;
	if (set_flags) *set_flags = s;
	return op;
}

static bool arm__encode_shift_imm(uint32_t in, uint32_t* out)
{
	uint32_t val = in;
	uint32_t ror = 0;

	for (ror = 0;
		(ror < 16) && (val >> 8);
		ror++, val = ((val << 2) | (val >> 30)));
	if (ror >= 16)
		return false;

	if (out)
		*out = (ror << 8) | val;
	return true;
}



bool arm_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count)
{
	if (!context)
		return false;

	arm__op_t*     op;
	arm_cond_e     cond      = arm_cond_al;
	arm_modifier_e modifier  = arm_modifier_ia;
	bool           set_flags = false;

	op = arm__parse_mnem(
		mnem, &cond, &modifier, &set_flags);
	if (!op)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Operation not recognized.");
		return false;
	}

	if (postfix && !op->postfix)
	{
		FCT_DEBUG_ERROR_TOKEN(mnem,
			"Operation can't have postfix");
		return false;
	}

	arm_instruction_t inst = { .mask = op->mask };
	switch (op->type)
	{
		case arm_inst_type_data:
			inst.dri.op = op->opcode;
			break;
		case arm_inst_type_mul:
			inst.mask |= 0x00000090;
			break;
		case arm_inst_type_lmul:
			inst.mask |= 0x00800090;
			break;
		case arm_inst_type_swap:
			inst.mask |= 0x01000090;
			break;
		case arm_inst_type_mem:
			inst.mask |= 0x04000000;
			break;
		case arm_inst_type_memm:
			inst.mask |= 0x08000000;
			break;
		case arm_inst_type_memhi:
			inst.mask |= 0x00400090;
			break;
		case arm_inst_type_memhr:
			inst.mask |= 0x00000090;
			break;
		case arm_inst_type_branch:
			inst.mask |= 0x0A000000;
			break;
		case arm_inst_type_bx:
			inst.mask |= 0x012FFF10;
			break;
		case arm_inst_type_copdt:
			inst.mask |= 0x0C000000;
			break;
		case arm_inst_type_copdo:
			inst.mask |= 0x0E000000;
			break;
		case arm_inst_type_coprt:
			inst.mask |= 0x0E000010;
			break;
		case arm_inst_type_swi:
			inst.mask |= 0x0F000000;
			break;
		default:
			return false;
	}

	inst.dri.cond      = cond;
	inst.dri.set_flags = set_flags;

	if (op->type == arm_inst_type_memm)
	{
		if (modifier & 4)
			modifier ^= (inst.memm.load ? 7 : 4);
		inst.memm.modifier = modifier;

		if (postfix)
		{
			if (!fct_tok_keyword(postfix, "wb"))
			{
				FCT_DEBUG_ERROR_TOKEN(postfix,
					"Invalid postfix for operation");
				return false;
			}
			inst.memm.modify = true;
		}
	}

	fct_asm_field_t* sym = NULL;
	bool             symrel = false;

	unsigned f;
	for (f = 0; (f < 4) && (op->fields[f] != _af_no); f++)
	{
		if ((f >= field_count) || !field[f])
			return false;

		if (field[f]->xform != FCT_ASM_FIELD_XFORM_NONE)
		{
			FCT_DEBUG_ERROR_FIELD(field[f],
				"No valid field transforms for ARM.");
			return false;
		}

		uint32_t val = 0;
		bool     reg = false;
		switch (op->fields[f])
		{
			case _af_rn:
			case _af_rd:
			case _af_rs:
			case _af_rm:
				if (field[f]->type != FCT_ASM_FIELD_TYPE_REGISTER)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Expected register");
					return false;
				}
				val = field[f]->reg.reg;
				reg = true;
				break;

			case _af_sh:
				if (field[f]->type != FCT_ASM_FIELD_TYPE_REGISTER)
				{
					val = field[f]->reg.reg;
					reg = true;

					/* TODO - Parse shift. */
				}
				else if ((field[f]->type != FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
					|| (field[f]->type != FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED))
				{
					val = field[f]->immediate.value;
				}
				else
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Expected register or immediate");
					return false;
				}
				break;

			case _af_rl:
				{
					unsigned i;
					for (i = f; i < field_count; i++)
					{
						if (!field[i]
							|| (field[i]->xform != FCT_ASM_FIELD_XFORM_NONE)
							|| (field[i]->type != FCT_ASM_FIELD_TYPE_REGISTER))
						{
							FCT_DEBUG_ERROR_FIELD(field[f],
								"Only untransformed registers allowed in register list.");
							return false;
						}

						uint32_t mask = (1UL << field[i]->reg.reg);
						if (val & mask)
						{
							FCT_DEBUG_ERROR_FIELD(field[f],
								"Register list contains duplicate entries.");
							return false;
						}

						val |= mask;
					}
				}
				break;

			case _af_swi:
			case _af_br:
				if ((field[f]->type != FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
					|| (field[f]->type != FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED))
				{
					val = field[f]->immediate.value;
				}
				else if (field[f]->type != FCT_ASM_FIELD_TYPE_SYMBOL)
				{
					if (sym)
					{
						FCT_DEBUG_ERROR_FIELD(field[f],
							"Multiple symbols in instruction not supported");
						return false;
					}

					sym    = field[f];
					symrel = (op->fields[f] == _af_br);
				}
				else
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Expected symbol or immediate");
					return false;
				}
				break;

			default:
				return false;
		}

		switch (op->fields[f])
		{
			case _af_sh:
				if (!reg && !arm__encode_shift_imm(val, &val))
				{
					/* TODO - Invert constant where this makes sense. */
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Can't encode immediate '%u' as shifted immediate.", (unsigned)val);
					return false;
				}
				break;
			case _af_br:
				if (val & 3)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Branch target unaligned.");
					return false;
				}
				val >>= 2;

				if ((val >> 31)
					? ((val & 0xFF800000) != 0xFF800000)
					: (val >> 23))
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"Branch offset out of range");
					return false;
				}
				break;
			case _af_swi:
				if (val >> 24)
				{
					FCT_DEBUG_ERROR_FIELD(field[f],
						"SWI code out of range");
					return false;
				}
				break;
			default:
				break;
		}

		switch (op->fields[f])
		{
			case _af_rn:
				inst.dri.rn = val;
				break;
			case _af_rd:
				inst.dri.rd = val;
				break;
			case _af_rs:
				inst.mult.rs = val;
				break;
			case _af_rm:
				inst.mult.rm = val;
				break;
			case _af_sh:
				if (reg)
				{
					inst.mult.rm = val;
					/* TODO - Encode shift. */
				} else {
					inst.di.immed = true;
					inst.di.imm = (val & 0xFF);
					inst.di.ror = (val >> 8);
				}
				break;
			case _af_rl:
				inst.memm.regs = val;
				break;
			case _af_swi:
			case _af_br:
				inst.swi.code = val;
				break;
			default:
				break;
		}
	}

	unsigned boundary = 0;
	if (op->boundary
		&& (cond == arm_cond_al))
		boundary = 4;

	/* TODO - Treat unconditional PC writes as boundaries. */

	if (sym)
	{
		if (!fct_asm_context_emit(
			context, &inst.mask, 4, 4,
			FCT_CHUNK_ACCESS_RX,
			FCT_CHUNK_CONTENT_DATA,
			sym->symbol.name, (symrel ? 4 : 0), symrel, true,
			boundary))
		{
			FCT_DEBUG_ERROR_FIELD(sym,
				"Failed to add architecture specific import.");
			return false;
		}
	}
	else
	{
		if (!fct_asm_context_emit_code(
			context, &inst.mask, 4, 4, boundary))
			return false;
	}

	return true;
}
