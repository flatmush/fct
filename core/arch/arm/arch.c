#include <fct/arch/arch.h>
#include <fct/object/format/elf.h>
#include "arm.h"


static fct_arch_spec_t fct_arch_spec__arm =
{
	.mau       =  8,
	.addr_size =  4,
	.word_size =  4,
	.inst_size =  4,

	.reg_count = 16,

	.interrupt_count = 0,

	.elf_machine = ELF_MACHINE_ARM,
};

static fct_keyval_t fct_arch__arm_reg_names[] =
{
	{ "lr", arm_reg_lr },
	{ "pc", arm_reg_pc },
	{ NULL, 0 }
};

fct_arch_t fct_arch__arm =
{
	.name = "arm",
	.base = NULL,
	.emu  = NULL,
	.spec = &fct_arch_spec__arm,

	.reg_names = fct_arch__arm_reg_names,

	.elf_osabi      = ELF_OSABI_STANDALONE,
	.elf_abiversion = 0,

	.gen_call    = arm_gen_call,
	.gen_return  = arm_gen_return,
	.gen_push    = NULL,
	.gen_pop     = NULL,
	.gen_syscall = arm_gen_syscall,

	.assemble    =        arm_assemble,
	.disassemble =        NULL,
	.link        = (void*)arm_link,
};

fct_arch_t* fct_arch_arm
	= &fct_arch__arm;
