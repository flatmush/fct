#ifndef __FCT_ARCH_ARM_H__
#define __FCT_ARCH_ARM_H__

#include <fct/arch/arch.h>


typedef enum
{
	arm_reg_sp = 13,
	arm_reg_lr = 14,
	arm_reg_pc = 15,
} arm_reg_e;

typedef enum
{
	arm_cond_zs =  0,
	arm_cond_zc =  1,
	arm_cond_cs =  2,
	arm_cond_cc =  3,
	arm_cond_ns =  4,
	arm_cond_nc =  5,
	arm_cond_vs =  6,
	arm_cond_vc =  7,
	arm_cond_hi =  8,
	arm_cond_ls =  9,
	arm_cond_ge = 10,
	arm_cond_lt = 11,
	arm_cond_gt = 12,
	arm_cond_le = 13,
	arm_cond_al = 14,
	arm_cond_nv = 15,
} arm_cond_e;

typedef enum
{
	arm_op_and =  0,
	arm_op_xor =  1,
	arm_op_sub =  2,
	arm_op_rsb =  3,
	arm_op_add =  4,
	arm_op_adc =  5,
	arm_op_sbc =  6,
	arm_op_rsc =  7,
	arm_op_tst =  8,
	arm_op_teq =  9,
	arm_op_cmp = 10,
	arm_op_cmn = 11,
	arm_op_orr = 12,
	arm_op_mov = 13,
	arm_op_bic = 14,
	arm_op_mvn = 15,
} arm_op_e;

typedef enum
{
	arm_shift_lsl = 0,
	arm_shift_lsr = 1,
	arm_shift_asr = 2,
	arm_shift_ror = 3,
} arm_shift_e;

typedef enum
{
	arm_modifier_db = 0,
	arm_modifier_da = 1,
	arm_modifier_ib = 2,
	arm_modifier_ia = 3,
} arm_modifier_e;



typedef enum
{
	arm_mode26_usr = 0,
	arm_mode26_fiq = 1,
	arm_mode26_irq = 2,
	arm_mode26_svc = 3,
} arm_mode26_e;

typedef enum
{
	arm_mode_usr = 0,
	arm_mode_fiq = 1,
	arm_mode_irq = 2,
	arm_mode_svc = 3,
	arm_mode_abt = 4,
	arm_mode_und = 5,
	arm_mode_sys = 6,
} arm_mode_e;

typedef enum
{
	arm_vec_reset = 0,
	arm_vec_undefined_instruction = 1,
	arm_vec_software_interrupt = 2,
	arm_vec_prefetch_abort = 3,
	arm_vec_data_abort = 4,
	arm_vec_reserved = 5,
	arm_vec_irq = 6,
	arm_vec_fiq = 7,
} arm_vec_e;



typedef struct __attribute__((__packed__))
{
	arm_mode26_e mode     :  2;
	unsigned     address  : 24;
	bool         fiq_en   :  1;
	bool         irq_en   :  1;
	bool         overflow :  1;
	bool         carry    :  1;
	bool         zero     :  1;
	bool         sign     :  1;
} arm_reg_pc26_t;

typedef struct __attribute__((__packed__))
{
	arm_mode_e mode     :  4;
	bool       thumb    :  1;
	bool       fiq_en   :  1;
	bool       irq_en   :  1;
	unsigned   res1     : 21;
	bool       overflow :  1;
	bool       carry    :  1;
	bool       zero     :  1;
	bool       sign     :  1;
} arm_reg_psr_t;



typedef union __attribute__((__packed__))
{
	struct __attribute__((__packed__))
	{
		unsigned   op2       : 12;
		arm_reg_e  rd        :  4;
		arm_reg_e  rn        :  4;
		bool       set_flags :  1;
		arm_op_e   op        :  4;
		bool       immed     :  1;
		unsigned   hole0     :  2; // =0b00
		arm_cond_e cond      :  4;
	} data;
	struct __attribute__((__packed__))
	{
		unsigned   imm       : 8;
		unsigned   ror       : 4;
		arm_reg_e  rd        : 4;
		arm_reg_e  rn        : 4;
		bool       set_flags : 1;
		arm_op_e   op        : 4;
		bool       immed     : 1;
		unsigned   hole0     : 2; // =0b00
		arm_cond_e cond      : 4;
	} di;
	struct __attribute__((__packed__))
	{
		arm_reg_e   rm        : 4;
		bool        shift_reg : 1;
		arm_shift_e shift     : 2;
		unsigned    shift_imm : 5;
		arm_reg_e   rd        : 4;
		arm_reg_e   rn        : 4;
		bool        set_flags : 1;
		arm_op_e    op        : 4;
		bool        immed     : 1;
		unsigned    hole0     : 2; // =0b00
		arm_cond_e  cond      : 4;
	} dri;
	struct __attribute__((__packed__))
	{
		arm_reg_e   rm        : 4;
		bool        shift_reg : 1;
		arm_shift_e shift     : 2;
		unsigned    hole1     : 1;
		unsigned    rs        : 4;
		arm_reg_e   rd        : 4;
		arm_reg_e   rn        : 4;
		bool        set_flags : 1;
		arm_op_e    op        : 4;
		bool        immed     : 1;
		unsigned    hole0     : 2; // =0b00
		arm_cond_e  cond      : 4;
	} drr;
	struct __attribute__((__packed__))
	{
		arm_reg_e  rm        :  4;
		unsigned   hole1     :  4; // =0b1001
		arm_reg_e  rs        :  4;
		arm_reg_e  rn        :  4;
		arm_reg_e  rd        :  4;
		bool       set_flags :  1;
		bool       acc       :  1;
		unsigned   hole0     :  6; // =0b000000
		arm_cond_e cond      :  4;
	} mult;
	struct __attribute__((__packed__))
	{
		arm_reg_e  rm        :  4;
		unsigned   hole1     :  4; // =0b1001
		arm_reg_e  rs        :  4;
		arm_reg_e  rdlo      :  4;
		arm_reg_e  rdhi      :  4;
		bool       set_flags :  1;
		bool       acc       :  1;
		bool       usign     :  1;
		unsigned   hole0     :  5; // =0b00001
		arm_cond_e cond      :  4;
	} lmul;
	struct __attribute__((__packed__))
	{
		arm_reg_e  rm    :  4;
		arm_reg_e  hole2 :  8; // =0b00001001
		arm_reg_e  rd    :  4;
		arm_reg_e  rn    :  4;
		unsigned   hole1 :  2; // =0b00
		bool       b     :  1;
		unsigned   hole0 :  5; // =0b00010
		arm_cond_e cond  :  4;
	} swap;
	struct __attribute__((__packed__))
	{
		unsigned       off      : 12;
		arm_reg_e      rd       :  4;
		arm_reg_e      rn       :  4;
		bool           load     :  1;
		bool           w        :  1;
		bool           b        :  1;
		arm_modifier_e modifier :  2;
		bool           immed    :  1;
		unsigned       hole0    :  2; // =0b01
		arm_cond_e     cond     :  4;
	} mem;
	struct __attribute__((__packed__))
	{
		unsigned       regs     : 16;
		arm_reg_e      rn       :  4;
		bool           load     :  1;
		bool           modify   :  1;
		bool           s        :  1;
		arm_modifier_e modifier :  2;
		unsigned       hole0    :  3; // =0b100
		arm_cond_e     cond     :  4;
	} memm;
	struct __attribute__((__packed__))
	{
		unsigned       off2     : 4;
		unsigned       hole3    : 1; // =0b1
		bool           h        : 1;
		bool           s        : 1;
		unsigned       hole2    : 1; // =0b1
		unsigned       off1     : 4;
		arm_reg_e      rd       : 4;
		arm_reg_e      rn       : 4;
		bool           load     : 1;
		bool           w        : 1;
		unsigned       hole1    : 1; // =0b1
		arm_modifier_e modifier : 2;
		unsigned       hole0    : 3; // =0b000
		arm_cond_e     cond     : 4;
	} memhi;
	struct __attribute__((__packed__))
	{
		unsigned       off2     : 4;
		unsigned       hole3    : 1; // =0b1
		bool           h        : 1;
		bool           s        : 1;
		unsigned       hole2    : 5; // =0b00001
		arm_reg_e      rd       : 4;
		arm_reg_e      rn       : 4;
		bool           load     : 1;
		bool           w        : 1;
		unsigned       hole1    : 1; // =0b0
		arm_modifier_e modifier : 2;
		unsigned       hole0    : 3; // =0b000
		arm_cond_e     cond     : 4;
	} memhr;
	struct __attribute__((__packed__))
	{
		unsigned   off   : 24;
		bool       link  :  1;
		unsigned   hole0 :  3; // =0b101
		arm_cond_e cond  :  4;
	} branch;
	struct __attribute__((__packed__))
	{
		arm_reg_e  rn    :  4;
		unsigned   hole0 : 24; // =0b0000100101111111111110001
		arm_cond_e cond  :  4;
	} bx;
	struct __attribute__((__packed__))
	{
		unsigned       off      : 8;
		unsigned       cpn      : 4;
		unsigned       crd      : 4;
		arm_reg_e      rn       : 4;
		bool           load     : 1;
		bool           w        : 1;
		bool           n        : 1;
		arm_modifier_e modifier : 2;
		unsigned       hole0    : 3; // =0b110
		arm_cond_e     cond     : 4;
	} copdt;
	struct __attribute__((__packed__))
	{
		unsigned   crm   : 4;
		unsigned   hole1 : 1; // =0b0
		unsigned   op2   : 3;
		unsigned   cpn   : 4;
		unsigned   crd   : 4;
		unsigned   crn   : 4;
		unsigned   op1   : 4;
		unsigned   hole0 : 4; // =0b1110
		arm_cond_e cond  : 4;
	} copdo;
	struct __attribute__((__packed__))
	{
		unsigned   crm   : 4;
		unsigned   hole1 : 1; // =0b1
		unsigned   op2   : 3;
		unsigned   cpn   : 4;
		unsigned   crd   : 4;
		unsigned   crn   : 4;
		bool       l     : 1;
		unsigned   op1   : 3;
		unsigned   hole0 : 4; // =0b1110
		arm_cond_e cond  : 4;
	} coprt;
	struct __attribute__((__packed__))
	{
		unsigned   code  : 24;
		unsigned   hole0 :  4; // =0b1111
		arm_cond_e cond  :  4;
	} swi;
	uint32_t mask;
} arm_instruction_t;


bool arm_gen_call(
	fct_asm_context_t* context, fct_asm_field_t* field);
bool arm_gen_return(fct_asm_context_t* context);
bool arm_gen_syscall(
	fct_asm_context_t* context, fct_asm_field_t* field);

bool arm_assemble(
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  field,
	unsigned           field_count);

bool arm_link(
	arm_instruction_t* code,
	uint64_t size, uint64_t symbol);


fct_arch_t fct_arch__arm;
#define FCT_ARCH_ARM_STATIC &fct_arch__arm
fct_arch_t* fct_arch_arm;


#define ELF_MACHINE_ARM (uint16_t)0x28

#endif
