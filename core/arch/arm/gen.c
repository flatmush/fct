#include <fct/types.h>
#include <fct/debug.h>
#include <fct/asm/context.h>
#include "arm.h"


bool arm_gen_call(
	fct_asm_context_t* context, fct_asm_field_t* field)
{
	if (!field)
		return false;

	fct_tok_t mnem =
	{
		.type = FCT_TOK_IDENT,
		.base = "bl",
		.size = 2,
		.file = field->file,
		.row  = field->row,
		.col  = field->col,
	};

	return arm_assemble(context,
		&mnem, NULL, &field, 1);
}

bool arm_gen_return(fct_asm_context_t* context)
{
	arm_instruction_t inst;
	inst.data.cond      = arm_cond_al;
	inst.data.hole0     = 0;
	inst.data.immed     = false;
	inst.data.op        = arm_op_mov;
	inst.data.set_flags = false;
	inst.data.rn        = arm_reg_lr;
	inst.data.rd        = arm_reg_pc;
	inst.data.op2       = 0;
	return fct_asm_context_emit_code(
		context, &inst.mask, sizeof(inst), sizeof(inst), sizeof(inst));
}

bool arm_gen_syscall(
	fct_asm_context_t* context, fct_asm_field_t* field)
{
	if (!field)
		return false;

	fct_tok_t mnem =
	{
		.type = FCT_TOK_IDENT,
		.base = "swi",
		.size = 3,
		.file = field->file,
		.row  = field->row,
		.col  = field->col,
	};

	return arm_assemble(context,
		&mnem, NULL, &field, 1);
}
