#ifndef __FCT_DEVICE_SYSTEM_H__
#define __FCT_DEVICE_SYSTEM_H__

#include <fct/device.h>

fct_device_t* fct_device_system_import(
	fct_arch_t* arch, const char* path);

#endif
