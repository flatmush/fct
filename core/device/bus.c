#include <fct/device.h>
#include "device.h"

#include <stdlib.h>
#include <stdio.h>



typedef struct
{
	fct_device_t* device;
	uint64_t      base;
	uint64_t      size;
} fct_device_bus__entry_t;

static void fct_device_bus__entry_delete(
	fct_device_bus__entry_t* entry)
{
	if (!entry)
		return;

	fct_device_delete(entry->device);
	free(entry);
}

static bool fct_device_bus__entry_collision(
	fct_device_bus__entry_t* a,
	fct_device_bus__entry_t* b)
{
	if (!a || !b)
		return false;

	return ((a->base < (b->base + b->size))
		&& ((a->base + a->size) > b->base));
}

static bool fct_device_bus__entry_find(
	fct_device_bus__entry_t* entry, uint64_t* addr)
{
	if (!entry || !addr)
		return false;

	return ((*addr >= entry->base)
		&& (*addr < (entry->base + entry->size)));
}

static const fct_class_t
	fct_class__device_bus_entry =
	{
		(void*)fct_device_bus__entry_delete,
		NULL,
		NULL,
		NULL,
		NULL,
	};



struct fct_device_subclass_s
{
	fct_list_t* devices;
	unsigned    mau, width;
};



static void fct_device_bus__delete(
	fct_device_t* device)
{
	if (!device)
		return;

	if (device->subclass)
		fct_list_delete(device->subclass->devices);
	free(device->subclass);
}



static bool fct_device_bus__params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width)
{
	if (!device
		|| !device->subclass)
		return false;

	if (mau  ) *mau   = device->subclass->mau  ;
	if (width) *width = device->subclass->width;
	return true;
}



static void fct_device_bus__read(
	fct_device_t* device, uint64_t addr, void* data)
{
	if (!device || !device->subclass)
		return;

	fct_device_bus__entry_t* entry
		= (fct_device_bus__entry_t*)fct_list_find(
				device->subclass->devices, &addr,
				(void*)fct_device_bus__entry_find);
	if (!entry) return;

	fct_device_read(entry->device, (addr - entry->base), data);
}

static void fct_device_bus__write(
	fct_device_t* device, uint64_t addr, uint32_t mask, void* data)
{
	if (!device || !device->subclass)
		return;

	fct_device_bus__entry_t* entry
		= (fct_device_bus__entry_t*)fct_list_find(
			device->subclass->devices, &addr,
			(void*)fct_device_bus__entry_find);
	if (!entry) return;

	fct_device_write(entry->device,
		(addr - entry->base), mask, data);
}



static fct_device_type_t fct_device_bus__type =
{
	.name          = "bus",
	.create_cb     = NULL,
	.delete_cb     = fct_device_bus__delete,
	.params_get_cb = fct_device_bus__params_get,
	.tick_cb       = NULL,
	.interrupt_cb  = NULL,
	.read_cb       = fct_device_bus__read,
	.write_cb      = fct_device_bus__write,

	.bus_cb    = NULL,
	.clkbus_cb = NULL,

	.port_attach_cb      = NULL,
	.interrupt_attach_cb = NULL,
};


fct_device_t* fct_device_bus_create(
	unsigned mau, unsigned width)
{
	fct_device_t* device
		= fct_device_create(
			&fct_device_bus__type, NULL);
	if (!device) return NULL;

	device->subclass
		= (fct_device_subclass_t*)malloc(
			sizeof(fct_device_subclass_t));
	if (!device->subclass)
	{
		fct_device_delete(device);
		return NULL;
	}

	device->subclass->devices = NULL;
	device->subclass->mau     = mau;
	device->subclass->width   = width;

	return device;
}



bool fct_device_bus_attach(
	fct_device_t* bus, fct_device_t* device,
	uint64_t base, uint64_t size)
{
	if (!bus || !bus->subclass
		|| (bus->type != &fct_device_bus__type))
		return false;

	if (!bus->subclass->devices)
	{
		bus->subclass->devices = fct_array_create(
			&fct_class__device_bus_entry);
		if (!bus->subclass->devices)
			return false;
	}

	unsigned mau, width;
	unsigned dmau, dwidth;
	if (!fct_device_params_get(bus, &mau, &width)
		|| !fct_device_params_get(device, &dmau, &dwidth))
		return false;

	if ((mau != dmau)
		|| (width != dwidth))
	{
		fct_device_t* busconv
			= fct_device_busconv_create(
				mau, width, device);
		bool success = fct_device_bus_attach(
			bus, busconv, base, size);
		fct_device_delete(busconv);
		return success;
	}

	fct_device_bus__entry_t nentry =
	{
		.device = device,
		.base   = base,
		.size   = size,
	};

	if (fct_list_find(bus->subclass->devices, &nentry,
		(void*)fct_device_bus__entry_collision))
		return false;

	fct_device_bus__entry_t* entry
		= (fct_device_bus__entry_t*)malloc(
			sizeof(fct_device_bus__entry_t));
	if (!entry) return false;

	*entry = nentry;

	entry->device = fct_device_reference(device);
	if (!entry->device)
	{
		free(entry);
		return false;
	}

	if (!fct_list_add(
		bus->subclass->devices, entry))
	{
		fct_device_bus__entry_delete(entry);
		return false;
	}

	return true;
}
