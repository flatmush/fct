#include <fct/device.h>
#include "device.h"

#include <stdlib.h>
#include <dlfcn.h>

#include <fct/debug.h>
#include <fct/util/bitaddr.h>



fct_device_type_t fct_device_memory__type;
fct_device_type_t fct_device_stdout__type;

typedef struct
{
	fct_device_type_t* type;
	void* handle;
	void (*term)(fct_device_type_t* type);
} fct_device_type__entry_t;

static fct_device_type__entry_t* fct_device_type__list  = NULL;
static unsigned                  fct_device_type__count = 2;

void fct_device_type_by_name__cleanup(void)
{
	unsigned i;
	for (i = 0; i < fct_device_type__count; i++)
	{
		if (fct_device_type__list[i].term)
			fct_device_type__list[i].term(
				fct_device_type__list[i].type);
		if (fct_device_type__list[i].handle)
			dlclose(fct_device_type__list[i].handle);
	}

	free(fct_device_type__list);
}

fct_device_type_t* fct_device_type_by_name(const char* name)
{
	if (!name || (strlen(name) == 0))
		return NULL;

	if (!fct_device_type__list)
	{
		fct_device_type__list
			= (fct_device_type__entry_t*)malloc(
				sizeof(fct_device_type__entry_t)
					* fct_device_type__count);
		if (!fct_device_type__list) return NULL;

		fct_device_type__list[0].type   = &fct_device_memory__type;
		fct_device_type__list[0].handle = NULL;
		fct_device_type__list[0].term   = NULL;
		fct_device_type__list[1].type   = &fct_device_stdout__type;
		fct_device_type__list[1].handle = NULL;
		fct_device_type__list[1].term   = NULL;

		atexit(fct_device_type_by_name__cleanup);
	}

	unsigned i;
	for (i = 0; i < fct_device_type__count; i++)
	{
		if (strcasecmp(name,
			fct_device_type__list[i].type->name) == 0)
			return fct_device_type__list[i].type;
	}

	char libname[strlen(name) + 17];
	sprintf(libname, "./fct-device-%s.so", name);

	void* handle = dlopen(
		libname, RTLD_LAZY | RTLD_LOCAL);
	if (!handle)
	{
		FCT_DEBUG_ERROR(
			"dlopen failed '%s'.", dlerror());
		return NULL;
	}

	fct_device_type_t* (*device_init)(void);
	void (*device_term)(fct_device_type_t* type);

	device_init = dlsym(handle, "device_init");
	device_term = dlsym(handle, "device_term");

	if (!device_init
		|| !device_term)
	{
		dlclose(handle);
		return NULL;
	}

	fct_device_type_t* type
		= device_init();
	if (!type)
	{
		dlclose(handle);
		return NULL;
	}

	fct_device_type__entry_t* nlist
		= (fct_device_type__entry_t*)realloc(
			fct_device_type__list,
			sizeof(fct_device_type__entry_t)
				* (fct_device_type__count + 1));
	if (!nlist)
	{
		device_term(type);
		dlclose(handle);
		return NULL;
	}

	nlist[fct_device_type__count].type   = type;
	nlist[fct_device_type__count].handle = handle;
	nlist[fct_device_type__count].term   = device_term;
	fct_device_type__list = nlist;
	fct_device_type__count++;

	return type;
}



static const fct_class_t
	fct_class__device =
	{
		.delete_cb    = (void*)fct_device_delete,
		.reference_cb = (void*)fct_device_reference,
		.copy_cb      = NULL,
		.import_cb    = NULL,
		.export_cb    = NULL,
	};
const fct_class_t* fct_class_device
	= &fct_class__device;

fct_device_t* fct_device_create_params(
	fct_device_type_t* type, fct_string_t* name, const fct_tok_t* params)
{
	if (!type || !type->create_cb)
		return NULL;
	return type->create_cb(name, params);
}

void fct_device_delete(
	fct_device_t* device)
{
	if (!device)
		return;

	if (device->refcnt > 0)
	{
		device->refcnt--;
		return;
	}

	if (device->type
		&& device->type->delete_cb)
		device->type->delete_cb(device);
	else
		free(device->subclass);

	fct_string_delete(device->name);
	free(device);
}

fct_device_t* fct_device_reference(
	fct_device_t* device)
{
	if (!device
		|| ((device->refcnt + 1) == 0))
		return NULL;

	device->refcnt++;
	return device;
}



bool fct_device_rename(
	fct_device_t* device,
	fct_string_t* name)
{
	if (!device)
		return false;

	fct_string_delete(device->name);
	device->name = name;
	return true;
}



bool fct_device_params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width)
{
	if (!device
		|| !device->type
		|| !device->type->params_get_cb)
		return false;
	return device->type->params_get_cb(device, mau, width);
}



void fct_device_tick(
	fct_device_t* device)
{
	if (device
		&& device->type
		&& device->type->tick_cb)
		device->type->tick_cb(device);
}

void fct_device_interrupt(
	fct_device_t* device, uint64_t number)
{
	if (device
		&& device->type
		&& device->type->interrupt_cb)
		device->type->interrupt_cb(device, number);
}



void fct_device_read(
	fct_device_t* device, uint64_t addr, void* data)
{
	if (device
		&& device->type
		&& device->type->read_cb)
		device->type->read_cb(device, addr, data);
}

void fct_device_write(
	fct_device_t* device, uint64_t addr, uint32_t mask, void* data)
{
	if (device
		&& device->type
		&& device->type->write_cb)
		device->type->write_cb(device, addr, mask, data);
}



fct_device_t* fct_device_bus(
	fct_device_t* device, const char* name)
{
	if (!device
		|| !device->type
		|| !device->type->bus_cb)
		return NULL;
	return device->type->bus_cb(device, name);
}

fct_device_t* fct_device_clkbus(
	fct_device_t* device, const char* name)
{
	if (!device
		|| !device->type
		|| !device->type->clkbus_cb)
		return NULL;
	return device->type->clkbus_cb(device, name);
}



bool fct_device_port_attach(
	fct_device_t* device, const char* name, fct_device_t* target)
{
	if (!device
		|| !device->type
		|| !device->type->port_attach_cb)
		return NULL;
	return device->type->port_attach_cb(
		device, name, target);
}

bool fct_device_interrupt_attach(
	fct_device_t* device, const char* name, fct_device_t* target, uint64_t number)
{
	if (!device
		|| !device->type
		|| !device->type->interrupt_attach_cb)
		return NULL;
	return device->type->interrupt_attach_cb(
		device, name, target, number);
}



void fct_device_reads(
	fct_device_t* device,
	uint64_t addr, uint64_t size, void* data)
{
	if (!device
		|| !device->type
		|| !device->type->read_cb)
		return;

	unsigned mau, width;
	if (!fct_device_params_get(
		device, &mau, &width))
		return;

	uint32_t line_size = (mau * width);
	uint8_t  line[(line_size + 7) >> 3];

	uint64_t i;
	for (i = 0; i < size; i++)
	{
		device->type->read_cb(device, (addr + i), line);
		bitaddr_copy(data, (i * line_size), line, 0, line_size);
	}
}

void fct_device_writes(
	fct_device_t* device,
	uint64_t addr, uint64_t size, uint32_t mask, void* data)
{
	if (!device
		|| !device->type
		|| !device->type->write_cb)
		return;

	unsigned mau, width;
	if (!fct_device_params_get(
		device, &mau, &width))
		return;

	uint32_t line_size = (mau * width);
	uint8_t  line[(line_size + 7) >> 3];

	uint64_t i;
	for (i = 0; i < size; i++)
	{
		bitaddr_copy(line, 0, data, (i * line_size), line_size);
		device->type->write_cb(device, (addr + i), mask, line);
	}
}
