#include "system.h"
#include <fct/device/device.h>
#include <fct/device.h>
#include <fct/parse.h>
#include <fct/debug.h>
#include <stdlib.h>


typedef struct
{
	fct_device_t** core;
	fct_list_t*    list;
} fct_device_system__context_t;


static bool fct_device_system__find_device(
	fct_device_t* device, fct_string_t* name)
{
	return fct_string_compare(
		device->name, name);
}


static bool fct_device_system__parse_core(
	fct_device_t** core, const fct_lex_line_t* line, fct_list_t* list)
{
	const const fct_tok_t* src
		= fct_lex_line_tokens_contiguous(line);
	if (!src) return false;

	unsigned i = 1;

	const fct_tok_t* name = &src[i];
	if (name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected name in device declaration");
		return false;
	}
	i += 1;

	const fct_tok_t* class = &src[i];
	if (class->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected device type in device declaration");
		return false;
	}
	i += 1;

	const fct_tok_t* library = NULL;
	if (src[i].type == FCT_TOK_MEMBER)
	{
		library = class;
		class = &src[++i];
		if (class->type != FCT_TOK_IDENT)
		{
			FCT_DEBUG_ERROR_TOKEN(&src[i],
				"Invalid device type in device declaration");
			return false;
		}
		i += 1;
	}

	fct_device_type_t* type = NULL;
	if (!library)
	{
		char type_name[class->size + 1];
		memcpy(type_name, class->base, class->size);
		type_name[class->size] = '\0';
		type = fct_device_type_by_name(type_name);
	}

	if (!type)
	{
		FCT_DEBUG_ERROR_TOKEN(class,
			"Unknown device type in device declaration");
		return false;
	}

	fct_string_t* sname
		= fct_tok_to_string(name);
	if (!sname) return false;

	fct_device_t* device
		= fct_device_create_params(
			type, sname, &src[i]);
	if (!device)
	{
		fct_string_delete(sname);
		return false;
	}

	if (!fct_list_add(list, device))
	{
		fct_device_delete(device);
		return false;
	}

	if (core)
		*core = fct_device_reference(device);
	return true;
}

static bool fct_device_system__parse_device(
	const fct_lex_line_t* line, fct_list_t* list)
{
	return fct_device_system__parse_core(
		NULL, line, list);
}

static bool fct_device_system__parse_system(
	const fct_lex_line_t* line, fct_list_t* list)
{
	const const fct_tok_t* src
		= fct_lex_line_tokens_contiguous(line);
	if (!src) return false;

	const fct_tok_t* name = &src[1];
	if (name->type != FCT_TOK_IDENT)
	{
		return false;
	}

	if (src[3].type != FCT_TOK_END)
	{
		return false;
	}

	char* path = fct_tok_path(&src[2]);
	if (!path)
	{
		return false;
	}

	fct_device_t* system
		= fct_device_system_import(
			NULL, path);
	free(path);
	if (!system) return false;

	fct_string_t* sname = fct_tok_to_string(name);
	if (!sname || !fct_device_rename(system, sname))
	{
		fct_device_delete(system);
		fct_string_delete(sname);
		return false;
	}

	if (!fct_list_add(list, system))
	{
		fct_device_delete(system);
		return false;
	}

	return true;
}

static bool fct_device_system__parse_bus_entry(
	fct_lex_line_t* line,
	fct_device_system__context_t* context)
{
	const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return false;

	unsigned i = 0;

	uint64_t addr;
	if (!fct_tok_uint64(&src[i], &addr))
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected address in bus entry");
		return false;
	}
	i += 1;

	uint64_t size;
	if (!fct_tok_uint64(&src[i], &size))
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected size in bus entry");
		return false;
	}
	i += 1;

	const fct_tok_t* device_name = &src[i++];
	if (device_name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(device_name,
			"Expected source device name in bus entry");
		return false;
	}

	const fct_tok_t* bus_name = NULL;
	if (src[i].type == FCT_TOK_MEMBER)
	{
		bus_name = &src[++i];
		if (bus_name->type != FCT_TOK_IDENT)
		{
			FCT_DEBUG_ERROR_TOKEN(bus_name,
				"Invalid bus name in bus entry");
			return false;
		}
		i += 1;
	}

	if (src[i].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected end of bus entry");
		return false;
	}

	fct_device_t* device = fct_list_find(
		context->list, FCT_TOK_TO_STRING_STATIC(device_name),
		(void*)fct_device_system__find_device);
	if (!device) return false;

	unsigned bus_width;
	if (!fct_device_params_get(
		*(context->core), NULL, &bus_width))
		return false;

	if ((addr % bus_width)
		|| (size % bus_width))
		return false;

	uint64_t bus_addr = addr / bus_width;
	uint64_t bus_size = size / bus_width;

	return fct_device_bus_attach(
		*(context->core), device, bus_addr, bus_size);
}

static bool fct_device_system__parse_bus(
	const fct_lex_line_t* line, fct_list_t* list)
{
	const const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return false;

	unsigned i = 1;

	const fct_tok_t* master_name = &src[i++];
	if (master_name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(master_name,
			"Expected master name in bus declaration");
		return false;
	}

	const fct_tok_t* bus_name = NULL;
	if (src[i].type == FCT_TOK_MEMBER)
	{
		bus_name = &src[++i];
		if (bus_name->type != FCT_TOK_IDENT)
		{
			FCT_DEBUG_ERROR_TOKEN(bus_name,
				"Expected source device bus name in clock declaration");
			return false;
		}
		i += 1;
	}

	if (src[i].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected end of bus declaration header");
		return false;
	}

	fct_device_t* device = fct_list_find(
		list, FCT_TOK_TO_STRING_STATIC(master_name),
		(void*)fct_device_system__find_device);
	if (!device) return false;

	fct_device_t* bus;
	if (bus_name)
	{
		char name[bus_name->size + 1];
		memcpy(name, bus_name->base, bus_name->size);
		name[bus_name->size] = '\0';

		bus = fct_device_bus(
			device, name);
	}
	else
	{
		bus = fct_device_bus(
			device, NULL);
	}

	if (!bus)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i - 1],
			"Failed to resolve bus");
		return false;
	}

	fct_device_system__context_t context = { &bus, list };
	return fct_lex_line_foreach(line, &context,
		(void*)fct_device_system__parse_bus_entry);
}

static bool fct_device_system__parse_port(
	const fct_lex_line_t* line, fct_list_t* list)
{
	const const fct_tok_t* src
		= fct_lex_line_tokens_contiguous(line);
	if (!src) return false;

	unsigned i = 1;

	const fct_tok_t* device_name = &src[i++];
	if (device_name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(device_name,
			"Expected device name in port declaration");
		return false;
	}

	if (src[i++].type != FCT_TOK_MEMBER)
	{
		FCT_DEBUG_ERROR_TOKEN(device_name,
			"Expected port name in port declaration");
		return false;
	}

	const fct_tok_t* name = &src[i++];
	if (name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(name,
			"Invalid port name");
		return false;
	}

	const fct_tok_t* target_name = &src[i++];
	if (target_name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(target_name,
			"Expected target in port declaration");
		return false;
	}

	if (src[i].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected end of port declaration");
		return false;
	}

	fct_device_t* device = fct_list_find(
		list, FCT_TOK_TO_STRING_STATIC(device_name),
		(void*)fct_device_system__find_device);
	if (!device) return false;

	fct_device_t* target = fct_list_find(
		list, FCT_TOK_TO_STRING_STATIC(target_name),
		(void*)fct_device_system__find_device);
	if (!target) return false;

	if (device == target)
		return false;

	char sname[name->size + 1];
	memcpy(sname, name->base, name->size);
	sname[name->size] = '\0';

	return fct_device_port_attach(
		device, sname, target);
}

static bool fct_device_system__parse_clock_entry(
	fct_lex_line_t* line,
	fct_device_system__context_t* context)
{
	const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return false;

	const fct_tok_t* device_name = &src[0];
	if (device_name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(device_name,
			"Expected source device name in clock bus entry");
		return false;
	}

	if (src[1].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[1],
			"Expected end of clock bus entry");
		return false;
	}

	fct_device_t* device = fct_list_find(
		context->list, FCT_TOK_TO_STRING_STATIC(device_name),
		(void*)fct_device_system__find_device);
	if (!device) return false;

	return fct_device_clkbus_attach(
		*(context->core), device);
}

static bool fct_device_system__parse_clock(
	const fct_lex_line_t* line, fct_list_t* list)
{
	const const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return false;

	unsigned i = 1;

	const fct_tok_t* master_name = &src[i++];
	if (master_name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(master_name,
			"Expected source device name in clock declaration");
		return false;
	}

	const fct_tok_t* bus_name = NULL;
	if (src[i].type == FCT_TOK_MEMBER)
	{
		bus_name = &src[++i];
		if (bus_name->type != FCT_TOK_IDENT)
		{
			FCT_DEBUG_ERROR_TOKEN(bus_name,
				"Expected source device bus name in clock declaration");
			return false;
		}
		i += 1;
	}

	if (src[i].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected end of clock declaration header");
		return false;
	}

	fct_device_t* device = fct_list_find(
		list, FCT_TOK_TO_STRING_STATIC(master_name),
		(void*)fct_device_system__find_device);
	if (!device) return false;

	fct_device_t* bus;
	if (bus_name)
	{
		char name[bus_name->size + 1];
		memcpy(name, bus_name->base, bus_name->size);
		name[bus_name->size] = '\0';

		bus = fct_device_clkbus(
			device, name);
	}
	else
	{
		bus = fct_device_clkbus(
			device, NULL);
	}

	if (!bus)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i - 1],
			"Failed to resolve clock bus");
		return false;
	}

	fct_device_system__context_t context = { &bus, list };
	return fct_lex_line_foreach(line, &context,
		(void*)fct_device_system__parse_clock_entry);
}

static bool fct_device_system__parse_intr_entry(
	fct_lex_line_t* line,
	fct_device_system__context_t* context)
{
	const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return false;

	unsigned i = 0;

	uint64_t number;
	if (!fct_tok_uint64(&src[i], &number))
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected interrupt number in interrupt entry");
		return false;
	}
	i += 1;

	const fct_tok_t* device_name = &src[i++];
	if (device_name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(device_name,
			"Expected source device name in interrupt entry");
		return false;
	}

	const fct_tok_t* intr_name = NULL;
	if (src[i].type == FCT_TOK_MEMBER)
	{
		intr_name = &src[++i];
		if (intr_name->type != FCT_TOK_IDENT)
		{
			FCT_DEBUG_ERROR_TOKEN(intr_name,
				"Invalid interrupt name in interrupt entry");
			return false;
		}
		i += 1;
	}

	if (src[i].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected end of interrupt entry");
		return false;
	}

	fct_device_t* device = fct_list_find(
		context->list, FCT_TOK_TO_STRING_STATIC(device_name),
		(void*)fct_device_system__find_device);
	if (!device) return false;

	if (!intr_name)
		return fct_device_interrupt_attach(
			device, NULL, *(context->core), number);

	char sname[intr_name->size + 1];
	memcpy(sname, intr_name->base, intr_name->size);
	sname[intr_name->size] = '\0';

	return fct_device_interrupt_attach(
		device, sname, *(context->core), number);
}

static bool fct_device_system__parse_interrupt(
	const fct_lex_line_t* line, fct_list_t* list)
{
	const const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return false;

	const fct_tok_t* master_name = &src[1];
	if (master_name->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(master_name,
			"Expected target name in interrupt declaration");
		return false;
	}

	if (src[2].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[2],
			"Expected end of interrupt declaration header");
		return false;
	}

	fct_device_t* device = fct_list_find(
		list, FCT_TOK_TO_STRING_STATIC(master_name),
		(void*)fct_device_system__find_device);
	if (!device) return false;

	fct_device_system__context_t context = { &device, list };
	return fct_lex_line_foreach(line, &context,
		(void*)fct_device_system__parse_intr_entry);
}

static bool fct_device_system__parse_line(
	const fct_lex_line_t* line,
	fct_device_system__context_t* context)
{
	const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return false;

	if (fct_tok_keyword(src, "core")
		&& context->core && !*(context->core))
	{
		return fct_device_system__parse_core(
			context->core, line, context->list);
	}

	const char* cmd_name[] =
	{
		"device",
		"system",
		"bus",
		"port",
		"clock",
		"interrupt",
		NULL
	};

	bool (*cmd_func[])(const fct_lex_line_t*, fct_list_t*) =
	{
		fct_device_system__parse_device,
		fct_device_system__parse_system,
		fct_device_system__parse_bus,
		fct_device_system__parse_port,
		fct_device_system__parse_clock,
		fct_device_system__parse_interrupt,
	};

	unsigned c;
	for (c = 0; cmd_name[c]; c++)
	{
		if (fct_tok_keyword(src, cmd_name[c]))
			return cmd_func[c](line, context->list);
	}

	return false;
}


static fct_device_t* fct_device_system_create(
	fct_arch_t* arch, const fct_lex_line_t* line)
{
	if (!line)
		return NULL;

	fct_list_t* list
		= fct_array_create(
			fct_class_device);
	if (!list) return NULL;

	fct_device_t* system = NULL;
	if (arch)
	{
		fct_string_t* core_name
			= fct_string_create("core");
		if (!core_name)
		{
			fct_list_delete(list);
			return NULL;
		}
		system = fct_device_core_create(
			core_name, arch);
		if (!system)
		{
			fct_string_delete(core_name);
			fct_list_delete(list);
			return NULL;
		}

		fct_device_t* system_ref
			= fct_device_reference(system);
		if (!system_ref)
		{
			fct_device_delete(system);
			fct_list_delete(list);
		}

		if (!fct_list_add(list, system_ref))
		{
			fct_device_delete(system_ref);
			fct_device_delete(system);
			fct_list_delete(list);
			return NULL;
		}
	}

	fct_device_system__context_t context
		= { &system, list };

	if (!fct_lex_line_foreach(line, &context,
		(void*)fct_device_system__parse_line))
	{
		FCT_DEBUG_ERROR_LEX_LINE(line,
			"Failed to parse system definition");
		fct_list_delete(list);
		fct_device_delete(system);
		return NULL;
	}

	if (!system)
	{
		FCT_DEBUG_ERROR_LEX_LINE(line,
			"System has no defined core");
	}

	fct_list_delete(list);
	return system;
}


fct_device_t* fct_device_system_import(
	fct_arch_t* arch, const char* path)
{
	fct_lex_context_t* lex
		= fct_lex_tokenize_file(path);
	if (!lex) return false;

	const fct_lex_line_t* root
		= fct_lex_line_root(lex);

	fct_device_t* device
		= fct_device_system_create(
			arch, root);
	fct_lex_context_delete(lex);

	return device;
}
