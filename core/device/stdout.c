#include <fct/device.h>
#include "device.h"

#include <stdlib.h>
#include <stdio.h>



fct_device_t* fct_device_stdout_create(fct_string_t* name);

static fct_device_t* fct_device_stdout__create(
	fct_string_t* name, const fct_tok_t* params)
{
	if (params && (params->type != FCT_TOK_END))
		return NULL;
	return fct_device_stdout_create(name);
}



static bool fct_device_stdout__params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width)
{
	if (!device)
		return false;

	if (mau  ) *mau   = 8;
	if (width) *width = 1;
	return true;
}



static void fct_device_stdout__write(
	fct_device_t* device, uint64_t addr, uint32_t mask, void* data)
{
	(void)device;
	(void)addr;
	if (mask & 1)
		fprintf(stdout, "%c", ((char*)data)[0]);
}



fct_device_type_t fct_device_stdout__type =
{
	.name          = "stdout",
	.create_cb     = fct_device_stdout__create,
	.delete_cb     = NULL,
	.params_get_cb = fct_device_stdout__params_get,
	.tick_cb       = NULL,
	.interrupt_cb  = NULL,
	.read_cb       = NULL,
	.write_cb      = fct_device_stdout__write,

	.bus_cb    = NULL,
	.clkbus_cb = NULL,

	.port_attach_cb      = NULL,
	.interrupt_attach_cb = NULL,
};



fct_device_t* fct_device_stdout_create(fct_string_t* name)
{
	return fct_device_create(
		&fct_device_stdout__type, name);
}
