#include <fct/device.h>
#include "device.h"

#include <stdlib.h>
#include <stdio.h>



struct fct_device_subclass_s
{
	fct_list_t* devices;
	unsigned    clkdiv, clk;
};



static void fct_device_clkbus__delete(
	fct_device_t* device)
{
	if (!device)
		return;

	if (device->subclass)
		fct_list_delete(device->subclass->devices);
	free(device->subclass);
}



static void fct_device_clkbus__tick(
	fct_device_t* device)
{
	if (!device || !device->subclass)
		return;

	if (device->subclass->clk == 0)
	{
		bool func(fct_device_t* device, void* param)
		{
			(void)param;
			fct_device_tick(device);
			return true;
		}

		fct_list_foreach(
			device->subclass->devices, NULL, (void*)func);
	}

	device->subclass->clk++;

	if (device->subclass->clk >= device->subclass->clkdiv)
		device->subclass->clk -= device->subclass->clkdiv;
}



static fct_device_type_t fct_device_clkbus__type =
{
	.name          = "clkbus",
	.create_cb     = NULL,
	.delete_cb     = fct_device_clkbus__delete,
	.params_get_cb = NULL,
	.tick_cb       = fct_device_clkbus__tick,
	.interrupt_cb  = NULL,
	.read_cb       = NULL,
	.write_cb      = NULL,

	.bus_cb    = NULL,
	.clkbus_cb = NULL,

	.port_attach_cb      = NULL,
	.interrupt_attach_cb = NULL,
};


fct_device_t* fct_device_clkbus_create(unsigned clkdiv)
{
	fct_device_t* device
		= fct_device_create(
			&fct_device_clkbus__type, NULL);
	if (!device) return NULL;

	device->subclass
		= (fct_device_subclass_t*)malloc(
			sizeof(fct_device_subclass_t));
	if (!device->subclass)
	{
		fct_device_delete(device);
		return NULL;
	}

	device->subclass->devices = NULL;
	device->subclass->clkdiv  = clkdiv;
	device->subclass->clk     = 0;

	return device;
}


static bool fct_device_clkbus__entry_collision(
	fct_device_t* a, fct_device_t* b)
{
	return (a == b);
}

bool fct_device_clkbus_attach(
	fct_device_t* clkbus, fct_device_t* device)
{
	if (!clkbus || !clkbus->subclass
		|| (clkbus->type != &fct_device_clkbus__type))
		return false;

	if (!clkbus->subclass->devices)
	{
		clkbus->subclass->devices
			= fct_array_create(fct_class_device);
		if (!clkbus->subclass->devices)
			return false;
	}

	if (fct_list_find(clkbus->subclass->devices, device,
		(void*)fct_device_clkbus__entry_collision))
		return false;

	fct_device_t* entry
		= fct_device_reference(device);
	if (!entry) return false;

	if (!fct_list_add(
		clkbus->subclass->devices, entry))
	{
		fct_device_delete(entry);
		return false;
	}

	return true;
}
