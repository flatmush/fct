#include <fct/device.h>
#include "device.h"

#include <stdlib.h>
#include <stdio.h>

#include <fct/util/bitaddr.h>
#include <fct/debug.h>



struct fct_device_subclass_s
{
	unsigned mau, width;
	fct_device_t* device;
	bool ssmau;
};



static void fct_device_busconv__delete(
	fct_device_t* device)
{
	if (!device)
		return;

	if (device->subclass)
		fct_device_delete(
			device->subclass->device);
	free(device->subclass);
}



static bool fct_device_busconv__params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width)
{
	if (!device
		|| !device->subclass)
		return false;

	if (mau  ) *mau   = device->subclass->mau  ;
	if (width) *width = device->subclass->width;
	return true;
}



static void fct_device_busconv__read(
	fct_device_t* device, uint64_t addr, void* data)
{
	if (!device
		|| !device->subclass)
		return;

	unsigned  read_size = (device->subclass->mau * device->subclass->width);
	uintptr_t line_size = (read_size + 7) >> 3;
	uint8_t   line[line_size];

	memset(line, 0x00, line_size);

	if (device->subclass->ssmau)
	{
		uint8_t unit[(device->subclass->mau + 7) >> 3];
		addr *= device->subclass->width;

		unsigned i;
		for (i = 0; i < device->subclass->width; i++)
		{
			fct_device_read(
				device->subclass->device,
				(addr + i), unit);
			bitaddr_copy(
				line, (device->subclass->mau * i),
				unit, 0, device->subclass->mau);
		}
	}
	else
	{
		fct_device_read(
			device->subclass->device,
			addr, line);
	}

	if (data)
		bitaddr_copy(data, 0, line, 0, read_size);
}

static void fct_device_busconv__write(
	fct_device_t* device, uint64_t addr, uint32_t mask, void* data)
{
	if (!device
		|| !device->subclass)
		return;

	if (device->subclass->ssmau)
	{
		uint8_t unit[(device->subclass->mau + 7) >> 3];
		addr *= device->subclass->width;

		unsigned i;
		for (i = 0; i < device->subclass->width; i++)
		{
			if ((mask & (1U << i)) == 0)
				continue;

			bitaddr_copy(unit, 0,
				data, (device->subclass->mau * i),
				device->subclass->mau);
			fct_device_write(
				device->subclass->device,
				(addr + i), 1, unit);
		}

		return;
	}
	else
	{
		unsigned twidth;
		if (!fct_device_params_get(
			device->subclass->device,
			NULL, &twidth))
			return;

		uint32_t maskf = ((1ULL << device->subclass->width) - 1);

		uint32_t dmask;
		if (mask == 0)
			dmask = 0;
		else if (mask == maskf)
			dmask = ((1ULL << twidth) - 1);
		else
			return;

		fct_device_write(
			device->subclass->device,
			addr, dmask, data);
	}
}



static fct_device_type_t fct_device_busconv__type =
{
	.name          = "busconv",
	.create_cb     = NULL,
	.delete_cb     = fct_device_busconv__delete,
	.params_get_cb = fct_device_busconv__params_get,
	.tick_cb       = NULL,
	.interrupt_cb  = NULL,
	.read_cb       = fct_device_busconv__read,
	.write_cb      = fct_device_busconv__write,

	.bus_cb    = NULL,
	.clkbus_cb = NULL,

	.port_attach_cb      = NULL,
	.interrupt_attach_cb = NULL,
};



fct_device_t* fct_device_busconv_create(
	unsigned mau, unsigned width, fct_device_t* target)
{
	unsigned tmau, twidth;
	if (!fct_device_params_get(
		target, &tmau, &twidth))
		return NULL;

	if ((mau * width)
		< (tmau * twidth))
		return NULL;

	if ((tmau == mau)
		&& (twidth == width))
		FCT_DEBUG_WARN(
			"Redundant bus converter, target matches bus.");

	fct_device_t* device
		= fct_device_create(
			&fct_device_busconv__type, NULL);
	if (!device) return NULL;

	device->subclass
		= (fct_device_subclass_t*)malloc(
			sizeof(fct_device_subclass_t));
	if (device->subclass)
	{
		device->subclass->device
			= fct_device_reference(target);
		device->subclass->mau   = mau;
		device->subclass->width = width;
		device->subclass->ssmau = ((tmau == mau) && (twidth == 1));
	}

	if (!device->subclass
		|| !device->subclass->device)
	{
		fct_device_delete(device);
		return NULL;
	}

	return device;
}
