#include <fct/device.h>
#include "device.h"

#include <stdlib.h>
#include <stdio.h>

#include <fct/arch/emu.h>



struct fct_device_subclass_s
{
	fct_arch_t*     arch;
	fct_arch_emu_t* emu;
	void*           core;
	fct_device_t*   bus[3];
	fct_device_t*   clkbus;
};



static void fct_device_core__delete(
	fct_device_t* device)
{
	if (!device)
		return;

	if (device->subclass)
	{
		fct_device_delete(device->subclass->bus[0]);
		fct_device_delete(device->subclass->bus[1]);
		fct_device_delete(device->subclass->bus[2]);
		fct_device_delete(device->subclass->clkbus);

		if (device->subclass->emu
			&& device->subclass->emu->delete_cb)
			device->subclass->emu->delete_cb(
				device->subclass->core);
	}
	free(device->subclass);
}



static bool fct_device_core__params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width)
{
	if (!device
		|| !device->subclass
		|| !device->subclass->arch)
		return false;

	unsigned pmau   = fct_arch_mau(device->subclass->arch);
	unsigned pwidth = fct_arch_word_size(device->subclass->arch);

	if ((pmau == 0)
		|| (pwidth == 0))
		return false;

	if (mau  ) *mau   = pmau;
	if (width) *width = pwidth;
	return true;
}



static void fct_device_core__tick(
	fct_device_t* device)
{
	if (!device || !device->subclass)
		return;

	if (device->subclass->emu
		&& device->subclass->emu->exec_cb)
		device->subclass->emu->exec_cb(
			device->subclass->core, device);

	fct_device_tick(device->subclass->clkbus);
}

static void fct_device_core__interrupt(
	fct_device_t* device, uint64_t number)
{
	if (!device || !device->subclass)
		return;

	fct_arch_emu_t* emu  = device->subclass->emu;
	void*           core = device->subclass->core;

	if (emu && emu->interrupt)
		emu->interrupt(core, number);
}



static void fct_device_core__read(
	fct_device_t* device, uint64_t addr, void* data)
{
	if (!device
		|| !device->subclass)
		return;

	fct_arch_emu_t* emu  = device->subclass->emu;
	void*           core = device->subclass->core;
	fct_device_t*   bus  = device->subclass->bus[0];
	if (!bus)       bus  = device->subclass->bus[2];

	if (emu && emu->mmap_read
		&& emu->mmap_read(core, addr, data))
		return;

	fct_device_read(bus, addr, data);
}

static void fct_device_core__write(
	fct_device_t* device, uint64_t addr, uint32_t mask, void* data)
{
	if (!device
		|| !device->subclass)
		return;

	fct_arch_emu_t* emu  = device->subclass->emu;
	void*           core = device->subclass->core;
	fct_device_t*   bus  = device->subclass->bus[1];
	if (!bus)       bus  = device->subclass->bus[2];

	if (emu && core && emu->mmap_write
		&& emu->mmap_write(core, bus, addr, mask, data))
		return;

	fct_device_write(bus, addr, mask, data);
}



static fct_device_t* fct_device_core__bus(
	fct_device_t* core, const char* name)
{
	if (!core || !core->subclass)
		return NULL;

	if (!name || (strlen(name) == 0))
	{
		if (core->subclass->bus[0]
			|| core->subclass->bus[1])
			return NULL;

		if (!core->subclass->bus[2])
		{
			unsigned mau, width;
			if (!fct_device_params_get(
				core, &mau, &width))
				return NULL;
			core->subclass->bus[2]
				= fct_device_bus_create(mau, width);
		}
		return core->subclass->bus[2];
	}

	if (core->subclass->bus[2])
		return NULL;

	if ((strlen(name) == 4)
		&& (strncasecmp(name, "read", 4) == 0))
	{
		if (!core->subclass->bus[0])
		{
			unsigned mau, width;
			if (!fct_device_params_get(
				core, &mau, &width))
				return NULL;
			core->subclass->bus[0]
				= fct_device_bus_create(mau, width);
		}
		return core->subclass->bus[0];
	}

	if ((strlen(name) == 5)
		&& (strncasecmp(name, "write", 5) == 0))
	{
		if (!core->subclass->bus[1])
		{
			unsigned mau, width;
			if (!fct_device_params_get(
				core, &mau, &width))
				return NULL;
			core->subclass->bus[1]
				= fct_device_bus_create(mau, width);
		}
		return core->subclass->bus[1];
	}

	return NULL;
}

static fct_device_t* fct_device_core__clkbus(
	fct_device_t* core, const char* name)
{
	if (!core || !core->subclass)
		return NULL;

	if (name && (strlen(name) > 0))
		return NULL;

	if (!core->subclass->clkbus)
		core->subclass->clkbus
			= fct_device_clkbus_create(1);
	return core->subclass->clkbus;
}



static fct_device_type_t fct_device_core__type =
{
	.name                = "core",
	.create_cb           = NULL,
	.delete_cb           = fct_device_core__delete,
	.params_get_cb       = fct_device_core__params_get,
	.tick_cb             = fct_device_core__tick,
	.interrupt_cb        = fct_device_core__interrupt,
	.read_cb             = fct_device_core__read,
	.write_cb            = fct_device_core__write,
	.bus_cb              = fct_device_core__bus,
	.clkbus_cb           = fct_device_core__clkbus,
	.port_attach_cb      = NULL,
	.interrupt_attach_cb = NULL,
};



fct_device_t* fct_device_core_create(
	fct_string_t* name, fct_arch_t* arch)
{
	fct_arch_emu_t* emu
		= fct_arch_emu(arch);
	if (!emu || !emu->create_cb)
		return NULL;

	fct_device_t* device
		= fct_device_create(
			&fct_device_core__type, name);
	if (!device) return NULL;

	device->subclass
		= (fct_device_subclass_t*)malloc(
			sizeof(fct_device_subclass_t));
	if (device->subclass)
	{
		device->subclass->arch = arch;
		device->subclass->emu  = emu;
		device->subclass->core = emu->create_cb();
		device->subclass->bus[0]  = NULL;
		device->subclass->bus[1]  = NULL;
		device->subclass->bus[2]  = NULL;
		device->subclass->clkbus  = NULL;
	}

	if (!device->subclass
		|| !device->subclass->core)
	{
		fct_device_delete(device);
		return NULL;
	}

	return device;
}
