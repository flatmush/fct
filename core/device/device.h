#ifndef __FCT_DEVICE_DEVICE_H__
#define __FCT_DEVICE_DEVICE_H__

typedef struct fct_device_s          fct_device_t;
typedef struct fct_device_type_s     fct_device_type_t;
typedef struct fct_device_subclass_s fct_device_subclass_t;

#include <fct/types.h>
#include <fct/lex/tok.h>

struct fct_device_type_s
{
	const char* name;

	fct_device_t* (*create_cb)(fct_string_t* name, const fct_tok_t* params);
	void          (*delete_cb)(fct_device_t* device);

	bool (*params_get_cb)(
		fct_device_t* device, unsigned* mau, unsigned* width);

	void (*tick_cb)(
		fct_device_t* device);
	void (*interrupt_cb)(
		fct_device_t* device, uint64_t number);

	void (*read_cb)(
		fct_device_t* device, uint64_t addr, void* data);
	void (*write_cb)(
		fct_device_t* device, uint64_t addr, uint32_t mask, void* data);

	fct_device_t* (*bus_cb   )(fct_device_t* device, const char* name);
	fct_device_t* (*clkbus_cb)(fct_device_t* device, const char* name);

	bool (*port_attach_cb)(
		fct_device_t* device, const char* name, fct_device_t* target);
	bool (*interrupt_attach_cb)(
		fct_device_t* device, const char* name, fct_device_t* target, uint64_t number);
};

struct fct_device_s
{
	fct_device_type_t*     type;
	fct_string_t*          name;
	fct_device_subclass_t* subclass;
	unsigned               refcnt;
};

static inline fct_device_t* fct_device_create(
	fct_device_type_t* type, fct_string_t* name)
{
	fct_device_t* device
		= (fct_device_t*)malloc(
			sizeof(fct_device_t));
	if (!device) return NULL;

	device->type     = type;
	device->name     = name;
	device->subclass = NULL;
	device->refcnt   = 0;

	return device;
}

#endif
