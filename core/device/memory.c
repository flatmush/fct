#include <fct/device.h>
#include "device.h"

#include <stdlib.h>
#include <stdio.h>

#include <fct/maubuff.h>
#include <fct/util/bitaddr.h>
#include <fct/debug.h>
#include <fct/parse.h>



struct fct_device_subclass_s
{
	unsigned mau, width;
	uint64_t size;
	fct_maubuff_t* memory;
	bool read, write, dirty;
	const char* path;
};



fct_device_t* fct_device_memory_create(
	fct_string_t* name, unsigned mau, unsigned width, uint64_t size);

static fct_device_t* fct_device_memory__create(
	fct_string_t* name, const fct_tok_t* params)
{
	if (!params)
		return NULL;

	uint64_t mau64, width64, size;

	if (!fct_tok_uint64(&params[0], &mau64)
		|| !fct_tok_uint64(&params[1], &width64)
		|| !fct_tok_uint64(&params[2], &size)
		|| (params[3].type != FCT_TOK_END))
		return NULL;

	unsigned mau   = mau64;
	unsigned width = width64;

	if ((mau != mau64)
		|| (width != width64))
		return NULL;

	return fct_device_memory_create(
		name, mau, width, size);
}

static void fct_device_memory__delete(
	fct_device_t* device)
{
	if (!device || !device->subclass)
		return;

	if (device->subclass->path
		&& device->subclass->dirty)
	{
		fct_buffer_t* buffer
			= fct_maubuff_to_buffer(
				device->subclass->memory);
		if (!fct_buffer_file_export(buffer, device->subclass->path))
			FCT_DEBUG_WARN(
				"Failed to store non-volatile memory back to '%s'.",
				device->subclass->path);
		fct_buffer_delete(buffer);
	}

	fct_maubuff_delete(device->subclass->memory);
	free(device->subclass);
}



static bool fct_device_memory__params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width)
{
	if (!device
		|| !device->subclass)
		return false;

	if (mau  ) *mau   = device->subclass->mau  ;
	if (width) *width = device->subclass->width;
	return true;
}



static void fct_device_memory__read(
	fct_device_t* device, uint64_t addr, void* data)
{
	if (!device || !device->subclass
		|| !device->subclass->read)
		return;

	fct_maubuff_read(
		device->subclass->memory,
		(addr % device->subclass->size) * device->subclass->width,
		device->subclass->width, data);
}

static void fct_device_memory__write(
	fct_device_t* device, uint64_t addr, uint32_t mask, void* data)
{
	if (!device || !device->subclass
		|| !device->subclass->write)
		return;

	uint32_t maskf = ((1ULL << device->subclass->width) - 1);

	if (((mask & maskf) == maskf)
		&& !fct_maubuff_modify(
			device->subclass->memory,
			(addr % device->subclass->size) * device->subclass->width,
			data, device->subclass->width))
		return;

	uint8_t line[((device->subclass->mau * device->subclass->width) + 7) >> 3];

	if (!fct_maubuff_read(
		device->subclass->memory,
		(addr % device->subclass->size) * device->subclass->width,
		device->subclass->width, line))
		return;

	unsigned i;
	for (i = 0; i < device->subclass->width; i++)
	{
		if ((mask & (1 << i)) != 0)
			bitaddr_copy(
				line, (device->subclass->mau * i),
				data, (device->subclass->mau * i), device->subclass->mau);
	}

	if (fct_maubuff_modify(
		device->subclass->memory,
		(addr % device->subclass->size) * device->subclass->width,
		line, device->subclass->width))
		device->subclass->dirty = true;
}



fct_device_type_t fct_device_memory__type =
{
	.name          = "memory",
	.create_cb     = fct_device_memory__create,
	.delete_cb     = fct_device_memory__delete,
	.params_get_cb = fct_device_memory__params_get,
	.tick_cb       = NULL,
	.interrupt_cb  = NULL,
	.read_cb       = fct_device_memory__read,
	.write_cb      = fct_device_memory__write,

	.bus_cb    = NULL,
	.clkbus_cb = NULL,

	.port_attach_cb      = NULL,
	.interrupt_attach_cb = NULL,
};



fct_device_t* fct_device_memory_create(
	fct_string_t* name, unsigned mau, unsigned width, uint64_t size)
{

	fct_device_t* device
		= fct_device_create(
			&fct_device_memory__type, name);
	if (!device) return NULL;

	device->subclass
		= (fct_device_subclass_t*)malloc(
			sizeof(fct_device_subclass_t));
	if (device->subclass)
	{
		device->subclass->memory
			= fct_maubuff_default_create(mau);
		device->subclass->mau   = mau;
		device->subclass->width = width;
		device->subclass->size  = size;
		device->subclass->read  = true;
		device->subclass->write = true;
		device->subclass->dirty = false;
		device->subclass->path  = NULL;
	}

	if (!device->subclass
		|| !device->subclass->memory
		|| !fct_maubuff_append(device->subclass->memory, NULL, (width * size)))
	{
		fct_device_delete(device);
		return NULL;
	}

	return device;
}
