#ifndef __FCT_ARCH_H__
#define __FCT_ARCH_H__

typedef struct fct_arch_s      fct_arch_t;
typedef struct fct_arch_spec_s fct_arch_spec_t;
typedef struct fct_arch_emu_s  fct_arch_emu_t;

#include <fct/types.h>
#include <fct/maubuff.h>
#include <fct/asm/context.h>
#include <fct/asm/field.h>
#include <fct/lex/tok.h>

fct_arch_t* fct_arch_by_name(fct_string_t* name);

bool fct_arch_is_ancestor(fct_arch_t* base, fct_arch_t* arch);

const char* fct_arch_reg_name_from_number(
	fct_arch_t* arch, unsigned number);
bool fct_arch_number_from_reg_name(
	fct_arch_t* arch, const char* name, unsigned* number);

bool fct_arch_assemble(
	fct_arch_t*        arch,
	fct_asm_context_t* context,
	const fct_tok_t*   mnem,
	const fct_tok_t*   postfix,
	fct_asm_field_t**  fields,
	unsigned           field_count);

unsigned fct_arch_disassemble(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned offset,
	fct_string_t* str);
fct_string_t* fct_arch_disassemble_buffer(
	fct_arch_t* arch,
	fct_maubuff_t* code, unsigned indent);

bool fct_arch_link(
	fct_arch_t* arch, void* code, uint64_t size, uint64_t symbol);

bool fct_arch_gen_call(
	fct_arch_t* arch, fct_asm_context_t* context,
	fct_asm_field_t* field);
bool fct_arch_gen_return(
	fct_arch_t* arch, fct_asm_context_t* context);
bool fct_arch_gen_push(
	fct_arch_t* arch, fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count);
bool fct_arch_gen_pop(
	fct_arch_t* arch, fct_asm_context_t* context,
	fct_asm_field_t** field, unsigned field_count);
bool fct_arch_gen_syscall(
	fct_arch_t* arch, fct_asm_context_t* context,
	fct_asm_field_t* field);

fct_string_t* fct_arch_name(fct_arch_t* arch);

fct_arch_emu_t* fct_arch_emu(fct_arch_t* arch);

const fct_arch_spec_t* fct_arch_spec(const fct_arch_t* arch);

unsigned fct_arch_mau(const fct_arch_t* arch);
unsigned fct_arch_addr_size(const fct_arch_t* arch);
unsigned fct_arch_word_size(const fct_arch_t* arch);
unsigned fct_arch_inst_size(const fct_arch_t* arch);
unsigned fct_arch_reg_count(const fct_arch_t* arch);

uint16_t fct_arch_elf_machine(const fct_arch_t* arch);
bool fct_arch_elf_abi(
	const fct_arch_t* arch,
	uint8_t* osabi,
	uint8_t* abiversion);

#endif
