#include <fct/debug.h>
#include <fct/debug/message.h>
#include <stdlib.h>
#include <stdio.h>



static fct_stack_t* fct_debug__stack = NULL;

void fct_debug_print(void)
{
	unsigned i;
	for (i = 0; i < fct_stack_depth(fct_debug__stack); i++)
	{
		fct_debug_message_t* message
			= fct_stack_peek_index(fct_debug__stack, i);
		fct_debug_message_print(message);
	}
}

static void fct_debug__exit(void)
{
	fct_debug_print();
	fct_stack_delete(fct_debug__stack);
	fct_debug__stack = NULL;
}

void fct_debug_logv(
	const char* file, unsigned line, unsigned offset,
	fct_debug_log_type_e type, const char* format, va_list args)
{
	if (type >= FCT_DEBUG_LOG_TYPE_COUNT)
	{
		FCT_DEBUG_CRITICAL("Invalid error type");
		return;
	}
	if (!format)
	{
		FCT_DEBUG_CRITICAL("NULL error message");
		return;
	}

	if (type == FCT_DEBUG_LOG_TYPE_CRITICAL)
	{
		fct_debug_print();

		static const char* labels[] =
		{
			"Info",
			"Warning",
			"Error",
			"Critical",
		};

		if (file)
		{
			fprintf(stderr, "%s:", file);
			if (line > 0)
			{
				fprintf(stderr, "%u", line);
				if (offset > 0)
					fprintf(stderr, ",%u", offset);
				fprintf(stderr, ":");
			}
			fprintf(stderr, " ");
		}

		fprintf(stderr, "%s: ", labels[type]);
		vfprintf(stderr, format, args);
		fprintf(stderr, "\n");

		fct_stack_delete(fct_debug__stack);
		fct_debug__stack = NULL;
		exit(EXIT_FAILURE);
	}

	if (!fct_debug__stack)
	{
		fct_debug__stack
			= (fct_stack_t*)fct_stack_create(
				fct_class_debug_message);
		if (!fct_debug__stack)
		{
			FCT_DEBUG_CRITICAL("Failed to allocate debug stack.");
			return;
		}
		atexit(fct_debug__exit);
	}

	fct_string_t* content
		= fct_string_create_formatv(format, args);
	if (!content)
	{
		FCT_DEBUG_CRITICAL("Failed to allocate debug string.");
		return;
	}

	fct_debug_message_t* message
		= fct_debug_message_create(
			file, line, offset, type, content);
	if (!message)
	{
		FCT_DEBUG_CRITICAL("Failed to allocate debug message.");
		fct_string_delete(content);
		return;
	}

	if (!fct_stack_push(fct_debug__stack, message))
	{
		FCT_DEBUG_CRITICAL("Failed to push debug message onto stack.");
		return;
	}
}

void fct_debug_log(
	const char* file, unsigned line, unsigned offset,
	fct_debug_log_type_e type, const char* format, ...)
{
	va_list args;
	va_start(args, format);
	fct_debug_logv(file, line, offset, type, format, args);
	va_end(args);
}

unsigned fct_debug_state(void)
{
	if (!fct_debug__stack)
		return 0;
	return fct_stack_pointer(fct_debug__stack);
}

void fct_debug_revert(unsigned state)
{
	if (!fct_debug__stack)
	{
		if (state == 0)
			return;
		FCT_DEBUG_CRITICAL(
			"Failed to revert debug stack as it is unallocated.");
		return;
	}

	if (!fct_stack_revert(fct_debug__stack, state))
	{
		FCT_DEBUG_CRITICAL("Failed to revert debug stack to %u.", state);
		return;
	}
}
