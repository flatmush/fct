#include "message.h"
#include <stdlib.h>



static const fct_class_t
	fct_class__debug_message =
	{
		.delete_cb    = (void*)fct_debug_message_delete,
		.reference_cb = NULL,
		.copy_cb      = NULL,
		.import_cb    = NULL,
		.export_cb    = NULL,
	};

const fct_class_t* fct_class_debug_message
	= &fct_class__debug_message;



fct_debug_message_t* fct_debug_message_create(
	const char* file, unsigned line, unsigned offset,
	fct_debug_log_type_e type, fct_string_t* content)
{
	if (!content
		|| fct_string_is_empty(content)
		|| (type >= FCT_DEBUG_LOG_TYPE_COUNT))
		return NULL;

	fct_debug_message_t* message
		= (fct_debug_message_t*)malloc(
			sizeof(fct_debug_message_t));
	if (!message) return NULL;

	message->file    = (file ? strdup(file) : NULL);
	message->line    = line;
	message->offset  = offset;
	message->type    = type;
	message->content = content;

	if (file && !message->file)
	{
		fct_debug_message_delete(message);
		return NULL;
	}

	return message;
}

void fct_debug_message_delete(fct_debug_message_t* message)
{
	if (!message)
		return;
	free(message->file);
	fct_string_delete(message->content);
	free(message);
}



void fct_debug_message_print(fct_debug_message_t* message)
{
	if (!message)
		return;

	static const char* labels[] =
	{
		"Info",
		"Warning",
		"Error",
		"Critical",
	};

	if (message->file)
	{
		fprintf(stderr, "%s:", message->file);
		if (message->line > 0)
		{
			fprintf(stderr, "%u", message->line);
			if (message->offset > 0)
				fprintf(stderr, ",%u", message->offset);
			fprintf(stderr, ":");
		}
		fprintf(stderr, " ");
	}

	fprintf(stderr, "%s: %.*s\n",
		labels[message->type],
		message->content->size,
		message->content->data);
}
