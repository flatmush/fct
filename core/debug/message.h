#ifndef __FCT_DEBUG_MESSAGE_H__
#define __FCT_DEBUG_MESSAGE_H__

typedef struct fct_debug_message_s fct_debug_message_t;

#include <fct/types.h>
#include <fct/debug.h>

struct fct_debug_message_s
{
	char*    file;
	unsigned line, offset;

	fct_debug_log_type_e type;
	fct_string_t*        content;
};

const fct_class_t* fct_class_debug_message;

fct_debug_message_t* fct_debug_message_create(
	const char* file, unsigned line, unsigned offset,
	fct_debug_log_type_e type, fct_string_t* content);
void fct_debug_message_delete(fct_debug_message_t* message);

void fct_debug_message_print(fct_debug_message_t* message);

#endif
