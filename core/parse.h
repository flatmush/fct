#ifndef __FCT_PARSE_H__
#define __FCT_PARSE_H__

#include <fct/types.h>

unsigned fct_parse_number_char(
	const char* src, uint32_t* number, bool quote);
unsigned fct_parse_number_base(
	const char* src, unsigned base, uint64_t* number);
unsigned fct_parse_number(
	const char* src, uint64_t* number);
unsigned fct_parse_immediate(
	const char* src, uint64_t* number,
	unsigned* size, bool* sign);
unsigned fct_parse_number_human(
	const char* src, uint64_t* value);

unsigned fct_parse_string(
	const char* src, char** string);
unsigned fct_parse_string_constant(
	const char* src, unsigned size,
	uint64_t** string, bool static_length, unsigned* length, bool* zero);


#include <fct/lex.h>

unsigned fct_parse_array_initializer(
	const fct_tok_t* src, unsigned size, bool allow_sign,
	uint64_t** array, bool static_count, unsigned* count, bool* zero);

unsigned fct_parse_vector(
	const fct_tok_t* src, uint64_t* vec,
	unsigned* size, bool* sign, unsigned* count);

#endif
