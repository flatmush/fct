#ifndef __FCT_MAUBUFF_H__
#define __FCT_MAUBUFF_H__

typedef struct fct_maubuff_s fct_maubuff_t;
typedef struct fct_maubuff_interface_s fct_maubuff_interface_t;

#include <fct/types.h>

const fct_class_t* fct_class_maubuff;


fct_maubuff_t* fct_maubuff_reference(fct_maubuff_t* buffer);
fct_maubuff_t* fct_maubuff_copy(fct_maubuff_t* buffer);
void           fct_maubuff_delete(fct_maubuff_t* buffer);

fct_maubuff_t* fct_maubuff_from_buffer(
	fct_buffer_t* buffer, uintptr_t offset, unsigned mau, uint32_t size);
fct_buffer_t*  fct_maubuff_to_buffer(fct_maubuff_t* buffer);

fct_string_t*  fct_maubuff_to_string_hex(
	fct_maubuff_t* buffer, unsigned indent, unsigned width);

fct_maubuff_t* fct_maubuff_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_maubuff_export(
	fct_maubuff_t* buffer, fct_list_t* strtab);

unsigned fct_maubuff_mau_get(fct_maubuff_t* buffer);
uint32_t fct_maubuff_size_get(fct_maubuff_t* buffer);
uint8_t* fct_maubuff_byte_pointer_get(fct_maubuff_t* buffer, uint32_t offset);
uint32_t fct_maubuff_byte_size_get(fct_maubuff_t* buffer);

bool fct_maubuff_append(fct_maubuff_t* buffer, const void* data, uint32_t size);
void fct_maubuff_truncate(fct_maubuff_t* buffer, uint32_t size);
bool fct_maubuff_append_maubuff(fct_maubuff_t* buffer, fct_maubuff_t* append);
bool fct_maubuff_modify(fct_maubuff_t* buffer, uint32_t offset, void* data, uint32_t size);
bool fct_maubuff_read(fct_maubuff_t* buffer, uint32_t offset, uint32_t size, void* data);

#include <fct/maubuff/byte.h>
#include <fct/maubuff/default.h>
#include <fct/maubuff/zero.h>

#endif
