#include <fct/lang/decl.h>
#include <stdlib.h>


static const fct_class_t
	fct_class__decl =
	{
		.delete_cb    = (void*)fct_decl_delete,
		.reference_cb = (void*)fct_decl_reference,
		.copy_cb      = (void*)fct_decl_copy,
		.import_cb    = NULL,
		.export_cb    = NULL,
	};
const fct_class_t* fct_class_decl
	= &fct_class__decl;



fct_decl_t* fct_decl_parse(
	const fct_lex_line_t* line)
{
	const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return NULL;

	fct_decl_t* decl
		= (fct_decl_t*)malloc(
			sizeof(fct_decl_t));
	if (!decl) return NULL;

	decl->type = NULL;
	decl->name = NULL;
	decl->init = NULL;
	decl->refcnt = 0;

	unsigned i = fct_type_parse(
		src, NULL, &decl->type);
	if (i == 0) return 0;

	if (src[i].type != FCT_TOK_IDENT)
	{
		fct_decl_delete(decl);
		return NULL;
	}

	decl->name = fct_tok_cstring(&src[i++]);
	if (!decl->name)
	{
		fct_decl_delete(decl);
		return NULL;
	}

	if (src[i].type == FCT_TOK_ASSIGN)
	{
		/* TODO - Handle initializers. */
		fct_decl_delete(decl);
		return NULL;
	}
	else if (src[i].type != FCT_TOK_END)
	{
		/* TODO - Handle struct initializers. */
		/* TODO - Handle functions. */

		fct_decl_delete(decl);
		return NULL;
	}

	return decl;
}

fct_decl_t* fct_decl_copy(fct_decl_t* decl)
{
	if (!decl)
		return NULL;

	/* TODO - Implement. */
	return NULL;
}

fct_decl_t* fct_decl_reference(fct_decl_t* decl)
{
	if (!decl)
		return NULL;

	if ((decl->refcnt + 1) == 0)
		return NULL;

	decl->refcnt++;
	return decl;
}

void fct_decl_delete(fct_decl_t* decl)
{
	if (!decl)
		return;

	if (decl->refcnt > 0)
	{
		decl->refcnt--;
		return;
	}

	fct_type_delete(decl->type);
	free(decl->init);
	free(decl->name);
	free(decl);
}


bool fct_decl_size_align(
	fct_decl_t* decl, fct_arch_t* arch,
	unsigned* size, unsigned* align)
{
	if (!decl) return false;
	return fct_type_size_align(
		decl->type, arch,
		size, align);
}
