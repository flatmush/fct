#include <fct/lang/type.h>
#include <stdlib.h>


static const fct_class_t
	fct_class__type =
	{
		.delete_cb    = (void*)fct_type_delete,
		.reference_cb = (void*)fct_type_reference,
		.copy_cb      = (void*)fct_type_copy,
		.import_cb    = NULL,
		.export_cb    = NULL,
	};
const fct_class_t* fct_class_type
	= &fct_class__type;



static unsigned fct_type_def_parse_attr_align(
	const fct_tok_t* src,
	unsigned* align,
	bool* align_to_size)
{
	if (!src || !align
		|| (*align != 0)
		|| !fct_tok_keyword(src, "align"))
		return 0;

	if (src[1].type != FCT_TOK_LPAREN)
	{
		*align_to_size = true;
		return 1;
	}

	unsigned ualign;
	if (*align_to_size
		|| !fct_tok_unsigned(&src[2], &ualign)
		|| (src[3].type != FCT_TOK_RPAREN)
		|| (ualign == 0))
		return 0;

	*align = ualign;
	return 4;
}

static unsigned fct_type_def_parse_attr_size(
	const fct_tok_t* src, unsigned* size)
{
	unsigned usize;
	if (!src || !size
		|| (*size != 0)
		|| !fct_tok_keyword(src, "size")
		|| (src[1].type != FCT_TOK_LPAREN)
		|| !fct_tok_unsigned(&src[2], &usize)
		|| (src[3].type != FCT_TOK_RPAREN)
		|| (usize == 0))
		return 0;

	*size = usize;
	return 4;
}

static unsigned fct_type_def_parse_attr_overflow(
	const fct_tok_t* src,
	fct_type_overflow_e* overflow)
{
	if (!src || !overflow
		|| (*overflow != FCT_TYPE_OVERFLOW_NONE))
		return 0;

	if (fct_tok_keyword(src, "modulo"))
		*overflow = FCT_TYPE_OVERFLOW_MODULO;
	else if (fct_tok_keyword(src, "saturate"))
		*overflow = FCT_TYPE_OVERFLOW_SATURATED;
	else
		return 0;

	return 1;
}

static fct_type_t* fct_type__create(
	fct_type_e type, fct_string_t* name)
{
	fct_type_t* atype
		= (fct_type_t*)malloc(
			sizeof(fct_type_t));
	if (!atype) return NULL;

	atype->type = type;
	atype->name = name;

	atype->align       = 0;
	atype->is_const    = false;
	atype->is_volatile = false;

	switch (type)
	{
		case FCT_TYPE_BOOLEAN:
			atype->boolean.size = 0;
			break;
		case FCT_TYPE_CHARACTER:
			atype->character.size = 0;
			break;
		case FCT_TYPE_FIXED:
		case FCT_TYPE_FLOAT:
			atype->fixed_float.is_signed = false;
			atype->fixed_float.overflow  = FCT_TYPE_OVERFLOW_NONE;
			atype->fixed_float.size      = 0;
			atype->fixed_float.fract     = 0;
			break;
		case FCT_TYPE_ENUM:
			atype->enumeration.type       = NULL;
			atype->enumeration.count      = 0;
			atype->enumeration.sequential = false;
			atype->enumeration.value = NULL;
			break;
		case FCT_TYPE_RANGE:
		case FCT_TYPE_COMPLEX:
		case FCT_TYPE_POINTER:
		case FCT_TYPE_ALIAS:
			atype->alias.type = NULL;
			break;
		case FCT_TYPE_VECTOR:
			atype->vector.type  = NULL;
			atype->vector.count = 0;
			break;
		case FCT_TYPE_MATRIX:
			atype->matrix.type   = NULL;
			atype->matrix.width  = 0;
			atype->matrix.height = 0;
			break;
		case FCT_TYPE_ARRAY:
			atype->array.type  = NULL;
			atype->array.base  = 0;
			atype->array.count = 0;
			break;
		case FCT_TYPE_STRUCT:
		case FCT_TYPE_BITFIELD:
			atype->struct_union_class.count  = 0;
			atype->struct_union_class.offset = NULL;
			atype->struct_union_class.member = NULL;
			break;
		case FCT_TYPE_FUNCTION:
			atype->function.return_type    = NULL;
			atype->function.argument_count = 0;
			atype->function.argument       = NULL;
			atype->function.code           = NULL;
			break;
		default:
			free(atype);
			return NULL;
	}

	atype->refcnt = 0;
	return atype;
}

fct_type_t* fct_type_def_parse(
	const fct_lex_line_t* line,
	unsigned offset)
{
	const fct_tok_t* src
		= fct_lex_line_tokens(line);
	if (!src) return NULL;

	unsigned i;
	for (i = 0; i < offset; i++)
	{
		if (src[i].type == FCT_TOK_END)
			return NULL;
	}

	if ((src[i++].type != FCT_TOK_DIRECTIVE)
		|| !fct_tok_keyword(&src[i++], "type"))
		return NULL;

	fct_tok_t ntok = src[i++];
	if (ntok.type != FCT_TOK_IDENT)
		return NULL;

	fct_type_e type_type;
	if (fct_tok_keyword(&src[i], "boolean"))
		type_type = FCT_TYPE_BOOLEAN;
	else if (fct_tok_keyword(&src[i], "character"))
		type_type = FCT_TYPE_CHARACTER;
	else if (fct_tok_keyword(&src[i], "fixed"))
		type_type = FCT_TYPE_FIXED;
	else if (fct_tok_keyword(&src[i], "float"))
		type_type = FCT_TYPE_FLOAT;
	else if (fct_tok_keyword(&src[i], "range"))
		type_type = FCT_TYPE_RANGE;
	else if (fct_tok_keyword(&src[i], "complex"))
		type_type = FCT_TYPE_COMPLEX;
	else if (fct_tok_keyword(&src[i], "enum"))
		type_type = FCT_TYPE_ENUM;
	else if (fct_tok_keyword(&src[i], "alias"))
		type_type = FCT_TYPE_ALIAS;
	else if (fct_tok_keyword(&src[i], "vector"))
		type_type = FCT_TYPE_VECTOR;
	else if (fct_tok_keyword(&src[i], "matrix"))
		type_type = FCT_TYPE_MATRIX;
	else if (fct_tok_keyword(&src[i], "struct"))
		type_type = FCT_TYPE_STRUCT;
	else if (fct_tok_keyword(&src[i], "union"))
		type_type = FCT_TYPE_STRUCT;
	else if (fct_tok_keyword(&src[i], "class"))
		type_type = FCT_TYPE_STRUCT;
	else
		return NULL;
	i += 1;

	fct_string_t* name
		= fct_tok_to_string(&ntok);
	if (!name) return NULL;

	fct_type_t* type = fct_type__create(
		type_type, name);
	if (!type)
	{
		fct_string_delete(name);
		return NULL;
	}

	switch (type->type)
	{
		case FCT_TYPE_ENUM:
			i += fct_type_parse(&src[i], NULL,
				&type->enumeration.type);
			break;
		case FCT_TYPE_RANGE:
		case FCT_TYPE_COMPLEX:
		case FCT_TYPE_ALIAS:
			i += fct_type_parse(&src[i], NULL,
				&type->alias.type);
			if (type->alias.type == NULL)
			{
				fct_type_delete(type);
				return NULL;
			}
			break;
		case FCT_TYPE_VECTOR:
			fct_type_delete(type);
			return NULL;
		case FCT_TYPE_MATRIX:
			fct_type_delete(type);
			return NULL;
		default:
			break;
	}

	bool align_to_size = false;
	while (src[i].type != FCT_TOK_END)
	{
		unsigned j;
		j = fct_type_def_parse_attr_align(
			&src[i], &type->align, &align_to_size);
		i += j; if (j != 0) continue;

		unsigned* size = NULL;
		fct_type_overflow_e* overflow = NULL;
		switch (type->type)
		{
			case FCT_TYPE_BOOLEAN:
				size = &type->boolean.size;
				break;
			case FCT_TYPE_CHARACTER:
				size = &type->character.size;
				break;
			case FCT_TYPE_FIXED:
			case FCT_TYPE_FLOAT:
				size     = &type->fixed_float.size;
				overflow = &type->fixed_float.overflow;
				break;
			default:
				break;
		}
		j = fct_type_def_parse_attr_size(&src[i], size);
		i += j; if (j != 0) continue;

		j = fct_type_def_parse_attr_overflow(&src[i], overflow);
		i += j; if (j != 0) continue;

		/* TODO - Parse other attributes. */

		fct_type_delete(type);
		return NULL;
	}

	/* TODO - Parse nested part of typedef. */

	return type;
}

unsigned fct_type_parse(
	const fct_tok_t* tok,
	fct_list_t* map,
	fct_type_t** type)
{
	if (!tok)
		return 0;

	/* TODO - Pointers. */

	if (tok->type != FCT_TOK_IDENT)
		return 0;

	unsigned i = 0;
	fct_string_t* name = FCT_TOK_TO_STRING_STATIC(&tok[i]);
	fct_type_t* t = fct_hashtab_find_by_key(map, name);
	if (!t) return 0;
	i += 1;

	/* TODO - Arrays. */

	if (type)
	{
		t = fct_type_reference(t);
		if (!t) return 0;
		*type = t;
	}
	return i;
}

fct_type_t* fct_type_copy(
	fct_type_t* type)
{
	if (!type)
		return NULL;

	fct_type_t* copy
		= (fct_type_t*)malloc(
			sizeof(fct_type_t));
	if (!copy) return NULL;

	*copy = *type;

	copy->refcnt = 0;

	if (type->name)
	{
		copy->name = fct_string_copy(type->name);
		if (!copy->name)
		{
			free(copy);
			return NULL;
		}
	}

	bool fail = false;
	switch (type->type)
	{
		case FCT_TYPE_RANGE:
		case FCT_TYPE_COMPLEX:
		case FCT_TYPE_POINTER:
		case FCT_TYPE_ALIAS:
			if (type->alias.type
				&& !fct_type_reference(
					type->alias.type))
			{
				copy->alias.type = NULL;
				fail = true;
			}
			break;

		case FCT_TYPE_ENUM:
		case FCT_TYPE_VECTOR:
		case FCT_TYPE_MATRIX:
		case FCT_TYPE_ARRAY:
		case FCT_TYPE_STRUCT:
		case FCT_TYPE_FUNCTION:
			/* TODO - Implement. */
			fail = true;
			break;

		default:
			break;
	}

	if (fail)
	{
		fct_type_delete(copy);
		return NULL;
	}

	return copy;
}

fct_type_t* fct_type_reference(
	fct_type_t* type)
{
	if (!type)
		return NULL;

	if ((type->refcnt + 1) == 0)
		return NULL;

	type->refcnt++;
	return type;
}

void fct_type_delete(fct_type_t* type)
{
	if (!type)
		return;

	if (type->refcnt > 0)
	{
		type->refcnt--;
		return;
	}

	switch (type->type)
	{
		case FCT_TYPE_ENUM:
			fct_type_delete(type->enumeration.type);
			free(type->enumeration.value);
			break;

		case FCT_TYPE_RANGE:
		case FCT_TYPE_COMPLEX:
		case FCT_TYPE_POINTER:
		case FCT_TYPE_ALIAS:
			fct_type_delete(type->alias.type);
			break;

		case FCT_TYPE_VECTOR:
			fct_type_delete(type->vector.type);
			break;

		case FCT_TYPE_MATRIX:
			fct_type_delete(type->matrix.type);
			break;

		case FCT_TYPE_ARRAY:
			fct_type_delete(type->array.type);
			break;

		case FCT_TYPE_STRUCT:
			{
				unsigned i;
				for (i = 0; i < type->struct_union_class.count; i++)
					fct_decl_delete(type->struct_union_class.member[i]);
				free(type->struct_union_class.member);
				free(type->struct_union_class.offset);
			}
			break;

		case FCT_TYPE_FUNCTION:
			{
				unsigned i;
				for (i = 0; i < type->function.argument_count; i++)
					fct_decl_delete(type->function.argument[i]);
				free(type->function.argument);
				free(type->function.return_type);

				/* TODO - Clear up actual code class. */
				free(type->function.code);
			}
			break;

		default:
			break;
	}

	free(type->name);
	free(type);
}



fct_string_t* fct_type_name(
	const fct_type_t* type)
{
	if (!type) return NULL;
	return type->name;
}

const fct_type_t* fct_type_base(
	const fct_type_t* type)
{
	if (!type)
		return NULL;

	switch (type->type)
	{
		case FCT_TYPE_ENUM:
			return type->enumeration.type;
		case FCT_TYPE_RANGE:
		case FCT_TYPE_COMPLEX:
		case FCT_TYPE_POINTER:
		case FCT_TYPE_ALIAS:
			return type->alias.type;
		case FCT_TYPE_VECTOR:
			return type->vector.type;
		case FCT_TYPE_MATRIX:
			return type->matrix.type;
		case FCT_TYPE_ARRAY:
			return type->array.type;
		case FCT_TYPE_FUNCTION:
			return type->function.return_type;
		default:
			break;
	}

	return NULL;
}

bool fct_type_size_align(
	const fct_type_t* type, fct_arch_t* arch,
	unsigned* size, unsigned* align)
{
	if (!type || !arch)
		return false;

	unsigned type_size = 0, type_align = 0;
	bool     is_mau = true;
	switch (type->type)
	{
		case FCT_TYPE_BOOLEAN:
			type_size = type->boolean.size;
			is_mau = (type_size == 0);
			if (is_mau)
				type_size = fct_arch_word_size(arch);
			break;

		case FCT_TYPE_CHARACTER:
			type_size = type->character.size;
			is_mau = (type_size == 0);
			if (is_mau)
				type_size = fct_arch_word_size(arch);
			break;

		case FCT_TYPE_FIXED:
		case FCT_TYPE_FLOAT:
			type_size = (type->fixed_float.size
				+ type->fixed_float.fract);
			is_mau = (type_size == 0);
			if (is_mau)
				type_size = fct_arch_word_size(arch);
			break;

		case FCT_TYPE_RANGE:
		case FCT_TYPE_COMPLEX:
			if (!fct_type_size_align(
				type->enumeration.type,
				arch, &type_size, &type_align))
				return false;
			type_size *= 2;
			break;

		case FCT_TYPE_ENUM:
			if (!fct_type_size_align(
				type->enumeration.type, arch,
				&type_size, &type_align))
				return false;
			break;

		case FCT_TYPE_POINTER:
			type_size = fct_arch_addr_size(arch);
			break;

		case FCT_TYPE_ALIAS:
			if (!fct_type_size_align(
				type->enumeration.type,
				arch, &type_size, &type_align))
				return false;
			break;

		case FCT_TYPE_VECTOR:
			if (type->vector.count == 0)
				return false;
			if (!fct_type_size_align(
				type->vector.type, arch,
				&type_size, &type_align))
				return false;
			type_size *= type->vector.count;
			break;

		case FCT_TYPE_MATRIX:
			if ((type->matrix.width == 0)
				|| (type->matrix.height == 0))
				return false;
			if (!fct_type_size_align(
				type->matrix.type, arch,
				&type_size, &type_align))
				return false;
			type_size *= type->matrix.width;
			type_size *= type->matrix.height;
			break;

		case FCT_TYPE_ARRAY:
			if (!fct_type_size_align(
				type->array.type, arch,
				&type_size, &type_align))
				return false;
			type_size *= type->array.count;
			break;

		case FCT_TYPE_STRUCT:
		case FCT_TYPE_BITFIELD:
			{
				is_mau = (type->type != FCT_TYPE_BITFIELD);
				unsigned i;
				for (i = 0; i < type->struct_union_class.count; i++)
				{
					unsigned elem_size, elem_align;
					fct_decl_t* member
						= type->struct_union_class.member[i];
					if (!fct_decl_size_align(member, arch,
						&elem_size, &elem_align))
						return false;
					elem_size += type->struct_union_class.offset[i];
					if (elem_size > type_size)
						type_size = elem_size;
					if (is_mau && (elem_align > type_align))
						type_align = elem_align;
				}
			}
			break;

		case FCT_TYPE_FUNCTION:
			/* TODO - Return relevant value. */
			return false;

		default:
			return false;
	}

	if (type->align > type_align)
		type_align = type->align;

	if (!is_mau)
	{
		unsigned mau = fct_arch_mau(arch);
		if (mau == 0) return false;
		if (type_size % mau)
			type_size += mau;
		type_size /= mau;
	}

	if (align) *align = type_align;
	if (size ) *size  = type_size;
	return true;
}
