#ifndef __FCT_LANG_DECL_H__
#define __FCT_LANG_DECL_H__

#include <fct/lang.h>

struct fct_decl_s
{
	fct_type_t* type;
	char*       name;
	void*       init;

	unsigned refcnt;
};

#endif
