#ifndef __FCT_LANG_TYPE_H__
#define __FCT_LANG_TYPE_H__

typedef enum
{
	FCT_TYPE_BOOLEAN = 0,
	FCT_TYPE_CHARACTER,
	FCT_TYPE_FIXED,
	FCT_TYPE_FLOAT,
	FCT_TYPE_RANGE,
	FCT_TYPE_COMPLEX,
	FCT_TYPE_ENUM,
	FCT_TYPE_POINTER,
	FCT_TYPE_ALIAS,
	FCT_TYPE_VECTOR,
	FCT_TYPE_MATRIX,
	FCT_TYPE_ARRAY,
	FCT_TYPE_STRUCT,
	FCT_TYPE_BITFIELD,
	FCT_TYPE_FUNCTION,

	FCT_TYPE_COUNT
} fct_type_e;

typedef enum
{
	FCT_TYPE_OVERFLOW_NONE = 0,
	FCT_TYPE_OVERFLOW_MODULO,
	FCT_TYPE_OVERFLOW_SATURATED,
} fct_type_overflow_e;

#include <fct/lang.h>

struct fct_type_s
{
	fct_string_t* name;
	fct_type_e    type;

	unsigned align;
	bool     is_const;
	bool     is_volatile;

	union
	{
		struct
		{
			unsigned size;
		} boolean;

		struct
		{
			unsigned size;
		} character;

		struct
		{
			bool                is_signed;
			fct_type_overflow_e overflow;
			unsigned            size;
			unsigned            fract;
		} fixed_float;

		struct
		{
			fct_type_t* type;
			unsigned    count;
			bool        sequential;
			void*       value;
		} enumeration;

		struct
		{
			fct_type_t* type;
		} alias;

		struct
		{
			fct_type_t* type;
			unsigned    count;
		} vector;

		struct
		{
			fct_type_t* type;
			unsigned    width, height;
		} matrix;

		struct
		{
			fct_type_t* type;
			unsigned    base, count;
		} array;

		struct
		{
			unsigned     count;
			unsigned*    offset;
			fct_decl_t** member;
		} struct_union_class;

		struct
		{
			fct_type_t*  return_type;
			unsigned     argument_count;
			fct_decl_t** argument;
			void*        code;
		} function;
	};

	unsigned refcnt;
};

#endif
