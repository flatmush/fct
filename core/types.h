#ifndef __FCT_TYPES_H__
#define __FCT_TYPES_H__

#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <limits.h>
#include <float.h>

typedef struct fct_string_s fct_string_t;

#include <fct/types/class.h>
#include <fct/types/buffer.h>
#include <fct/types/list.h>
#include <fct/types/string.h>
#include <fct/types/stack.h>
#include <fct/types/keyval.h>
#include <fct/types/strtab.h>

#endif
