#include <fct/parse.h>
#include <fct/types.h>
#include <stdlib.h>



unsigned fct_parse_string(
	const char* src, char** string)
{
	if (!src)
		return 0;

	unsigned i = 0;

	if (src[i++] != '\"')
		return 0;

	char*    s = NULL;
	unsigned size = 16;
	unsigned len  = 0;

	if (string)
	{
		s = malloc(size);
		if (!s) return 0;
	}

	while ((src[i] != '\"')
		&& (src[i] != '\0'))
	{
		uint32_t n;
		i += fct_parse_number_char(&src[i], &n, false);

		if (string)
		{
			if ((len + 2) > size)
			{
				size <<= 1;
				char* sn = (char*)realloc(s, size);
				if (!sn)
				{
					free(s);
					return 0;
				}
				s = sn;
			}

			s[len++] = n;
		}
	}

    if (src[i++] != '\"')
	{
		free(s);
		return 0;
	}

	if (string)
	{
		char* sn = realloc(s, (len + 1));
		if (sn) s = sn;
		s[len] = '\0';
		*string = s;
	}
	else
	{
		free(s);
	}

	return i;
}

unsigned fct_parse_string_constant(
	const char* src, unsigned size,
	uint64_t** string, bool static_length, unsigned* length, bool* zero)
{
	if (!src)
		return 0;

	if (static_length && !length)
		return 0;

	unsigned i = 0, j;
	if (src[i++] != '\"')
		return 0;

	uint64_t* chars       = NULL;
	unsigned  char_length = 0;
	bool      chars_zero  = true;

	if (static_length)
	{
		if (string)
		{
			chars = malloc(
				sizeof(uint64_t) * *length);
			if (!chars) return 0;
			char_length = *length;
		}

		unsigned k;
		for (k = 0; k < *length; k++)
		{
			if (src[i] == '\"')
				break;

			uint32_t n;
			j = fct_parse_number_char(
				&src[i], &n, false);
			if (!j) break;
			i += j;

			if ((size < (sizeof(uint64_t) * 8))
				&& ((n >> size) > 0))
			{
				free(chars);
				return 0;
			}

			if (n != 0)
				chars_zero = false;
			if (chars)
				chars[k] = n;
		}
		if (chars)
		{
			while (k < *length)
				chars[k++] = 0;
		}
	}
	else
	{
		while ((src[i] != '\"') && (src[i] != '\0'))
		{
			uint32_t n;
			j = fct_parse_number_char(
				&src[i], &n, false);
			if (!j) break;
			i += j;

			if ((size < (sizeof(uint64_t) * 8))
				&& ((n >> size) > 0))
			{
				free(chars);
				return 0;
			}

			if (n != 0)
				chars_zero = false;

			if (string)
			{
				uint64_t* nchars
					= (uint64_t*)realloc(chars,
						((char_length + 1) * sizeof(uint64_t)));
				if (!nchars)
				{
					free(chars);
					return 0;
				}
				chars = nchars;
				chars[char_length++] = n;
			}
		}
	}

	if (src[i++] != '\"')
	{
		free(chars);
		return 0;
	}

	if (string)
	{
		uint64_t* nchars
			= (uint64_t*)realloc(chars,
				((char_length + 1) * sizeof(*nchars)));
		if (!nchars)
		{
			free(chars);
			return 0;
		}
		chars = nchars;
		chars[char_length++] = '\0';
	}

	if (string)
		*string = chars;
	else
		free(chars);

	if (length && !static_length)
		*length = char_length;

	if (zero)
		*zero = chars_zero;

	return i;
}
