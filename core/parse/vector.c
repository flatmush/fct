#include <fct/parse.h>
#include <fct/types.h>



unsigned fct_parse_vector(
	const fct_tok_t* src, uint64_t* vec,
	unsigned* size, bool* sign, unsigned* count)
{
	if (fct_tok_keyword(src, "vec"))
		return 0;

	if (src[1].type != FCT_TOK_LPAREN)
		return 0;

	unsigned i     = 2;
	unsigned vsize = 0;
	bool     vsign = false;
	unsigned vcount;
	for (vcount = 0; src[i].type != FCT_TOK_RPAREN; vcount++)
	{
		if (vcount > 0)
		{
			if (src[i++].type != FCT_TOK_COMMA)
				return 0;
		}

		if (src[i].type != FCT_TOK_INTEGER)
			return 0;

		uint64_t value;
		unsigned bits;
		bool is_signed;
		unsigned len = fct_parse_immediate(
			src[i].base, &value, &bits, &is_signed);
		if (len != src[i].size) return 0;
		i += 1;

		if (vsign != is_signed)
		{
			if (vsign)
				bits++;
			else
				vsize++;
			vsign = true;
		}

		if (bits > vsize)
			vsize = bits;

		if (vec) vec[vcount] = value;
	}

	if (vsize > 64)
		return 0;

	if (size ) *size  = vsize;
	if (sign ) *sign  = vsign;
	if (count) *count = vcount;
	return (i + 1);
}
