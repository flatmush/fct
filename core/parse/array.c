#include <fct/parse.h>
#include <fct/types.h>
#include <stdlib.h>



unsigned fct_parse_array_initializer(
	const fct_tok_t* src, unsigned size, bool allow_sign,
	uint64_t** array, bool static_count, unsigned* count, bool* zero)
{
	if (!src)
		return 0;

	if (src[0].type == FCT_TOK_STRING)
	{
		unsigned len = fct_parse_string_constant(
			src[0].base, size, array, static_count, count, zero);
		return (len == src[0].size ? 1 : 0);
	}

	if (static_count && !count)
		return 0;

	uint64_t* values      = NULL;
	unsigned  value_count = 0;
	bool      values_zero = true;

	unsigned i = 0;
	if (static_count)
	{
		if (src[i].type != FCT_TOK_INTEGER)
			return 0;

		if (array)
		{
			values = malloc(
				sizeof(uint64_t) * *count);
			if (!values) return 0;
		}

		bool is_zero = false;
		if (src[i + 1].type == FCT_TOK_END)
		{
			uint64_t number;
			unsigned len = fct_parse_number(
				src[i].base, &number);
			is_zero = ((len == src[i].size)
				&& (number == 0));
		}

		if (is_zero)
		{
			if (values)
				memset(values, 0x00, *count);
			i += 1;
		}
		else
		{
			unsigned k;
			for (k = 0; k < *count; k++)
			{
				if (k > 0)
				{
					if (src[i++].type != FCT_TOK_COMMA)
					{
						free(values);
						return 0;
					}
				}

				if (src[i].type != FCT_TOK_INTEGER)
				{
					free(values);
					return 0;
				}

				uint64_t n;
				unsigned bits;
				bool is_signed;
				unsigned len = fct_parse_immediate(
					src[i].base, &n, &bits, &is_signed);
				if ((len != src[i].size) || (bits > size)
					|| (is_signed && !allow_sign))
				{
					free(values);
					return 0;
				}
				i += 1;

				if (n != 0)
					values_zero = false;
				if (values)
					values[k] = n;
			}
		}
	}
	else
	{
		while (src[i].type != FCT_TOK_END)
		{
			if (value_count > 0)
			{
				if (src[i++].type != FCT_TOK_COMMA)
				{
					free(values);
					return 0;
				}
			}

			if (src[i].type != FCT_TOK_INTEGER)
			{
				free(values);
				return 0;
			}

			uint64_t n;
			unsigned bits;
			bool is_signed;
			unsigned len = fct_parse_immediate(
				src[i].base, &n, &bits, &is_signed);
			if ((len != src[i].size) || (bits > size)
				|| (is_signed && !allow_sign))
			{
				free(values);
				return 0;
			}
			i += 1;

			if (array)
			{
				uint64_t* nvalues
					= (uint64_t*)realloc(values,
						((value_count + 1) * sizeof(n)));
				if (!nvalues)
				{
					free(values);
					return 0;
				}
				values = nvalues;
				values[value_count++] = n;

				if (n != 0)
					values_zero = false;
			}
		}
	}

	if (array)
		*array = values;
	else
		free(values);

	if (count && !static_count)
		*count = value_count;

	if (zero)
		*zero = values_zero;

	return i;
}
