#include <fct/types.h>



static inline unsigned _xdigit(char c)
{
	if (isdigit(c)) return (c - '0');
	return (10 + (toupper(c) - 'A'));
}



unsigned fct_parse_number_char(
	const char* src, uint32_t* number, bool quote)
{
	if (!src)
		return 0;

	unsigned i = 0;
	unsigned v;
	if (quote && (src[i++] != '\''))
		return 0;

	if (src[i] == '\\')
	{
		switch (src[++i])
		{
			case 'n':
				v = '\n';
				break;
			case 'r':
				v = '\r';
				break;
			case '\\':
				v = '\\';
				break;
			case 't':
				v = '\t';
				break;
			case '0':
				v = '\0';
			break;
			default:
				v = src[i];
				break;
		}
	} else {
		v = src[i];
	}

	if (quote && (src[++i] != '\''))
		return 0;
	if (number)
		*number = v;
	return ++i;
}

unsigned fct_parse_number_base(
	const char* src, unsigned base, uint64_t* number)
{
	if (!src)
		return 0;

	if (!isxdigit(*src))
		return 0;

	unsigned i;
	uint64_t v;
	for (i = 0, v = 0; isxdigit(src[i]); i++)
	{
		unsigned d = _xdigit(src[i]);
		if (d >= base)
			break;
		if (v > (0xFFFFFFFFFFFFFFFFULL / base))
			return 0;
		v *= base;
		v += d;
	}

	if (number)
		*number = v;
	return i;
}

unsigned fct_parse_number(const char* src, uint64_t* number)
{
	if (!src)
		return 0;

	unsigned i = 0;
	if (!isdigit(src[i]))
		return 0;

	unsigned base = 10;
	if ((src[i] == '0')
		&& isalpha(src[i + 1]))
	{
		switch (src[i + 1])
		{
			case 'b':
			case 'B':
				base = 2;
				i += 2;
				break;
			case 'x':
			case 'X':
			case 'h':
			case 'H':
				base = 16;
				i += 2;
				break;
			default:
				break;
		}
	}

	unsigned j = fct_parse_number_base(&src[i], base, number);
	if (!j) return 0;
	i += j;

	return i;
}

unsigned fct_parse_immediate(
	const char* src, uint64_t* number,
	unsigned* size, bool* sign)
{
	unsigned i = 0;


	bool negate = (src[i] == '-');
	if (negate || (src[i] == '+'))
		i++;

	uint64_t n;
	unsigned j = fct_parse_number(&src[i], &n);
	if (!j)
	{
		uint32_t w;
		j = fct_parse_number_char(&src[i], &w, true);
		if (!j) return 0;
		n = w;
	}
	i += j;

	unsigned bits;
	if (negate)
	{
		if( ((n + 1) >> 63)
			|| (n >> 63))
			return 0;
		n = ~n + 1;

		uint64_t k = n;
		bits = 65;
		if (~k == 0) { k = 0; bits = 1; }
		if ((k >> 32) == 0xFFFFFFFF) { k <<= 32; bits -= 32; }
		if ((k >> 48) == 0x0000FFFF) { k <<= 16; bits -= 16; }
		if ((k >> 56) == 0x000000FF) { k <<=  8; bits -=  8; }
		if ((k >> 60) == 0x0000000F) { k <<=  4; bits -=  4; }
		if ((k >> 62) == 0x00000003) { k <<=  2; bits -=  2; }
		if ((k >> 63) == 0x00000001) {           bits -=  1; }
	}
	else
	{
		uint64_t k = n;
		bits = 64;
		if ((k >> 32) == 0) { k <<= 32; bits -= 32; }
		if ((k >> 48) == 0) { k <<= 16; bits -= 16; }
		if ((k >> 56) == 0) { k <<=  8; bits -=  8; }
		if ((k >> 60) == 0) { k <<=  4; bits -=  4; }
		if ((k >> 62) == 0) { k <<=  2; bits -=  2; }
		if ((k >> 63) == 0) {           bits -=  1; }
	}

	if (number) *number = n;
	if (size  ) *size   = bits;
	if (sign  ) *sign   = negate;
	return i;
}



unsigned fct_parse_number_human(
	const char* src, uint64_t* value)
{
	uint64_t num;
	unsigned numlen
		= fct_parse_number_base(src, 10, &num);
	if (numlen == 0)
		return 0;

	const char* name = "kKMGTPE";

	unsigned i;
	unsigned shift;
	for (i = 0; name[i] != '\0'; i++)
	{
		if (i < 2)
			shift = 10;
		else
			shift += 10;

		if (src[numlen] == name[i])
		{
			uint64_t snum = num << shift;
			if ((snum >> shift) != num)
				return 0;
			num = snum;
			numlen++;
			break;
		}
	}

	if (value) *value = num;
	return numlen;
}
