#ifndef __FCT_DEVICE_H__
#define __FCT_DEVICE_H__

typedef struct fct_device_s fct_device_t;
typedef struct fct_device_type_s fct_device_type_t;

#include <fct/types.h>
#include <fct/lex/tok.h>

const fct_class_t* fct_class_device;



fct_device_type_t* fct_device_type_by_name(const char* name);



fct_device_t* fct_device_create_params(
	fct_device_type_t* type, fct_string_t* name, const fct_tok_t* params);
void fct_device_delete(
	fct_device_t* device);
fct_device_t* fct_device_reference(
	fct_device_t* device);

bool fct_device_rename(
	fct_device_t* device,
	fct_string_t* name);

bool fct_device_params_get(
	fct_device_t* device,
	unsigned* mau, unsigned* width);

void fct_device_tick(
	fct_device_t* device);
void fct_device_interrupt(
	fct_device_t* device, uint64_t number);

void fct_device_read(
	fct_device_t* device, uint64_t addr, void* data);
void fct_device_write(
	fct_device_t* device, uint64_t addr, uint32_t mask, void* data);

fct_device_t* fct_device_bus(
	fct_device_t* device, const char* name);
fct_device_t* fct_device_clkbus(
	fct_device_t* device, const char* name);
bool fct_device_port_attach(
	fct_device_t* device, const char* name, fct_device_t* target);
bool fct_device_interrupt_attach(
	fct_device_t* device, const char* name, fct_device_t* target, uint64_t number);

void fct_device_reads(
	fct_device_t* device, uint64_t addr, uint64_t size, void* data);
void fct_device_writes(
	fct_device_t* device, uint64_t addr, uint64_t size, uint32_t mask, void* data);



fct_device_t* fct_device_stdout_create(fct_string_t* name);

fct_device_t* fct_device_memory_create(
	fct_string_t* name, unsigned mau, unsigned width, uint64_t size);

fct_device_t* fct_device_bus_create(
	unsigned mau, unsigned width);
bool fct_device_bus_attach(
	fct_device_t* bus, fct_device_t* device,
	uint64_t base, uint64_t size);

fct_device_t* fct_device_busconv_create(
	unsigned mau, unsigned width, fct_device_t* target);

fct_device_t* fct_device_clkbus_create(unsigned clkdiv);
bool fct_device_clkbus_attach(
	fct_device_t* clkbus, fct_device_t* device);

#include <fct/arch.h>
fct_device_t* fct_device_core_create(fct_string_t* name, fct_arch_t* arch);

#endif
