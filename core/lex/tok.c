#include "tok.h"
#include <stdlib.h>
#include <fct/parse.h>
#include <fct/util/path.h>



bool fct_tok_keyword(
	const fct_tok_t* tok,
	const char* keyword)
{
	return (tok && keyword
		&& (tok->type == FCT_TOK_IDENT)
		&& (tok->size == strlen(keyword))
		&& (strncmp(tok->base, keyword, tok->size) == 0));
}

char* fct_tok_cstring(
	const fct_tok_t* tok)
{
	if (!tok || (tok->type != FCT_TOK_STRING))
		return NULL;

	char* string = NULL;
	unsigned len = fct_parse_string(tok->base, &string);
	if (len != tok->size)
	{
		free(string);
		return NULL;
	}

	return string;
}

bool fct_tok_uint64(
	const fct_tok_t* tok,
	uint64_t* value)
{
	if (!tok || (tok->type != FCT_TOK_INTEGER))
		return false;

	uint64_t v = 0;
	unsigned len = fct_parse_number(tok->base, &v);
	if (len != tok->size)
		len = fct_parse_number_human(tok->base, &v);
	if (len != tok->size)
		return false;

	if (value) *value = v;
	return true;
}

bool fct_tok_unsigned(const fct_tok_t* tok, unsigned* value)
{
	uint64_t uval64;
	if (!fct_tok_uint64(tok, &uval64))
		return false;
	unsigned uval = (unsigned)uval64;
	if ((uint64_t)uval != uval64)
		return false;
	if (value) *value = uval;
	return true;
}

bool fct_tok_char(
	const fct_tok_t* tok,
	uint32_t* character)
{
	if (!tok || (tok->type != FCT_TOK_CHARACTER))
		return false;

	uint32_t c;
	unsigned len = fct_parse_number_char(
		tok->base, &c, true);
	if (len != tok->size)
		return false;

	if (character) *character = c;
	return true;
}

char* fct_tok_path(const fct_tok_t* tok)
{
	if (!tok || (tok->type != FCT_TOK_STRING))
		return NULL;

	char* path = NULL;
	unsigned len = fct_parse_string(tok->base, &path);
	if (len != tok->size)
	{
		free(path);
		return NULL;
	}

	if (tok->file)
	{
		char* rpath = path_relative(
			tok->file, path);
		free(path);
		path = rpath;
	}

	return path;
}



fct_string_t* fct_tok_to_string(const fct_tok_t* tok)
{
	if (!tok) return NULL;
	return fct_string_create_format(
		"%.*s", tok->size, tok->base);
}



void fct_debug_log_at_tokenv(const fct_tok_t* token,
	fct_debug_log_type_e type, const char* format, va_list args)
{
	if (token)
		fct_debug_logv(
			token->file, token->row, token->col,
			type, format, args);
	else
		fct_debug_logv(NULL, 0, 0, type, format, args);
}

void fct_debug_log_at_token(const fct_tok_t* token,
	fct_debug_log_type_e type, const char* format, ...)
{
	va_list args;
	va_start(args, format);
	fct_debug_log_at_tokenv(token, type, format, args);
	va_end(args);
}
