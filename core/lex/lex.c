#include "lex.h"
#include <stdlib.h>
#include <fct/util/path.h>
#include <fct/parse.h>



static inline bool fct_lex__is_base_digit(
	char c, unsigned base)
{
	if (!isalnum(c))
		return false;
	return ((unsigned)(isdigit(c) ? (c - '0')
		: (tolower(c) - 'a')) < base);
}

static inline bool fct_lex__is_ident_char(
	char c, bool initial)
{
	switch (c)
	{
		case '_':
			return true;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			return !initial;
		default:
			break;
	}

	return isalpha(c);
}



static unsigned fct_lex__ident(
	const char* ptr)
{
	unsigned i = 0;
	if (!fct_lex__is_ident_char(ptr[i], true))
		return 0;
	for (i++; fct_lex__is_ident_char(ptr[i], false); i++);
	return i;
}

static unsigned fct_lex__number(
	const char* ptr, fct_tok_e* type)
{
	unsigned i = 0;
	if ((ptr[i] == '+')
		|| (ptr[i] == '-'))
		i++;

	unsigned base = 10;
	if (ptr[i] == '0')
	{
		switch (tolower(ptr[i + 1]))
		{
			case 'b':
				base = 2;
				i += 2;
				break;
			case 'h':
			case 'x':
				base = 16;
				i += 2;
				break;
			default:
				break;
		}
	}

	if (!fct_lex__is_base_digit(ptr[i], base))
		return 0;

	for(i++; fct_lex__is_base_digit(ptr[i], base); i++);

	fct_tok_e t = FCT_TOK_INTEGER;

	if (isalpha(ptr[i]))
	{
		if (type) *type = t;
		return (i + 1);
	}

	if (ptr[i] == '.')
	{
		t = FCT_TOK_REAL;

		if (!fct_lex__is_base_digit(ptr[++i], base))
			return 0;
		for(i++; fct_lex__is_base_digit(ptr[i], base); i++);
	}

	if (tolower(ptr[i]) == 'e')
	{
		t = FCT_TOK_REAL;

		i += 1;
		if ((ptr[i] == '+')
			|| (ptr[i] == '-'))
			i += 1;
		if (!fct_lex__is_base_digit(ptr[i], base))
			return 0;
		for(i++; fct_lex__is_base_digit(ptr[i], base); i++);
	}

	if (type) *type = t;
	return i;
}

static unsigned fct_lex__string(
	const char* ptr, char c)
{
	unsigned i = 0;
	if (ptr[i++] != c)
		return 0;

	bool escape = false;
	for (; ptr[i] != '\0'; i++)
	{
		if (!escape && (ptr[i] == c))
			break;
		if (ptr[i] == '\n')
			return 0;
		escape = (ptr[i] == '\\');
	}

	return (ptr[i] == c ? (i + 1) : 0);
}

static unsigned fct_lex__op(
	const char* ptr, fct_tok_e* type)
{
	switch (ptr[0])
	{
		case '+':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN_PLUS;
				return 2;
			}
			*type = FCT_TOK_PLUS;
			return 1;
		case '-':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN_MINUS;
				return 2;
			}
			*type = FCT_TOK_MINUS;
			return 1;
		case '*':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN_MULTIPLY;
				return 2;
			}
			*type = FCT_TOK_OP_MULTIPLY;
			return 1;
		case '/':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN_DIVIDE;
				return 2;
			}
			*type = FCT_TOK_OP_DIVIDE;
			return 1;
		case '%':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN_MODULO;
				return 2;
			}
			*type = FCT_TOK_OP_MODULO;
			return 1;
		case '!':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_OP_NOT_EQUAL;
				return 2;
			}
			*type = FCT_TOK_EXCLAMATION;
			return 1;
		case '~':
			*type = FCT_TOK_OP_INVERT;
			return 1;
		case '&':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN_AND;
				return 2;
			}
			*type = FCT_TOK_OP_AND;
			return 1;
		case '|':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN_OR;
				return 2;
			}
			*type = FCT_TOK_OP_OR;
			return 1;
		case '^':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN_XOR;
				return 2;
			}
			*type = FCT_TOK_OP_XOR;
			return 1;
		case '<':
			if (strncmp(ptr, "<<<=", 4) == 0)
			{
				*type = FCT_TOK_ASSIGN_LROT;
				return 4;
			}
			else if (strncmp(ptr, "<<<", 3) == 0)
			{
				*type = FCT_TOK_OP_LROT;
				return 3;
			}
			else if (strncmp(ptr, "<<=", 3) == 0)
			{
				*type = FCT_TOK_ASSIGN_LSHIFT;
				return 3;
			}
			else if (strncmp(ptr, "<<", 2) == 0)
			{
				*type = FCT_TOK_OP_LSHIFT;
				return 2;
			}
			else if (strncmp(ptr, "<=", 2) == 0)
			{
				*type = FCT_TOK_OP_LESS_EQUAL;
				return 2;
			}
			else if (strncmp(ptr, "<?", 2) == 0)
			{
				*type = FCT_TOK_OP_MINIMUM;
				return 2;
			}
			*type = FCT_TOK_OP_LESS_THAN;
			return 1;
		case '>':
			if (strncmp(ptr, ">>>=", 4) == 0)
			{
				*type = FCT_TOK_ASSIGN_RROT;
				return 4;
			}
			else if (strncmp(ptr, ">>>", 3) == 0)
			{
				*type = FCT_TOK_OP_RROT;
				return 3;
			}
			else if (strncmp(ptr, ">>=", 3) == 0)
			{
				*type = FCT_TOK_ASSIGN_RSHIFT;
				return 3;
			}
			else if (strncmp(ptr, ">>", 2) == 0)
			{
				*type = FCT_TOK_OP_RSHIFT;
				return 2;
			}
			else if (strncmp(ptr, ">=", 2) == 0)
			{
				*type = FCT_TOK_OP_MORE_EQUAL;
				return 2;
			}
			else if (strncmp(ptr, ">?", 2) == 0)
			{
				*type = FCT_TOK_OP_MAXIMUM;
				return 2;
			}
			*type = FCT_TOK_OP_MORE_THAN;
			return 1;
		case '?':
			*type = FCT_TOK_QUESTION;
			return 1;
		case ':':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN;
				return 2;
			}
			*type = FCT_TOK_COLON;
			return 1;
		case '=':
			if (ptr[1] == '=')
			{
				*type = FCT_TOK_ASSIGN;
				return 2;
			}
			break;
		case '@':
			*type = FCT_TOK_OP_ADDRESS_OF;
			return 2;
		default:
			break;
	}

	return 0;
}


static bool fct_lex__line_prealloc(
	const char* file, unsigned* row, const char* ptr,
	unsigned* indent, unsigned* length,
	fct_tok_t** tokens, unsigned* count)
{
	unsigned i;
	for (i = 0; ptr[i] == '\t'; i++);
	unsigned base_indent = i;
	unsigned last_indent = base_indent;

	unsigned brackets = 0;
	unsigned parens   = 0;

	unsigned start_of_line = 0;

	fct_tok_t* tok = *tokens;
	fct_tok_e prev = FCT_TOK_END;
	unsigned  t    = 0;

	while (true)
	{
		if (ptr[i] == ' ')
		{
			FCT_DEBUG_ERROR_FILE_LINE(file, *row,
				"Line can't begin with space"
				", use tab character for indent");
			return false;
		}

		do
		{
			if (t >= *count)
			{
				unsigned ncount = (*count << 1);
				fct_tok_t* ntokens
					= realloc(*tokens,
						ncount * sizeof(fct_tok_t));
				if (!ntokens) return false;
				*tokens = ntokens;
				*count  = ncount;
				tok = *tokens;
			}

			bool has_space = (ptr[i] == ' ');
			for (; ptr[i] == ' '; i++);

			tok[t].base = &ptr[i];
			tok[t].size = 1;

			tok[t].file = file;
			tok[t].row  = *row;
			tok[t].col  = (i - start_of_line);

			switch (ptr[i])
			{
				case '_':
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'X':
				case 'Y':
				case 'Z':
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
				case 'h':
				case 'i':
				case 'j':
				case 'k':
				case 'l':
				case 'm':
				case 'n':
				case 'o':
				case 'p':
				case 'q':
				case 'r':
				case 's':
				case 't':
				case 'u':
				case 'v':
				case 'w':
				case 'x':
				case 'y':
				case 'z':
					tok[t].type = FCT_TOK_IDENT;
					tok[t].size = fct_lex__ident(&ptr[i]);
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					tok[t].size = fct_lex__number(&ptr[i], &tok[t].type);
					break;
				case '.':
					if (ptr[i + 1] == '.')
					{
						tok[t].type = FCT_TOK_RANGE;
						tok[t].size = 2;
					}
					else
					{
						if (!fct_lex__is_ident_char(
							ptr[i + 1], true))
						{
							FCT_DEBUG_ERROR_TOKEN(&tok[t],
								"Point must be directly followed by ident");
							return false;
						}

						if ((prev == FCT_TOK_IDENT) && !has_space)
						{
							tok[t].type = FCT_TOK_MEMBER;
							tok[t].size = 1;
						}
						else if ((fct_lex__ident(&ptr[i + 1]) == 7)
							&& (strncmp(&ptr[i + 1], "include", 7) == 0))
						{
							tok[t].type = FCT_TOK_INCLUDE;
							tok[t].size = 8;
						}
						else
						{
							tok[t].type= FCT_TOK_DIRECTIVE;
							tok[t].size = 1;
						}
					}
					break;
				case '\'':
					tok[t].type = FCT_TOK_CHARACTER;
					tok[t].size = fct_lex__string(&ptr[i], ptr[i]);
					break;
				case '\"':
					tok[t].type = FCT_TOK_STRING;
					tok[t].size = fct_lex__string(&ptr[i], ptr[i]);
					break;
				case '$':
					tok[t].type = FCT_TOK_REGISTER;
					break;
				case '[':
					tok[t].type = FCT_TOK_LBRACKET;
					if ((brackets + 1) == 0)
						return false;
					brackets += 1;
					break;
				case ']':
					tok[t].type = FCT_TOK_RBRACKET;
					if (brackets == 0)
					{
						FCT_DEBUG_ERROR_TOKEN(&tok[t],
							"Attempting to close unopened square bracketed expression");
						return false;
					}
					brackets -= 1;
					break;
				case '(':
					tok[t].type = FCT_TOK_LPAREN;
					if ((parens + 1) == 0)
						return false;
					parens += 1;
					break;
				case ')':
					tok[t].type = FCT_TOK_RPAREN;
					if (parens == 0)
					{
						FCT_DEBUG_ERROR_TOKEN(&tok[t],
							"Attempting to close unopened bracketed expression");
						return false;
					}
					parens -= 1;
					break;
				case ',':
					tok[t].type = FCT_TOK_COMMA;
					break;
				case '+':
				case '-':
					tok[t].size = fct_lex__number(&ptr[i], &tok[t].type);
					if (tok[t].size == 0)
						tok[t].size = fct_lex__op(
							&ptr[i], &tok[t].type);
					break;
				case '*':
				case '/':
				case '%':
				case '!':
				case '~':
				case '&':
				case '|':
				case '^':
				case '<':
				case '>':
				case '?':
				case ':':
				case '=':
				case '@':
					tok[t].size = fct_lex__op(
						&ptr[i], &tok[t].type);
					break;
				case '\0':
				case '\n':
					if (has_space)
					{
						FCT_DEBUG_ERROR_TOKEN(&tok[t],
							"Trailing whitespace");
						return false;
					}
					tok[t].type = FCT_TOK_END;
					tok[t].size = 0;
					break;
				case ';':
					tok[t].type = FCT_TOK_END;
					tok[t].size = 0;
					break;
				case '\t':
					FCT_DEBUG_ERROR_TOKEN(&tok[t],
						"Tabs must only be used for initial indent");
					return false;
				default:
					if (isprint(ptr[i]))
						FCT_DEBUG_ERROR_TOKEN(&tok[t],
							"Unexpected character '%c'", ptr[i]);
					else
						FCT_DEBUG_ERROR_TOKEN(&tok[t],
							"Unexpected character 0x%02" PRIX8, ptr[i]);
					return false;
			}

			if ((tok[t].size == 0)
				&& (tok[t].type != FCT_TOK_END))
			{
				FCT_DEBUG_ERROR_TOKEN(&tok[t],
					"Malformed token");
				return false;
			}

			if ((prev == FCT_TOK_COMMA) && !has_space
				&& (tok[t].type != FCT_TOK_END))
			{
				FCT_DEBUG_ERROR_TOKEN(&tok[t],
					"Expected space or newline after comma");
				return false;
			}

			i += tok[t].size;
			prev = tok[t].type;
		} while (tok[t++].type != FCT_TOK_END);

		if (ptr[i] == ';')
			for (i++; (ptr[i] != '\0') && (ptr[i] != '\n'); i++);
		if (ptr[i] == '\n')
		{
			i += 1;
			start_of_line = i;
			*row += 1;
		}

		if ((brackets + parens) == 0)
			break;

		if (ptr[i] == '\0')
		{
			FCT_DEBUG_ERROR_FILE_LINE(file, (*row - 1),
				"Unclosed brackets");
			return false;
		}

		unsigned k;
		for (k = 0; ptr[i] == '\t'; k++, i++);
		if ((k < (base_indent + 1))
			|| (k > (last_indent + 1)))
		{
			FCT_DEBUG_ERROR_FILE_LINE(file, (*row - 1),
				"Incorrect indentation of bracketed expression");			
			return false;
		}
		last_indent = k; 
	}

	if (t > *count)
	{
		fct_tok_t* ntokens
			= realloc(*tokens,
				t * sizeof(fct_tok_t));
		if (ntokens)
		{
			*tokens = ntokens;
			*count  = t;
		}
	}

	if (indent)
		*indent += base_indent;
	if (length)
		*length = i;
	return true;
}

static fct_tok_t* fct_lex__line(
	const char* file, unsigned* row, const char* ptr,
	unsigned* indent, unsigned* length)
{
	unsigned len   = 0;
	unsigned count = 32;
	fct_tok_t* tokens
		= (fct_tok_t*)malloc(
			count * sizeof(fct_tok_t));
	if (!tokens) return NULL;

	if (!fct_lex__line_prealloc(
		file, row, ptr,
		indent, &len,
		&tokens, &count))
	{
		free(tokens);
		return NULL;
	}

	if (length) *length = len;
	return tokens;
}



static fct_lex_line_t* fct_lex_line__tail(
	fct_lex_line_t* line, unsigned depth)
{
	if (!line)
		return NULL;

	if (depth == 0)
		return line;

	unsigned i, j;
	for (i = 0, j = (line->child_count - 1);
		i < line->child_count; i++, j--)
	{
		fct_lex_line_t* tail = fct_lex_line__tail(
			&line->child[j], (depth - 1));
		if (tail) return tail;
	}

	return NULL;
}

static bool fct_lex_line__add_line(
	fct_lex_line_t* line,
	fct_tok_t* tokens)
{
	if (!line || !tokens)
		return false;

	fct_lex_line_t* nchild
		= (fct_lex_line_t*)realloc(line->child,
			((line->child_count + 1)
				* sizeof(fct_lex_line_t)));
	if (!nchild) return false;

	line->child = nchild;
	line->child[line->child_count].tokens = tokens;
	line->child[line->child_count].contig = NULL;
	line->child[line->child_count].child_count = 0;
	line->child[line->child_count].child = NULL;
	line->child_count++;

	return true;
}


static bool fct_lex__tokenize_file(
	fct_lex_context_t* context,
	const char* path, unsigned indent);

static bool fct_lex__tokenize(
	fct_lex_context_t* context,
	const char* path, const char* src,
	unsigned indent)
{
	if (!src)
		return false;

	fct_tok_t* tokens;
	unsigned depth, length;

	unsigned i = 0, row = 1;
	while (src[i] != '\0')
	{
		unsigned nrow = row;
		depth = indent;
		tokens = fct_lex__line(
			path, &nrow, &src[i], &depth, &length);
		if (!tokens) return false;

		if (tokens[0].type == FCT_TOK_END)
		{
			/* Ignore empty lines. */
			free(tokens);

			if (depth > indent)
			{
				FCT_DEBUG_ERROR_FILE_LINE(path, row,
					"Empty lines must not be indented");
				return false;
			}
		}
		else if (tokens[0].type == FCT_TOK_INCLUDE)
		{
			bool success = true;
			unsigned j;
			for (j = 1; tokens[j].type != FCT_TOK_END; j++)
			{
				char* ipath = fct_tok_path(&tokens[j]);
				if (!ipath)
				{
					FCT_DEBUG_ERROR_TOKEN(&tokens[j],
						"Invalid path in include directive");
					free(ipath);
					success = false;
					break;
				}

				success = fct_lex__tokenize_file(
					context, ipath, depth);
				free(ipath);
				if (!success) break;
			}
			free(tokens);

			if (!success)
				return false;
		}
		else
		{
			fct_lex_line_t* tail
				= fct_lex_line__tail(
					&context->root, depth);
			if (!tail)
			{
				FCT_DEBUG_ERROR_FILE_LINE(path, row,
					"Indented line has no parent");
				free(tokens);
				return false;
			}

			if (!fct_lex_line__add_line(tail, tokens))
			{
				free(tokens);
				return false;
			}
		}

		i += length;
		row = nrow;
	}

	return true;
}

static bool fct_lex__tokenize_file(
	fct_lex_context_t* context,
	const char* path, unsigned indent)
{
	if (!context)
		return false;

	char* spath = path_optimize(path);
	if (!spath) return false;

	if (!fct_list_add(context->paths, spath))
	{
		free(spath);
		return false;
	}

	fct_string_t* file
		= fct_string_file_import(spath);
	if (!file) return false;

	if (!fct_string_null_terminate(file)
		|| !fct_list_add(context->files, file))
	{
		fct_string_delete(file);
		return false;
	}

	return fct_lex__tokenize(
		context, spath, file->data, indent);
}


fct_lex_context_t* fct_lex_tokenize_file(const char* path)
{
	fct_lex_context_t* context
		= (fct_lex_context_t*)malloc(
			sizeof(fct_lex_context_t));
	if (!context) return NULL;

	context->root.tokens      = NULL;
	context->root.contig      = NULL;
	context->root.child_count = 0;
	context->root.child       = NULL;

	context->paths = fct_array_create(NULL);
	context->files = fct_array_create(fct_class_string);
	if (!context->paths
		|| !context->files
		|| !fct_lex__tokenize_file(
			context, path, 0))
	{
		fct_lex_context_delete(context);
		return NULL;
	}

	return context;
}

void fct_lex_line__delete(
	fct_lex_line_t line)
{
	unsigned i;
	for (i = 0; i < line.child_count; i++)
		fct_lex_line__delete(line.child[i]);
	free(line.contig);
	free(line.tokens);
}

void fct_lex_context_delete(
	fct_lex_context_t* context)
{
	if (!context)
		return;

	fct_lex_line__delete(context->root);
	fct_list_delete(context->paths);
	fct_list_delete(context->files);
	free(context);
}


const fct_lex_line_t* fct_lex_line_root(
	fct_lex_context_t* context)
{
	return (context ? &context->root : NULL);
}

bool fct_lex_line_foreach(
	const fct_lex_line_t* line, void* param,
	bool (*func)(fct_lex_line_t*, void*))
{
	if (!line || !func)
		return false;

	unsigned i;
	for (i = 0; i < line->child_count; i++)
	{
		if (!func(&line->child[i], param))
			return false;
	}

	return true;
}


const fct_tok_t* fct_lex_line_tokens(
	const fct_lex_line_t* line)
{
	return (line ? line->tokens : NULL);
}


static unsigned fct_lex_line_tokens_contiguous__count(
	const fct_lex_line_t* line)
{
	unsigned i = 0;
	if (line->tokens)
		for (; line->tokens[i].type != FCT_TOK_END; i++);

	unsigned j;
	for (j = 0; j < line->child_count; j++)
		i += fct_lex_line_tokens_contiguous__count(&line->child[j]);

	return i;
}

static void fct_lex_line_tokens_contiguous__copy(
	fct_tok_t* dst, const fct_lex_line_t* line)
{
	unsigned i = 0;
	if (line->tokens)
	{
		for (; line->tokens[i].type != FCT_TOK_END; i++)
			dst[i] = line->tokens[i];
		dst[i] = line->tokens[i];
	}

	unsigned j;
	for (j = 0; j < line->child_count; j++)
		fct_lex_line_tokens_contiguous__copy(
			&dst[i], &line->child[j]);
}

const fct_tok_t* fct_lex_line_tokens_contiguous(
	const fct_lex_line_t* line)
{
	if (!line)
		return NULL;

	if (line->child_count == 0)
		return line->tokens;

	if (!line->contig)
	{
		unsigned count
			= fct_lex_line_tokens_contiguous__count(line);

		fct_lex_line_t* mline
			= (fct_lex_line_t*)line;

		mline->contig = (fct_tok_t*)malloc(
			(count + 1) * sizeof(fct_tok_t));
		if (!mline->contig) return NULL;
		fct_lex_line_tokens_contiguous__copy(
			mline->contig, line);
	}

	return line->contig;
}



void fct_debug_log_at_lex_line(const fct_lex_line_t* line,
	fct_debug_log_type_e type, const char* format, ...)
{
	va_list args;
	va_start(args, format);
	const fct_tok_t* token
		= fct_lex_line_tokens(line);
	if (token)
		fct_debug_logv(
			token->file, token->row, 0,
			type, format, args);
	else
		fct_debug_logv(NULL, 0, 0, type, format, args);
	va_end(args);
}
