#ifndef __FCT_LEX_LEX_H__
#define __FCT_LEX_LEX_H__

#include <fct/lex.h>


struct fct_lex_line_s
{
	fct_tok_t*      tokens;
	fct_tok_t*      contig;
	unsigned        child_count;
	fct_lex_line_t* child;
};

struct fct_lex_context_s
{
	fct_lex_line_t root;
	fct_list_t*    paths;
	fct_list_t*    files;
};

#endif
