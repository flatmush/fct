#ifndef __FCT_LEX_TOK_H__
#define __FCT_LEX_TOK_H__

#include <fct/types.h>

typedef enum
{
	FCT_TOK_END = 0,

	FCT_TOK_IDENT,
	FCT_TOK_MEMBER,
	FCT_TOK_STRING,
	FCT_TOK_CHARACTER,
	FCT_TOK_INTEGER,
	FCT_TOK_REAL,

	FCT_TOK_DIRECTIVE,
	FCT_TOK_REGISTER,

	FCT_TOK_LBRACKET,
	FCT_TOK_RBRACKET,
	FCT_TOK_LPAREN,
	FCT_TOK_RPAREN,

	FCT_TOK_COMMA,
	FCT_TOK_RANGE,

	FCT_TOK_PLUS,
	FCT_TOK_MINUS,
	FCT_TOK_OP_MULTIPLY,
	FCT_TOK_OP_DIVIDE,
	FCT_TOK_OP_MODULO,
	FCT_TOK_OP_INVERT,
	FCT_TOK_OP_AND,
	FCT_TOK_OP_OR,
	FCT_TOK_OP_XOR,
	FCT_TOK_OP_LROT,
	FCT_TOK_OP_RROT,
	FCT_TOK_OP_LSHIFT,
	FCT_TOK_OP_RSHIFT,
	FCT_TOK_EXCLAMATION,
	FCT_TOK_OP_NOT_EQUAL,
	FCT_TOK_OP_EQUAL,
	FCT_TOK_OP_LESS_THAN,
	FCT_TOK_OP_LESS_EQUAL,
	FCT_TOK_OP_MORE_THAN,
	FCT_TOK_OP_MORE_EQUAL,
	FCT_TOK_QUESTION,
	FCT_TOK_COLON,
	FCT_TOK_OP_ADDRESS_OF,
	FCT_TOK_OP_MINIMUM,
	FCT_TOK_OP_MAXIMUM,

	FCT_TOK_ASSIGN,
	FCT_TOK_ASSIGN_PLUS,
	FCT_TOK_ASSIGN_MINUS,
	FCT_TOK_ASSIGN_MULTIPLY,
	FCT_TOK_ASSIGN_DIVIDE,
	FCT_TOK_ASSIGN_MODULO,
	FCT_TOK_ASSIGN_AND,
	FCT_TOK_ASSIGN_OR,
	FCT_TOK_ASSIGN_XOR,
	FCT_TOK_ASSIGN_LROT,
	FCT_TOK_ASSIGN_RROT,
	FCT_TOK_ASSIGN_LSHIFT,
	FCT_TOK_ASSIGN_RSHIFT,

	FCT_TOK_INCLUDE,

	FCT_TOK_COUNT
} fct_tok_e;

typedef struct
{
	fct_tok_e   type;

	const char* base;
	unsigned    size;

	const char* file;
	unsigned    row, col;
} fct_tok_t;


bool  fct_tok_keyword(const fct_tok_t* tok, const char* keyword);
char* fct_tok_cstring(const fct_tok_t* tok);
bool  fct_tok_uint64(const fct_tok_t* tok, uint64_t* value);
bool  fct_tok_unsigned(const fct_tok_t* tok, unsigned* value);
bool  fct_tok_char(const fct_tok_t* tok, uint32_t* character);
char* fct_tok_path(const fct_tok_t* tok);

#define FCT_TOK_TO_STRING_STATIC(t) \
	FCT_STRING_STATIC_SECTION((t)->base, (t)->size)
fct_string_t* fct_tok_to_string(const fct_tok_t* tok);


#include <fct/debug.h>

#define FCT_DEBUG_ERROR_TOKEN(t, m...) \
	fct_debug_log_at_token(t, FCT_DEBUG_LOG_TYPE_ERROR, m)
#define FCT_DEBUG_WARN_TOKEN(t, m...) \
	fct_debug_log_at_token(t, FCT_DEBUG_LOG_TYPE_WARNING, m)

void fct_debug_log_at_tokenv(const fct_tok_t* token,
	fct_debug_log_type_e type, const char* format, va_list args);
void fct_debug_log_at_token(const fct_tok_t* token,
	fct_debug_log_type_e type, const char* format, ...);

#endif
