#ifndef __FCT_LEX_H__
#define __FCT_LEX_H__

#include <fct/types.h>
#include <fct/lex/tok.h>

typedef struct fct_lex_context_s fct_lex_context_t;
typedef struct fct_lex_line_s    fct_lex_line_t;

fct_lex_context_t* fct_lex_tokenize_file(const char* path);
void fct_lex_context_delete(fct_lex_context_t* context);

const fct_lex_line_t* fct_lex_line_root(
	fct_lex_context_t* context);

bool fct_lex_line_foreach(
	const fct_lex_line_t* line, void* param,
	bool (*func)(fct_lex_line_t*, void*));

const fct_tok_t* fct_lex_line_tokens(
	const fct_lex_line_t* line);
const fct_tok_t* fct_lex_line_tokens_contiguous(
	const fct_lex_line_t* line);


#include <fct/debug.h>

#define FCT_DEBUG_ERROR_LEX_LINE(l, m...) \
	fct_debug_log_at_lex_line(l, FCT_DEBUG_LOG_TYPE_ERROR, m)
#define FCT_DEBUG_WARN_LEX_LINE(l, m...) \
	fct_debug_log_at_lex_line(l, FCT_DEBUG_LOG_TYPE_WARNING, m)

void fct_debug_log_at_lex_line(const fct_lex_line_t* line,
	fct_debug_log_type_e type, const char* format, ...);

#endif
