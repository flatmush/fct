#ifndef __FCT_ASM_JOB_LIST_H__
#define __FCT_ASM_JOB_LIST_H__

#include <fct/types.h>

typedef fct_list_t fct_asm_job_list_t;

#include <fct/object.h>

fct_asm_job_list_t* fct_asm_job_list_create(void);
void fct_asm_job_list_delete(fct_asm_job_list_t* list);

bool fct_asm_job_list_add(fct_asm_job_list_t* list, const char* path);

fct_object_t* fct_asm_job_list_process(fct_asm_job_list_t* list);

#endif
