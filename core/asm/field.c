#include "field.h"
#include <fct/parse.h>
#include <stdlib.h>



static fct_asm_field_t* fct_asm_field_create(
	fct_asm_field_type_e type)
{
	fct_asm_field_t* field
		= (fct_asm_field_t*)malloc(
			sizeof(fct_asm_field_t));
	if (!field) return NULL;

	field->type  = type;
	field->xform = FCT_ASM_FIELD_XFORM_NONE;

	field->file = NULL;
	field->row  = 0;
	field->col  = 0;

	return field;
}

fct_asm_field_t* fct_asm_field_create_register(
	unsigned reg, const fct_tok_t* name)
{
	fct_asm_field_t* field
		= fct_asm_field_create(
			FCT_ASM_FIELD_TYPE_REGISTER);
	if (!field) return NULL;

	field->reg.reg  = reg;
	field->reg.name = name;
	return field;
}

fct_asm_field_t* fct_asm_field_create_immediate(
	unsigned size, unsigned count,
	uint64_t* values, bool sign)
{
	if ((size == 0) || (size > 64)
		|| (count == 0) || !values)
		return NULL;

	fct_asm_field_type_e type = (count > 1
		? (sign ? FCT_ASM_FIELD_TYPE_VECTOR_SIGNED
			: FCT_ASM_FIELD_TYPE_VECTOR_UNSIGNED)
		: (sign ? FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED
			: FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED));

	fct_asm_field_t* field
		= fct_asm_field_create(type);
	if (!field) return NULL;

	if (count == 1)
	{
		field->immediate.size  = size;
		field->immediate.value = values[0];
	}
	else
	{
		field->vector.value
			= (uint64_t*)malloc(
				count * sizeof(uint64_t));
		if (!field->vector.value)
		{
			fct_asm_field_delete(field);
			return NULL;
		}

		field->vector.size  = size;
		field->vector.count = count;
		unsigned i;
		for (i = 0; i < count; i++)
			field->vector.value[i] = values[i];
	}

	return field;
}

fct_asm_field_t* fct_asm_field_create_symbol(
	fct_string_t* name)
{
	if (!name)
		return NULL;

	fct_asm_field_t* field
		= fct_asm_field_create(FCT_ASM_FIELD_TYPE_SYMBOL);
	if (!field) return NULL;

	field->symbol.name = fct_string_reference(name);
	if (!field->symbol.name)
	{
		fct_asm_field_delete(field);
		return NULL;
	}

	return field;
}

fct_asm_field_t* fct_asm_field_create_command(
	fct_string_t* mnem, fct_asm_field_t* field)
{
	if (!mnem || !field)
		return NULL;

	fct_asm_field_t* nfield
		= fct_asm_field_create(FCT_ASM_FIELD_TYPE_COMMAND);
	if (!nfield) return NULL;

	nfield->command.mnem  = fct_string_reference(mnem);
	nfield->command.field = field;

	if (!nfield->command.mnem)
	{
		fct_asm_field_delete(nfield);
		return NULL;
	}

	return nfield;
}

void fct_asm_field_delete(fct_asm_field_t* field)
{
	switch (field->type)
	{
		case FCT_ASM_FIELD_TYPE_SYMBOL:
			fct_string_delete(field->symbol.name);
			break;
		case FCT_ASM_FIELD_TYPE_VECTOR_UNSIGNED:
		case FCT_ASM_FIELD_TYPE_VECTOR_SIGNED:
			free(field->vector.value);
			break;
		case FCT_ASM_FIELD_TYPE_COMMAND:
			fct_string_delete(field->command.mnem);
			fct_asm_field_delete(field->command.field);
			break;
		default:
			break;
	}
	free(field);
}



static fct_string_t* fct_asm_field_dump__register(
	unsigned reg,
	fct_arch_t* arch)
{
	fct_string_t* dump
		= fct_string_create("");
	if (!dump) return NULL;

	char reg_sigil = '$';
	if (!fct_string_append_static_format(
			dump, "%c", reg_sigil))
	{
		fct_string_delete(dump);
		return NULL;
	}

	const char* name
		= fct_arch_reg_name_from_number(arch, reg);
	if (name)
	{
		if (!fct_string_append_static(dump, name))
		{
			fct_string_delete(dump);
			return NULL;
		}
	}
	else if (!fct_string_append_static_format(
			dump, "%u", reg))
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}

static fct_string_t* fct_asm_field_dump__immediate(
	uint64_t value,
	bool     sign)
{
	fct_string_t* dump
		= fct_string_create("");
	if (!dump) return NULL;

	const char* format = (sign ? "%" PRId64 : "%" PRIu64);
	if (!fct_string_append_static_format(
		dump, format, value))
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}

static fct_string_t* fct_asm_field_dump__vector(
	fct_asm_field_t* field)
{
	fct_string_t* dump
		= fct_string_create_format("vec%u(", field->vector.count);
	if (!dump) return NULL;

	bool sign = (field->type == FCT_ASM_FIELD_TYPE_VECTOR_SIGNED);

	unsigned i;
	for (i = 0; i < field->vector.count; i++)
	{
		if ((i != 0) && !fct_string_append_static(
			dump, ", "))
		{
			fct_string_delete(dump);
			return NULL;
		}

		fct_string_t* is
			= fct_asm_field_dump__immediate(
				field->vector.value[i], sign);
		bool appended
			= fct_string_append(dump, is);
		fct_string_delete(is);
		if (!is || !appended)
		{
			fct_string_delete(dump);
			return NULL;
		}
	}

	if (!fct_string_append_static(dump, ")"))
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}

fct_string_t* fct_asm_field_dump(
	fct_asm_field_t* field,
	fct_arch_t* arch)
{
	if (!field)
		return NULL;

	if (field->xform >= FCT_ASM_FIELD_XFORM_COUNT)
		return NULL;

	fct_string_t* fs;
	switch (field->type)
	{
		case FCT_ASM_FIELD_TYPE_REGISTER:
			fs = fct_asm_field_dump__register(
				field->reg.reg, arch);
			break;
		case FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED:
			fs = fct_asm_field_dump__immediate(
				field->immediate.value, false);
			break;
		case FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED:
			fs = fct_asm_field_dump__immediate(
				field->immediate.value, true);
			break;
		case FCT_ASM_FIELD_TYPE_SYMBOL:
			fs = fct_string_reference(
				field->symbol.name);
			break;
		case FCT_ASM_FIELD_TYPE_VECTOR_UNSIGNED:
		case FCT_ASM_FIELD_TYPE_VECTOR_SIGNED:
			fs = fct_asm_field_dump__vector(
				field);
			break;
		default:
			return NULL;
	}
	if (!fs || (field->xform == FCT_ASM_FIELD_XFORM_NONE))
		return fs;

	const char xform[] = { '\0', '!', '~', '-' };

	fct_string_t* dump
		= fct_string_create_format("%c", xform[field->xform]);
	if (!dump)
	{
		fct_string_delete(fs);
		return NULL;
	}

	bool appended = fct_string_append(dump, fs);
	fct_string_delete(fs);
	if (!appended)
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}


static unsigned fct_asm_field_parse__xform(
	const fct_tok_t* src,
	fct_asm_field_xform_e* xform)
{
	if (!src)
		return 0;

	switch (src[0].type)
	{
		case FCT_TOK_EXCLAMATION:
			if (xform) *xform = FCT_ASM_FIELD_XFORM_NOT;
			return 1;
		case FCT_TOK_OP_INVERT:
			if (xform) *xform = FCT_ASM_FIELD_XFORM_INVERT;
			return 1;
		case FCT_TOK_MINUS:
			if (xform) *xform = FCT_ASM_FIELD_XFORM_NEGATE;
			return 1;
		default:
			break;
	}

	return 0;
}

static unsigned fct_asm_field_parse__register(
	fct_arch_t* arch,
	const fct_tok_t* src,
	fct_asm_field_t** field)
{
	fct_asm_field_xform_e xform = FCT_ASM_FIELD_XFORM_NONE;
	unsigned i = fct_asm_field_parse__xform(src, &xform);

	if (src[i++].type != FCT_TOK_REGISTER)
		return 0;

	const fct_tok_t* name = NULL;
	unsigned reg;
	if (src[i].type == FCT_TOK_INTEGER)
	{
		uint64_t reg64;
		unsigned len = fct_parse_number_base(
			src[i].base, 10, &reg64);
		if (len != src[i].size)
			return 0;

		reg = reg64;
		if (reg != reg64)
			return 0;
	}
	else if (src[i].type == FCT_TOK_IDENT)
	{
		char cname[src[i].size + 1];
		memcpy(cname, src[i].base, src[i].size);
		cname[src[i].size] = '\0';

		if (!fct_arch_number_from_reg_name(
			arch, cname, &reg))
			return 0;

		name = &src[i];
	}
	else
	{
		return 0;
	}

	/* Bounds check register. */
	if (reg >= fct_arch_reg_count(arch))
		return 0;

	if (field)
	{
		fct_asm_field_t* f
			= fct_asm_field_create_register(reg, name);
		if (!f) return 0;
		f->xform = xform;
		*field = f;
	}

	return (i + 1);
}

static unsigned fct_asm_field_parse__immediate(
	fct_arch_t* arch,
	const fct_tok_t* src,
	fct_asm_field_t** field)
{
	fct_asm_field_xform_e xform = FCT_ASM_FIELD_XFORM_NONE;
	unsigned i = fct_asm_field_parse__xform(src, &xform);

	if (xform == FCT_ASM_FIELD_XFORM_NEGATE)
	{
		FCT_DEBUG_ERROR_TOKEN(src,
			"Unexpected negation operator in immediate");
		return 0;
	}

	if ((src[i].type != FCT_TOK_INTEGER)
		&& (src[i].type != FCT_TOK_CHARACTER))
		return 0;

	uint64_t value;
	unsigned size;
	bool is_signed;
	unsigned len = fct_parse_immediate(
		src[i].base, &value, &size, &is_signed);
	if (len != src[i].size) return 0;

	{
		unsigned word_size = fct_arch_word_size(arch);
		unsigned addr_size = fct_arch_addr_size(arch);
		if (addr_size > word_size)
			word_size = addr_size;
		word_size *= fct_arch_mau(arch);
		if (size > word_size)
			return 0;
	}

	switch (xform)
	{
		case FCT_ASM_FIELD_XFORM_NONE:
			break;
		case FCT_ASM_FIELD_XFORM_NOT:
			value = !value;
			break;
		case FCT_ASM_FIELD_XFORM_INVERT:
			value = ~value;
			break;
		default:
			return 0;
	}

	if (field)
	{
		fct_asm_field_t* f
			= fct_asm_field_create_immediate(
				size, 1, &value, is_signed);
		if (!f) return 0;
		*field = f;
	}

	return (i + 1);
}

static unsigned fct_asm_field_parse__symbol(
	const fct_tok_t* src, fct_asm_field_t** field)
{
	if (src[0].type != FCT_TOK_IDENT)
		return 0;

	if (field)
	{
		fct_string_t* name = fct_tok_to_string(src);
		fct_asm_field_t* f = fct_asm_field_create_symbol(name);
		fct_string_delete(name);
		if (!f) return 0;
		*field = f;
	}

	return 1;
}

static unsigned fct_asm_field_parse__vector(
	const fct_tok_t* src, fct_asm_field_t** field)
{
	fct_asm_field_xform_e xform = FCT_ASM_FIELD_XFORM_NONE;
	unsigned xlen = fct_asm_field_parse__xform(src, &xform);

	unsigned size;
	bool is_signed;
	unsigned count;

	unsigned len = fct_parse_vector(
		&src[xlen], NULL, &size, &is_signed, &count);
	if (len == 0) return 0;

	uint64_t value[count];
	fct_parse_vector(
		&src[xlen], value, NULL, NULL, NULL);

	if ((xform == FCT_ASM_FIELD_XFORM_NEGATE)
		&& !is_signed)
	{
		if (++size > 64)
			return 0;
		is_signed = true;
	}

	unsigned i;
	switch (xform)
	{
		case FCT_ASM_FIELD_XFORM_NONE:
			break;
		case FCT_ASM_FIELD_XFORM_NOT:
			for (i = 0; i < count; i++)
				value[i] = !value[i];
			break;
		case FCT_ASM_FIELD_XFORM_INVERT:
			for (i = 0; i < count; i++)
				value[i] = ~value[i];
			break;
		case FCT_ASM_FIELD_XFORM_NEGATE:
			for (i = 0; i < count; i++)
				value[i] = ~value[i] + 1;
			break;
		default:
			return 0;
	}

	if (field)
	{
		fct_asm_field_t* f
			= fct_asm_field_create_immediate(size, count, value, is_signed);
		if (!f) return 0;
		*field = f;
	}

	return (xlen + len);
}

static unsigned fct_asm_field_parse__nr(
	fct_arch_t* arch,
	const fct_tok_t* src, fct_asm_field_t** field)
{
	unsigned len;

	len = fct_asm_field_parse__register(
		arch, src, field);
	if (len > 0) return len;

	len = fct_asm_field_parse__immediate(
		arch, src, field);
	if (len > 0) return len;

	len = fct_asm_field_parse__symbol(
		src, field);
	if (len > 0) return len;

	len = fct_asm_field_parse__vector(
		src, field);
	if (len > 0) return len;

	return 0;
}

static unsigned fct_asm_field_parse__command(
	fct_arch_t* arch,
	const fct_tok_t* src, fct_asm_field_t** field)
{
	if (src[0].type != FCT_TOK_IDENT)
		return 0;

	fct_asm_field_t* sfield;
	unsigned flen = fct_asm_field_parse__nr(
		arch, &src[1], &sfield);
	if (flen == 0) return 0;

	if (field)
	{
		fct_string_t* mnem = fct_tok_to_string(src);
		fct_asm_field_t* f = fct_asm_field_create_command(mnem, sfield);
		fct_string_delete(mnem);
		if (!f)
		{
			fct_asm_field_delete(sfield);
			return 0;
		}
		*field = f;
	}
	else
	{
		fct_asm_field_delete(sfield);
	}

	return (1 + flen);
}

unsigned fct_asm_field_parse(
	fct_arch_t* arch,
	const fct_tok_t* src, fct_asm_field_t** field)
{
	if (!src) return 0;

	unsigned len;

	len = fct_asm_field_parse__nr(
		arch, src, field);
	if (len > 0) return len;

	len = fct_asm_field_parse__command(
		arch, src, field);
	if (len > 0) return len;

	if (field && *field)
	{
		(*field)->file = src->file;
		(*field)->row  = src->row;
		(*field)->col  = src->col;
	}

	return 0;
}



bool fct_asm_field_not(fct_asm_field_t* field)
{
	if (!field)
		return false;

	if (field->xform == FCT_ASM_FIELD_XFORM_NOT)
	{
		field->xform = FCT_ASM_FIELD_XFORM_NONE;
		return true;
	}

	if ((field->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
		|| (field->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED))
	{
		field->immediate.value = !field->immediate.value;
		return true;
	}
	else if ((field->type == FCT_ASM_FIELD_TYPE_VECTOR_UNSIGNED)
		|| (field->type == FCT_ASM_FIELD_TYPE_VECTOR_SIGNED))
	{
		unsigned i;
		for (i = 0; i < field->vector.count; i++)
			field->vector.value[i] = !field->vector.value[i];
		return true;
	}

	if (field->xform != FCT_ASM_FIELD_XFORM_NONE)
		return false;
	field->xform = FCT_ASM_FIELD_XFORM_NOT;
	return true;
}

bool fct_asm_field_invert(fct_asm_field_t* field)
{
	if (!field)
		return false;

	if (field->xform == FCT_ASM_FIELD_XFORM_INVERT)
	{
		field->xform = FCT_ASM_FIELD_XFORM_NONE;
		return true;
	}

	if ((field->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
		|| (field->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED))
	{
		field->immediate.value = ~field->immediate.value;
		return true;
	}
	else if ((field->type == FCT_ASM_FIELD_TYPE_VECTOR_UNSIGNED)
		|| (field->type == FCT_ASM_FIELD_TYPE_VECTOR_SIGNED))
	{
		unsigned i;
		for (i = 0; i < field->vector.count; i++)
			field->vector.value[i] = ~field->vector.value[i];
		return true;
	}

	if (field->xform != FCT_ASM_FIELD_XFORM_NONE)
		return false;
	field->xform = FCT_ASM_FIELD_XFORM_INVERT;
	return true;
}

bool fct_asm_field_negate(fct_asm_field_t* field)
{
	if (!field)
		return false;

	if (field->xform == FCT_ASM_FIELD_XFORM_NEGATE)
	{
		field->xform = FCT_ASM_FIELD_XFORM_NONE;
		return true;
	}

	if (field->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED)
	{
		field->immediate.value = ~field->immediate.value + 1;
		return true;
	}
	else if (field->type == FCT_ASM_FIELD_TYPE_VECTOR_SIGNED)
	{
		unsigned i;
		for (i = 0; i < field->vector.count; i++)
			field->vector.value[i] = ~field->vector.value[i] + 1;
		return true;
	}
	else if ((field->type == FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED)
		|| (field->type == FCT_ASM_FIELD_TYPE_VECTOR_UNSIGNED))
	{
		return false;
	}

	if (field->xform != FCT_ASM_FIELD_XFORM_NONE)
		return false;
	field->xform = FCT_ASM_FIELD_XFORM_NEGATE;
	return true;
}



void fct_debug_log_at_field(const fct_asm_field_t* field,
	fct_debug_log_type_e type, const char* format, ...)
{
	va_list args;
	va_start(args, format);
	if (field)
		fct_debug_logv(
			field->file, field->row, field->col,
			type, format, args);
	else
		fct_debug_logv(NULL, 0, 0, type, format, args);
	va_end(args);
}
