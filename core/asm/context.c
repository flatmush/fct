#include "context.h"

#include <stdlib.h>
#include <fct/types.h>
#include <fct/debug.h>
#include <fct/parse.h>

#include "directive.h"



static bool fct_asm_context__boundary(fct_asm_context_t* context)
{
	if (!context)
		return false;

	if (context->chunk)
	{
		if (!context->object)
		{
			context->object = fct_object_create();
			if (!context->object)
				return false;
		}

		if (context->chunk_static
			? !fct_object_chunk_place(
				context->object, context->chunk, context->chunk_address)
			: !fct_object_chunk_add(context->object, context->chunk))
			return false;
	}

	context->chunk         = NULL;
	context->chunk_static  = false;
	context->chunk_address = 0;
	context->chunk_align   = 0;
	context->boundary      = 0;
	return true;
}



fct_asm_context_t* fct_asm_context_create(
	fct_asm_job_list_t* job_list, const char* path)
{
	fct_asm_context_t* context
		= (fct_asm_context_t*)malloc(
			sizeof(fct_asm_context_t));
	if (!context) return NULL;

	context->job_list = job_list;

	context->arch = NULL;

	context->scope = fct_string_create(path);
	if (!fct_string_append_static(context->scope, ":"))
	{
		fct_string_delete(context->scope);
		free(context);
		return NULL;
	}

	context->object = NULL;

	context->chunk         = NULL;
	context->chunk_static  = false;
	context->chunk_address = 0;
	context->chunk_align   = 0;

	context->labels  = NULL;
	context->globals = NULL;

	context->types = fct_hashtab_create(
		fct_class_type,
		(void*)fct_type_name,
		(void*)fct_string_compare,
		(void*)fct_string_hash8);

	fct_array_create(fct_class_type);

	context->boundary = 0;

	return context;
}

void fct_asm_context_delete(fct_asm_context_t* context)
{
	if (!context)
		return;
	fct_chunk_delete(context->chunk);
	fct_object_delete(context->object);
	fct_list_delete(context->labels);
	fct_list_delete(context->globals);
	fct_list_delete(context->types);
	free(context);
}



static unsigned fct_asm_context_assemble_line(
	fct_asm_context_t* context, const fct_tok_t* src)
{
	if (!context || !src)
		return 0;

	const fct_tok_t* mnem    = src;
	const fct_tok_t* postfix = NULL;

	unsigned i = 0;
	if (src[i++].type != FCT_TOK_IDENT)
		return 0;

	if (src[i].type == FCT_TOK_MEMBER)
	{
		if (src[++i].type != FCT_TOK_IDENT)
			return 0;
		postfix = &src[i++];
	}

	unsigned field_max = 16;
	fct_asm_field_t** field
		= (fct_asm_field_t**)malloc(
			sizeof(fct_asm_field_t*) * field_max);
	if (!field) return 0;
	unsigned field_count = 0;

	while (src[i].type != FCT_TOK_END)
	{
		if (field_count > 0)
		{
			if (src[i].type != FCT_TOK_COMMA)
			{
				FCT_DEBUG_ERROR_TOKEN(&src[i],
					"Expected comma or end of line after field.");
				break;
			}
			i++;
		}

		if (field_count >= field_max)
		{
			field_max <<= 1;
			fct_asm_field_t** nfield
				= (fct_asm_field_t**)realloc(field,
					sizeof(fct_asm_field_t*) * field_max);
			if (!nfield) break;
			field = nfield;

			unsigned f;
			for (f = field_count; f < field_max; f++)
				field[f] = NULL;
		}

		unsigned j = fct_asm_field_parse(
			context->arch, &src[i], &field[field_count]);
		if (j == 0)
		{
			FCT_DEBUG_ERROR_TOKEN(&src[i],
				"Failed to parse field.");
			break;
		}
		field_count++;
		i += j;
	}

	bool success = false;
	if (src[i].type == FCT_TOK_END)
	{
		success = fct_arch_assemble(
			context->arch, context,
			mnem, postfix,
			field, field_count);
	}

	unsigned f;
	for (f = 0; f < field_count; f++)
		fct_asm_field_delete(field[f]);
	free(field);

	return (success ? i : 0);
}

static bool fct_asm_context__assemble(
	const fct_lex_line_t* line,
	fct_asm_context_t* context)
{
	const fct_tok_t* tokens
		= fct_lex_line_tokens_contiguous(line);

	unsigned i = 0;
	while (tokens[i].type != FCT_TOK_END)
	{
		bool global = (tokens[i + 1].type == FCT_TOK_EXCLAMATION);
		if (!global && (tokens[i + 1].type != FCT_TOK_COLON))
			break;

		if (tokens[i].type == FCT_TOK_IDENT)
		{
			char label[tokens[i].size + 1];
			memcpy(label, tokens[i].base, tokens[i].size);
			label[tokens[i].size] = '\0';

			if (!fct_asm_context_export_label(
				context, label, global))
			{
				FCT_DEBUG_ERROR_TOKEN(&tokens[i],
					"Failed to export label.");
				return false;
			}
		}
		else if (tokens[i].type == FCT_TOK_INTEGER)
		{
			if (!context->arch)
			{
				FCT_DEBUG_ERROR_TOKEN(&tokens[i],
					"Address labels cannot come before arch definition.");
				return false;
			}

			uint64_t addr;
			if (!fct_tok_uint64(&tokens[i], &addr))
			{
				FCT_DEBUG_ERROR_TOKEN(&tokens[i],
					"Failed to parse address.");
				return false;
			}

			unsigned bits = fct_arch_mau(context->arch)
				* fct_arch_addr_size(context->arch);
			if ((bits < (sizeof(addr) << 3))
				&& ((addr >> bits) != 0))
			{
				FCT_DEBUG_ERROR_TOKEN(&tokens[i],
					"Address out of range.");
				return false;
			}
			if (!fct_asm_context_address(context, addr))
			{
				FCT_DEBUG_ERROR_TOKEN(&tokens[i],
					"Failed to set address.");
				return false;
			}
		}
		else
		{
			break;
		}

		i += 2;
	}

	if (tokens[i].type == FCT_TOK_END)
		return true;

	if (tokens[i].type == FCT_TOK_DIRECTIVE)
	{
		if (!fct_asm_directive_parse(
			context, line, i))
		{
			FCT_DEBUG_ERROR_TOKEN(&tokens[i],
				"Failed to parse directive.");
			return false;
		}
	}
	else
	{
		if (!context->arch)
		{
			FCT_DEBUG_ERROR_TOKEN(&tokens[i],
				"Code cannot come before arch definition.");
			return false;
		}

		if (fct_asm_context_assemble_line(
			context, &tokens[i]) == 0)
		{
			FCT_DEBUG_ERROR_TOKEN(&tokens[i],
				"Failed to parse line.");
			return false;
		}
	}

	return true;
}

bool fct_asm_context_assemble(
	fct_asm_context_t* context,
	fct_lex_context_t* lex)
{
	if (!context || !lex)
		return false;

	const fct_lex_line_t* root
		= fct_lex_line_root(lex);

	return fct_lex_line_foreach(
		root, context, (void*)fct_asm_context__assemble);
}

bool fct_asm_context_assemble_file(
	fct_asm_context_t* context, const char* path)
{
	if (!context)
		return false;

	fct_lex_context_t* lex
		= fct_lex_tokenize_file(path);
	if (!lex) return false;

	bool success = fct_asm_context_assemble(context, lex);
	fct_lex_context_delete(lex);
	return success;
}



fct_object_t* fct_asm_context_object(
	fct_asm_context_t* context)
{
	if (!context)
		return NULL;

	if ((fct_list_count(context->labels) > 0)
		|| (fct_list_count(context->globals) > 0))
	{
		FCT_DEBUG_ERROR("Can't create object, dangling labels.");
		return NULL;
	}

	if (context->chunk)
	{
		FCT_DEBUG_ERROR(
			"Can't create object, final chunk doesn't end on a boundary.");
		return NULL;
	}

	fct_object_t* object
		= context->object;
	context->object = NULL;

	return object;
}



static bool fct_asm_context__export_label(
	fct_asm_context_t* context,
	fct_string_t* name, uint64_t address,
	uint64_t size, bool global)
{
	if (!context || !name)
		return false;

	fct_exsym_t* symbol
		= fct_exsym_create(
			name, (global ? NULL : context->scope), address, size);
	if (!symbol) return false;

	if (context->chunk_static
		? fct_object_exports_add_symbol(context->object, symbol)
		: fct_chunk_exports_add_symbol(context->chunk, symbol))
		return true;

	fct_exsym_delete(symbol);
	return false;
}

static bool fct_asm_context__import(
	fct_asm_context_t* context,
	fct_string_t* name,
	uint64_t address, uint64_t offset, uint64_t size,
	bool relative, bool arch_spec)
{
	if (!context || !context->chunk || !name)
		return false;

	fct_imsym_t* symbol
		= fct_imsym_create(
			name, context->scope,
			address, offset, size,
			relative, arch_spec);
	if (!symbol) return false;

	if (!fct_chunk_imports_add_symbol(
		context->chunk, symbol))
	{
		fct_imsym_delete(symbol);
		return false;
	}

	return true;
}

bool fct_asm_context__coalign(
	uint64_t a, uint64_t b, uint64_t* c)
{

	uint64_t min = (a < b ? a : b);
	uint64_t max = (a > b ? a : b);

	if ((min <= 1)
		|| (min == max)
		|| ((max % min) == 0))
	{
		*c = max;
		return true;
	}

	if (((min & (min - 1)) == 0)
		&& ((max & (max - 1)) == 0))
	{
		*c = max;
		return true;
	}

	/* TODO - Better co-alignment for non-powers of two
	          using euclidian algorithm. */

	if (((a >> 32) == 0)
		&& ((b >> 32) == 0))
	{
		uint64_t mult = (a * b);
		if ((mult >> 32) != 0)
			return false;
		*c = mult;
		return true;
	}

	return false;
}

bool fct_asm_context_emit(
	fct_asm_context_t* context,
	const void* data, uint64_t size, uint64_t align,
	fct_chunk_access_e  access,
	fct_chunk_content_e content,
	fct_string_t* symbol, uint64_t sym_off, bool sym_rel, bool sym_as,
	unsigned boundary)
{
	if (!context)
		return false;

	if (!context->chunk)
	{
		if (!fct_asm_context__coalign(
			align, context->chunk_align, &align))
			return false;

		if (!context->chunk_static
			&& (fct_list_count(context->labels) == 0)
			&& (fct_list_count(context->globals) == 0))
		{
			FCT_DEBUG_ERROR("Can't create chunk, unreachable.");
			return false;
		}

		context->chunk = fct_chunk_create(
			context->arch, access, content, align);
		if (!context->chunk)
			return false;
	}
	else
	{
		if ((context->chunk->access != access)
			|| (context->chunk->content != content))
			return false;

		if (align > 1)
		{
			uint64_t csize = fct_chunk_size_get(context->chunk);
			if (fct_chunk_is_static(context->chunk))
			{
				uint64_t addr = context->chunk->address + csize;
				if ((addr % align) != 0)
					return false;
			}
			else if ((context->chunk->align <= 1)
				|| ((context->chunk->align % align) != 0)
				|| ((csize % align) != 0))
				return false;
		}
	}

	uint64_t csize = fct_chunk_size_get(context->chunk);

	if (!fct_chunk_append(
		context->chunk, data, size))
		return false;

	bool success = true;

	if ((fct_list_count(context->globals) > 0)
		|| (fct_list_count(context->labels) > 0))
	{
		uint64_t address = csize;
		if (context->chunk_static)
		{
			if (!context->object)
			{
				context->object = fct_object_create();
				if (!context->object)
					return false;
			}

			address += context->chunk_address;
		}


		bool global;
		bool func(fct_string_t* label, void* param)
		{
			(void)param;
			if (!fct_asm_context__export_label(
				context, label, address,
				fct_arch_addr_size(context->arch), global))
				success = false;

			return true;
		}

		global = true;
		fct_list_foreach(
			context->globals, NULL, (void*)func);

		global = false;
		fct_list_foreach(
			context->labels, NULL, (void*)func);

		fct_list_delete(context->labels);
		fct_list_delete(context->globals);
		context->labels  = NULL;
		context->globals = NULL;
	}

	if (symbol && !fct_asm_context__import(
		context, symbol, csize, sym_off, size, sym_rel, sym_as))
		success = false;

	if (boundary > context->boundary)
		context->boundary = boundary;

	if (context->boundary > 0)
	{
		context->boundary -= size;
		if ((context->boundary == 0)
			&& !fct_asm_context__boundary(context))
			success = false;
	}

	return success;
}



bool fct_asm_context_export_label(
	fct_asm_context_t* context, const char* name, bool global)
{
	if (!context)
		return false;

	if (global && !context->globals)
	{
		context->globals
			= fct_array_create(fct_class_string);
		if (!context->globals)
			return false;
	}
	else if (!global && !context->labels)
	{
		context->labels
			= fct_array_create(fct_class_string);
		if (!context->labels)
			return false;
	}

	fct_list_t* list = (global
		? context->globals
		: context->labels);

	fct_string_t* label
		= fct_string_create(name);
	if (!label) return false;

	if (!fct_list_add(list, label))
	{
		fct_string_delete(label);
		return false;
	}

	return true;
}

bool fct_asm_context_address(fct_asm_context_t* context, uint64_t address)
{
	if (!context || context->chunk)
		return false;

	if (context->chunk_static)
		return (context->chunk_address == address);

	if ((context->chunk_align != 0)
		&& ((address % context->chunk_align) != 0))
		return false;

	context->chunk_static  = true;
	context->chunk_address = address;
	return true;
}

bool fct_asm_context_align(fct_asm_context_t* context, uint64_t alignment)
{
	if (!context
		|| (alignment == 0)
		|| context->chunk)
		return false;

	if (context->chunk_static)
		return ((context->chunk_address % alignment) == 0);

	if (!fct_asm_context__coalign(
		alignment, context->chunk_align, &alignment))
		return false;

	context->chunk_align = alignment;
	return true;
}



bool fct_asm_context_type_add(
	fct_asm_context_t* context, fct_type_t* type)
{
	if (!context || !type)
		return false;
	return fct_list_add(context->types, type);
}

static bool fct_asm_context__type_find(fct_type_t* type, const char* name)
{
	fct_string_t* tn = fct_type_name(type);
	if (!name || !tn) return false;
	return fct_string_compare(tn,
		FCT_STRING_STATIC(name));
}

fct_type_t* fct_asm_context_type_find(
	fct_asm_context_t* context, const char* name)
{
	if (!context || !name)
		return NULL;
	return fct_list_find(context->types,
			(void*)name, (void*)fct_asm_context__type_find);
}



bool fct_asm_context_symbol_resolve(
	fct_asm_context_t* context, fct_string_t* name,
	bool relative, uint64_t offset,
	uint64_t* value, uint64_t* size)
{
	if (!context)
		return false;

	uint64_t address = fct_chunk_size_get(context->chunk);
	return fct_chunk_symbol_resolve(
		context->chunk, context->object, name,
		relative, address, offset,
		value, size);
}
