#ifndef __FCT_ASM_DIRECTIVE_H__
#define __FCT_ASM_DIRECTIVE_H__

#include "context.h"

bool fct_asm_directive_parse(
	fct_asm_context_t* context,
	const fct_lex_line_t* line,
	unsigned offset);

#endif
