#ifndef __FCT_ASM_FIELD_H__
#define __FCT_ASM_FIELD_H__

typedef enum
{
	FCT_ASM_FIELD_TYPE_REGISTER,
	FCT_ASM_FIELD_TYPE_IMMEDIATE_UNSIGNED,
	FCT_ASM_FIELD_TYPE_IMMEDIATE_SIGNED,
	FCT_ASM_FIELD_TYPE_VECTOR_UNSIGNED,
	FCT_ASM_FIELD_TYPE_VECTOR_SIGNED,
	FCT_ASM_FIELD_TYPE_SYMBOL,
	FCT_ASM_FIELD_TYPE_COMMAND,

	FCT_ASM_FIELD_TYPE_COUNT
} fct_asm_field_type_e;

typedef enum
{
	FCT_ASM_FIELD_XFORM_NONE = 0,
	FCT_ASM_FIELD_XFORM_NOT,
	FCT_ASM_FIELD_XFORM_INVERT,
	FCT_ASM_FIELD_XFORM_NEGATE,

	FCT_ASM_FIELD_XFORM_COUNT
} fct_asm_field_xform_e;

typedef struct fct_asm_field_s fct_asm_field_t;


#include <fct/types.h>
#include <fct/arch.h>
#include <fct/lex.h>

struct fct_asm_field_s
{
	fct_asm_field_type_e  type;
	fct_asm_field_xform_e xform;

	union
	{
		struct
		{
			unsigned         reg;
			const fct_tok_t* name;
		} reg;

		struct
		{
			unsigned size;
			uint64_t value;
		} immediate;

		struct
		{
			fct_string_t* name;
		} symbol;

		struct
		{
			unsigned  size;
			unsigned  count;
			uint64_t* value;
		} vector;

		struct
		{
			fct_string_t*    mnem;
			fct_asm_field_t* field;
		} command;

		const char* file;
		unsigned    row, col;
	};
};


fct_asm_field_t* fct_asm_field_create_register(
	unsigned reg, const fct_tok_t* name);
fct_asm_field_t* fct_asm_field_create_immediate(
	unsigned size, unsigned count, uint64_t* values, bool sign);
fct_asm_field_t* fct_asm_field_create_symbol(
	fct_string_t* name);
fct_asm_field_t* fct_asm_field_create_command(
	fct_string_t* mnem, fct_asm_field_t* field);

void fct_asm_field_delete(fct_asm_field_t* field);

fct_string_t* fct_asm_field_dump(
	fct_asm_field_t* field,
	fct_arch_t* arch);

bool fct_asm_field_not(fct_asm_field_t* field);
bool fct_asm_field_invert(fct_asm_field_t* field);
bool fct_asm_field_negate(fct_asm_field_t* field);


#include <fct/lex.h>

unsigned fct_asm_field_parse(
	fct_arch_t* arch,
	const fct_tok_t* src,
	fct_asm_field_t** field);


#include <fct/debug.h>

#define FCT_DEBUG_ERROR_FIELD(f, m...) \
	fct_debug_log_at_field(f, FCT_DEBUG_LOG_TYPE_ERROR, m)
#define FCT_DEBUG_WARN_FIELD(t, m...) \
	fct_debug_log_at_field(f, FCT_DEBUG_LOG_TYPE_WARNING, m)

void fct_debug_log_at_field(const fct_asm_field_t* field,
	fct_debug_log_type_e type, const char* format, ...);

#endif
