#include "directive.h"

#include <stdlib.h>
#include <fct/types.h>
#include <fct/object.h>
#include <fct/debug.h>
#include <fct/lang.h>
#include <fct/util/bitaddr.h>
#include <fct/parse.h>



static bool fct_asm_directive_parse_arch(
	fct_asm_context_t* context,
	const fct_lex_line_t* line,
	unsigned offset)
{
	if (!context || !line)
		return false;

	const fct_tok_t* src
		= fct_lex_line_tokens_contiguous(line);
	if (!src) return false;
	src = &src[offset + 2];

	if (src[0].type != FCT_TOK_IDENT)
		return false;

	unsigned i, len;
	for (i = 1, len = src[0].size;
		src[i].type == FCT_TOK_MEMBER; i += 2)
	{
		if (src[i + 1].type != FCT_TOK_IDENT)
			return false;
		len += src[i].size + src[i + 1].size;
	}

	if (src[i].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Expected EOL after arch definition.");
		return false;
	}

	fct_arch_t* arch
		= fct_arch_by_name(
			FCT_STRING_STATIC_SECTION(src[0].base, len));
	if (!arch)
	{
		FCT_DEBUG_ERROR_TOKEN(src,
			"Unknown arch.");
		return false;
	}

	if ((context->arch)
		&& (context->arch != arch))
	{
		FCT_DEBUG_ERROR_TOKEN(src,
			"Multiple differing arch directives.");
		return false;
	}

	if (!context->object)
	{
		context->object = fct_object_create();
		if (!context->object)
		{
			FCT_DEBUG_ERROR_TOKEN(src,
				"Failed to create object.");
			return false;
		}
	}

	context->arch = arch;
	return true;
}

static bool fct_asm_directive_parse_symbol(
	fct_asm_context_t* context,
	const fct_lex_line_t* line,
	unsigned offset)
{
	if (!context
		|| !context->object
		|| !line)
		return false;

	const fct_tok_t* src
		= fct_lex_line_tokens_contiguous(line);
	if (!src) return false;
	src = &src[offset + 2];

	if (src[0].type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(src,
			"Expected identifier after symbol directive.");
		return false;
	}

	unsigned i = 1;
	bool global = (src[i].type == FCT_TOK_EXCLAMATION);
	if (global) i++;

	if (src[i].type != FCT_TOK_INTEGER)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Symbol must have an integer value.");
		return false;
	}

	if (src[i + 1].type != FCT_TOK_END)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i + 1],
			"Expected EOL after symbol definition.");
		return false;
	}

	unsigned size = fct_arch_mau(context->arch)
		* fct_arch_addr_size(context->arch);

	uint64_t value;
	unsigned bits;
	unsigned len = fct_parse_immediate(
		src[i].base, &value, &bits, NULL);
	if ((len != src[i].size)
		|| (bits > size))
		return false;

	fct_string_t* namestr = fct_tok_to_string(src);
	fct_exsym_t* symbol = fct_exsym_create(
		namestr, (global ? NULL : context->scope),
		value, fct_arch_addr_size(context->arch));
	fct_string_delete(namestr);
	if (!symbol) return false;

	if (fct_object_exports_add_symbol(
		context->object, symbol))
		return true;

	fct_exsym_delete(symbol);
	return false;
}

static bool fct_asm_directive_parse_type(
	fct_asm_context_t* context,
	const fct_lex_line_t* line,
	unsigned offset)
{
	if (!context)
		return false;

	fct_type_t* type
		= fct_type_def_parse(
			line, offset);
	if (!type) return false;

	if (!fct_asm_context_type_add(
		context, type))
	{
		fct_type_delete(type);
		return false;
	}

	return true;
}

static bool fct_asm_directive_parse_decl(
	fct_asm_context_t* context,
	const fct_lex_line_t* line,
	unsigned offset)
{
	if (!context || !line)
		return false;

	const fct_tok_t* src
		= fct_lex_line_tokens_contiguous(line);
	if (!src) return false;
	src = &src[offset + 2];

	if (!context->labels)
	{
		FCT_DEBUG_ERROR_TOKEN(src,
			"Declaration without label is inaccessable.");
		return false;
	}

	unsigned i = 0;

	fct_chunk_access_e access
		= FCT_CHUNK_ACCESS_RW;
	if (fct_tok_keyword(&src[i], "const"))
	{
		i += 1;
		access &= ~FCT_CHUNK_ACCESS_W;
	}

	const fct_tok_t* type_tok = &src[i++];
	if (type_tok->type != FCT_TOK_IDENT)
	{
		FCT_DEBUG_ERROR_TOKEN(type_tok,
			"Expected type name in declaration.");
		return false;
	}

	char type_name[type_tok->size + 1];
	memcpy(type_name, type_tok->base, type_tok->size);
	type_name[type_tok->size] = '\0';

	fct_type_t* type
		= fct_asm_context_type_find(context, type_name);
	if (!type)
	{
		FCT_DEBUG_ERROR_TOKEN(type_tok,
			"Unknown type '%s' in declaration.", type_name);
		return false;
	}

	bool     array        = false;
	bool     array_static = false;
	uint64_t array_count  = 0;
	if (src[i].type == FCT_TOK_LBRACKET)
	{
		i += 1;

		array_static = fct_tok_uint64(
			&src[i], &array_count);
		if (array_static) i += 1;

		if (src[i].type != FCT_TOK_RBRACKET)
		{
			FCT_DEBUG_ERROR_TOKEN(&src[i],
				"Array count brackets unclosed.");
			return false;
		}
		i += 1;

		array = true;
	}

	unsigned type_size, type_align;
	if (!fct_type_size_align(
		type, context->arch,
		&type_size, &type_align))
		return false;

	if (src[i].type != FCT_TOK_END)
	{
		unsigned max_width = type_size
			* fct_arch_mau(context->arch);

		if (!array)
		{
			uint64_t         number = 0;
			const fct_tok_t* symbol = NULL;

			if (src[i].type == FCT_TOK_INTEGER)
			{
				unsigned bits;
				unsigned len = fct_parse_immediate(
					src[i].base, &number, &bits, NULL);
				if ((len != src[i].size)
					|| (bits > max_width))
					return false;
			}
			else if (src[i].type == FCT_TOK_IDENT)
			{
				symbol = &src[i];
			}
			else
			{
				FCT_DEBUG_ERROR_TOKEN(&src[i],
					"Expected initializer after declaration.");
			}
			i += 1;

			if (src[i].type != FCT_TOK_END)
			{
				FCT_DEBUG_ERROR_TOKEN(&src[i],
					"Expected EOL after declaration.");
				return false;
			}

			if (symbol)
			{
				fct_string_t* name = fct_string_static(
					symbol->base, symbol->size);

				bool success = fct_asm_context_emit(
					context, &number, type_size, type_align,
					access, FCT_CHUNK_CONTENT_DATA,
					name, 0, false, false, type_size);

				fct_string_delete(name);
				if (!success)
					return false;
			}
			else
			{
				if (!fct_asm_context_emit(
					context, &number, type_size, type_align,
					access,
					(number == 0
						? FCT_CHUNK_CONTENT_ZERO
						: FCT_CHUNK_CONTENT_DATA),
					NULL, 0, false, false, type_size))
					return false;
			}
		}
		else if (fct_tok_keyword(&src[i], "import"))
		{
			i += 1;

			char* path = fct_tok_path(&src[i]);
			if (!path)
			{
				FCT_DEBUG_ERROR_TOKEN(&src[i],
					"Invalid decl initializer import path");
				return false;
			}
			i += 1;

			if (src[i].type != FCT_TOK_END)
			{
				FCT_DEBUG_ERROR_TOKEN(&src[i],
					"Expected EOL after declaration.");
				return false;
			}

			fct_buffer_t* buffer
				= fct_buffer_file_import(path);
			if (!buffer)
			{
				FCT_DEBUG_ERROR_TOKEN(&src[i],
					"Failed to import decl initializer '%s'.", path);
				free(path);
				return false;
			}

			uint64_t buffer_size
				= fct_buffer_size_get(buffer);
			if (((buffer_size * 8) % max_width) >= 8)
			{
				FCT_DEBUG_WARN_TOKEN(&src[i],
					"Data import file '%s' size doesn't match type.", path);
			}
			free(path);

			uint64_t count = ((buffer_size * 8) / max_width);
			uint64_t size = (type_size * count);

			bool success = fct_asm_context_emit(
				context, fct_buffer_data_get(buffer), size, type_align,
				access, FCT_CHUNK_CONTENT_DATA,
				NULL, 0, false, false, size);
			fct_buffer_delete(buffer);

			return success;
		}
		else
		{
			uint64_t* array_data = NULL;
			unsigned  count = array_count;
			bool      zero;

			unsigned len = fct_parse_array_initializer(
				&src[i], max_width, true,
				&array_data, array_static, &count, &zero);
			if (len == 0)
			{
				FCT_DEBUG_ERROR_TOKEN(&src[i],
					"Expected array initializer after declaration.");
				return false;
			}
			i += len;

			if (src[i].type != FCT_TOK_END)
			{
				FCT_DEBUG_ERROR_TOKEN(&src[i],
					"Expected EOL after declaration.");
				return false;
			}

			unsigned size = (type_size * count);

			if (!zero)
			{
				uint8_t buff[((count * max_width) + 7) >> 3];

				unsigned k, o;
				for (k = 0, o = 0; k < count; k++, o += max_width)
					bitaddr_copy(buff, o, (void*)&array_data[k], 0, max_width);

				free(array_data);

				if (!fct_asm_context_emit(
					context, buff, size, type_align,
					access, FCT_CHUNK_CONTENT_DATA,
					NULL, 0, false, false, size))
					return false;
			}
			else
			{
				free(array_data);
				if (!fct_asm_context_emit(
					context, NULL, size, type_align,
					access, FCT_CHUNK_CONTENT_ZERO,
					NULL, 0, false, false, size))
					return false;
			}
		}

		return true;
	}

	if (array && !array_static)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Can't create unsized array without initializer.");
		return false;
	}

	unsigned size = type_size;
	if (array) size *= array_count;

	if (!fct_asm_context_emit(
		context, NULL, size, type_align,
		access, FCT_CHUNK_CONTENT_UNINITIALIZED,
		NULL, 0, false, false, size))
		return false;

	return true;
}

static bool fct_asm_directive_parse_import(
	fct_asm_context_t* context,
	const fct_lex_line_t* line,
	unsigned offset)
{
	if (!context || !line)
		return false;

	const fct_tok_t* src
		= fct_lex_line_tokens_contiguous(line);
	if (!src) return false;
	src = &src[offset + 2];

	unsigned i;
	for (i = 0; src[i].type != FCT_TOK_END; i++)
	{
		char* path = fct_tok_path(&src[i]);
		if (!path)
		{
			FCT_DEBUG_ERROR_TOKEN(&src[i],
				"Invalid import path.");
			return false;
		}

		bool success = fct_asm_job_list_add(
			context->job_list, path);
		free(path);

		if (!success)
			return false;
	}

	return true;
}



typedef struct
{
	const char* name;
	bool (*func)(fct_asm_context_t*,
		const fct_lex_line_t*, unsigned);
} fct_asm__directive_t;

bool fct_asm_directive_parse(
	fct_asm_context_t* context,
	const fct_lex_line_t* line,
	unsigned offset)
{
	if (!context || !line)
		return false;

	const fct_tok_t* src
		= fct_lex_line_tokens_contiguous(line);
	if (!src) return false;

	unsigned i;
	for (i = 0; i < offset; i++)
	{
		if (src[i].type == FCT_TOK_END)
			return false;
	}

	if (src[i++].type != FCT_TOK_DIRECTIVE)
		return false;

	static fct_asm__directive_t directives[] =
	{
		{ "arch"   , fct_asm_directive_parse_arch    },
		{ "symbol" , fct_asm_directive_parse_symbol  },
		{ "type"   , fct_asm_directive_parse_type    },
		{ "decl"   , fct_asm_directive_parse_decl    },
		{ "import" , fct_asm_directive_parse_import  },
		{ NULL, NULL }
	};

	unsigned k;
	for (k = 0; directives[k].name
		&& !fct_tok_keyword(&src[i], directives[k].name); k++);
	if (!directives[k].name)
	{
		FCT_DEBUG_ERROR_TOKEN(&src[i],
			"Unknown directive '%.*s'.", src[i].size, src[i].base);
		return false;
	}

	return directives[k].func(
		context, line, offset);
}
