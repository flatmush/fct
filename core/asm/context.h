#ifndef __FCT_ASM_CONTEXT_H__
#define __FCT_ASM_CONTEXT_H__

typedef struct fct_asm_context_s fct_asm_context_t;

#include <fct/arch.h>
#include <fct/lang.h>
#include <fct/asm/job.h>
#include <fct/object.h>
#include <fct/lex.h>



struct fct_asm_context_s
{
	fct_asm_job_list_t* job_list;

	fct_arch_t* arch;

	fct_string_t* scope;

	fct_object_t* object;

	fct_chunk_t* chunk;
	bool         chunk_static;
	uint64_t     chunk_address;
	uint64_t     chunk_align;

	fct_list_t* labels;
	fct_list_t* globals;

	fct_list_t* types;

	unsigned boundary;
};



fct_asm_context_t* fct_asm_context_create(
	fct_asm_job_list_t* job_list, const char* path);
void fct_asm_context_delete(fct_asm_context_t* context);

bool fct_asm_context_assemble_string(
	fct_asm_context_t* context, fct_string_t* src);
bool fct_asm_context_assemble_file(
	fct_asm_context_t* context, const char* path);

fct_object_t* fct_asm_context_object(
	fct_asm_context_t* context);

bool fct_asm_context_emit(
	fct_asm_context_t* context,
	const void* data, uint64_t size, uint64_t align,
	fct_chunk_access_e  access,
	fct_chunk_content_e content,
	fct_string_t* symbol, uint64_t sym_off, bool sym_rel, bool sym_as,
	unsigned boundary);

static inline bool fct_asm_context_emit_code(
	fct_asm_context_t* context,
	const void* data, uint64_t size, uint64_t align,
	unsigned boundary)
{
	return fct_asm_context_emit(
		context, data, size, align,
		FCT_CHUNK_ACCESS_RX,
		FCT_CHUNK_CONTENT_DATA,
		NULL, 0, false, false,
		boundary);
}

bool fct_asm_context_export_label(
	fct_asm_context_t* context, const char* name, bool global);
bool fct_asm_context_address(
	fct_asm_context_t* context, uint64_t address);
bool fct_asm_context_align(
	fct_asm_context_t* context, uint64_t alignment);

bool fct_asm_context_type_add(
	fct_asm_context_t* context, fct_type_t* type);
fct_type_t* fct_asm_context_type_find(
	fct_asm_context_t* context, const char* name);

bool fct_asm_context_symbol_resolve(
	fct_asm_context_t* context, fct_string_t* name,
	bool relative, uint64_t offset,
	uint64_t* value, uint64_t* size);

#endif
