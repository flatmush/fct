#include "job.h"

#include <stdlib.h>
#include <string.h>

#include <fct/asm/context.h>
#include <fct/debug.h>



typedef struct fct_asm_job_s fct_asm_job_t;

struct fct_asm_job_s
{
	char*         path;
	fct_object_t* object;
};

static fct_asm_job_t* fct_asm_job_create(const char* path)
{
	fct_asm_job_t* job
		= (fct_asm_job_t*)malloc(
			sizeof(fct_asm_job_t));
	if (!job) return NULL;
	job->path   = strdup(path);
	job->object = NULL;

	if (!job->path)
	{
		free(job);
		return NULL;
	}

	return job;
}

static void fct_asm_job_delete(fct_asm_job_t* job)
{
	if (!job)
		return;
	fct_object_delete(job->object);
	free(job->path);
	free(job);
}

static const fct_class_t fct_class_asm__job =
{
	.delete_cb    = (void*)fct_asm_job_delete,
	.reference_cb = NULL,
	.copy_cb      = NULL,
	.import_cb    = NULL,
	.export_cb    = NULL,
};
static const fct_class_t* fct_class_asm_job
	= &fct_class_asm__job;



fct_asm_job_list_t* fct_asm_job_list_create(void)
{
	return fct_array_create(fct_class_asm_job);
}

void fct_asm_job_list_delete(fct_asm_job_list_t* list)
{
	fct_list_delete(list);
}



static bool fct_asm_job_list__find(
	fct_asm_job_t* job, const char* path)
{
	if (!job || !path)
		return false;
	return (strcmp(job->path, path) == 0);
}

bool fct_asm_job_list_add(fct_asm_job_list_t* list, const char* path)
{
	if (!path)
		return false;

	if (fct_list_find(list, (void*)path,
		(void*)fct_asm_job_list__find))
		return true;

	fct_asm_job_t* job
		= fct_asm_job_create(path);
	if (!job) return false;

	if (fct_list_add(list, job))
		return true;

	fct_asm_job_delete(job);
	return false;
}



static bool fct_asm_job_list__link(
	fct_asm_job_t* job,
	fct_object_t** object)
{
	if (!object)
		return false;

	if (!job || !job->object)
	{
		fct_object_delete(*object);
		*object = NULL;
		return false;
	}

	if (*object)
	{
		fct_object_t* lobject
			= fct_object_merge(*object, job->object);
		fct_object_delete(*object);
		*object = lobject;
	}
	else
	{
		*object = fct_object_copy(job->object);
	}

	return (*object != NULL);
}

static bool fct_asm_job_list__assemble(
	fct_asm_job_t* job,
	fct_asm_job_list_t* list)
{
	if (!job)
		return false;

	if (job->object)
		return true;

	fct_asm_context_t* context
		= fct_asm_context_create(list, job->path);
	if (!context)
	{
		FCT_DEBUG_ERROR_FILE(
			job->path, "Failed to create assembler context");
		return false;
	}

	if (!fct_asm_context_assemble_file(
		context, job->path))
	{
		FCT_DEBUG_ERROR_FILE(
			job->path, "Failed to assemble");
		fct_asm_context_delete(context);
		return false;
	}

	job->object = fct_asm_context_object(context);
	fct_asm_context_delete(context);

	if (!job->object)
	{
		FCT_DEBUG_ERROR_FILE(
			job->path, "Failed to create object");
		return false;
	}

	return true;
}

fct_object_t* fct_asm_job_list_process(fct_asm_job_list_t* list)
{
	unsigned count
		= fct_list_count(list);

	while (true)
	{
		if (!fct_list_foreach(list, list,
			(void*)fct_asm_job_list__assemble))
			return NULL;

		unsigned ncount = fct_list_count(list);
		if (ncount == count)
			break;
		count = ncount;
	}

	fct_object_t* object = NULL;
	fct_list_foreach(list, &object,
		(void*)fct_asm_job_list__link);
	return object;
}
