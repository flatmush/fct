#ifndef __FCT_DEBUG_H__
#define __FCT_DEBUG_H__

#include <fct/types.h>
#include <stdarg.h>

typedef enum
{
	FCT_DEBUG_LOG_TYPE_INFO,
	FCT_DEBUG_LOG_TYPE_WARNING,
	FCT_DEBUG_LOG_TYPE_ERROR,
	FCT_DEBUG_LOG_TYPE_CRITICAL,

	FCT_DEBUG_LOG_TYPE_COUNT
} fct_debug_log_type_e;


void fct_debug_print(void);

void fct_debug_logv(
	const char* file, unsigned line, unsigned offset,
	fct_debug_log_type_e type, const char* format, va_list args);
void fct_debug_log(
	const char* file, unsigned line, unsigned offset,
	fct_debug_log_type_e type, const char* format, ...);

unsigned fct_debug_state(void);
void fct_debug_revert(unsigned state);


#define FCT_DEBUG_INFO(m...) \
	fct_debug_log(NULL, 0, 0, FCT_DEBUG_LOG_TYPE_INFO, m)
#define FCT_DEBUG_WARN(m...) \
	fct_debug_log(NULL, 0, 0, FCT_DEBUG_LOG_TYPE_WARNING, m)
#define FCT_DEBUG_ERROR(m...) \
	fct_debug_log(NULL, 0, 0, FCT_DEBUG_LOG_TYPE_ERROR, m)
#define FCT_DEBUG_CRITICAL(m...) \
	fct_debug_log(NULL, 0, 0, FCT_DEBUG_LOG_TYPE_CRITICAL, m)

#define FCT_DEBUG_INFO_FILE(f, m...) \
	fct_debug_log(f, 0, 0, FCT_DEBUG_LOG_TYPE_INFO, m)
#define FCT_DEBUG_WARN_FILE(f, m...) \
	fct_debug_log(f, 0, 0, FCT_DEBUG_LOG_TYPE_WARNING, m)
#define FCT_DEBUG_ERROR_FILE(f, m...) \
	fct_debug_log(f, 0, 0, FCT_DEBUG_LOG_TYPE_ERROR, m)
#define FCT_DEBUG_CRITICAL_FILE(f, m...) \
	fct_debug_log(f, 0, 0, FCT_DEBUG_LOG_TYPE_CRITICAL, m)

#define FCT_DEBUG_INFO_FILE_LINE(f, l, m...) \
	fct_debug_log(f, l, 0, FCT_DEBUG_LOG_TYPE_INFO, m)
#define FCT_DEBUG_WARN_FILE_LINE(f, l, m...) \
	fct_debug_log(f, l, 0, FCT_DEBUG_LOG_TYPE_WARNING, m)
#define FCT_DEBUG_ERROR_FILE_LINE(f, l, m...) \
	fct_debug_log(f, l, 0, FCT_DEBUG_LOG_TYPE_ERROR, m)
#define FCT_DEBUG_CRITICAL_FILE_LINE(f, l, m...) \
	fct_debug_log(f, l, 0, FCT_DEBUG_LOG_TYPE_CRITICAL, m)

#define FCT_DEBUG_INFO_FILE_POS(f, l, p, m...) \
	fct_debug_log(f, l, p, FCT_DEBUG_LOG_TYPE_INFO, m)
#define FCT_DEBUG_WARN_FILE_POS(f, l, p, m...) \
	fct_debug_log(f, l, p, FCT_DEBUG_LOG_TYPE_WARNING, m)
#define FCT_DEBUG_ERROR_FILE_POS(f, l, p, m...) \
	fct_debug_log(f, l, p, FCT_DEBUG_LOG_TYPE_ERROR, m)
#define FCT_DEBUG_CRITICAL_FILE_POS(f, l, p, m...) \
	fct_debug_log(f, l, p, FCT_DEBUG_LOG_TYPE_CRITICAL, m)

#endif
