#ifndef __FCT_MAUBUFF_ZERO_H__
#define __FCT_MAUBUFF_ZERO_H__

#include <fct/maubuff.h>

fct_maubuff_t* fct_maubuff_zero_create(unsigned mau);

bool fct_maubuff_is_zero(fct_maubuff_t* buffer);

#endif
