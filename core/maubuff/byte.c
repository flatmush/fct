#include <fct/maubuff.h>
#include "maubuff.h"
#include <stdlib.h>


typedef struct
{
	unsigned  mau;
	uint8_t*  data;
	uint32_t  size;
} fct_maubuff_byte__t;



static void fct_maubuff_byte__delete(
	fct_maubuff_byte__t* buffer)
{
	if (!buffer)
		return;
	free(buffer->data);
	free(buffer);
}



static fct_buffer_t* fct_maubuff_byte__to_buffer(
	fct_maubuff_byte__t* buffer)
{
	if (!buffer)
		return NULL;

	fct_buffer_t* b
		= fct_buffer_create();
	if (!b) return NULL;

	if (!fct_buffer_append(b, buffer->data,
		(buffer->size * buffer->mau)))
	{
		fct_buffer_delete(b);
		return NULL;
	}

	return b;
}



static unsigned fct_maubuff_byte__mau_get(
	fct_maubuff_byte__t* buffer)
{
	if (!buffer)
		return 0;
	return (buffer->mau * 8);
}

static uint32_t fct_maubuff_byte__size_get(
	fct_maubuff_byte__t* buffer)
{
	if (!buffer)
		return 0;
	return buffer->size;
}

static bool fct_maubuff_byte__append(
	fct_maubuff_byte__t* buffer,
	const void* data, uint32_t size)
{
	if (size == 0)
		return true;
	if (!buffer)
		return false;

	uint32_t nsize = (buffer->size + size) * buffer->mau;
	uint8_t* ndata
		= (uint8_t*)realloc(buffer->data, nsize);
	if (!ndata) return false;
	buffer->data = ndata;

	if (data)
		memcpy(&buffer->data[buffer->size * buffer->mau],
			data, (size * buffer->mau));
	else
		memset(&buffer->data[buffer->size * buffer->mau],
			0x00, (size * buffer->mau));
	buffer->size += size;
	return true;
}

static void fct_maubuff_byte__truncate(
	fct_maubuff_byte__t* buffer, uint32_t size)
{
	if (!buffer
		|| (buffer->size < size))
		return;

	buffer->size = size;

	uint8_t* ndata
		= (uint8_t*)realloc(
			buffer->data, size);
	if (ndata) buffer->data = ndata;
}

static bool fct_maubuff_byte__modify(
	fct_maubuff_byte__t* buffer,
	uint32_t offset, void* data, uint32_t size)
{
	if (size == 0)
		return true;
	if (!buffer
		|| ((offset + size) > buffer->size))
		return false;

	if (data)
		memcpy(&buffer->data[offset * buffer->mau],
			data, (size * buffer->mau));
	else
		memset(&buffer->data[offset * buffer->mau],
			0x00, (size * buffer->mau));
	return true;
}

static bool fct_maubuff_byte__read(
	fct_maubuff_byte__t* buffer,
	uint32_t offset, uint32_t size, void* data)
{
	if (size == 0)
		return true;
	if (!buffer
		|| ((offset + size) > buffer->size))
		return false;

	if (data)
		memcpy(data,
			&buffer->data[offset * buffer->mau],
			(size * buffer->mau));
	return true;
}

static fct_maubuff_t* fct_maubuff_byte__copy(
	fct_maubuff_byte__t* buffer)
{
	if (!buffer)
		return NULL;

	fct_maubuff_t* copy
		= fct_maubuff_byte_create(buffer->mau << 3);
	if (!copy) return NULL;

	if (!fct_maubuff_byte__append(
		copy->data, buffer->data, buffer->size))
	{
		fct_maubuff_delete(copy);
		return NULL;
	}

	return copy;
}



static bool fct_maubuff_byte__append_maubuff(
	fct_maubuff_byte__t* buffer, fct_maubuff_t* append)
{
	if (!buffer || !append)
		return false;

	unsigned amau = fct_maubuff_mau_get(append);
	if (((amau & 7) != 0)
		|| ((amau >> 3) != buffer->mau))
		return false;

	uint32_t size = fct_maubuff_size_get(append);

	uint32_t nsize = (buffer->size + size) * buffer->mau;
	uint32_t osize = buffer->size * buffer->mau;
	uint8_t* ndata = (uint8_t*)realloc(buffer->data, nsize);
	if (!ndata) return false;
	buffer->data = ndata;

	if (!fct_maubuff_read(append, 0, size, &buffer->data[osize]))
	{
		uint8_t* odata = (uint8_t*)realloc(buffer->data, osize);
		if (odata) buffer->data = odata;
		return false;
	}

	buffer->size += size;
	return true;
}



static fct_maubuff_interface_t
	_fct_maubuff_interface_byte =
	{
		.delete_cb         = (void*)fct_maubuff_byte__delete,
		.to_buffer_cb      = (void*)fct_maubuff_byte__to_buffer,
		.mau_get_cb        = (void*)fct_maubuff_byte__mau_get,
		.size_get_cb       = (void*)fct_maubuff_byte__size_get,
		.append_cb         = (void*)fct_maubuff_byte__append,
		.truncate_cb       = (void*)fct_maubuff_byte__truncate,
		.modify_cb         = (void*)fct_maubuff_byte__modify,
		.read_cb           = (void*)fct_maubuff_byte__read,
		.copy_cb           = (void*)fct_maubuff_byte__copy,
		.append_maubuff_cb = (void*)fct_maubuff_byte__append_maubuff,
	};



fct_maubuff_t* fct_maubuff_byte_create(unsigned mau)
{
	if ((mau == 0) || ((mau & 7) != 0))
		return NULL;

	fct_maubuff_t* buffer
		= (fct_maubuff_t*)malloc(sizeof(fct_maubuff_t));
	if (!buffer)
		return NULL;

	buffer->interface
		= &_fct_maubuff_interface_byte;

	fct_maubuff_byte__t* _buffer
		= (fct_maubuff_byte__t*)malloc(sizeof(fct_maubuff_byte__t));
	if (!_buffer)
	{
		free(buffer);
		return NULL;
	}
	buffer->data = _buffer;
	buffer->refcnt = 0;

	_buffer->mau  = (mau >> 3);
	_buffer->size = 0;
	_buffer->data = NULL;

	return buffer;
}



fct_maubuff_t* fct_maubuff_byte_from_buffer(
	fct_buffer_t* buffer, uintptr_t offset, unsigned mau, uint32_t size)
{
	if (!buffer
		|| (mau == 0)
		|| ((mau & 7) != 0))
		return NULL;

	fct_maubuff_t* b
		= fct_maubuff_byte_create(mau);
	if (!b) return NULL;

	fct_maubuff_byte__t* bb
		= (fct_maubuff_byte__t*)b->data;

	if (!fct_maubuff_byte__append(
		bb, NULL, size))
	{
		fct_maubuff_delete(b);
		return NULL;
	}

	if (!fct_buffer_read(buffer,
		offset, (size * bb->mau), bb->data))
	{
		fct_maubuff_delete(b);
		return NULL;
	}

	return b;
}
