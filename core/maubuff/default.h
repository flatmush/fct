#ifndef __FCT_MAUBUFF_DEFAULT_H__
#define __FCT_MAUBUFF_DEFAULT_H__

#include <fct/maubuff.h>

fct_maubuff_t* fct_maubuff_default_create(unsigned mau);

fct_maubuff_t* fct_maubuff_default_from_buffer(
	fct_buffer_t* buffer, uintptr_t offset, unsigned mau, uint32_t size);

#endif
