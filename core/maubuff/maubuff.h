#ifndef __FCT_MAUBUFF_BUFFER_H__
#define __FCT_MAUBUFF_BUFFER_H__

#include <fct/types.h>
#include <fct/maubuff.h>

struct fct_maubuff_interface_s
{
	void     (*delete_cb)(fct_maubuff_t* buffer);

	fct_buffer_t* (*to_buffer_cb)(fct_maubuff_t* buffer);

	unsigned (*mau_get_cb)(fct_maubuff_t* buffer);
	uint32_t (*size_get_cb)(fct_maubuff_t* buffer);

	bool (*append_cb)(fct_maubuff_t* buffer, const void* data, uint32_t size);
	bool (*truncate_cb)(fct_maubuff_t* buffer, uint32_t size);
	bool (*modify_cb)(fct_maubuff_t* buffer, uint32_t offset, void* data, uint32_t size);
	bool (*read_cb)(fct_maubuff_t* buffer, uint32_t offset, uint32_t size, void* data);

	fct_maubuff_t* (*copy_cb)(fct_maubuff_t* buffer);

	bool     (*append_maubuff_cb)(fct_maubuff_t* buffer, fct_maubuff_t* append);
};

struct fct_maubuff_s
{
	fct_maubuff_interface_t* interface;
	void* data;
	unsigned refcnt;
};

#endif
