#include <fct/maubuff.h>
#include "maubuff.h"
#include <fct/util/bitaddr.h>
#include <stdlib.h>



typedef struct
{
	unsigned  mau;
	uint8_t*  data;
	uint32_t  size;
} fct_maubuff_default__t;



static void fct_maubuff_default__delete(
	fct_maubuff_default__t* buffer)
{
	if (!buffer)
		return;
	free(buffer->data);
	free(buffer);
}



static fct_buffer_t* fct_maubuff_default__to_buffer(
	fct_maubuff_default__t* buffer)
{
	if (!buffer)
		return NULL;

	fct_buffer_t* b
		= fct_buffer_create();
	if (!b) return NULL;

	if (!fct_buffer_append(b, buffer->data,
		(((buffer->size * buffer->mau) + 7) >> 3)))
	{
		fct_buffer_delete(b);
		return NULL;
	}

	return b;
}



static unsigned fct_maubuff_default__mau_get(
	fct_maubuff_default__t* buffer)
{
	if (!buffer)
		return 0;
	return buffer->mau;
}

static uint32_t fct_maubuff_default__size_get(
	fct_maubuff_default__t* buffer)
{
	if (!buffer)
		return 0;
	return buffer->size;
}

static bool fct_maubuff_default__append(
	fct_maubuff_default__t* buffer,
	const void* data, uint32_t size)
{
	if (size == 0)
		return true;
	if (!buffer || !data)
		return false;

	uint32_t nsize = ((((buffer->size + size) * buffer->mau) + 7) >> 3);
	uint8_t* ndata = (uint8_t*)realloc(buffer->data, nsize);
	if (!ndata)
		return false;
	buffer->data = ndata;

	bitaddr_copy(buffer->data, (buffer->size * buffer->mau), data, 0, (size * buffer->mau));
	buffer->size += size;
	return true;
}

static void fct_maubuff_default__truncate(
	fct_maubuff_default__t* buffer, uint32_t size)
{
	if (!buffer
		|| (buffer->size < size))
		return;

	buffer->size = size;

	uint32_t nsize = (((buffer->size * buffer->mau) + 7) >> 3);
	uint8_t* ndata = (uint8_t*)realloc(buffer->data, nsize);
	if (ndata) buffer->data = ndata;
}

static bool fct_maubuff_default__modify(
	fct_maubuff_default__t* buffer,
	uint32_t offset, void* data, uint32_t size)
{
	if (size == 0)
		return true;
	if (!buffer
		|| ((offset + size) > buffer->size))
		return false;

	bitaddr_copy(buffer->data, (offset * buffer->mau), data, 0, (size * buffer->mau));
	return true;
}

static bool fct_maubuff_default__read(
	fct_maubuff_default__t* buffer,
	uint32_t offset, uint32_t size, void* data)
{
	if (size == 0)
		return true;
	if (!buffer
		|| ((offset + size) > buffer->size))
		return false;

	if (data)
		bitaddr_copy(data, 0, buffer->data,
			(offset * buffer->mau), (size * buffer->mau));
	return true;
}

static fct_maubuff_t* fct_maubuff_default__copy(
	fct_maubuff_default__t* buffer)
{
	if (!buffer)
		return NULL;

	fct_maubuff_t* copy
		= fct_maubuff_default_create(buffer->mau);
	if (!copy) return NULL;

	if (!fct_maubuff_default__append(
		copy->data, buffer->data, buffer->size))
	{
		fct_maubuff_delete(copy);
		return NULL;
	}

	return copy;
}



static bool fct_maubuff_default__append_maubuff(
	fct_maubuff_default__t* buffer, fct_maubuff_t* append)
{
	if (!buffer || !append)
		return false;

	if (buffer->mau != fct_maubuff_mau_get(append))
		return false;

	if ((buffer->size * buffer->mau) & 7)
	{
		/* TODO - Support non-byte-aligned buffer appends. */
		return false;
	}

	uint32_t size = fct_maubuff_size_get(append);

	uint32_t nsize = ((((buffer->size + size) * buffer->mau) + 7) >> 3);
	uint32_t osize = (((buffer->size * buffer->mau) + 7) >> 3);
	uint8_t* ndata = (uint8_t*)realloc(buffer->data, nsize);
	if (!ndata) return false;
	buffer->data = ndata;

	if (!fct_maubuff_read(append, 0, size,
		(void*)((uintptr_t)buffer->data + ((buffer->size * buffer->mau) >> 3))))
	{
		uint8_t* odata = (uint8_t*)realloc(buffer->data, osize);
		if (odata) buffer->data = odata;
		return false;
	}

	buffer->size += size;
	return true;
}



static fct_maubuff_interface_t
	_fct_maubuff_interface_default =
	{
		.delete_cb         = (void*)fct_maubuff_default__delete,
		.to_buffer_cb      = (void*)fct_maubuff_default__to_buffer,
		.mau_get_cb        = (void*)fct_maubuff_default__mau_get,
		.size_get_cb       = (void*)fct_maubuff_default__size_get,
		.append_cb         = (void*)fct_maubuff_default__append,
		.truncate_cb       = (void*)fct_maubuff_default__truncate,
		.modify_cb         = (void*)fct_maubuff_default__modify,
		.read_cb           = (void*)fct_maubuff_default__read,
		.copy_cb           = (void*)fct_maubuff_default__copy,
		.append_maubuff_cb = (void*)fct_maubuff_default__append_maubuff,
	};



fct_maubuff_t* fct_maubuff_default_create(unsigned mau)
{
	if ((mau & 7) == 0)
		return fct_maubuff_byte_create(mau);

	fct_maubuff_t* buffer
		= (fct_maubuff_t*)malloc(
			sizeof(fct_maubuff_t));
	if (!buffer) return NULL;

	buffer->interface
		= &_fct_maubuff_interface_default;

	fct_maubuff_default__t* _buffer
		= (fct_maubuff_default__t*)malloc(
			sizeof(fct_maubuff_default__t));
	if (!_buffer)
	{
		free(buffer);
		return NULL;
	}
	buffer->data = _buffer;
	buffer->refcnt = 0;

	_buffer->mau  = mau;
	_buffer->size = 0;
	_buffer->data = NULL;

	return buffer;
}



fct_maubuff_t* fct_maubuff_default_from_buffer(
	fct_buffer_t* buffer, uintptr_t offset, unsigned mau, uint32_t size)
{
	if (!buffer || (mau == 0))
		return NULL;

	fct_maubuff_t* b
		= fct_maubuff_default_create(mau);
	if (!b) return NULL;

	fct_maubuff_default__t* bb
		= (fct_maubuff_default__t*)b->data;

	if (!fct_maubuff_default__append(
		bb, NULL, size))
	{
		fct_maubuff_delete(b);
		return NULL;
	}

	if (!fct_buffer_read(buffer,
		offset, (((size * mau) + 7) >> 3), bb->data))
	{
		fct_maubuff_delete(b);
		return NULL;
	}

	return b;
}
