#include "maubuff.h"
#include <fct/debug.h>
#include <fct/util/bitaddr.h>
#include <stdlib.h>
#include <string.h>



static const fct_class_t
	fct_class__maubuff =
	{
		.delete_cb    = (void*)fct_maubuff_delete,
		.reference_cb = (void*)fct_maubuff_reference,
		.copy_cb      = (void*)fct_maubuff_copy,
		.import_cb    = (void*)fct_maubuff_import,
		.export_cb    = (void*)fct_maubuff_export,
	};
const fct_class_t* fct_class_maubuff
	= &fct_class__maubuff;



fct_maubuff_t* fct_maubuff_reference(fct_maubuff_t* buffer)
{
	if (!buffer)
		return NULL;

	if ((buffer->refcnt + 1) == 0)
		return fct_maubuff_copy(buffer);

	buffer->refcnt++;
	return buffer;
}



fct_maubuff_t* fct_maubuff_copy(fct_maubuff_t* buffer)
{
	if (!buffer)
		return NULL;

	if (buffer->interface
		&& buffer->interface->copy_cb)
		return buffer->interface->copy_cb(buffer->data);

	unsigned mau = fct_maubuff_mau_get(buffer);
	if (mau == 0) return NULL;

	fct_maubuff_t* copy
		= fct_maubuff_default_create(mau);
	if (!copy) return NULL;

	if (!fct_maubuff_append_maubuff(copy, buffer))
	{
		fct_maubuff_delete(copy);
		return NULL;
	}

	return copy;
}

void fct_maubuff_delete(fct_maubuff_t* buffer)
{
	if (!buffer)
		return;

	if (buffer->refcnt)
	{
		buffer->refcnt--;
		return;
	}

	if (buffer->interface && buffer->interface->delete_cb)
		buffer->interface->delete_cb(buffer->data);
	else
		free(buffer->data);
	free(buffer);
}



fct_maubuff_t* fct_maubuff_from_buffer(
	fct_buffer_t* buffer, uintptr_t offset, unsigned mau, uint32_t size)
{
	if (!buffer || (mau == 0))
		return NULL;

	if ((mau & 7) == 0)
		return fct_maubuff_byte_from_buffer(
			buffer, offset, mau, size);

	return fct_maubuff_default_from_buffer(
		buffer, offset, mau, size);
}

fct_buffer_t* fct_maubuff_to_buffer(fct_maubuff_t* buffer)
{
	if (!buffer
		|| !buffer->interface
		|| !buffer->interface->to_buffer_cb)
		return NULL;

	return buffer->interface->to_buffer_cb(buffer->data);
}



fct_string_t* fct_maubuff_to_string_hex(
	fct_maubuff_t* buffer, unsigned indent, unsigned width)
{
	if (!buffer
		|| !buffer->interface
		|| !buffer->interface->read_cb)
		return NULL;

	char indentstr[indent + 1];
	memset(indentstr, '\t', indent);
	indentstr[indent] = '\0';

	if (width == 0)
		width = 1;

	unsigned mau = fct_maubuff_mau_get(buffer);
	if (mau == 0) return NULL;

	fct_string_t* hexstr
		= fct_string_create("");
	if (!hexstr) return NULL;

	uint32_t size
		= fct_maubuff_size_get(buffer);
	if (size == 0)
		return hexstr;

	unsigned i;
	for (i = 0; i < size; i++)
	{
		uint8_t elem[(mau + 7) >> 3];

		if (!fct_maubuff_read(
			buffer, i, 1, elem))
		{
			fct_string_delete(hexstr);
			return NULL;
		}

		if ((i % width) == 0)
		{
			if (!fct_string_append_static_format(
				hexstr, "%s%s", (i > 0 ? "\n" : ""), indentstr))
			{
				fct_string_delete(hexstr);
				return NULL;
			}
		}
		else if (!fct_string_append_static_format(hexstr, " "))
		{
			fct_string_delete(hexstr);
			return NULL;
		}

		if (mau & 3)
			mau += 4 - (mau & 3);

		unsigned j, k;
		for (j = 0, k = (mau - 4); j < mau; j += 4, k -= 4)
		{
			uint8_t nibble = 0x00;
			bitaddr_copy(&nibble, 0, elem, k, 4);
			char digit = "0123456789ABCDEF"[nibble];
			if (!fct_string_append_static_format(
				hexstr, "%c", digit))
			{
				fct_string_delete(hexstr);
				return NULL;
			}
		}
	}

	if (!fct_string_append_static_format(hexstr, "\n"))
	{
		fct_string_delete(hexstr);
		return NULL;
	}

	return hexstr;
}



typedef struct
__attribute__((__packed__))
{
	uint64_t size;
	uint32_t mau, flags;
} fct_maubuff_store_t;

static const uint32_t fct_maubuff_store_flag_zero = (1U << 0);

fct_maubuff_t* fct_maubuff_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	(void)strtab;

	if (!buffer)
		return NULL;

	unsigned o = (offset ? *offset : 0);

	fct_maubuff_store_t header;
	if (!fct_buffer_read(buffer,
		o, sizeof(header), &header))
		return NULL;
	o += sizeof(header);

	if (header.size >> 32)
		return NULL;

	fct_maubuff_t* mbuff;
	if (header.flags & fct_maubuff_store_flag_zero)
	{
		mbuff = fct_maubuff_zero_create(header.mau);
		if (!fct_maubuff_append(mbuff, NULL, header.size))
		{
			fct_maubuff_delete(mbuff);
			return NULL;
		}
	}
	else
	{
		mbuff = fct_maubuff_from_buffer(
				buffer, o, header.mau, header.size);
		if (!mbuff) return NULL;
		o += fct_maubuff_byte_size_get(mbuff);
	}

	if (offset)
		*offset += o;
	return mbuff;
}

fct_buffer_t* fct_maubuff_export(
	fct_maubuff_t* buffer, fct_list_t* strtab)
{
	(void)strtab;

	if (!buffer)
		return NULL;

	fct_maubuff_store_t header;
	header.size  = fct_maubuff_size_get(buffer);
	header.mau   = fct_maubuff_mau_get(buffer);
	header.flags = 0;

	bool is_zero = fct_maubuff_is_zero(buffer);
	if (is_zero)
		header.flags |= fct_maubuff_store_flag_zero;

	if (header.mau == 0)
		return NULL;

	fct_buffer_t* store
		= fct_buffer_create();
	if (!store) return NULL;

	if (!fct_buffer_append(
		store, &header, sizeof(header)))
	{
		fct_buffer_delete(store);
		return NULL;
	}

	if (!is_zero)
	{
		fct_buffer_t* mbuffer
			= fct_maubuff_to_buffer(buffer);

		if (!mbuffer
			|| !fct_buffer_append_buffer(store, mbuffer))
		{
			fct_buffer_delete(mbuffer);
			fct_buffer_delete(store);
			return NULL;
		}

		fct_buffer_delete(mbuffer);
	}

	return store;
}



unsigned fct_maubuff_mau_get(fct_maubuff_t* buffer)
{
	if (!buffer
		|| !buffer->interface
		|| !buffer->interface->mau_get_cb)
		return 0;
	return buffer->interface->mau_get_cb(buffer->data);
}

uint32_t fct_maubuff_size_get(fct_maubuff_t* buffer)
{
	if (!buffer
		|| !buffer->interface
		|| !buffer->interface->size_get_cb)
		return 0;
	return buffer->interface->size_get_cb(buffer->data);
}

uint32_t fct_maubuff_byte_size_get(fct_maubuff_t* buffer)
{
	uint32_t size = fct_maubuff_size_get(buffer);
	uint32_t mau  = fct_maubuff_mau_get(buffer);
	return (((size * mau) + 7) >> 3);
}



bool fct_maubuff_append(
	fct_maubuff_t* buffer, const void* data, uint32_t size)
{
	if (size == 0)
		return true;
	if (!buffer
		|| !buffer->interface
		|| !buffer->interface->append_cb)
		return false;
	return buffer->interface->append_cb(buffer->data, data, size);
}

void fct_maubuff_truncate(
	fct_maubuff_t* buffer, uint32_t size)
{
	if (buffer
		&& buffer->interface
		&& buffer->interface->truncate_cb)
		buffer->interface->truncate_cb(buffer->data, size);
}

bool fct_maubuff_modify(
	fct_maubuff_t* buffer, uint32_t offset, void* data, uint32_t size)
{
	if (size == 0)
		return true;
	if (!buffer
		|| !buffer->interface
		|| !buffer->interface->modify_cb)
		return false;
	return buffer->interface->modify_cb(buffer->data, offset, data, size);
}

bool fct_maubuff_read(
	fct_maubuff_t* buffer, uint32_t offset, uint32_t size, void* data)
{
	if (size == 0)
		return true;
	if (!buffer || !data
		|| !buffer->interface
		|| !buffer->interface->read_cb)
		return false;
	return buffer->interface->read_cb(buffer->data, offset, size, data);
}



bool fct_maubuff_append_maubuff(
	fct_maubuff_t* buffer, fct_maubuff_t* append)
{
	if (!buffer || !append
		|| !buffer->interface
		|| !buffer->interface->append_maubuff_cb)
		return false;
	return buffer->interface->append_maubuff_cb(buffer->data, append);
}
