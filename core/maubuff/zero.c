#include <fct/maubuff.h>
#include "maubuff.h"
#include <fct/util/bitaddr.h>
#include <stdlib.h>


typedef struct
{
	unsigned  mau;
	uint32_t  size;
} fct_maubuff_zero__t;



static void fct_maubuff_zero__delete(
	fct_maubuff_zero__t* buffer)
{
	if (!buffer)
		return;
	free(buffer);
}



static fct_buffer_t* fct_maubuff_zero__to_buffer(
	fct_maubuff_zero__t* buffer)
{
	if (!buffer)
		return NULL;

	fct_buffer_t* b
		= fct_buffer_create();
	if (!b) return NULL;

	if (!fct_buffer_append(b, NULL,
		(((buffer->size * buffer->mau) + 7) >> 3)))
	{
		fct_buffer_delete(b);
		return NULL;
	}

	return b;
}



static unsigned fct_maubuff_zero__mau_get(
	fct_maubuff_zero__t* buffer)
{
	if (!buffer)
		return 0;
	return buffer->mau;
}

static uint32_t fct_maubuff_zero__size_get(
	fct_maubuff_zero__t* buffer)
{
	if (!buffer)
		return 0;
	return buffer->size;
}

static bool fct_maubuff_zero__append(
	fct_maubuff_zero__t* buffer,
	const void* data, uint32_t size)
{
	(void)data;

	if (size == 0)
		return true;
	if (!buffer)
		return false;

	buffer->size += size;
	return true;
}

static void fct_maubuff_zero__truncate(
	fct_maubuff_zero__t* buffer, uint32_t size)
{
	if (!buffer
		|| (buffer->size < size))
		return;
	buffer->size = size;
}

static bool fct_maubuff_zero__modify(
	fct_maubuff_zero__t* buffer,
	uint32_t offset, void* data, uint32_t size)
{
	if (size == 0)
		return true;
	if (!buffer || data
		|| ((offset + size) > buffer->size))
		return false;

	return true;
}

static bool fct_maubuff_zero__read(
	fct_maubuff_zero__t* buffer,
	uint32_t offset, uint32_t size, void* data)
{
	if (size == 0)
		return true;
	if (!buffer
		|| ((offset + size) > buffer->size))
		return false;

	if (data)
		bitaddr_clear(data, 0, (size * buffer->mau));
	return true;
}

static fct_maubuff_t* fct_maubuff_zero__copy(
	fct_maubuff_zero__t* buffer)
{
	if (!buffer)
		return NULL;

	fct_maubuff_t* copy = fct_maubuff_zero_create(buffer->mau);
	if (!copy) return NULL;

	if (!fct_maubuff_zero__append(
		copy->data, NULL, buffer->size))
	{
		fct_maubuff_delete(copy);
		return NULL;
	}

	return copy;
}



static bool fct_maubuff_zero__append_maubuff(
	fct_maubuff_zero__t* buffer, fct_maubuff_t* append)
{
	if (!buffer || !append)
		return false;

	if (buffer->mau != fct_maubuff_mau_get(append))
		return false;

	buffer->size += fct_maubuff_size_get(append);
	return true;
}



static fct_maubuff_interface_t
	_fct_maubuff_interface_zero =
	{
		.delete_cb         = (void*)fct_maubuff_zero__delete,
		.to_buffer_cb      = (void*)fct_maubuff_zero__to_buffer,
		.mau_get_cb        = (void*)fct_maubuff_zero__mau_get,
		.size_get_cb       = (void*)fct_maubuff_zero__size_get,
		.append_cb         = (void*)fct_maubuff_zero__append,
		.truncate_cb       = (void*)fct_maubuff_zero__truncate,
		.modify_cb         = (void*)fct_maubuff_zero__modify,
		.read_cb           = (void*)fct_maubuff_zero__read,
		.copy_cb           = (void*)fct_maubuff_zero__copy,
		.append_maubuff_cb = (void*)fct_maubuff_zero__append_maubuff,
	};



fct_maubuff_t* fct_maubuff_zero_create(unsigned mau)
{
	fct_maubuff_t* buffer
		= (fct_maubuff_t*)malloc(sizeof(fct_maubuff_t));
	if (!buffer)
		return NULL;

	buffer->interface
		= &_fct_maubuff_interface_zero;

	fct_maubuff_zero__t* _buffer
		= (fct_maubuff_zero__t*)malloc(sizeof(fct_maubuff_zero__t));
	if (!_buffer)
	{
		free(buffer);
		return NULL;
	}
	buffer->data = _buffer;
	buffer->refcnt = 0;

	_buffer->mau  = mau;
	_buffer->size = 0;

	return buffer;
}

bool fct_maubuff_is_zero(fct_maubuff_t* buffer)
{
	if (!buffer)
		return false;
	return (buffer->interface == &_fct_maubuff_interface_zero);
}
