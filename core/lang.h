#ifndef __FCT_LANG_H__
#define __FCT_LANG_H__

typedef struct fct_type_s fct_type_t;
typedef struct fct_decl_s fct_decl_t;

#include <fct/types.h>

const fct_class_t* fct_class_type;
const fct_class_t* fct_class_decl;

#include <fct/lex.h>
#include <fct/arch.h>


fct_type_t* fct_type_def_parse(
	const fct_lex_line_t* line,
	unsigned offset);

unsigned fct_type_parse(
	const fct_tok_t* tok,
	fct_list_t* map,
	fct_type_t** type);

fct_type_t* fct_type_copy(fct_type_t* type);
fct_type_t* fct_type_reference(fct_type_t* type);
void        fct_type_delete(fct_type_t* type);

fct_string_t*     fct_type_name(const fct_type_t* type);
const fct_type_t* fct_type_base(const fct_type_t* type);

bool fct_type_size_align(
	const fct_type_t* type, fct_arch_t* arch,
	unsigned* size, unsigned* align);


fct_decl_t* fct_decl_parse(const fct_lex_line_t* line);

fct_decl_t* fct_decl_copy(fct_decl_t* decl);
fct_decl_t* fct_decl_reference(fct_decl_t* decl);
void        fct_decl_delete(fct_decl_t* decl);

bool fct_decl_size_align(
	fct_decl_t* decl, fct_arch_t* arch,
	unsigned* size, unsigned* align);

#endif
