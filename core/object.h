#ifndef __FCT_OBJECT_H__
#define __FCT_OBJECT_H__

typedef struct fct_object_s fct_object_t;

#include <fct/types.h>
#include <fct/object/exsym.h>
#include <fct/object/exsymtab.h>
#include <fct/object/imsym.h>
#include <fct/object/imsymtab.h>
#include <fct/object/chunk.h>
#include <fct/object/format.h>

struct fct_object_s
{
	fct_exsymtab_t* exports;
	fct_list_t*     chunks_static;
	fct_list_t*     chunks_dynamic;
};

const fct_class_t* fct_class_object;

fct_object_t* fct_object_create(void);
fct_object_t* fct_object_copy(fct_object_t* object);
fct_object_t* fct_object_merge(fct_object_t* a, fct_object_t* b);
void          fct_object_delete(fct_object_t* object);

fct_object_t* fct_object_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_object_export(
	fct_object_t* object, fct_list_t* strtab);

fct_buffer_t* fct_object_export_raw(fct_object_t* object);
fct_string_t* fct_object_export_raw_hex(fct_object_t* object);

fct_object_t* fct_object_file_import(const char* path);
bool          fct_object_file_export(fct_object_t* object, const char* path);
bool          fct_object_file_export_raw(fct_object_t* object, const char* path);
bool          fct_object_file_export_raw_hex(fct_object_t* object, const char* path);

bool fct_object_file_export_format(fct_object_t* object,
	fct_object_format_e format, const char* path);

fct_string_t* fct_object_dump(fct_object_t* object, unsigned indent);

fct_object_t* fct_object_optimize(fct_object_t* object);
unsigned      fct_object_imports_resolve(fct_object_t* object);

bool fct_object_exports_add_symbol(
	fct_object_t* object, fct_exsym_t* symbol);
bool fct_object_exports_add_symtab(
	fct_object_t* object, fct_exsymtab_t* table);

bool fct_object_chunk_add(
	fct_object_t* object, fct_chunk_t* chunk);
bool fct_object_chunk_place(
	fct_object_t* object, fct_chunk_t* chunk, uint64_t address);

fct_object_t* fct_object_flatten(fct_object_t* object);


#include <fct/arch.h>

fct_arch_t* fct_object_arch(fct_object_t* object);

#endif
