#ifndef __PATH_H__
#define __PATH_H__

#include <stdbool.h>

char* path_optimize(const char* path);
char* path_resolve(const char* path);
char* path_relative(const char* base, const char* path);

char* path_dir(const char* path);
char* path_file(const char* path);
char* path_file_name(const char* path);
char* path_file_ext(const char* path);

bool  path_file_has_ext(const char* path, const char* ext);

#endif
