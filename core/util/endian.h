#ifndef __ENDIAN_H__
#define __ENDIAN_H__

#include <stdint.h>

static inline void endian_swap(uint8_t* data, uintptr_t size)
{
	uintptr_t i, j;
	for (i = 0, j = (size - 1); i < (size >> 1); i++, j--)
	{
		uint8_t swap = data[i];
		data[i] = data[j];
		data[j] = swap;
	}
}

#if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
	#define ENDIAN_FIX(p, be) if (be) endian_swap((uint8_t*)&p, sizeof(p))
#elif (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
	#define ENDIAN_FIX(p, be) if (!be) endian_swap((uint8_t*)&p, sizeof(p))
#else
	#error "Only big and little endian platforms supported"
#endif

#endif
