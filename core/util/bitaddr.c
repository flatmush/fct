#include "bitaddr.h"
#include <string.h>



uint8_t bitaddr_read8(
	const uint8_t* src, unsigned offset, uint8_t size)
{
	unsigned read_max  = (8 - offset);
	unsigned read_size = (size > read_max ? read_max : size);
	uint8_t data = (src[0] >> offset) & ((1UL << read_size) - 1);
	if (read_size < size)
	{
		data |= (src[1] << read_size);
		data &= ((1UL << size) - 1);
	}
	return data;
}

void bitaddr_write8(
	uint8_t* dst, uint8_t offset, unsigned size, uint8_t value)
{
	unsigned write_max  = (8 - offset);
	unsigned write_size = (size > write_max ? write_max : size);
	dst[0] &= ~(((1UL << write_size) - 1) << offset);
	dst[0] |= (value & ((1UL << write_size) - 1)) << offset;
	if (write_size < size)
	{
		dst[1] &= ~(1UL << (size - write_size));
		dst[1] |= value >> write_size;
	}
}



uint32_t bitaddr_read32(
	const uint32_t* src, unsigned offset, uint32_t size)
{
	unsigned read_max  = (32 - offset);
	unsigned read_size = (size > read_max ? read_max : size);
	uint32_t data = (src[0] >> offset) & ((1UL << read_size) - 1);
	if (read_size < size)
	{
		data |= (src[1] << read_size);
		data &= ((1UL << size) - 1);
	}
	return data;
}

void bitaddr_write32(
	uint32_t* dst, uint32_t offset, unsigned size, uint32_t value)
{
	unsigned write_max  = (32 - offset);
	unsigned write_size = (size > write_max ? write_max : size);
	dst[0] &= ~(((1UL << write_size) - 1) << offset);
	dst[0] |= (value & ((1UL << write_size) - 1)) << offset;
	if (write_size < size)
	{
		dst[1] &= ~(1UL << (size - write_size));
		dst[1] |= value >> write_size;
	}
}



void bitaddr_clear(
	uint8_t* dst, uint32_t dst_offset, uint32_t size)
{
	dst += (dst_offset >> 3);
	dst_offset &= 0x07;

	if (dst_offset == 0)
	{
		memset(dst, 0x00, (size >> 3));
		dst += (size >> 3);
		size &= 0x07;
	} else {
		for(; size >= 8; size -= 8)
			bitaddr_write8(dst++, dst_offset, 8, 0);
	}
	if (size != 0)
		bitaddr_write8(dst, dst_offset, size, 0);
}

void bitaddr_copy(
	uint8_t* dst, uint32_t dst_offset,
	const uint8_t* src, uint32_t src_offset, uint32_t size)
{
	if (!src)
	{
		bitaddr_clear(dst, dst_offset, size);
		return;
	}

	dst += (dst_offset >> 3);
	dst_offset &= 0x07;
	src += (src_offset >> 3);
	src_offset &= 0x07;

	if ((dst_offset == 0)
		&& (src_offset == 0))
	{
		memcpy(dst, src, (size >> 3));
		src += (size >> 3);
		dst += (size >> 3);
		size &= 0x07;
	} else if (dst_offset == 0) {
		for(; size >= 8; size -= 8)
			*dst++ = bitaddr_read8(src++, src_offset, 8);
	} else if (src_offset == 0) {
		for(; size >= 8; size -= 8)
			bitaddr_write8(dst++, dst_offset, 8, *src++);
	} else {
		for(; size >= 8; size -= 8)
			bitaddr_write8(dst++, dst_offset, 8,
				bitaddr_read8(src++, src_offset, 8));
	}
	if (size != 0)
		bitaddr_write8(dst, dst_offset, size,
			bitaddr_read8(src, src_offset, size));
}
