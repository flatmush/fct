#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>



static const char* path__file(const char* path)
{
	if (!path) return NULL;
	const char* npath;
	for (npath = strchr(path, '/'); npath;
		npath = strchr(&npath[1], '/'))
		path = &npath[1];
	return path;
}

static const char* path__file_ext(const char* path)
{
	if (!path || (path[0] == '\0'))
		return NULL;
	const char *next, *ext = NULL;
	for (next = strchr(&path[1], '.'); next;
		next = strchr(&next[1], '.'))
		ext = &next[1];
	return ext;
}



char* path_optimize(const char* path)
{
	if (!path || (path[0] == '\0'))
		return NULL;

	if (strcmp(path, ".") == 0)
		return strdup(path);

	unsigned c_count = 1;
	const char* src;
	for (src = path; *src != '\0'; src++)
		c_count += (*src == '/' ? 1 : 0);

	const char* c[c_count];
	unsigned c_len[c_count];

	c[0] = path;

	unsigned ci = 0;
	unsigned cl = 0;
	for (src = path; *src != '\0'; src++)
	{
		if (*src == '/')
		{
			c_len[ci] = cl;
			c[++ci] = &src[1];
			cl = 0;
		}
		else
		{
			cl++;
		}
	}
	c_len[ci] = cl;

	const char* nc[c_count];
	unsigned nc_len[c_count];
	unsigned nc_count = 0;

	for (ci = 0; ci < c_count; ci++)
	{
		if ((c_len[ci] == 0)
			&& (nc_count != 0)
			&& ((ci + 1) != c_count))
			continue;

		if ((c_len[ci] == 1)
			&& (c[ci][0] == '.'))
			continue;

		if ((c_len[ci] == 2)
			&& (strncmp(c[ci], "..", 2) == 0)
			&& (nc_count > 0))
		{
			bool was_root = (nc_count == 1) && (nc_len[0] == 0);
			if (!was_root)
				nc_count--;
			continue;
		}

		nc[nc_count] = c[ci];
		nc_len[nc_count] = c_len[ci];
		nc_count++;
	}

	unsigned nc_total = nc_count;
	for (ci = 0; ci < nc_count; ci++)
		nc_total += nc_len[ci];

	char* npath = (char*)malloc(nc_total);
	if (!npath) return NULL;

	for (ci = 0, cl = 0; ci < nc_count; ci++)
	{
		if (ci > 0)
			npath[cl++] = '/';
		if (nc_len[ci] > 0)
		{
			memcpy(&npath[cl], nc[ci], nc_len[ci]);
			cl += nc_len[ci];
		}
	}
	npath[nc_total - 1] = '\0';

	return npath;
}

char* path_resolve(const char* path)
{
	if (!path)
		return NULL;
	if (path[0] == '/')
		return path_optimize(path);

	long clen = pathconf(".", _PC_PATH_MAX);
	if (clen < 0)
		return NULL;
	size_t plen = strlen(path);

	char rpath[clen + plen + 2];
	if (!getcwd(rpath, clen))
		return NULL;
	clen = strlen(rpath);
	if (clen == 0)
		return NULL;

	if (rpath[clen - 1] != '/')
		rpath[clen++] = '/';
	memcpy(&rpath[clen], path, (plen + 1));

	return path_optimize(rpath);
}



char* path_dir(const char* path)
{
	if (!path)
		return NULL;

	const char* end = path__file(path);
	uintptr_t dlen = ((uintptr_t)end - (uintptr_t)path);
	char* ret = (char*)malloc(dlen + 1);
	if (!ret) return NULL;
	memcpy(ret, path, dlen);
	ret[dlen] = '\0';
	return ret;
}

char* path_file(const char* path)
{
	path = path__file(path);
	if (!path) return NULL;
	return strdup(path);
}

char* path_file_name(const char* path)
{
	path = path__file(path);
	if (!path) return NULL;

	const char* end
		= path__file_ext(path);
	if (!end) return strdup(path);

	size_t plen = ((uintptr_t)--end - (uintptr_t)path);
	char* ret = (char*)malloc(plen + 1);
	if (!ret) return NULL;
	memcpy(ret, path, plen);
	ret[plen] = '\0';
	return ret;
}

char* path_file_ext(const char* path)
{
	path = path__file(path);
	path = path__file_ext(path);
	if (!path) return NULL;
	return strdup(path);
}



bool path_file_has_ext(const char* path, const char* ext)
{
	path = path__file(path);
	const char* pext
		= path__file_ext(path);
	if (!pext) return false;

	size_t elen = strlen(ext);
	size_t plen = strlen(pext);

	return ((elen == plen)
		&& (strncasecmp(ext, pext, elen) == 0));
}



char* path_relative(const char* base, const char* path)
{
	if (!path || (path[0] == '\0'))
		return NULL;
	if (!base || (base[0] == '\0')
		|| (path[0] == '/'))
		return path_optimize(path);

	uintptr_t base_len
		= ((uintptr_t)path__file(base) - (uintptr_t)base);
	if (base_len == 0)
		return path_optimize(path);

	uintptr_t path_len = strlen(path);
	char full[base_len + path_len + 1];
	memcpy(full, base, base_len);
	memcpy(&full[base_len], path, path_len);
	full[base_len + path_len] = '\0';

	return path_optimize(full);
}
