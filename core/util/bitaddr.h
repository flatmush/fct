#ifndef __BITADDR_H__
#define __BITADDR_H__

#include <stdint.h>

uint8_t bitaddr_read8(
	const uint8_t* src, unsigned offset, uint8_t size);
void bitaddr_write8(
	uint8_t* dst, uint8_t offset, unsigned size, uint8_t value);

uint32_t bitaddr_read32(
	const uint32_t* src, unsigned offset, uint32_t size);
void     bitaddr_write32(
	uint32_t* dst, uint32_t offset, unsigned size, uint32_t value);

void bitaddr_clear(
	uint8_t* dst, uint32_t dst_offset, uint32_t size);
void bitaddr_copy(
	uint8_t* dst, uint32_t dst_offset,
	const uint8_t* src, uint32_t src_offset, uint32_t size);

#endif
