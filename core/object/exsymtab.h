#ifndef __FCT_OBJECT_EXSYMTAB_H__
#define __FCT_OBJECT_EXSYMTAB_H__

#include <fct/types.h>
#include <fct/object/exsym.h>

typedef fct_list_t fct_exsymtab_t;

const fct_class_t* fct_class_exsymtab;

fct_exsymtab_t* fct_exsymtab_create(void);
fct_exsymtab_t* fct_exsymtab_copy(fct_exsymtab_t* table);
fct_exsymtab_t* fct_exsymtab_merge(fct_exsymtab_t* a, fct_exsymtab_t* b);
void            fct_exsymtab_delete(fct_exsymtab_t* table);

bool fct_exsymtab_foreach(
	fct_exsymtab_t* table, void* param,
	bool (*func)(fct_exsym_t* symbol, void* param));

bool fct_exsymtab_add(fct_exsymtab_t* table, fct_exsym_t* exsym);

unsigned fct_exsymtab_count(fct_exsymtab_t* table);

fct_exsym_t* fct_exsymtab_find_by_exact_name(fct_exsymtab_t* table, fct_string_t* name);
fct_exsym_t* fct_exsymtab_find_by_name(fct_exsymtab_t* table, fct_string_t* name);
fct_exsym_t* fct_exsymtab_find_by_attrib(fct_exsymtab_t* table, fct_exsym_t* exsym);

fct_exsymtab_t* fct_exsymtab_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_exsymtab_export(
	fct_exsymtab_t* table, fct_list_t* strtab);

fct_string_t* fct_exsymtab_dump(fct_exsymtab_t* table, unsigned indent);

bool            fct_exsymtab_offset(fct_exsymtab_t* table, uint32_t offset);
fct_exsymtab_t* fct_exsymtab_merge_offset(fct_exsymtab_t* a, fct_exsymtab_t* b, uint32_t offset);

#endif
