#ifndef __FCT_OBJECT_IHEX_H__
#define __FCT_OBJECT_IHEX_H__

#include <fct/types.h>
#include <fct/object.h>

fct_string_t* fct_object_export_ihex(fct_object_t* object);

bool fct_object_file_export_ihex(fct_object_t* object, const char* path);

#endif
