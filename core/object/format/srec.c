#include "srec.h"
#include <fct/debug.h>

typedef enum
{
	SREC_TYPE_HEADER   = 0,
	SREC_TYPE_DATA_16  = 1,
	SREC_TYPE_DATA_24  = 2,
	SREC_TYPE_DATA_32  = 3,
	SREC_TYPE_COUNT_16 = 5,
	SREC_TYPE_COUNT_24 = 6,
	SREC_TYPE_START_32 = 7,
	SREC_TYPE_START_24 = 8,
	SREC_TYPE_START_16 = 9,
} srec_type_e;


static bool fct_object_srec__encode_line(
	srec_type_e type, uint8_t count, uint32_t addr, const uint8_t* data,
	fct_string_t* string)
{
	if (type >= 10) return false;

	unsigned addr_size;
	switch (type)
	{
		case SREC_TYPE_HEADER:
		case SREC_TYPE_DATA_16:
		case SREC_TYPE_COUNT_16:
		case SREC_TYPE_START_16:
			if ((addr >> 16) > 0)
				return false;
			addr_size = 2;
			break;

		case SREC_TYPE_DATA_24:
		case SREC_TYPE_COUNT_24:
		case SREC_TYPE_START_24:
			if ((addr >> 24) > 0)
				return false;
			addr_size = 3;
			break;

		case SREC_TYPE_DATA_32:
		case SREC_TYPE_START_32:
			addr_size = 4;
			break;

		default:
			return false;
	}

	char line[1 + 1 + 2 + (addr_size * 2) + (count * 2) + 2 + 1 + 1];

	sprintf(line, "S%X%02X", type, count);

	uint8_t checksum = count + (addr >> 8) + (addr & 0xFF) + type;

	char* lp = &line[4];
	unsigned i, j;
	for (i = 0, j = (addr_size - 1); i < addr_size; i++, j--, lp += 2)
	{
		sprintf(lp, "%02X", (addr >> (j * 8)) & 0xFF);
		checksum += (addr >> (j * 8)) & 0xFF;
	}

	for (i = 0; i < count; i++, lp += 2)
	{
		sprintf(lp, "%02X", data[i]);
		checksum += data[i];
	}

	checksum = (checksum ^ 0xFF);
	sprintf(lp, "%02X\n", checksum);

	return fct_string_append_static(string, line);
}

typedef struct
{
	srec_type_e   start;
	srec_type_e   data;
	unsigned      record_count;
	fct_string_t* string;
} fct_object_srec__print_chunk_params_t;

static bool fct_object_srec__print_chunk(fct_chunk_t* chunk,
	fct_object_srec__print_chunk_params_t* params)
{
	if (!chunk) return true;

	bool has_data = false;
	switch (chunk->content)
	{
		case FCT_CHUNK_CONTENT_UNINITIALIZED:
			return true;

		case FCT_CHUNK_CONTENT_ZERO:
			break;

		case FCT_CHUNK_CONTENT_DATA:
			has_data = true;
			break;

		default:
			return false;
	}

	uint64_t size = fct_chunk_size_get(chunk);
	if (size == 0) return true;

	unsigned mau = 8;
	if (chunk->arch)
		mau = fct_arch_mau(chunk->arch);
	else if (chunk->buffer)
		mau = fct_maubuff_mau_get(chunk->buffer);

	if (mau != 8)
	{
		FCT_DEBUG_ERROR(
			"Only byte-addressed files can be exported as srec");
		return false;
	}

	uint64_t offset = 0;
	uint64_t addr = chunk->address;

	while (size > 0)
	{
		unsigned lsize = 16;
		if ((addr & 0xF) != 0)
			lsize = 16 - (addr & 0x0F);
		if (lsize > size) lsize = size;

		if ((addr >> 32) > 0) return false;

		uint8_t data[lsize];
		memset(data, 0x00, lsize);
		if (has_data && !fct_maubuff_read(
			chunk->buffer, offset, lsize, &data))
			return false;
		offset += lsize;
		size   -= lsize;

		if (!fct_object_srec__encode_line(
			params->data, lsize, addr, data,
			params->string))
			return false;
		addr += lsize;
	}

	return true;
}

static bool fct_object_srec__scan_type(fct_chunk_t* chunk,
	fct_object_srec__print_chunk_params_t* params)
{
	if (!chunk) return true;

	switch (chunk->content)
	{
		case FCT_CHUNK_CONTENT_UNINITIALIZED:
			return true;

		case FCT_CHUNK_CONTENT_ZERO:
		case FCT_CHUNK_CONTENT_DATA:
			break;

		default:
			return false;
	}

	uint64_t size = fct_chunk_size_get(chunk);
	if (size == 0) return true;

	unsigned addr_size = 0;
	unsigned mau = 8;
	if (chunk->arch)
	{
		mau = fct_arch_mau(chunk->arch);
		addr_size = fct_arch_addr_size(chunk->arch) * mau;
	}
	else if (chunk->buffer)
	{
		mau = fct_maubuff_mau_get(chunk->buffer);
	}

	if (mau != 8)
	{
		FCT_DEBUG_ERROR(
			"Only byte-addressed files can be exported as srec");
		return false;
	}

	if ((chunk->address >> 32) != 0)
		return false;
	if (((1ULL << 32) - chunk->address) < size)
		return false;

	if (params->data >= SREC_TYPE_DATA_32)
		return true;

	if ((addr_size > 24)
		|| (chunk->address >= (1ULL << 24))
		|| (((1ULL << 24) - chunk->address) < size))
	{
		params->data  = SREC_TYPE_DATA_32;
		params->start = SREC_TYPE_START_32;
	}
	else if ((addr_size > 16)
		|| (chunk->address >= (1ULL << 24))
		|| (((1ULL << 16) - chunk->address) < size))
	{
		params->data  = SREC_TYPE_DATA_24;
		params->start = SREC_TYPE_START_24;
	}

	return true;
}

fct_string_t* fct_object_export_srec(fct_object_t* object)
{
	if (!object) return NULL;

	if (fct_list_count(object->chunks_dynamic) != 0)
	{
		FCT_DEBUG_ERROR(
			"Only a flat object can be exported as srec");
		return NULL;
	}

	fct_object_srec__print_chunk_params_t params;
	params.start = SREC_TYPE_START_16;
	params.data  = SREC_TYPE_DATA_16;
	params.record_count = 0;
	params.string = fct_string_create("");

	if (!params.string) return NULL;

	if (!fct_list_foreach(
		object->chunks_static, &params,
		(void*)fct_object_srec__scan_type))
	{
		fct_string_delete(params.string);
		return NULL;
	}

	if (!fct_object_srec__encode_line(
		SREC_TYPE_HEADER, 4, 0x0000, (const uint8_t*)"FCT\0", params.string))
	{
		fct_string_delete(params.string);
		return false;
	}

	if (!fct_list_foreach(
		object->chunks_static, &params,
		(void*)fct_object_srec__print_chunk))
	{
		fct_string_delete(params.string);
		return NULL;
	}

	if (params.record_count < (1U << 24))
	{
		if (!fct_object_srec__encode_line(
			(params.record_count < (1U << 16) ? SREC_TYPE_COUNT_16 : SREC_TYPE_COUNT_24),
			0, params.record_count, NULL, params.string))
		{
			fct_string_delete(params.string);
			return false;
		}
	}

	/* TODO - Handle entry-point. */
	if (!fct_object_srec__encode_line(
		params.start, 0, 0x0000, NULL, params.string))
	{
		fct_string_delete(params.string);
		return false;
	}

	return params.string;
}

bool fct_object_file_export_srec(fct_object_t* object, const char* path)
{
	if (!object)
		return false;

	fct_string_t* string
		= fct_object_export_srec(object);
	bool success = fct_string_file_export(string, path);
	fct_string_delete(string);
	return success;
}
