#include "ihex.h"
#include <fct/debug.h>

typedef enum
{
	IHEX_TYPE_DATA            = 0x00,
	IHEX_TYPE_EOF             = 0x01,
	IHEX_TYPE_EXT_SEG_ADDR    = 0x02,
	IHEX_TYPE_START_SEG_ADDR  = 0x03,
	IHEX_TYPE_EXT_LIN_ADDR    = 0x04,
	IHEX_TYPE_START_LIN_ADDR  = 0x05,
} ihex_type_e;


static bool fct_object_ihex__encode_line(
	uint8_t count, uint16_t addr, uint8_t type, const uint8_t* data,
	fct_string_t* string)
{
	char line[1 + 2 + 4 + 2 + (count * 2) + 2 + 1 + 1];

	sprintf(line, ":%02X%04X%02X", count, addr, type);

	uint8_t checksum = count + (addr >> 8) + (addr & 0xFF) + type;

	char* lp = &line[9];
	unsigned i;
	for (i = 0; i < count; i++, lp += 2)
	{
		sprintf(lp, "%02X", data[i]);
		checksum += data[i];
	}

	checksum = (checksum ^ 0xFF) + 1;
	sprintf(lp, "%02X\n", checksum);

	return fct_string_append_static(string, line);
}

typedef struct
{
	uint64_t      base;
	fct_string_t* string;
} fct_object_ihex__print_chunk_params_t;

static bool fct_object_ihex__print_chunk(fct_chunk_t* chunk,
	fct_object_ihex__print_chunk_params_t* params)
{
	if (!chunk) return true;

	bool has_data = false;
	switch (chunk->content)
	{
		case FCT_CHUNK_CONTENT_UNINITIALIZED:
			return true;

		case FCT_CHUNK_CONTENT_ZERO:
			break;

		case FCT_CHUNK_CONTENT_DATA:
			has_data = true;
			break;

		default:
			return false;
	}

	uint64_t size = fct_chunk_size_get(chunk);
	if (size == 0) return true;

	unsigned mau = 8;
	if (chunk->arch)
		mau = fct_arch_mau(chunk->arch);
	else if (chunk->buffer)
		mau = fct_maubuff_mau_get(chunk->buffer);

	if (mau != 8)
	{
		FCT_DEBUG_ERROR(
			"Only byte-addressed files can be exported as IHEX");
		return false;
	}

	uint64_t offset = 0;
	uint64_t addr = chunk->address;

	while (size > 0)
	{
		unsigned lsize = 16;
		if ((addr & 0xF) != 0)
			lsize = 16 - (addr & 0x0F);
		if (lsize > size) lsize = size;

		if ((addr >> 32) > 0) return false;

		if ((addr >> 16) != (params->base >> 16))
		{
			uint8_t data[2] =
			{
				((addr >> 16) & 0xFF),
				((addr >> 24) & 0xFF),
			};
			if (!fct_object_ihex__encode_line(
				2, 0, IHEX_TYPE_EXT_LIN_ADDR, data,
				params->string))
				return false;
			params->base = (addr & 0xFFFF0000);
		}

		uint8_t data[lsize];
		memset(data, 0x00, lsize);
		if (has_data && !fct_maubuff_read(
			chunk->buffer, offset, lsize, &data))
			return false;
		offset += lsize;
		size   -= lsize;

		if (!fct_object_ihex__encode_line(
			lsize, (addr & 0xFFFF), IHEX_TYPE_DATA, data,
			params->string))
			return false;
		addr += lsize;
	}

	return true;
}


fct_string_t* fct_object_export_ihex(fct_object_t* object)
{
	if (!object) return NULL;

	if (fct_list_count(object->chunks_dynamic) != 0)
	{
		FCT_DEBUG_ERROR(
			"Only a flat object can be exported as IHEX");
		return NULL;
	}

	fct_object_ihex__print_chunk_params_t params;
	params.base = 0;
	params.string = fct_string_create("");

	if (!params.string) return NULL;

	if (!fct_list_foreach(
		object->chunks_static, &params,
		(void*)fct_object_ihex__print_chunk))
	{
		fct_string_delete(params.string);
		return NULL;
	}

	/* TODO - Handle entry-point. */

	if (!fct_object_ihex__encode_line(
		0, 0, IHEX_TYPE_EOF, NULL, params.string))
	{
		fct_string_delete(params.string);
		return false;
	}

	return params.string;
}

bool fct_object_file_export_ihex(fct_object_t* object, const char* path)
{
	if (!object)
		return false;

	fct_string_t* string
		= fct_object_export_ihex(object);
	bool success = fct_string_file_export(string, path);
	fct_string_delete(string);
	return success;
}
