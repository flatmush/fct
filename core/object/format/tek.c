#include "tek.h"
#include <fct/debug.h>


typedef enum
{
	TEK_TYPE_DATA = 6,
	TEK_TYPE_TERM = 8,
} tek_type_e;


static bool fct_object_tek__encode_line(
	uint8_t count, uint32_t addr, uint8_t type, const uint8_t* data,
	fct_string_t* string)
{
	if (type >= 16) return false;

	uint8_t checksum = (count >> 4) + (count & 0xF) + (type & 0xF) + 8;

	unsigned i;
	for (i = 0; i < 8; i++)
		checksum += (addr >> (i * 4)) & 0x0F;
	for (i = 0; i < count; i++)
		checksum += (data[i] >> 4) + (data[i] & 0x0F);

	char line[1 + 2 + 1 + 2 + 9 + (count * 2) + 1 + 1];
	sprintf(line, "%%%02X%X%02X8%08X", count, type, checksum, addr);

	char* lp = &line[15];
	for (i = 0; i < count; i++, lp += 2)
		sprintf(lp, "%02X", data[i]);
	sprintf(lp, "\n");

	return fct_string_append_static(string, line);
}

static bool fct_object_tek__print_chunk(
	fct_chunk_t* chunk, fct_string_t* string)
{
	if (!chunk) return true;

	bool has_data = false;
	switch (chunk->content)
	{
		case FCT_CHUNK_CONTENT_UNINITIALIZED:
			return true;

		case FCT_CHUNK_CONTENT_ZERO:
			break;

		case FCT_CHUNK_CONTENT_DATA:
			has_data = true;
			break;

		default:
			return false;
	}

	uint64_t size = fct_chunk_size_get(chunk);
	if (size == 0) return true;

	unsigned mau = 8;
	if (chunk->arch)
		mau = fct_arch_mau(chunk->arch);
	else if (chunk->buffer)
		mau = fct_maubuff_mau_get(chunk->buffer);

	if (mau != 8)
	{
		FCT_DEBUG_ERROR(
			"Only byte-addressed files can be exported as tek");
		return false;
	}

	uint64_t offset = 0;
	uint64_t addr = chunk->address;

	while (size > 0)
	{
		unsigned lsize = 16;
		if ((addr & 0xF) != 0)
			lsize = 16 - (addr & 0x0F);
		if (lsize > size) lsize = size;

		if ((addr >> 32) > 0) return false;

		uint8_t data[lsize];
		memset(data, 0x00, lsize);
		if (has_data && !fct_maubuff_read(
			chunk->buffer, offset, lsize, &data))
			return false;
		offset += lsize;
		size   -= lsize;

		if (!fct_object_tek__encode_line(
			lsize, addr, TEK_TYPE_DATA, data, string))
			return false;
		addr += lsize;
	}

	return true;
}


fct_string_t* fct_object_export_tek(fct_object_t* object)
{
	if (!object) return NULL;

	if (fct_list_count(object->chunks_dynamic) != 0)
	{
		FCT_DEBUG_ERROR(
			"Only a flat object can be exported as tek");
		return NULL;
	}

	fct_string_t* string = fct_string_create("");

	if (!string) return NULL;

	if (!fct_list_foreach(
		object->chunks_static, string,
		(void*)fct_object_tek__print_chunk))
	{
		fct_string_delete(string);
		return NULL;
	}

	/* TODO - Handle entry-point. */
	if (!fct_object_tek__encode_line(
		0, 0, TEK_TYPE_TERM, NULL, string))
	{
		fct_string_delete(string);
		return false;
	}

	return string;
}

bool fct_object_file_export_tek(fct_object_t* object, const char* path)
{
	if (!object)
		return false;

	fct_string_t* string
		= fct_object_export_tek(object);
	bool success = fct_string_file_export(string, path);
	fct_string_delete(string);
	return success;
}
