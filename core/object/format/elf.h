#ifndef __FCT_OBJECT_ELF_H__
#define __FCT_OBJECT_ELF_H__

#include <fct/types.h>

#define ELF_MACHINE_NONE (uint16_t)0x0000

#define ELF_OSABI_NONE       (uint8_t)0x00
#define ELF_OSABI_STANDALONE (uint8_t)0xFF

#endif
