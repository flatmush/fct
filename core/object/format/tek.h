#ifndef __FCT_OBJECT_TEK_H__
#define __FCT_OBJECT_TEK_H__

#include <fct/types.h>
#include <fct/object.h>

fct_string_t* fct_object_export_tek(fct_object_t* object);

bool fct_object_file_export_tek(fct_object_t* object, const char* path);

#endif
