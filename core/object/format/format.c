#include <fct/object.h>


static const char* fct_object_format_name[] =
{
	"fobject",
	"bin",
	"hex",
	"ihex",
	"srec",
	"tek",
	NULL
};

bool fct_object_format_from_string(
	const char* name, fct_object_format_e* format)
{
	if (!name) return false;

	unsigned i;
	for (i = 0; fct_object_format_name[i]; i++)
	{
		if (strcasecmp(name, fct_object_format_name[i]) == 0)
		{
			if (format) *format = i;
			return true;
		}
	}

	return false;
}


const char* fct_object_format_to_string(
	fct_object_format_e format)
{
	if (format >= FCT_OBJECT_FORMAT_COUNT)
		return NULL;
	return fct_object_format_name[format];
}


bool fct_object_format_is_flat(
	fct_object_format_e format)
{
	switch (format)
	{
		case FCT_OBJECT_FORMAT_BINARY:
		case FCT_OBJECT_FORMAT_HEX:
		case FCT_OBJECT_FORMAT_IHEX:
		case FCT_OBJECT_FORMAT_SREC:
		case FCT_OBJECT_FORMAT_TEK:
			return true;

		default:
			break;
	}

	return false;
}
