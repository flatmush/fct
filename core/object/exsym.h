#ifndef __FCT_OBJECT_EXSYM_H__
#define __FCT_OBJECT_EXSYM_H__

typedef struct fct_exsym_s fct_exsym_t;

#include <fct/types.h>


struct fct_exsym_s
{
	fct_string_t* name;
	uint64_t      value;
	uint64_t      size;
};

const fct_class_t* fct_class_exsym;


fct_exsym_t* fct_exsym_create(
	fct_string_t* name,
	fct_string_t* scope,
	uint64_t value, uint64_t size);
fct_exsym_t* fct_exsym_copy(fct_exsym_t* exsym);
void         fct_exsym_delete(fct_exsym_t* exsym);

fct_exsym_t* fct_exsym_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_exsym_export(
	fct_exsym_t* exsym, fct_list_t* strtab);

fct_string_t* fct_exsym_dump(fct_exsym_t* exsym, unsigned indent);

bool fct_exsym_compare(fct_exsym_t* a, fct_exsym_t* b);

#endif
