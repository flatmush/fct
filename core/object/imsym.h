#ifndef __FCT_OBJECT_IMSYM_H__
#define __FCT_OBJECT_IMSYM_H__

typedef struct fct_imsym_s fct_imsym_t;

#include <fct/types.h>


typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool     resolved  :  1;
		bool     relative  :  1;
		bool     arch_spec :  1;
		unsigned reserved0 : 29;
		unsigned reserved1 : 32;
	};
	uint64_t mask;
} fct_imsym_flags_t;

struct fct_imsym_s
{
	fct_string_t*     name;
	uint64_t          address;
	uint64_t          offset;
	uint64_t          size;
	fct_imsym_flags_t flags;
};

const fct_class_t* fct_class_imsym;


fct_imsym_t* fct_imsym_create(
	fct_string_t* name,
	fct_string_t* scope,
	uint64_t address, uint64_t offset, uint64_t size,
	bool relative, bool arch_spec);
fct_imsym_t* fct_imsym_copy(fct_imsym_t* imsym);
void         fct_imsym_delete(fct_imsym_t* imsym);

fct_string_t* fct_imsym_descope(fct_string_t* name);

fct_imsym_t* fct_imsym_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_imsym_export(
	fct_imsym_t* imsym, fct_list_t* strtab);

fct_string_t* fct_imsym_dump(fct_imsym_t* imsym, unsigned indent);

bool fct_imsym_compare(fct_imsym_t* a, fct_imsym_t* b);

#endif
