#ifndef __FCT_OBJECT_CHUNK_H__
#define __FCT_OBJECT_CHUNK_H__

typedef struct fct_chunk_s fct_chunk_t;

typedef enum
{
	FCT_CHUNK_CONTENT_UNINITIALIZED = 0,
	FCT_CHUNK_CONTENT_ZERO,
	FCT_CHUNK_CONTENT_DATA,
	FCT_CHUNK_CONTENT_COUNT
} fct_chunk_content_e;

typedef enum
{
	FCT_CHUNK_ACCESS_NONE = 0,
	FCT_CHUNK_ACCESS_R    = 1,
	FCT_CHUNK_ACCESS_W    = 2,
	FCT_CHUNK_ACCESS_RW   = 3,
	FCT_CHUNK_ACCESS_X    = 4,
	FCT_CHUNK_ACCESS_RX   = 5,
	FCT_CHUNK_ACCESS_WX   = 6,
	FCT_CHUNK_ACCESS_RWX  = 7,
} fct_chunk_access_e;


#include <fct/types.h>
#include <fct/object.h>
#include <fct/maubuff.h>
#include <fct/arch.h>


static inline const char* fct_chunk_content_string(
	fct_chunk_content_e content)
{
	if (content >= FCT_CHUNK_CONTENT_COUNT)
		return NULL;
	static const char* content_str[3] =
		{ "uninitialized", "zero", "data" };
	return content_str[content];
}

static inline const char* fct_chunk_access_string(
	fct_chunk_access_e access)
{
	static const char* access_str[8] =
		{ "none", "r", "w", "rw", "x", "rx", "wx", "rwx" };
	return access_str[access & 7];
}


struct fct_chunk_s
{
	fct_string_t*       name;
	bool                is_static;
	uint64_t            address, align;
	fct_imsymtab_t*     imports;
	fct_exsymtab_t*     exports;
	fct_arch_t*         arch;
	fct_chunk_access_e  access;
	fct_chunk_content_e content;
	fct_maubuff_t*      buffer;
	unsigned            refcnt;
};

const fct_class_t* fct_class_chunk;

static inline bool fct_chunk_is_static(fct_chunk_t* chunk)
	{ return (chunk && chunk->is_static); }

fct_chunk_t* fct_chunk_create(
	fct_arch_t*         arch,
	fct_chunk_access_e  access,
	fct_chunk_content_e content,
	uint64_t            align);
fct_chunk_t* fct_chunk_reference(fct_chunk_t* chunk);
fct_chunk_t* fct_chunk_copy(fct_chunk_t* chunk);
fct_chunk_t* fct_chunk_merge(fct_chunk_t* a, fct_chunk_t* b);
void         fct_chunk_delete(fct_chunk_t* chunk);

uint64_t fct_chunk_size_get(fct_chunk_t* chunk);

fct_chunk_t* fct_chunk_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_chunk_export(
	fct_chunk_t* chunk, fct_list_t* strtab);

fct_string_t* fct_chunk_dump(
	fct_chunk_t* chunk, unsigned indent);

bool fct_chunk_align(fct_chunk_t* chunk, uint64_t align);

bool fct_chunk_imports_add_symbol(fct_chunk_t* chunk, fct_imsym_t* symbol);
bool fct_chunk_exports_add_symbol(fct_chunk_t* chunk, fct_exsym_t* symbol);

bool fct_chunk_symbol_resolve(
	fct_chunk_t* chunk, fct_object_t* object, fct_string_t* name,
	bool relative, uint64_t address, uint64_t offset,
	uint64_t* value, uint64_t* size);

void fct_chunk_imports_resolve(fct_chunk_t* chunk, fct_object_t* object);

void fct_chunk_imports_remove_resolved(fct_chunk_t* chunk);

bool fct_chunk_append(fct_chunk_t* chunk, const void* data, uint64_t size);
void fct_chunk_truncate(fct_chunk_t* chunk, uint64_t size);

bool fct_chunk_adjoining(fct_chunk_t* a, fct_chunk_t* b);

#endif
