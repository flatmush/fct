#include "exsymtab.h"
#include <stdlib.h>



static const fct_class_t
	fct_class__exsymtab =
	{
		.delete_cb    = (void*)fct_exsymtab_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_exsymtab_copy,
		.import_cb    = (void*)fct_exsymtab_import,
		.export_cb    = (void*)fct_exsymtab_export,
	};
const fct_class_t* fct_class_exsymtab
	= &fct_class__exsymtab;



static fct_string_t* fct_exsymtab__key(fct_exsym_t* symbol)
{
	return (symbol ? symbol->name : NULL);
}

static uint8_t fct_exsymtab__hash(fct_string_t* key)
{
	if (!key) return 0;
	unsigned i;
	uint8_t h;
	for (i = 0, h = 0; i < key->size; i++)
		h += key->data[i];
	return h;
}

fct_exsymtab_t* fct_exsymtab_create(void)
{
	return fct_hashtab_create(
		fct_class_exsym,
		(void*)fct_exsymtab__key,
		(void*)fct_string_compare,
		(void*)fct_exsymtab__hash);
}



fct_exsymtab_t* fct_exsymtab_copy(fct_exsymtab_t* table)
{
	return fct_list_copy(table);
}

fct_exsymtab_t* fct_exsymtab_merge(fct_exsymtab_t* a, fct_exsymtab_t* b)
{
	if (!a) return fct_list_copy(b);
	if (!b) return fct_list_copy(a);

	fct_exsymtab_t* table
		= fct_exsymtab_create();
	if (!table)
		return NULL;

	bool func(fct_exsym_t* symbol, void* params)
	{
		(void)params;
		fct_exsym_t* exsym
			= fct_exsym_copy(symbol);
		if (!exsym) return false;
		if (!fct_exsymtab_add(table, exsym))
		{
			fct_exsym_delete(exsym);
			return false;
		}

		return true;
	}

	if (!fct_exsymtab_foreach(a, NULL, func)
		|| !fct_exsymtab_foreach(b, NULL, func))
	{
		fct_exsymtab_delete(table);
		return NULL;
	}

	return table;
}

void fct_exsymtab_delete(fct_exsymtab_t* table)
{
	fct_list_delete(table);
}



bool fct_exsymtab_foreach(
	fct_exsymtab_t* table, void* param,
	bool (*func)(fct_exsym_t* symbol, void* param))
{
	return fct_list_foreach(table, param, (void*)func);
}



fct_exsymtab_t* fct_exsymtab_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	fct_exsymtab_t* table
		= fct_exsymtab_create();
	if (!table) return NULL;

	if (!fct_list_import_named(
		table, "EXSYMTAB",
		buffer, offset, strtab))
	{
		fct_list_delete(table);
		return NULL;
	}

	/* TODO - Ensure exclusivity of loaded entries. */

	return table;
}

fct_buffer_t* fct_exsymtab_export(
	fct_exsymtab_t* table, fct_list_t* strtab)
{
	return fct_list_export_named(
		"EXSYMTAB", table, strtab);
}



fct_string_t* fct_exsymtab_dump(fct_exsymtab_t* table, unsigned indent)
{
	if (!table)
		return NULL;

	return fct_list_dump(table,
		indent, (void*)fct_exsym_dump);
}



bool fct_exsymtab_add(fct_exsymtab_t* table, fct_exsym_t* exsym)
{
	if (!table || !exsym)
		return false;

	if (fct_exsymtab_find_by_attrib(
		table, exsym))
	{
		fct_exsym_delete(exsym);
		return true;
	}

	if (fct_exsymtab_find_by_exact_name(table, exsym->name))
		return false;

	return fct_list_add(table, exsym);
}



unsigned fct_exsymtab_count(fct_exsymtab_t* table)
{
	return fct_list_count(table);
}



fct_exsym_t* fct_exsymtab_find_by_exact_name(
	fct_exsymtab_t* table, fct_string_t* name)
{
	return fct_hashtab_find_by_key(table, name);
}

static fct_string_t* fct_exsymtab__descope(fct_string_t* name)
{
	if (!name)
		return NULL;

	unsigned scope = 0;
	unsigned pscope = 0;

	unsigned i;
	for (i = 0; i < name->size; i++)
	{
		if ((name->data[i] == '.')
			|| (name->data[i] == ':'))
		{
			pscope = scope;
			scope = i;
		}
	}

	if ((scope == 0) || ((scope + 1) >= name->size))
		return NULL;

	if ((name->data[scope] == ':') || (pscope == 0))
		return fct_string_create_format(
			"%.*s", (name->size - (scope + 1)),
			&name->data[scope + 1]);

	fct_string_t* descope
		= fct_string_create_format(
			"%.*s", (pscope + 1), name->data);
	if (!descope) return NULL;

	if (!fct_string_append_static_format(
		descope, "%.*s", (name->size - (scope + 1)),
		&name->data[scope + 1]))
	{
		fct_string_delete(descope);
		return NULL;
	}

	return descope;
}

fct_exsym_t* fct_exsymtab_find_by_name(
	fct_exsymtab_t* table, fct_string_t* name)
{
	fct_exsym_t* exsym
		= fct_exsymtab_find_by_exact_name(table, name);
	if (exsym) return exsym;

	fct_string_t* dname
		= fct_exsymtab__descope(name);
	while (dname)
	{
		exsym = fct_exsymtab_find_by_exact_name(table, dname);
		if (exsym) break;

		fct_string_t* nname = dname;
		dname = fct_exsymtab__descope(dname);
		fct_string_delete(nname);
	}
	fct_string_delete(dname);

	return exsym;
}

fct_exsym_t* fct_exsymtab_find_by_attrib(fct_exsymtab_t* table, fct_exsym_t* exsym)
{
	return fct_list_find(
		table, exsym, (void*)fct_exsym_compare);
}



bool fct_exsymtab_offset(fct_exsymtab_t* table, uint32_t offset)
{
	if (offset == 0)
		return true;
	if (!table)
		return false;

	bool func(fct_exsym_t* symbol, void* params)
	{
		(void)params;
		if (symbol)
			symbol->value += offset;
		return true;
	}

	fct_exsymtab_foreach(
		table, NULL, func);
	return true;
}

fct_exsymtab_t* fct_exsymtab_merge_offset(fct_exsymtab_t* a, fct_exsymtab_t* b, uint32_t offset)
{
	if (!b) return fct_exsymtab_copy(a);

	fct_exsymtab_t* nb = fct_exsymtab_copy(b);
	if (!fct_exsymtab_offset(nb, offset))
	{
		fct_exsymtab_delete(nb);
		return NULL;
	}

	fct_exsymtab_t* merge = fct_exsymtab_merge(a, nb);
	fct_exsymtab_delete(nb);
	return merge;
}
