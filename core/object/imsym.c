#include "imsym.h"
#include <stdlib.h>



static const fct_class_t
	fct_class__imsym =
	{
		.delete_cb    = (void*)fct_imsym_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_imsym_copy,
		.import_cb    = (void*)fct_imsym_import,
		.export_cb    = (void*)fct_imsym_export,
	};
const fct_class_t* fct_class_imsym
	= &fct_class__imsym;



static fct_imsym_t* fct_imsym__create_flags(
	fct_string_t* name,
	fct_string_t* scope,
	uint64_t address, uint64_t offset,
	uint64_t size, uint64_t flags)
{
	if (!name || !name->data
		|| (name->size == 0))
		return NULL;

	fct_imsym_t* imsym
		= (fct_imsym_t*)malloc(sizeof(fct_imsym_t));
	if (!imsym)
		return NULL;

	fct_string_t* fullname;
	if (scope)
	{
		fullname = fct_string_copy(scope);
		if (!fct_string_append(fullname, name))
		{
			fct_string_delete(fullname);
			fullname = NULL;
		}
	}
	else
	{
		fullname = fct_string_reference(name);
	}

	if (!fullname)
	{
		free(imsym);
		return NULL;
	}

	imsym->name       = fullname;
	imsym->address    = address;
	imsym->offset     = offset;
	imsym->size       = size;
	imsym->flags.mask = flags;
	return imsym;
}

fct_imsym_t* fct_imsym_create(
	fct_string_t* name,
	fct_string_t* scope,
	uint64_t address, uint64_t offset, uint64_t size,
	bool relative, bool arch_spec)
{
	fct_imsym_t* imsym = fct_imsym__create_flags(
		name, scope, address, offset, size, 0);
	if (!imsym) return NULL;
	imsym->flags.relative  = relative;
	imsym->flags.arch_spec = arch_spec;
	return imsym;
}

fct_imsym_t* fct_imsym_copy(fct_imsym_t* imsym)
{
	if (!imsym) return NULL;
	return fct_imsym__create_flags(
		imsym->name, NULL,
		imsym->address, imsym->offset,
		imsym->size, imsym->flags.mask);
}

void fct_imsym_delete(fct_imsym_t* imsym)
{
	if (!imsym)
		return;

	fct_string_delete(imsym->name);
	free(imsym);
}



typedef struct
__attribute__((__packed__))
{
	uint64_t          address;
	uint64_t          offset;
	uint64_t          size;
	fct_imsym_flags_t flags;
} fct_imsym_store_t;

fct_imsym_t* fct_imsym_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	fct_buffer_t* ibuffer
		= fct_buffer_subsection_riff64(buffer,
			(offset ? *offset : 0), "IMSYM");
	if (!ibuffer) return NULL;

	unsigned o = 0;

	fct_string_t* name;
	if (!fct_string_import_table(
		ibuffer, &o, strtab, &name))
	{
		fct_buffer_delete(ibuffer);
		return NULL;
	}
	if (!name)
	{
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	fct_imsym_store_t data;
	if (!fct_buffer_read(ibuffer, o, sizeof(data), &data))
	{
		fct_string_delete(name);
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	fct_imsym_t* imsym
		= fct_imsym__create_flags(
			name, NULL, data.address, data.offset,
			data.size, data.flags.mask);
	if (!imsym)
	{
		fct_string_delete(name);
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	if (offset)
		*offset += fct_buffer_size_get(ibuffer)
			+ sizeof(riff64_header_t);
	fct_buffer_delete(ibuffer);
	return imsym;
}

fct_buffer_t* fct_imsym_export(fct_imsym_t* imsym, fct_list_t* strtab)
{
	if (!imsym)
		return NULL;

	fct_buffer_t* buffer
		= fct_buffer_create_riff64("IMSYM");
	if (!buffer) return NULL;

	fct_buffer_t* name
		= fct_string_export_table(
			imsym->name, strtab);
	if (!name
		|| !fct_buffer_append_buffer(buffer, name))
	{
		fct_buffer_delete(name);
		fct_buffer_delete(buffer);
		return NULL;
	}
	fct_buffer_delete(name);

	fct_imsym_store_t data;
	data.address = imsym->address;
	data.offset  = imsym->offset;
	data.flags   = imsym->flags;
	data.size    = imsym->size;

	if (!name
		|| !fct_buffer_append(buffer, &data, sizeof(data)))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	return buffer;
}



fct_string_t* fct_imsym_dump(fct_imsym_t* imsym, unsigned indent)
{
	if (!imsym)
		return NULL;

	char indentstr[indent + 1];
	memset(indentstr, '\t', indent);
	indentstr[indent] = '\0';

	fct_string_t* dump
		= fct_string_create_format(
			"%ssymbol { ", indentstr);
	if (!dump) return NULL;

	bool success = true;
	success = success && fct_string_append(
		dump, imsym->name);

	success = success && fct_string_append_static_format(
		dump, " %s0x%016" PRIX64,
		(imsym->flags.relative ? "+" : ""),
		imsym->address);

	if (imsym->offset > 0)
	{
		success = success && fct_string_append_static_format(
			dump, "+%" PRIu64, imsym->offset);
	}

	success = success && fct_string_append_static_format(
		dump, "[%" PRIu64 "]", imsym->size);

	if (imsym->flags.arch_spec)
		success = success && fct_string_append_static_format(
			dump, " ARCH_SPEC");

	if (imsym->flags.resolved)
		success = success && fct_string_append_static_format(
			dump, " RESOLVED");

	success = success && fct_string_append_static_format(
		dump, " }\n");

	if (!success)
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}



bool fct_imsym_compare(fct_imsym_t* a, fct_imsym_t* b)
{
	if (a == b)
		return true;
	return (a && b
		&& (a->address == b->address)
		&& (a->offset == b->offset)
		&& (a->flags.mask == b->flags.mask)
		&& (a->size == b->size)
		&& fct_string_compare(a->name, b->name));
}
