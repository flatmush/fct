#ifndef __FCT_OBJECT_IMSYMTAB_H__
#define __FCT_OBJECT_IMSYMTAB_H__

#include <fct/types.h>
#include <fct/object/imsym.h>

typedef fct_list_t fct_imsymtab_t;

const fct_class_t* fct_class_imsymtab;

fct_imsymtab_t* fct_imsymtab_create(void);
fct_imsymtab_t* fct_imsymtab_copy(fct_imsymtab_t* table);
fct_imsymtab_t* fct_imsymtab_merge(fct_imsymtab_t* a, fct_imsymtab_t* b);
void            fct_imsymtab_delete(fct_imsymtab_t* table);

bool fct_imsymtab_foreach(
	fct_imsymtab_t* table, void* param,
	bool (*func)(fct_imsym_t* symbol, void* param));

bool fct_imsymtab_add(fct_imsymtab_t* table, fct_imsym_t* imsym);

unsigned fct_imsymtab_count(fct_imsymtab_t* table);
unsigned fct_imsymtab_count_unresolved(fct_imsymtab_t* table);
unsigned fct_imsymtab_count_resolved(fct_imsymtab_t* table);

fct_imsym_t* fct_imsymtab_find_by_name(fct_imsymtab_t* table, fct_string_t* name);
fct_imsym_t* fct_imsymtab_find_by_attrib(fct_imsymtab_t* table, fct_imsym_t* imsym);

fct_imsymtab_t* fct_imsymtab_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab);
fct_buffer_t* fct_imsymtab_export(
	fct_imsymtab_t* table, fct_list_t* strtab);

fct_string_t* fct_imsymtab_dump(fct_imsymtab_t* table, unsigned indent);

bool            fct_imsymtab_offset(fct_imsymtab_t* table, uint32_t offset);
fct_imsymtab_t* fct_imsymtab_merge_offset(fct_imsymtab_t* a, fct_imsymtab_t* b, uint32_t offset);

void fct_imsymtab_remove_resolved(fct_imsymtab_t* table);

#endif
