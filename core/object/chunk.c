#include "chunk.h"
#include <fct/maubuff.h>
#include <fct/object.h>
#include <fct/debug.h>
#include <stdlib.h>



static const fct_class_t
	fct_class__chunk =
	{
		.delete_cb    = (void*)fct_chunk_delete,
		.reference_cb = (void*)fct_chunk_reference,
		.copy_cb      = (void*)fct_chunk_copy,
		.import_cb    = (void*)fct_chunk_import,
		.export_cb    = (void*)fct_chunk_export,
	};
const fct_class_t* fct_class_chunk
	= &fct_class__chunk;



fct_chunk_t* fct_chunk_create(
	fct_arch_t*         arch,
	fct_chunk_access_e  access,
	fct_chunk_content_e content,
	uint64_t            align)
{
	if (align & (align - 1))
		return NULL;

	if ((unsigned)access > FCT_CHUNK_ACCESS_RWX)
		return NULL;

	switch (content)
	{
		case FCT_CHUNK_CONTENT_DATA:
		case FCT_CHUNK_CONTENT_ZERO:
		case FCT_CHUNK_CONTENT_UNINITIALIZED:
			break;
		default:
			return NULL;
	}

	fct_chunk_t* chunk
		= (fct_chunk_t*)malloc(sizeof(fct_chunk_t));
	if (!chunk)
		return NULL;

	chunk->name = NULL;

	chunk->is_static = false;
	chunk->address   = 0;
	chunk->align     = align;

	chunk->arch    = arch;
	chunk->access  = access;
	chunk->content = content;

	chunk->imports = fct_imsymtab_create();
	chunk->exports = fct_exsymtab_create();

	unsigned mau = fct_arch_mau(arch);

	switch (content)
	{
		case FCT_CHUNK_CONTENT_UNINITIALIZED:
		case FCT_CHUNK_CONTENT_ZERO:
			chunk->buffer = fct_maubuff_zero_create(mau);
			break;
		default:
			chunk->buffer = fct_maubuff_default_create(mau);
			break;
	}

	chunk->refcnt = 0;

	if (!chunk->buffer
		|| !chunk->imports
		|| !chunk->exports)
	{
		fct_chunk_delete(chunk);
		return NULL;
	}

	return chunk;
}

fct_chunk_t* fct_chunk_reference(fct_chunk_t* chunk)
{
	if (!chunk)
		return NULL;

	if ((chunk->refcnt + 1) == 0)
		return fct_chunk_copy(chunk);

	chunk->refcnt++;
	return chunk;
}

fct_chunk_t* fct_chunk_copy(fct_chunk_t* chunk)
{
	if (!chunk)
		return NULL;

	fct_chunk_t* copy
		= (fct_chunk_t*)malloc(sizeof(fct_chunk_t));
	if (!copy)
		return NULL;

	copy->name = fct_string_reference(chunk->name);
	if (chunk->name && !copy->name)
	{
		free(copy);
		return NULL;
	}

	copy->is_static = chunk->is_static;
	copy->address   = chunk->address;
	copy->align     = chunk->align;

	copy->arch    = chunk->arch;
	copy->access  = chunk->access;
	copy->content = chunk->content;

	copy->imports = fct_imsymtab_copy(chunk->imports);
	copy->exports = fct_exsymtab_copy(chunk->exports);

	copy->buffer = fct_maubuff_copy(chunk->buffer);
	if ((chunk->buffer && !copy->buffer)
		|| (chunk->imports && !copy->imports)
		|| (chunk->exports && !copy->exports))
	{
		fct_chunk_delete(copy);
		return NULL;
	}

	copy->refcnt = 0;

	return copy;
}

fct_chunk_t* fct_chunk_merge(fct_chunk_t* a, fct_chunk_t* b)
{
	if (!a) return fct_chunk_copy(b);
	if (!b) return fct_chunk_copy(a);

	if (a->name && b->name
		&& !fct_string_compare(a->name, b->name))
	{
		FCT_DEBUG_ERROR(
			"Can't merge chunks with different names");
		return NULL;
	}

	unsigned mau = fct_maubuff_mau_get(a->buffer);
	if (fct_maubuff_mau_get(b->buffer) != mau)
	{
		FCT_DEBUG_ERROR(
			"Can't merge chunks with different addressing granularities");
		return NULL;
	}

	fct_arch_t* arch = (a->arch ? a->arch : b->arch);
	if ((a->arch && b->arch) && (a->arch != b->arch))
	{
		FCT_DEBUG_ERROR(
			"Can't merge chunks with different arch's");
		return NULL;
	}

	fct_chunk_access_e access = (a->access | b->access);

	fct_chunk_content_e content
		= (a->content > b->content ? a->content : b->content);

	/* Merge dynamic chunks */
	if (!fct_chunk_is_static(a)
		&& !fct_chunk_is_static(b))
	{
		if (a->align > b->align)
		{
			fct_chunk_t* c;
			c = a; a = b; b = c;
		}

		/* TODO - Handle non-power-of-two alignment. */
		if (b->align
			&& ((a->align & (a->align - 1))
				|| (b->align & (b->align - 1))))
			return NULL;

		fct_chunk_t* merge
			= (fct_chunk_t*)malloc(sizeof(fct_chunk_t));
		if (!merge) return NULL;

		merge->name = fct_string_reference(a->name ? a->name : b->name);
		if ((a->name || b->name) && !merge->name)
		{
			free(merge);
			return NULL;
		}

		merge->is_static = false;
		merge->address   = 0;
		merge->align     = a->align;

		merge->arch    = arch;
		merge->access  = access;
		merge->content = content;

		uint64_t offset
			= fct_maubuff_size_get(a->buffer);

		uint64_t align = 0;
		if (b->align > 0)
		{
			align = (offset & (b->align - 1));
			if (align) align = (b->align - align);
		}
		offset += align;

		merge->imports = fct_imsymtab_merge_offset(a->imports, b->imports, offset);
		merge->exports = fct_exsymtab_merge_offset(a->exports, b->exports, offset);

		switch (content)
		{
			case FCT_CHUNK_CONTENT_UNINITIALIZED:
			case FCT_CHUNK_CONTENT_ZERO:
				merge->buffer = fct_maubuff_zero_create(mau);
				break;
			default:
				merge->buffer = fct_maubuff_default_create(mau);
				break;
		}

		merge->refcnt = 0;

		if (!merge->imports
			|| !merge->exports)
		{
			fct_chunk_delete(merge);
			return NULL;
		}

		if (!fct_maubuff_append_maubuff(merge->buffer, a->buffer)
			|| (align && !fct_maubuff_append(merge->buffer, NULL, align))
			|| !fct_maubuff_append_maubuff(merge->buffer, b->buffer))
		{
			fct_chunk_delete(merge);
			return NULL;
		}

		fct_chunk_imports_resolve(merge, NULL);
		return merge;
	}

	/* Merge static chunks. */
	if (fct_chunk_is_static(a)
		&& fct_chunk_is_static(b))
	{
		if (a->address > b->address)
		{
			fct_chunk_t* c;
			c = a; a = b; b = c;
		}

		uint64_t padding
			= (a->address + fct_chunk_size_get(a));
		if (b->address < padding)
		{
			FCT_DEBUG_ERROR(
				"Can't merge overlapping static chunks");
			return NULL;
		}
		padding = (b->address - padding);

		uint64_t offset = fct_chunk_size_get(a) + padding;

		fct_chunk_t* merge
			= (fct_chunk_t*)malloc(sizeof(fct_chunk_t));
		if (!merge) return NULL;

		merge->name = fct_string_reference(a->name ? a->name : b->name);
		if ((a->name || b->name) && !merge->name)
		{
			free(merge);
			return NULL;
		}

		merge->is_static = true;
		merge->address   = a->address;
		merge->align     = a->align;

		merge->arch    = arch;
		merge->access  = access;
		merge->content = content;

		merge->imports = fct_imsymtab_merge_offset(a->imports, b->imports, offset);
		merge->exports = fct_exsymtab_merge(a->exports, b->exports);

		switch (content)
		{
			case FCT_CHUNK_CONTENT_UNINITIALIZED:
			case FCT_CHUNK_CONTENT_ZERO:
				merge->buffer = fct_maubuff_zero_create(mau);
				break;
			default:
				merge->buffer = fct_maubuff_default_create(mau);
				break;
		}

		merge->refcnt = 0;

		if (!merge->imports
			|| !merge->exports)
		{
			fct_chunk_delete(merge);
			return NULL;
		}

		if (!fct_maubuff_append_maubuff(merge->buffer, a->buffer)
			|| (padding && !fct_maubuff_append(merge->buffer, NULL, padding))
			|| !fct_maubuff_append_maubuff(merge->buffer, b->buffer))
		{
			fct_chunk_delete(merge);
			return NULL;
		}

		return merge;
	}

	/* Merge static and dynamic chunk. */
	if (!fct_chunk_is_static(a))
	{
		fct_chunk_t* c = a;
		a = b; b = c;
	}

	fct_chunk_t* nb = fct_chunk_copy(b);
	if (!nb) return NULL;

	uint64_t baddr = a->address;
	uint64_t balign = (baddr & (nb->align - 1));
	baddr += fct_maubuff_size_get(a->buffer);
	if (balign)
		baddr += (nb->align - balign);

	nb->address   = baddr;
	nb->is_static = true;

	fct_chunk_t* merge = fct_chunk_merge(a, nb);
	fct_chunk_delete(nb);
	return merge;
}

void fct_chunk_delete(fct_chunk_t* chunk)
{
	if (!chunk)
		return;

	if (chunk->refcnt > 0)
	{
		chunk->refcnt--;
		return;
	}

	fct_string_delete(chunk->name);
	fct_imsymtab_delete(chunk->imports);
	fct_exsymtab_delete(chunk->exports);
	fct_maubuff_delete(chunk->buffer);
	free(chunk);
}



uint64_t fct_chunk_size_get(fct_chunk_t* chunk)
{
	if (!chunk)
		return 0;
	return fct_maubuff_size_get(chunk->buffer);
}



typedef struct
__attribute__((__packed__))
{
	uint64_t address, align;
	uint32_t access;
	uint32_t content;
} fct_chunk_store_t;

fct_chunk_t* fct_chunk_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	fct_buffer_t* ibuffer
		= fct_buffer_subsection_riff64(buffer,
			(offset ? *offset : 0), "CHUNK");
	if (!ibuffer) return NULL;

	unsigned o = 0;

	fct_string_t* name;
	if (!fct_string_import_table(
			ibuffer, &o, strtab, &name))
	{
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	fct_chunk_store_t header;
	if (!fct_buffer_read(ibuffer,
		o, sizeof(header), &header))
	{
		fct_buffer_delete(ibuffer);
		return NULL;
	}
	o += sizeof(header);

	fct_chunk_t* chunk
		= (fct_chunk_t*)malloc(
			sizeof(fct_chunk_t));
	if (!chunk)
	{
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	chunk->name      = name;
	chunk->is_static = false;
	chunk->address   = header.address;
	chunk->align     = header.align;
	chunk->imports   = NULL;
	chunk->exports   = NULL;
	chunk->arch      = NULL;
	chunk->access    = header.access;
	chunk->content   = header.content;

	{
		fct_string_t* arch;
		if (!fct_string_import_table(
			ibuffer, &o, strtab, &arch))
		{
			fct_chunk_delete(chunk);
			fct_buffer_delete(ibuffer);
			return NULL;
		}
		if (arch)
		{
			chunk->arch = fct_arch_by_name(arch);
			if (!chunk->arch)
			{
				FCT_DEBUG_ERROR(
					"Can't find arch '%.*s'.", arch->size, arch->data);
				fct_string_delete(arch);
				fct_chunk_delete(chunk);
				fct_buffer_delete(ibuffer);
				return NULL;
			}
		}
		fct_string_delete(arch);
	}

	chunk->imports = fct_imsymtab_import(ibuffer, &o, strtab);
	chunk->exports = fct_exsymtab_import(ibuffer, &o, strtab);
	chunk->buffer  = fct_maubuff_import(ibuffer, &o, strtab);

	chunk->refcnt = 0;

	if (!chunk->imports
		|| !chunk->exports
		|| !chunk->buffer)
	{
		fct_chunk_delete(chunk);
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	if (offset)
		*offset += fct_buffer_size_get(ibuffer)
			+ sizeof(riff64_header_t);
	fct_buffer_delete(ibuffer);
	return chunk;
}

fct_buffer_t* fct_chunk_export(
	fct_chunk_t* chunk, fct_list_t* strtab)
{
	if (!chunk)
		return NULL;

	fct_chunk_store_t header;
	header.address = chunk->address;
	header.align   = chunk->align;
	header.access  = chunk->access;
	header.content = chunk->content;

	fct_buffer_t* buffer
		= fct_buffer_create_riff64("CHUNK");
	if (!buffer) return NULL;

	{
		fct_buffer_t* name
			= fct_string_export_table(
				chunk->name, strtab);
		if (!name
			|| !fct_buffer_append_buffer(buffer, name))
		{
			fct_buffer_delete(name);
			fct_buffer_delete(buffer);
			return NULL;
		}
		fct_buffer_delete(name);
	}

	if (!fct_buffer_append(buffer,
		&header, sizeof(header)))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	{
		fct_string_t* arch_name
			= fct_arch_name(chunk->arch);
		fct_buffer_t* b
			= fct_string_export_table(arch_name, strtab);
		fct_string_delete(arch_name);
		if (!b || !fct_buffer_append_buffer(buffer, b))
		{
			fct_buffer_delete(b);
			fct_buffer_delete(buffer);
			return NULL;
		}
		fct_buffer_delete(b);
	}

	{
		fct_buffer_t* b
			= fct_imsymtab_export(
				chunk->imports, strtab);
		if (!b
			|| !fct_buffer_append_buffer(buffer, b))
		{
			fct_buffer_delete(b);
			fct_buffer_delete(buffer);
			return NULL;
		}
		fct_buffer_delete(b);
	}

	{
		fct_buffer_t* b
			= fct_exsymtab_export(
				chunk->exports, strtab);
		if (!b
			|| !fct_buffer_append_buffer(buffer, b))
		{
			fct_buffer_delete(b);
			fct_buffer_delete(buffer);
			return NULL;
		}
		fct_buffer_delete(b);
	}

	{
		fct_buffer_t* b
			= fct_maubuff_export(
				chunk->buffer, strtab);
		if (!b
			|| !fct_buffer_append_buffer(buffer, b))
		{
			fct_buffer_delete(b);
			fct_buffer_delete(buffer);
			return NULL;
		}
		fct_buffer_delete(b);
	}

	return buffer;
}



fct_string_t* fct_chunk_dump(
	fct_chunk_t* chunk, unsigned indent)
{
	if (!chunk)
		return NULL;

	char indentstr[indent + 1];
	memset(indentstr, '\t', indent);
	indentstr[indent] = '\0';

	fct_string_t* dump
		= fct_string_create_format(
			"%schunk", indentstr);
	if (!dump) return NULL;

	bool success = true;

	if (chunk->name)
	{
		success = success && fct_string_append_static(dump, " \"");
		success = success && fct_string_append(dump, chunk->name);
		success = success && fct_string_append_static(dump, "\"");
	}

	success = success && fct_string_append_static_format(
		dump, "\n%s{\n", indentstr);

	success = success && fct_string_append_static_format(
		dump, "%s\tstatic:    %s\n", indentstr,
		(chunk->is_static ? "true" : "false"));

	if (chunk->is_static)
	{
		success = success && fct_string_append_static_format(
			dump, "%s\taddress:   0x%016" PRIX64 "\n", indentstr, chunk->address);
	}
	else if (chunk->align > 0)
	{
		success = success && fct_string_append_static_format(
			dump, "%s\talignment: %" PRIu64 "\n", indentstr, chunk->align);
	}

	success = success && fct_string_append_static_format(
		dump, "%s\tsize:      %" PRIu64 "\n",
		indentstr, fct_chunk_size_get(chunk));

	success = success && fct_string_append_static_format(
		dump, "%s\taccess:    %s\n", indentstr,
		fct_chunk_access_string(chunk->access));

	if (chunk->access & FCT_CHUNK_ACCESS_X)
	{
		fct_string_t* arch_name
			= fct_arch_name(chunk->arch);
		if (arch_name)
		{
			success = success && fct_string_append_static_format(
				dump, "%s\tarch:      ", indentstr);
			success = success && fct_string_append(dump, arch_name);
			success = success && fct_string_append_static(dump, "\n");
		}
		fct_string_delete(arch_name);
	}

	if (fct_imsymtab_count(chunk->imports) > 0)
	{
		success = success && fct_string_append_static_format(
			dump, "%s\timports\n", indentstr);
		fct_string_t* import_dump
			= fct_imsymtab_dump(chunk->imports, (indent + 1));
		success = success && (import_dump != NULL);
		success = success && fct_string_append(dump, import_dump);
		fct_string_delete(import_dump);
	}

	if (fct_exsymtab_count(chunk->exports) > 0)
	{
		success = success && fct_string_append_static_format(
			dump, "%s\texports\n", indentstr);
		fct_string_t* export_dump
			= fct_exsymtab_dump(chunk->exports, (indent + 1));
		success = success && (export_dump != NULL);
		success = success && fct_string_append(dump, export_dump);
		fct_string_delete(export_dump);
	}

	switch (chunk->content)
	{
		case FCT_CHUNK_CONTENT_UNINITIALIZED:
			break;

		case FCT_CHUNK_CONTENT_ZERO:
			success = success && fct_string_append_static_format(
				dump, "%s\tdata\n%s\t{0}\n", indentstr, indentstr);
			break;

		case FCT_CHUNK_CONTENT_DATA:
		{
			fct_string_t* dsm = NULL;
			if ((chunk->access & FCT_CHUNK_ACCESS_X) != 0)
				dsm = fct_arch_disassemble_buffer(
					chunk->arch, chunk->buffer, (indent + 2));
			if (dsm)
			{
				success = success && fct_string_append_static_format(
					dump, "%s\tcode\n%s\t{\n", indentstr, indentstr);
				success = success && fct_string_append(dump, dsm);
				fct_string_delete(dsm);
			}
			else
			{
				unsigned width = 4;
				if (chunk->arch)
					width = fct_arch_word_size(chunk->arch);
				success = success && fct_string_append_static_format(
					dump, "%s\tdata\n%s\t{\n", indentstr, indentstr);
				fct_string_t* hex = fct_maubuff_to_string_hex(
					chunk->buffer, (indent + 2), width);
				if (!hex) success = false;
				success = success && fct_string_append(dump, hex);
				fct_string_delete(hex);
			}
			success = success && fct_string_append_static_format(
				dump, "%s\t}\n", indentstr);
		}
		break;

		default:
			success = success && fct_string_append_static_format(
				dump, "%s\tdata\n%s\t{?}\n", indentstr, indentstr);
			break;
	}

	success = success && fct_string_append_static_format(
		dump, "%s}\n", indentstr);

	if (!success)
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}



bool fct_chunk_align(fct_chunk_t* chunk, uint64_t align)
{
	if (align == 0)
		return true;
	if (!chunk
		|| (align & (align - 1)))
		return false;

	if (fct_chunk_is_static(chunk)
		&& (chunk->address & (align - 1)) != 0)
		return false;

	if ((chunk->align == 0)
		|| (align > chunk->align))
		chunk->align = align;
	return true;
}



bool fct_chunk_imports_add_symbol(fct_chunk_t* chunk, fct_imsym_t* symbol)
{
	if (!chunk)
		return false;
	return fct_imsymtab_add(chunk->imports, symbol);
}

bool fct_chunk_exports_add_symbol(fct_chunk_t* chunk, fct_exsym_t* symbol)
{
	if (!chunk)
		return false;

	if (fct_chunk_is_static(chunk))
		return false;

	return fct_exsymtab_add(chunk->exports, symbol);
}



static bool fct_chunk_symbol_resolve_internal(
	fct_chunk_t* chunk, fct_string_t* name,
	uint64_t address, uint64_t offset,
	uint64_t* value, uint64_t* size)
{
	if (!chunk) return false;

	fct_exsym_t* match
		= fct_exsymtab_find_by_name(
			chunk->exports, name);
	if (!match) return false;

	if (value) *value = (match->value - (offset + address));
	if (size ) *size  = match->size;
	return true;
}

static bool fct_chunk_symbol_resolve_object(
	fct_chunk_t* chunk, fct_object_t* object, fct_string_t* name,
	bool relative, uint64_t address, uint64_t offset,
	uint64_t* value, uint64_t* size)
{
	if (!object)
		return false;

	fct_exsym_t* match
		= fct_exsymtab_find_by_name(
			object->exports, name);
	if (!match) return false;

	uint64_t o = match->value - offset;
	if (relative)
	{
		if (!fct_chunk_is_static(chunk))
			return false;
		o -= (address + chunk->address);
	}

	if (value) *value = o;
	if (size ) *size  = match->size;
	return true;
}

bool fct_chunk_symbol_resolve(
	fct_chunk_t* chunk, fct_object_t* object, fct_string_t* name,
	bool relative, uint64_t address, uint64_t offset,
	uint64_t* value, uint64_t* size)
{
	if (relative && fct_chunk_symbol_resolve_internal(
		chunk, name, address, offset, value, size))
		return true;
	return fct_chunk_symbol_resolve_object(
		chunk, object, name,
		relative, address, offset,
		value, size);
}



void fct_chunk_imports_resolve(fct_chunk_t* chunk, fct_object_t* object)
{
	if (!chunk)
		return;

	bool internal_func(
		fct_imsym_t* symbol, void* params)
	{
		(void)params;
		if (!symbol
			||  symbol->flags.resolved
			|| !symbol->flags.relative)
			return true;

		uint64_t offset, size;
		if (!fct_chunk_symbol_resolve_internal(
			chunk, symbol->name,
			symbol->address, symbol->offset,
			&offset, &size))
			return true;

		if (symbol->flags.arch_spec)
		{
			unsigned mau = fct_maubuff_mau_get(chunk->buffer);
			uint32_t data[((symbol->size * mau) + 31) >> 5];
			if (fct_maubuff_read(chunk->buffer, symbol->address, symbol->size, data)
				&& fct_arch_link(chunk->arch, data, symbol->size, offset)
				&& fct_maubuff_modify(chunk->buffer, symbol->address, data, symbol->size))
				symbol->flags.resolved = true;
		}
		else if (symbol->size == size)
		{
			if (fct_maubuff_modify(chunk->buffer,
				symbol->address, &offset, symbol->size))
				symbol->flags.resolved = true;
		}

		return true;
	}

	fct_imsymtab_foreach(
		chunk->imports, NULL, internal_func);

	if (!object)
		return;

	bool object_func(fct_imsym_t* symbol, void* params)
	{
		(void)params;
		if (!symbol
			|| symbol->flags.resolved)
			return true;

		uint64_t offset, size;
		if (!fct_chunk_symbol_resolve_object(
			chunk, object, symbol->name,
			symbol->flags.relative, symbol->address, symbol->offset,
			&offset, &size))
			return true;

		if (symbol->flags.arch_spec)
		{
			unsigned mau = fct_maubuff_mau_get(chunk->buffer);
			uint32_t data[((symbol->size * mau) + 31) >> 5];
			if (fct_maubuff_read(chunk->buffer, symbol->address, symbol->size, data)
				&& fct_arch_link(chunk->arch, data, symbol->size, offset)
				&& fct_maubuff_modify(chunk->buffer, symbol->address, data, symbol->size))
				symbol->flags.resolved = true;
		}
		else if (symbol->size == size)
		{
			if (fct_maubuff_modify(chunk->buffer,
				symbol->address, &offset, symbol->size))
				symbol->flags.resolved = true;
		}

		return true;
	}

	fct_imsymtab_foreach(
		chunk->imports, NULL, object_func);
}



void fct_chunk_imports_remove_resolved(fct_chunk_t* chunk)
{
	if (!chunk)
		return;
	fct_imsymtab_remove_resolved(chunk->imports);
}



bool fct_chunk_append(fct_chunk_t* chunk, const void* data, uint64_t size)
{
	if (size == 0)
		return true;
	if (!chunk)
		return false;

	return fct_maubuff_append(chunk->buffer, data, size);
}

void fct_chunk_truncate(fct_chunk_t* chunk, uint64_t size)
{
	if (!chunk)
		return;
	fct_maubuff_truncate(chunk->buffer, size);
}



bool fct_chunk_adjoining(fct_chunk_t* a, fct_chunk_t* b)
{
	if (!a || !b)
		return false;
	return (b->address == (a->address + fct_chunk_size_get(a)));
}
