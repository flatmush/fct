#include "imsymtab.h"
#include <stdlib.h>



static const fct_class_t
	fct_class__imsymtab =
	{
		.delete_cb    = (void*)fct_imsymtab_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_imsymtab_copy,
		.import_cb    = (void*)fct_imsymtab_import,
		.export_cb    = (void*)fct_imsymtab_export,
	};
const fct_class_t* fct_class_imsymtab
	= &fct_class__imsymtab;



fct_imsymtab_t* fct_imsymtab_create(void)
{
	return fct_array_create(fct_class_imsym);
}



fct_imsymtab_t* fct_imsymtab_copy(fct_imsymtab_t* table)
{
	return fct_list_copy(table);
}



fct_imsymtab_t* fct_imsymtab_merge(
	fct_imsymtab_t* a, fct_imsymtab_t* b)
{
	if (!a) return fct_list_copy(b);
	if (!b) return fct_list_copy(a);

	fct_imsymtab_t* table
		= fct_imsymtab_create();
	if (!table) return NULL;

	bool func(fct_imsym_t* symbol, void* param)
	{
		(void)param;
		fct_imsym_t* imsym
			= fct_imsym_copy(symbol);
		if (!symbol) return false;

		if (!fct_imsymtab_add(
			table, imsym))
		{
			fct_imsym_delete(imsym);
			return false;
		}

		return true;
	}

	if (!fct_imsymtab_foreach(a, NULL, func)
		|| !fct_imsymtab_foreach(b, NULL, func))
	{
		fct_imsymtab_delete(table);
		return NULL;
	}

	return table;
}

void fct_imsymtab_delete(fct_imsymtab_t* table)
{
	fct_list_delete(table);
}



bool fct_imsymtab_foreach(
	fct_imsymtab_t* table, void* param,
	bool (*func)(fct_imsym_t* symbol, void* param))
{
	return fct_list_foreach(table, param, (void*)func);
}



fct_imsymtab_t* fct_imsymtab_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	fct_imsymtab_t* table
		= fct_imsymtab_create();
	if (!table) return NULL;

	if (!fct_list_import_named(
		table, "IMSYMTAB",
		buffer, offset, strtab))
	{
		fct_list_delete(table);
		return NULL;
	}

	return table;
}

fct_buffer_t* fct_imsymtab_export(
	fct_imsymtab_t* table, fct_list_t* strtab)
{
	return fct_list_export_named(
		"IMSYMTAB", table, strtab);
}



fct_string_t* fct_imsymtab_dump(fct_imsymtab_t* table, unsigned indent)
{
	if (!table)
		return NULL;

	return fct_list_dump(table,
		indent, (void*)fct_imsym_dump);
}



bool fct_imsymtab_add(fct_imsymtab_t* table, fct_imsym_t* imsym)
{
	if (!imsym)
		return false;

	if (fct_imsymtab_find_by_attrib(
		table, imsym))
	{
		fct_imsym_delete(imsym);
		return true;
	}

	return fct_list_add(table, imsym);
}



unsigned fct_imsymtab_count(fct_imsymtab_t* table)
{
	return (unsigned)fct_list_count(table);
}

unsigned fct_imsymtab_count_unresolved(fct_imsymtab_t* table)
{
	unsigned unresolved = 0;

	bool func(fct_imsym_t* symbol, void* param)
	{
		(void)param;
		if (symbol && !symbol->flags.resolved)
			unresolved++;
		return true;
	}

	fct_imsymtab_foreach(
		table, NULL, func);
	return unresolved;
}

unsigned fct_imsymtab_count_resolved(fct_imsymtab_t* table)
{
	return (fct_imsymtab_count(table)
		- fct_imsymtab_count_unresolved(table));
}



fct_imsym_t* fct_imsymtab_find_by_attrib(fct_imsymtab_t* table, fct_imsym_t* imsym)
{
	return fct_list_find(
		table, imsym, (void*)fct_imsym_compare);
}



bool fct_imsymtab_offset(fct_imsymtab_t* table, uint32_t offset)
{
	if (offset == 0)
		return true;
	if (!table)
		return false;

	bool func(fct_imsym_t* symbol, void* param)
	{
		(void)param;
		if (symbol)
			symbol->address += offset;
		return true;
	}

	fct_imsymtab_foreach(
		table, NULL, func);
	return true;
}

fct_imsymtab_t* fct_imsymtab_merge_offset(fct_imsymtab_t* a, fct_imsymtab_t* b, uint32_t offset)
{
	if (!b) return fct_imsymtab_copy(a);

	fct_imsymtab_t* nb = fct_imsymtab_copy(b);
	if (!fct_imsymtab_offset(nb, offset))
	{
		fct_imsymtab_delete(nb);
		return NULL;
	}

	fct_imsymtab_t* merge = fct_imsymtab_merge(a, nb);
	fct_imsymtab_delete(nb);
	return merge;
}



void fct_imsymtab_remove_resolved(fct_imsymtab_t* table)
{
	if (!table)
		return;

	bool is_resolved(
		fct_imsym_t* symbol, void* param)
	{
		(void)param;
		return (symbol && symbol->flags.resolved);
	}

	fct_list_find_delete_all(
		table, NULL, (void*)is_resolved);
}
