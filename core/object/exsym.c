#include "exsym.h"
#include <stdlib.h>



static const fct_class_t
	fct_class__exsym =
	{
		.delete_cb    = (void*)fct_exsym_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_exsym_copy,
		.import_cb    = (void*)fct_exsym_import,
		.export_cb    = (void*)fct_exsym_export,
	};
const fct_class_t* fct_class_exsym
	= &fct_class__exsym;



fct_exsym_t* fct_exsym_create(
	fct_string_t* name,
	fct_string_t* scope,
	uint64_t value, uint64_t size)
{
	if (!name || !name->data
		|| (name->size == 0))
		return NULL;

	fct_exsym_t* exsym
		= (fct_exsym_t*)malloc(sizeof(fct_exsym_t));
	if (!exsym)
		return NULL;

	fct_string_t* fullname;
	if (scope)
	{
		fullname = fct_string_copy(scope);
		if (!fct_string_append(fullname, name))
		{
			fct_string_delete(fullname);
			fullname = NULL;
		}
	}
	else
	{
		fullname = fct_string_reference(name);
	}

	if (!fullname)
	{
		free(exsym);
		return NULL;
	}

	exsym->name  = fullname;
	exsym->value = value;
	exsym->size  = size;
	return exsym;
}

fct_exsym_t* fct_exsym_copy(fct_exsym_t* exsym)
{
	if (!exsym)
		return NULL;
	return fct_exsym_create(
		exsym->name, NULL,
		exsym->value, exsym->size);
}

void fct_exsym_delete(fct_exsym_t* exsym)
{
	if (!exsym)
		return;

	fct_string_delete(exsym->name);
	free(exsym);
}



typedef struct
__attribute__((__packed__))
{
	uint64_t value;
	uint64_t size;
} fct_exsym_store_t;

fct_exsym_t* fct_exsym_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	fct_buffer_t* ibuffer
		= fct_buffer_subsection_riff64(buffer,
			(offset ? *offset : 0), "EXSYM");
	if (!ibuffer) return NULL;

	unsigned o = 0;

	fct_string_t* name;
	if (!fct_string_import_table(
		ibuffer, &o, strtab, &name))
	{
		fct_buffer_delete(ibuffer);
		return NULL;
	}
	if (!name)
	{
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	fct_exsym_store_t data;
	if (!fct_buffer_read(ibuffer, o, sizeof(data), &data))
	{
		fct_string_delete(name);
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	fct_exsym_t* exsym
		= fct_exsym_create(name, NULL, data.value, data.size);
	if (!exsym)
	{
		fct_string_delete(name);
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	if (offset)
		*offset += fct_buffer_size_get(ibuffer)
			+ sizeof(riff64_header_t);
	fct_buffer_delete(ibuffer);
	return exsym;
}

fct_buffer_t* fct_exsym_export(fct_exsym_t* exsym, fct_list_t* strtab)
{
	if (!exsym)
		return NULL;

	fct_buffer_t* buffer
		= fct_buffer_create_riff64("EXSYM");
	if (!buffer) return NULL;

	fct_buffer_t* name
		= fct_string_export_table(
			exsym->name, strtab);
	if (!name
		|| !fct_buffer_append_buffer(buffer, name))
	{
		fct_buffer_delete(name);
		fct_buffer_delete(buffer);
		return NULL;
	}
	fct_buffer_delete(name);

	fct_exsym_store_t data;
	data.value = exsym->value;
	data.size  = exsym->size;

	if (!name
		|| !fct_buffer_append(buffer, &data, sizeof(data)))
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	return buffer;
}



fct_string_t* fct_exsym_dump(fct_exsym_t* exsym, unsigned indent)
{
	if (!exsym)
		return NULL;

	char indentstr[indent + 1];
	memset(indentstr, '\t', indent);
	indentstr[indent] = '\0';

	fct_string_t* dump
		= fct_string_create_format(
			"%ssymbol { ", indentstr);
	if (!dump) return NULL;

	bool success = true;
	success = success && fct_string_append(
		dump, exsym->name);
	success = success && fct_string_append_static_format(
		dump, " 0x%016" PRIX64, exsym->value);
	success = success && fct_string_append_static_format(
		dump, "[%" PRIu64 "]", exsym->size);
	success = success && fct_string_append_static_format(
		dump, " }\n");

	if (!success)
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}



bool fct_exsym_compare(fct_exsym_t* a, fct_exsym_t* b)
{
	if (a == b)
		return true;
	return (a && b
		&& (a->value == b->value)
		&& (a->size == b->size)
		&& fct_string_compare(a->name, b->name));
}
