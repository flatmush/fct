#include <fct/object.h>
#include <stdlib.h>

#include <fct/debug.h>



static const fct_class_t
	fct_class__object =
	{
		.delete_cb    = (void*)fct_object_delete,
		.reference_cb = NULL,
		.copy_cb      = (void*)fct_object_copy,
		.import_cb    = (void*)fct_object_import,
		.export_cb    = (void*)fct_object_export,
	};
const fct_class_t* fct_class_object
	= &fct_class__object;



static int fct_object__static_chunk_sort(
	fct_chunk_t* a, fct_chunk_t* b)
{
	if (!a) return (!b ? 0 : 1);
	if (!b) return -1;

	if (a->address == b->address)
		return 0;
	return (b->address > a->address ? 1 : -1);
}

static int fct_object__dynamic_chunk_sort(
	fct_chunk_t* a, fct_chunk_t* b)
{
	if (!a) return (!b ? 0 : 1);
	if (!b) return -1;

	if ((a->arch && b->arch)
		&& (a->arch != b->arch))
		return ((uintptr_t)b->arch > (uintptr_t)a->arch ? 1 : -1);

	if (a->content != b->content)
		return (b->content > a->content ? 1 : -1);
	if (a->access != b->access)
		return (b->access > a->access ? 1 : -1);
	if (a->align != b->align)
		return (b->align < a->align ? 1 : -1);

	uint64_t asize = fct_chunk_size_get(a);
	uint64_t bsize = fct_chunk_size_get(b);

	if (asize == bsize) return 0;
	return (bsize < asize ? 1 : -1);
}

fct_object_t* fct_object_create(void)
{
	fct_object_t* object
		= (fct_object_t*)malloc(sizeof(fct_object_t));
	if (!object) return NULL;

	object->exports
		= fct_exsymtab_create();
	object->chunks_static
		= fct_slist_create(fct_class_chunk,
			(void*)fct_object__static_chunk_sort);
	object->chunks_dynamic
		= fct_slist_create(fct_class_chunk,
			(void*)fct_object__dynamic_chunk_sort);
	return object;
}

fct_object_t* fct_object_copy(fct_object_t* object)
{
	fct_object_t* copy
		= (fct_object_t*)malloc(sizeof(fct_object_t));
	if (!copy) return NULL;

	copy->exports = fct_exsymtab_copy(object->exports);
	copy->chunks_static
		= fct_list_copy(object->chunks_static);
	copy->chunks_dynamic
		= fct_list_copy(object->chunks_dynamic);

	if ((object->exports && !copy->exports)
		|| (object->chunks_static && !copy->chunks_static)
		|| (object->chunks_dynamic && !copy->chunks_dynamic))
	{
		fct_object_delete(copy);
		return NULL;
	}

	return copy;
}

fct_object_t* fct_object_merge(fct_object_t* a, fct_object_t* b)
{
	if (!a) return fct_object_copy(b);
	if (!b) return fct_object_copy(a);

	fct_object_t* object
		= fct_object_copy(a);
	if (!object) return NULL;

	if (object->exports || b->exports)
	{
		fct_exsymtab_t* exports
			= fct_exsymtab_merge(object->exports, b->exports);
		if (!exports)
		{
			fct_object_delete(object);
			return NULL;
		}
		fct_exsymtab_delete(object->exports);
		object->exports = exports;
	}

	bool merge(fct_chunk_t* chunk, void* params)
	{
		(void)params;

		fct_chunk_t* nchunk
			= fct_chunk_copy(chunk);
		if (!nchunk) return false;

		if (!fct_object_chunk_add(object, nchunk))
		{
			fct_chunk_delete(nchunk);
			return false;
		}

		return true;
	}

	if (!fct_list_foreach(b->chunks_static, NULL, (void*)merge)
		|| !fct_list_foreach(b->chunks_dynamic, NULL, (void*)merge))
	{
		fct_object_delete(object);
		return NULL;
	}

	return object;
}

void fct_object_delete(fct_object_t* object)
{
	if (!object)
		return;

	fct_exsymtab_delete(object->exports);
	fct_list_delete(object->chunks_static);
	fct_list_delete(object->chunks_dynamic);
	free(object);
}



typedef struct
__attribute__((__packed__))
{
	uint64_t version;
} fct_object_header_t;

static const fct_object_header_t fct_object_header =
{
	.version = 1,
};

fct_object_t* fct_object_import(
	fct_buffer_t* buffer, unsigned* offset, fct_list_t* strtab)
{
	fct_buffer_t* ibuffer
		= fct_buffer_subsection_riff64(buffer,
			(offset ? *offset : 0), "FOBJECT");
	if (!ibuffer) return NULL;

	unsigned o = 0;

	fct_object_header_t header;
	if (!fct_buffer_read(ibuffer,
		o, sizeof(header), &header)
		|| (header.version != fct_object_header.version))
	{
		fct_buffer_delete(ibuffer);
		return NULL;
	}
	o += sizeof(header);

	fct_list_t* ostrtab = strtab;
	if (!ostrtab)
	{
		ostrtab = fct_strtab_create();
		if (!fct_list_import(
			ostrtab, ibuffer, &o, NULL))
		{
			fct_list_delete(ostrtab);
			fct_buffer_delete(ibuffer);
			return NULL;
		}
	}

	fct_object_t* object
		= (fct_object_t*)malloc(
			sizeof(fct_object_t));
	if (!object)
	{
		if (ostrtab != strtab)
			fct_list_delete(ostrtab);
		fct_buffer_delete(ibuffer);
		return NULL;
	}

	object->exports
		= fct_exsymtab_import(ibuffer, &o, ostrtab);

	object->chunks_static
		= fct_slist_create(fct_class_chunk,
			(void*)fct_object__static_chunk_sort);
	object->chunks_dynamic
		= fct_slist_create(fct_class_chunk,
			(void*)fct_object__dynamic_chunk_sort);

	if (!fct_list_import(
		object->chunks_static,
		ibuffer, &o, ostrtab))
	{
		fct_list_delete(object->chunks_static);
		object->chunks_static = NULL;
	}

	if (!fct_list_import(
		object->chunks_dynamic,
		ibuffer, &o, ostrtab))
	{
		fct_list_delete(object->chunks_dynamic);
		object->chunks_dynamic = NULL;
	}

	/* Mark static chunks as static. */
	{
		bool mark_static(fct_chunk_t* chunk, void* params)
		{
			(void)params;
			if (chunk) chunk->is_static = true;
			return true;
		}

		fct_list_foreach(
			object->chunks_static,
			NULL, (void*)mark_static);
	}

	fct_buffer_delete(ibuffer);

	if (ostrtab != strtab)
		fct_list_delete(ostrtab);

	if (!object->exports
		|| !object->chunks_static
		|| !object->chunks_dynamic)
	{
		fct_object_delete(object);
		return NULL;
	}

	if (offset)
		*offset = fct_buffer_size_get(ibuffer)
			+ sizeof(riff64_header_t);
	return object;
}

fct_buffer_t* fct_object_export(
	fct_object_t* object, fct_list_t* strtab)
{
	if (!object)
		return NULL;

	fct_list_t* ostrtab = strtab;
	if (!ostrtab)
	{
		ostrtab = fct_strtab_create();
		if (!ostrtab) return NULL;
	}

	fct_buffer_t* bexport
		= fct_exsymtab_export(
			object->exports, ostrtab);
	fct_buffer_t* bstatic
		= fct_list_export(
			object->chunks_static, ostrtab);
	fct_buffer_t* bdynamic
		= fct_list_export(
			object->chunks_dynamic, ostrtab);
	fct_buffer_t* bstrtab
		= fct_list_export(
			ostrtab, NULL);

	if (ostrtab != strtab)
		fct_list_delete(ostrtab);

	if (!bexport
		|| !bstatic
		|| !bdynamic
		|| !bstrtab)
	{
		fct_buffer_delete(bstrtab);
		fct_buffer_delete(bdynamic);
		fct_buffer_delete(bstatic);
		fct_buffer_delete(bexport);
		return NULL;
	}

	fct_buffer_t* buffer
		= fct_buffer_create_riff64("FOBJECT");

	bool success = (buffer
		&& fct_buffer_append(
			buffer, (void*)&fct_object_header, sizeof(fct_object_header))
		&& fct_buffer_append_buffer(buffer, bstrtab)
		&& fct_buffer_append_buffer(buffer, bexport)
		&& fct_buffer_append_buffer(buffer, bstatic)
		&& fct_buffer_append_buffer(buffer, bdynamic));

	fct_buffer_delete(bstrtab);
	fct_buffer_delete(bdynamic);
	fct_buffer_delete(bstatic);
	fct_buffer_delete(bexport);

	if (!success)
	{
		fct_buffer_delete(buffer);
		return NULL;
	}

	return buffer;
}



static fct_maubuff_t* fct_object__flat_maubuff(fct_object_t* object)
{
	if (!object)
		return NULL;

	if (fct_list_count(object->chunks_dynamic) > 0)
	{
		FCT_DEBUG_ERROR(
			"Can't raw export an object with dynamic chunks.");
		return NULL;
	}

	fct_maubuff_t* maubuff = NULL;
	uint64_t tail = 0;

	bool flatten(fct_chunk_t* chunk, void* params)
	{
		(void)params;

		if (!maubuff)
		{
			maubuff = fct_maubuff_default_create(
				fct_maubuff_mau_get(chunk->buffer));
			if (!maubuff) return false;
		}
		else
		{
			if (chunk->address < tail)
				return false;
			uint64_t zsize = chunk->address - tail;
			if (!fct_maubuff_append(maubuff, NULL, zsize))
				return false;
		}

		if (!fct_maubuff_append_maubuff(
			maubuff, chunk->buffer))
			return false;

		tail = chunk->address + fct_chunk_size_get(chunk);
		return true;
	}

	if (!fct_list_foreach(
		object->chunks_static,
		NULL, (void*)flatten))
	{
		fct_maubuff_delete(maubuff);
		return NULL;
	}

	return maubuff;
}

fct_buffer_t* fct_object_export_raw(fct_object_t* object)
{
	fct_maubuff_t* maubuff
		= fct_object__flat_maubuff(object);
	fct_buffer_t* buffer
		= fct_maubuff_to_buffer(maubuff);
	fct_maubuff_delete(maubuff);
	return buffer;
}

fct_string_t* fct_object_export_raw_hex(fct_object_t* object)
{
	fct_maubuff_t* maubuff
		= fct_object__flat_maubuff(object);
	fct_string_t* string
		= fct_maubuff_to_string_hex(maubuff, 0, 16);
	fct_maubuff_delete(maubuff);
	return string;
}



fct_object_t* fct_object_file_import(const char* path)
{
	fct_buffer_t* buffer
		= fct_buffer_file_import(path);
	if (!buffer) return NULL;

	fct_object_t* object
		= fct_object_import(buffer, NULL, NULL);
	fct_buffer_delete(buffer);
	return object;
}

bool fct_object_file_export(fct_object_t* object, const char* path)
{
	if (!object)
		return false;

	fct_buffer_t* buffer
		= fct_object_export(object, NULL);
	bool success = fct_buffer_file_export(buffer, path);
	fct_buffer_delete(buffer);
	return success;
}

bool fct_object_file_export_raw(fct_object_t* object, const char* path)
{
	if (!object)
		return false;

	fct_buffer_t* buffer
		= fct_object_export_raw(object);
	bool success = fct_buffer_file_export(buffer, path);
	fct_buffer_delete(buffer);
	return success;
}

bool fct_object_file_export_raw_hex(fct_object_t* object, const char* path)
{
	if (!object)
		return false;

	fct_string_t* string
		= fct_object_export_raw_hex(object);
	bool success = fct_string_file_export(string, path);
	fct_string_delete(string);
	return success;
}

bool fct_object_file_export_format(fct_object_t* object,
	fct_object_format_e format, const char* path)
{
	if (!object) return false;

	switch (format)
	{
		case FCT_OBJECT_FORMAT_FOBJECT:
			return fct_object_file_export(object, path);

		case FCT_OBJECT_FORMAT_BINARY:
			return fct_object_file_export_raw(object, path);

		case FCT_OBJECT_FORMAT_HEX:
			return fct_object_file_export_raw_hex(object, path);

		case FCT_OBJECT_FORMAT_IHEX:
			return fct_object_file_export_ihex(object, path);

		case FCT_OBJECT_FORMAT_SREC:
			return fct_object_file_export_srec(object, path);

		case FCT_OBJECT_FORMAT_TEK:
			return fct_object_file_export_tek(object, path);

		default:
			break;
	}

	return false;
}



fct_string_t* fct_object_dump(fct_object_t* object, unsigned indent)
{
	if (!object)
		return NULL;

	char indentstr[indent + 1];
	memset(indentstr, '\t', indent);
	indentstr[indent] = '\0';

	fct_string_t* dump
		= fct_string_create_format(
			"%sobject\n%s{\n", indentstr, indentstr);
	if (!dump) return NULL;

	bool success = true;

	if (fct_exsymtab_count(object->exports) > 0)
	{
		success = success && fct_string_append_static_format(
			dump, "%s\texports\n", indentstr);
		fct_string_t* export_dump
			= fct_exsymtab_dump(object->exports, (indent + 1));
		success = success && (export_dump != NULL);
		success = success && fct_string_append(dump, export_dump);
		fct_string_delete(export_dump);
	}

	if (fct_list_count(object->chunks_static) > 0)
	{
		success = success && fct_string_append_static_format(
			dump, "%s\tstatic-chunks\n", indentstr);
		fct_string_t* static_dump
			= fct_list_dump(
				object->chunks_static, (indent + 1),
				(void*)fct_chunk_dump);
		success = success && (static_dump != NULL);
		success = success && fct_string_append(dump, static_dump);
		fct_string_delete(static_dump);
	}

	if (fct_list_count(object->chunks_dynamic) > 0)
	{
		success = success && fct_string_append_static_format(
			dump, "%s\tdynamic-chunks\n", indentstr);
		fct_string_t* dynamic_dump
			= fct_list_dump(
				object->chunks_dynamic, (indent + 1),
				(void*)fct_chunk_dump);
		success = success && (dynamic_dump != NULL);
		success = success && fct_string_append(dump, dynamic_dump);
		fct_string_delete(dynamic_dump);
	}

	success = success && fct_string_append_static_format(
		dump, "%s}\n", indentstr);

	if (!success)
	{
		fct_string_delete(dump);
		return NULL;
	}

	return dump;
}



static bool fct_chunk__adjoining_mergable(
    fct_chunk_t* a, fct_chunk_t* b)
{
    if (!fct_chunk_adjoining(a, b))
        return false;

    if ((a->arch && b->arch)
        && (a->arch != b->arch))
        return false;

    return ((a->content == b->content)
		&& (a->access == b->access));
}

static bool fct_chunk__dynamic_mergable(
    fct_chunk_t* a, fct_chunk_t* b)
{
	if ((a->arch && b->arch)
		&& (a->arch != b->arch))
		return false;

	if ((a->content != b->content)
		|| (a->access != b->access))
		return false;

	if ((a->align & (a->align -1))
		|| (b->align & (b->align -1)))
		return false;

    return true;
}

fct_object_t* fct_object_optimize(fct_object_t* object)
{
	if (!object)
		return NULL;

	fct_object_t* opt
		= fct_object_copy(object);
	if (!opt) return NULL;

	fct_object_imports_resolve(opt);

	/* Delete empty static chunks. */
	{
		bool is_empty(fct_chunk_t* chunk, void* params)
		{
			(void)params;
			return (fct_chunk_size_get(chunk) == 0);
		}

		fct_list_find_delete_all(
			opt->chunks_static, NULL, (void*)is_empty);
	}

	if (fct_list_count(opt->chunks_static) > 0)
	{
		/* Merge adjoining static chunks. */
		fct_slist_node_merge(
			opt->chunks_static,
			(void*)fct_chunk__adjoining_mergable,
			(void*)fct_chunk_merge);
	}

	if (fct_list_count(opt->chunks_dynamic) > 0)
	{
		/* Merge similar static chunks. */
		fct_slist_node_merge(
			opt->chunks_dynamic,
			(void*)fct_chunk__dynamic_mergable,
			(void*)fct_chunk_merge);
	}

	/* Delete resolved imports. */
	{
		bool func(fct_chunk_t* chunk, void* param)
		{
			(void)param;
			fct_chunk_imports_remove_resolved(chunk);
			return true;
		}

		fct_list_foreach(opt->chunks_static , NULL, (void*)func);
		fct_list_foreach(opt->chunks_dynamic, NULL, (void*)func);
	}

	return opt;
}

unsigned fct_object_imports_resolve(fct_object_t* object)
{
	if (!object)
		return 0;

	unsigned imports = 0;

	bool resolve(fct_chunk_t* chunk, void* params)
	{
		(void)params;
		if (!chunk) return true;
		fct_chunk_imports_resolve(chunk, object);
		imports += fct_imsymtab_count_unresolved(chunk->imports);
		return true;
	}

	fct_list_foreach(object->chunks_static , NULL, (void*)resolve);
	fct_list_foreach(object->chunks_dynamic, NULL, (void*)resolve);

	return imports;
}



bool fct_object_exports_add_symbol(
	fct_object_t* object, fct_exsym_t* symbol)
{
	if (!object) return false;
	return fct_exsymtab_add(
		object->exports, symbol);
}

bool fct_object_exports_add_symtab(fct_object_t* object, fct_exsymtab_t* table)
{
	if (!object || !table)
		return false;

	fct_exsymtab_t* nexports
		= fct_exsymtab_merge(object->exports, table);
	if (!nexports)
		return false;
	fct_exsymtab_delete(object->exports);
	object->exports = nexports;
	return true;
}



bool fct_object_chunk_add(
	fct_object_t* object, fct_chunk_t* chunk)
{
	if (!object || !chunk)
		return false;

	if (fct_chunk_is_static(chunk))
		return fct_object_chunk_place(
			object, chunk, chunk->address);

	if (fct_list_contains(
		object->chunks_dynamic, (void*)chunk))
		return true;

	if (fct_list_contains(
		object->chunks_static, (void*)chunk))
		return false;

	return fct_list_add(
		object->chunks_dynamic, chunk);
}

bool fct_object_chunk_place(
	fct_object_t* object, fct_chunk_t* chunk, uint64_t address)
{
	if (!object || !chunk)
		return false;

	if (fct_list_contains(
		object->chunks_static, (void*)chunk))
		return true;

	if (fct_list_contains(
		object->chunks_dynamic, (void*)chunk))
		return false;

	if (fct_chunk_is_static(chunk)
		&& (chunk->address != address))
	{
		FCT_DEBUG_ERROR(
			"Trying to place chunk already placed chunk at new address.");
		return false;
	}

	/* TODO - Handle non-power-of-two aligns. */
	if (chunk->align && (address & (chunk->align - 1)))
	{
		FCT_DEBUG_ERROR(
			"Trying to place chunk at address which doesn't match its alignment.");
		return false;
	}

	uint64_t csize = fct_chunk_size_get(chunk);
	if (csize > 0)
	{
		bool overlap(fct_chunk_t* schunk, void* param)
		{
			(void)param;
			if (!schunk) return true;

			uint64_t ssize = fct_chunk_size_get(schunk);
			if (ssize == 0) return true;

			return (((address + csize) < schunk->address)
				|| (address >= (schunk->address + ssize)));
		}

		if (!fct_list_foreach(
			object->chunks_static, NULL, (void*)overlap))
		{
			FCT_DEBUG_ERROR(
				"Chunk placement failed due to overlap.");
			return false;
		}
	}

	chunk->address   = address;
	chunk->is_static = true;

	if (!fct_list_add(
		object->chunks_static, chunk))
		return false;

	if (chunk->exports)
	{
		fct_exsymtab_t* exports
			= fct_exsymtab_copy(chunk->exports);
		if (!exports
			|| !fct_exsymtab_offset(exports, address)
			|| !fct_object_exports_add_symtab(object, exports))
		{
			FCT_DEBUG_WARN(
				"Failed to append exports to object exports");
			fct_exsymtab_delete(exports);
		}
		else
		{
			fct_exsymtab_delete(exports);
			fct_exsymtab_delete(chunk->exports);
			chunk->exports = fct_exsymtab_create();
		}
	}

	return true;
}



fct_object_t* fct_object_flatten(fct_object_t* object)
{
	fct_object_t* flat
		= fct_object_copy(object);
	if (!flat) return NULL;

	if (fct_list_count(
		flat->chunks_dynamic) == 0)
		return flat;

	fct_list_t* dynamic
		= flat->chunks_dynamic;
	flat->chunks_dynamic
		= fct_slist_create(fct_class_chunk,
			(void*)fct_object__dynamic_chunk_sort);
	if (!flat->chunks_dynamic)
	{
		flat->chunks_dynamic = dynamic;
		fct_object_delete(flat);
		return NULL;
	}

	bool place_dynamic(fct_chunk_t* chunk, void* params)
	{
		(void)params;
		uint64_t addr;
		uint64_t tail = 0;

		bool static_position(fct_chunk_t* schunk, void* params)
		{
			(void)params;
			addr = tail;
			tail = schunk->address + fct_chunk_size_get(schunk);

			if (chunk->align)
			{
				uint64_t misalign = (tail % chunk->align);
				if (misalign) tail += (chunk->align - misalign);
			}

			return ((addr + fct_chunk_size_get(chunk)) > schunk->address);
		}

		if (fct_list_foreach(
			flat->chunks_static,
			NULL, (void*)static_position))
		{
			addr = tail;
		}

		/* Reference chunk to retain when we delete "dynamic". */
		fct_chunk_t* rchunk
			= fct_chunk_reference(chunk);
		if (!rchunk) return false;

		if (!fct_object_chunk_place(flat, chunk, addr))
		{
			fct_chunk_delete(rchunk);
			return false;
		}

		return true;
	}

	bool success = fct_list_foreach(
		dynamic, NULL, (void*)place_dynamic);
	fct_list_delete(dynamic);
	if (!success)
	{
			fct_object_delete(flat);
			return NULL;
	}

	if (fct_list_count(object->chunks_static) == 0)
		FCT_DEBUG_WARN(
			"Flattening object which has no pre-defined static addresses.");

	unsigned unresolved = fct_object_imports_resolve(flat);
	if (unresolved > 0)
	{
		FCT_DEBUG_ERROR(
			"Unable to flatten object, %u unresolved symbol%s.",
			unresolved, (unresolved > 1 ? "s" : ""));
			
		fct_string_fprint(fct_object_dump(flat, 0), stdout);
			
		fct_object_delete(flat);
		return NULL;
	}

	return flat;
}



fct_arch_t* fct_object_arch(fct_object_t* object)
{
	if (!object)
		return NULL;

	fct_arch_t* arch = NULL;
	bool arch_find(fct_chunk_t* chunk, void* params)
	{
		(void)params;
		if (!chunk || !chunk->arch)
			return true;

		if (!arch)
		{
			arch = chunk->arch;
			return true;
		}

		if (fct_arch_is_ancestor(arch, chunk->arch))
			return true;

		if (!fct_arch_is_ancestor(chunk->arch, arch))
			return false;

		arch = chunk->arch;
		return true;
	}

	if (!fct_list_foreach(object->chunks_static, NULL, (void*)arch_find)
		|| !fct_list_foreach(object->chunks_dynamic, NULL, (void*)arch_find))
		return NULL;

	return arch;
}
