#ifndef __FCT_OBJECT_FORMAT_H__
#define __FCT_OBJECT_FORMAT_H__

typedef enum
{
	FCT_OBJECT_FORMAT_FOBJECT,
	FCT_OBJECT_FORMAT_BINARY,
	FCT_OBJECT_FORMAT_HEX,
	FCT_OBJECT_FORMAT_IHEX,
	FCT_OBJECT_FORMAT_SREC,
	FCT_OBJECT_FORMAT_TEK,

	FCT_OBJECT_FORMAT_COUNT
} fct_object_format_e;

#include <fct/types.h>

bool fct_object_format_from_string(
	const char* name, fct_object_format_e* format);
const char* fct_object_format_to_string(
	fct_object_format_e format);

bool fct_object_format_is_flat(
	fct_object_format_e format);


#include <fct/object/format/ihex.h>
#include <fct/object/format/srec.h>
#include <fct/object/format/tek.h>

#endif
