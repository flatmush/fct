.arch f32x.oabi

.include "stdtypes.h"
.import  "memcpy.s"


.symbol video_text        0x80002000
.symbol video_font_cursor 0x80003000
.symbol video_config      0x80004000
.symbol video_background  0x80004001
.symbol video_foreground  0x80004002
.symbol video_cursor_key  0x80004003
.symbol video_cursor_x    0x80004004
.symbol video_cursor_y    0x80004006
.symbol video_text_offset 0x80004008
.symbol video_framebuffer 0x8000400C


; void video_init()
video_init!
mov $t0, 0x05
sb  $t0, video_config

return


; void video_print(char* message)
video_print!
push $lr

mov $a1, $a0
mov $a0, video_text
call strcpy

pop $lr
return
