.arch f32x.oabi

_memcpy:
lb   $t0, $a1
sb   $t0, $a0
add  $a0, $a0, 1
add  $a1, $a1, 1
memcpy!
add  $a2, $a2, $a0
sub  $t1, $a2, $a0
bnez $t1, _memcpy
return


strcpy!
lb   $t0, $a1
jeqz $t0, $lr
sb   $t0, $a0
add  $a0, $a0, 1
add  $a1, $a1, 1
b    strcpy
