.arch f32x.oabi

.symbol timer_int_count 0x80001000
.symbol timer_divide    0x80001004
.symbol timer_latched   0x80001008
.symbol timer_control   0x8000100C


; void timer_init(uint32_t divide)
timer_init!
sw $a0, timer_divide
return


; uint32_t timer_read(void)
timer_read!
mov $t0, timer_control

lb  $t1, $t0
or  $t1, $t1, 0x02
sb  $t1, $t0

lw  $a0, timer_latched
return


; void timer_intr(uint32_t period, void (*handler)(void))
timer_intr!

sw  $a0, timer_int_count

cst $a1, 0x040

lb  $t0, timer_control
and $t0, $t0, 0xDF
or  $t0, $t0, 0x10
sw  $t0, timer_control

cld $t0, 0x009
or  $t0, $t0, 0x00000001
cst $t0, 0x009

return
