.arch f32x.oabi

.include "stdtypes.h"
.import  "init.s" "print.s" "timer.s" "video.s"

message:  .decl const char[] import "message.txt"
time_msg: .decl const char[] "\rtime: "


main!
push $lr

mov $a0, message
call print

mov $a0, 1024
call timer_init

call video_init

mov $a0, message
call video_print

mov $a0, 1000
mov $a1, timer_intr_handler
call timer_intr

time_loop:
mov $a0, time_msg
call print
call timer_read
call print_number
b time_loop

; pop $lr
; mov $a0, 0
; return



intr_message: .decl char[] "Interrupt!\n"

timer_intr_handler:
push $a0, $t0, $lr

mov $t0, 0x00000001
cst $t0, 0x00B

mov $a0, intr_message
call print

pop $a0, $t0, $lr
sysmov $zero, $zero
