.arch f32x.oabi

.include "stdtypes.h"

.symbol serial_fifo 0x80000000


print!
lb   $t0, $a0, 0
jeqz $t0, $lr
sb   $t0, serial_fifo
add  $a0, $a0, 1
b print


_number: .decl char[11] 0

print_number!
push $lr

mov  $t2, _number
mov  $t1, 10

_print_number_loop:
udivmod $a0, $a0, 10
add     $t0, $carry, '0'
sub     $t1, $t1, 1
add     $t3, $t2, $t1
sb      $t0, $t3
bnez    $t1, _print_number_loop

mov  $a0, $t3
call print

pop  $lr
return
