.arch f32x.oabi

.include "types.h"

0000: f32x_version: .decl int32

0024: vector_reset:     .decl ptr
0024: vector_syscall:   .decl ptr
0028: vector_exception: .decl ptr
002C: vector_interrupt: .decl ptr

0034: regbase_user:      .decl ptr
0034: regbase_system:    .decl ptr
0038: regbase_exception: .decl ptr
003C: regbase_interrupt: .decl ptr

0040: j os_init
