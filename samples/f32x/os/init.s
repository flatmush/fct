.arch f32x.oabi

.include "types.h"


.import "os/f32x.s"
.import "os/syscall.s"
.import "os/exception.s"
.import "os/interrupt.s"


.decl int32 user_regs[64]

os_init:
call syscall_init
call exception_init
call interrupt_init

; todo - setup userland
mov $t0, user_regs
sw $t0, regbase_user

mov $int, 0
