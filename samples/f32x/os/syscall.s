.arch f32x.oabi

.include "types.h"


.decl ptr syscall_table =
{
	0,
	reset,
}

.symbol syscall_count 2


syscall_handler:

shs $4, $int, syscall_count
; todo - exception if out of range
movnez $int, $4, 0

sll $4, $int, 2
add $4, $4, syscall_table
lw $4, $4
jl $lr, $4
mov $int, 0


syscall_init:
adr $t0, syscall_handler
sw $t0, syscall_vector
return
