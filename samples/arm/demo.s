.arch arm

0x0000: vec_rst: b init
0x0004: vec_und: b init
0x0008: vec_swi: b init
0x000C: vec_pre: b init
0x0010: vec_abt: b init
0x0014: vec_res: b init
0x0018: vec_irq: b init
0x001C: vec_fiq: b init


init:
b main


main:
mov $8, 0x80000000 ; Load address of serial-out (FIFO).

mov $0, 'H'
mov $1, 'e'
mov $2, 'l'
mov $3, 'l'
mov $4, 'o'
mov $5, ' '
mov $6, 'W'
mov $7, 'o'
stmia $8, $0, $1, $2, $3, $4, $5, $6, $7
mov $0, 'r'
mov $1, 'l'
mov $2, 'd'
mov $3, '!'
mov $4, '\n'
stmia $8, $0, $1, $2, $3, $4

b exit


exit:
b exit
