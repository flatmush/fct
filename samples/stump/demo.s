.arch stump

.symbol serial 0x8000

.type char character size(16)
string: .decl const char[] "Hello World!\n"

0x0000: main:
	mov  $0, string ; ptr = string
_loop:
	ld   $1, $0, 0  ; c = *ptr
	subs $2, $1, 0  ; if (c == '\0')
	beq  exit       ;     goto exit;
	st   $1, serial ; *serial = c;
	add  $0, $0, 1  ; ptr++;
	bal  _loop      ; goto _loop;

exit:
	bal exit
