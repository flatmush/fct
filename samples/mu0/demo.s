.arch mu0

.symbol serial 0x1000

.type char character size(16)
.type int  fixed size(16) modulo

string: .decl char[]  "Hello World!\n"
count:  .decl int 0
one:    .decl int 1


0x0000: main:
lda string
jge exit

sto serial
lda main
add one
sto main
jmp main

exit: stp
