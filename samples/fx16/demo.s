.arch fx16

; Vector Table
0x0000: vec_reset:      jmp init
0x0002: vec_inst_abort: jmp init
0x0004: vec_data_abort: jmp init
0x0006: vec_undefined:  jmp init
0x0008: vec_reserved0:  jmp init
0x000A: vec_reserved1:  jmp init
0x000C: vec_system:     jmp init
0x000E: vec_interrupt:  jmp init


init:
mov $sp, 0x8000 ; Set stack address.
mov $config, 0b1100000010000000 ; Set config flags.
bic $config, $config, 0x08 ; Enter user-mode
jmp main

.import "stdlib.s"
.symbol serial 0x8000

main:
mov $addr, serial ; Set the data address to serial-out (FIFO).

mov $data, 'H'
mov $data, 'e'
mov $data, 'l'
mov $data, 'l'
mov $data, 'o'
mov $data, ' '
mov $data, 'W'
mov $data, 'o'
mov $data, 'r'
mov $data, 'l'
mov $data, 'd'
mov $data, '!'
mov $data, '\n'

mov $0, 3
mov $1, 2
add $link, $pc, 2
mov $pc, multiply
add $data, $0, '0'
mov $data, '\n'

jmp exit
