.arch fx16

multiply!
	push $2
	push $3
	push $4
	push $5

	mov $2, 0x8000
	mov $3, 15
	zero $5

multiply__loop:
	and $4, $1, $2
	xor $4, $4, $2
	cmb $pc, multiply__loop_end, $4

	lsl $4, $0, $3
	add $5, $5, $4
	xor $1, $1, $2
	jmp multiply__loop_end

multiply__loop_end:
	lsr $2, $2, 1
	sub $3, $3, 1
	cmb $pc, multiply__loop, $1
	mov $0, $5
	pop $5
	pop $4
	pop $3
	pop $2
	jmp $link


exit!
	jmp exit
