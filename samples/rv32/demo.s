.arch rv32.rvg

0x0000:
j init

init:
j main


.type char character size(8)
message: .decl const char[] "Hello World!\n"


print:
li   $t0, 0x80000000 ; Load address of serial-out (FIFO).
print__loop:
lbu  $t1, $a0, 0
beqz $t1, print__end
sw   $t1, $t0, 0
addi $a0, $a0, 1
b    print__loop
print__end:
return


main:
li $a0, message
call print
call exit

exit:
j exit
