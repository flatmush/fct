.arch mips32.n32

0x0000:
j init
nop

init:
j main
nop



.type char character size(8)
message: .decl const char[] "Hello World!\n"



print:
li $t0, 0x80000000 ; Load address of serial-out (FIFO).
_print_loop:
lbu $t1, $a0, 0
beq $t1, $zero, _print_end
nop
sw $t1, $t0, 0
beq $zero, $zero, _print_loop
addiu $a0, $a0, 1
_print_end:
return
nop



main:
li $a0, message
call print
nop
call exit
nop

exit:
j exit
nop
