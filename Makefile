all: core tools devices

core:
	BASE=core/ $(MAKE) -f core/Makefile

tools: core
	BASE=tools/ $(MAKE) -f tools/Makefile

devices: core tools
	BASE=devices/ PATH=$(PATH):. $(MAKE) -f devices/Makefile

clean:
	BASE=core/ $(MAKE) -f core/Makefile clean
	BASE=tools/ $(MAKE) -f tools/Makefile clean
	BASE=devices/ $(MAKE) -f devices/Makefile.clean

install:
	BASE=tools/ $(MAKE) -f tools/Makefile install

uninstall:
	BASE=tools/ $(MAKE) -f tools/Makefile uninstall

cppcheck:
	BASE=core/ $(MAKE) -f core/Makefile cppcheck
	BASE=tools/ $(MAKE) -f tools/Makefile cppcheck

.PHONY : core tools devices clean install uninstall cppcheck
